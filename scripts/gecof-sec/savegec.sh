#!/bin/bash
echo ""
echo "dump complet gec"
mongodump -d gec -o /opt/workspace/save_db/gec.json;
echo ""
echo "export tenant"
mongoexport -d gec -c tenant -o /opt/workspace/save_db/tenant.json;
echo ""
echo "export users"
mongoexport -d gec -c users -o /opt/workspace/save_db/users.json;
echo ""
echo "export code"
mongoexport -d gec -c code -o /opt/workspace/save_db/code.json;

