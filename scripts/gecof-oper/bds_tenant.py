import os
filepath = 'bds-tenant.txt'
with open(filepath) as file:
   lines = file.read().replace('\n', '')
   bds = lines.split(',')
   for tenant in bds:
       print("\n bd = {}\n".format(tenant))
       command = "mongodump -d {} -o /opt/workspace/save_db/save/{}.json".format(tenant,tenant)
       os.system(command)
