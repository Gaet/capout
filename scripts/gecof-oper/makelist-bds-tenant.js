const environment = {
    databaseEngine: 'mongo',
    databaseUser: 'admin',
    databasePassword: 'admin*',
    databaseHost: '127.0.0.1',
    databaseName: 'gec',
    databasePort: '27017',
    isDebug: true,
    fakeAuth: false,
    fakeTokenUsername: 'albo@tono.com',
    fakeTokenTenant: 'tenant1',
    jwtSecretKey: 'gecSeCrEtKey2019',
    jwtExpiration: 3600000,
};

fs = require('fs');

let url = `mongodb://${environment.databaseUser}:${encodeURIComponent(
    environment.databasePassword,
)}@${environment.databaseHost}/?authSource=admin`;

const mongoose = require('mongoose'),
    Admin = mongoose.mongo.Admin;

/// create a connection to the DB
const connection = mongoose.createConnection(url);
// const regExp = /tenant_test_bd_/;
var bdsTenant = '';

connection.on('open', function () {
    // connection established
    new Admin(connection.db).listDatabases((err, result) => {
        // database list stored in result.databases
        var allDatabases = result.databases;
        var i = 0;
        try {

            for (let base of allDatabases) {
                if (base.name.toString().length >= 24) {
                    console.log("bd tenant trouvée : "+base.name)
                    if (i == 0) {
                        bdsTenant = base.name
                    } else {
                        bdsTenant = bdsTenant + ',' + base.name
                    }
                    i++;
                }
            };
            fs.writeFile("bds-tenant.txt", bdsTenant, (err) => {
                if (err) throw err;
                console.log("bds-tenant.txt saved!");
                process.exit(0);

            });
        }
        catch (err) {
            process.exit(0)
        }

    });
});

