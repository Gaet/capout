#!/bin/bash
echo ""
#echo "dump complet gec"
#mongodump -d gec -o /opt/workspace/save_db/save/gec.json;
echo "rm des sauvegardes"
rm -rf ../save/*
echo "rm des anciens tar"
rm /opt/workspace/save_db/*.tar
echo "export tenant"
mongoexport -d gec -c tenant -o /opt/workspace/save_db/save/tenant.json;
echo ""
echo "export users"
mongoexport -d gec -c users -o /opt/workspace/save_db/save/users.json;
echo ""
echo "export code"
mongoexport -d gec -c code -o /opt/workspace/save_db/save/code.json;
echo "export des logs"
mongoexport -d gec -c applog -o /opt/workspace/save_db/save/applog.json;
echo "exports all bds tenant"
node /opt/workspace/save_db/scripts/makelist-bds-tenant.js
echo "traitement sur le fichier export bds-tenant.txt"
python3 /opt/workspace/save_db/scripts/bds_tenant.py
echo "tar du rep contenant les bds tenant"
tar -cvf /opt/workspace/save_db/save_bds.tar ../save
echo "tar du rep des pdfs"
tar -cvf /opt/workspace/save_db/save_pdfs.tar ../../gec/pdf
echo "fin"