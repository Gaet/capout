let chai = require('chai');
let should = chai.should();

const environment = {
    databaseEngine: 'mongo',
    databaseUser: 'admin',
    databasePassword: 'admin*',
    databaseHost: 'localhost',
    databaseName: 'gec',
    isDebug: true,
    fakeAuth: true,
    fakeTokenUsername: 'albo@tono.com',
    fakeTokenTenant: 'tenant1',
    jwtSecretKey: 'gecSeCrEtKey2019',
    jwtExpiration: 3600,
};

// inspired from https://github.com/szokodiakos/typegoose/issues/175
//  next : je pense qu'il va falloir enlever tous les droits !! parce que je corrige le problème
//  s'il n'y a pas d'authentification !
describe('Multi db', () => {
  it('We connect to global db then tenant db', done => {
    var mongoose = require('mongoose');

    // TODO on peut faire mieux en injectant le token de bd dans database.providers
    let url = `mongodb://${environment.databaseUser}:${encodeURIComponent(environment.databasePassword)}@${environment.databaseHost}/${environment.databaseName}?authSource=admin`;
    console.log(`Connection url is ${url}`);
    mongoose.connect(url);
    var db = mongoose.connection;
    db.on('error', (error) => {
      console.log(error);
      should.fail('Database shall be accessible');
      done();
    });
    db.once('open', function(err) {
      console.log(err)
      var codeTenantSchema = new mongoose.Schema({
        type: String,
        name:String,
        list: {
            id: String,
            value: String,
            label:String,
            name:String,
        },
      });
      var codeSchema = new mongoose.Schema({
        type: String,
        name: String,
        list: {
            id: String,
            value: String,
        },
      });

      // on vérifie qu'on a des codes au niveau de la base générale gec
      var Code = db.model('code', codeSchema, 'code');
      Code.find((err, codes) => {
        codes.should.have.length.above(0)
        console.log('codes from gec database');
        // console.log(codes);
        should.not.exist(err);
      });

      // on vérifie qu'on a des codes au niveau de la base pour le tenant1
      var db2 = db.useDb('tenant1');
      var Code2 = db2.model('codetenant', codeTenantSchema, 'codetenant');
      Code2.find((err, codes) => {
          codes.should.have.length.above(0)
          console.log('codes from tenant1 database');
          // console.log(codes);
          should.not.exist(err);
      });
      done();
    });
  });
});
