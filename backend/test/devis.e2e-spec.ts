import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { DevisModule } from '../src/controllers/devis/devis.module';
import { IDevisStructure, initDevisStructure } from '../../common/dto/devis.dto';
import { AppModule } from '../src/app.module';
import { TENANT_TEST, getToken, initTenant } from './ressources/testHelper';
import { environment } from '../src/environment/environment';
jest.setTimeout(30000);
describe('Devis', () => {
  let app: INestApplication;
  let token = '';

  environment.databaseName = 'gecTest';
  environment.databasePort = '27018';

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [DevisModule, AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
   token = await initTenant(app,TENANT_TEST);
  });

  // create devis
  let idDevis = '';
  let devis: IDevisStructure = initDevisStructure;
  devis.idClient = 'kjsdhskjdhskjdhskjdhsd123';
  devis.titre = 'test_devis';
  it('test devis.e2e - post /devis', async () => {
    const resPostDevis = await request(app.getHttpServer())
      .post('/devis')
      .set('Authorization', token)
      .send(devis);
    expect(resPostDevis.status).toEqual(200);

    idDevis = await resPostDevis.body.data._id;

    expect(idDevis).not.toEqual('');
  });

  it('test devis.e2e - get /devis', async () => {
    // get devis
    const resGetDevis = await request(app.getHttpServer())
      .get('/devis/' + idDevis)
      .set('Authorization', token);

    expect(resGetDevis.status).toEqual(200);
    expect(resGetDevis.body.titre).toEqual(initDevisStructure.titre);
  });

  it('test devis.e2e - put /devis', async () => {
    // update devis
    devis.titre = 'test_devis_update';
    devis._id = idDevis;
    devis.idClient = 'kjsdhskjdhskjdhskjdhsd123_update';

    const resPutDevis = await request(app.getHttpServer())
      .put('/devis/' + idDevis)
      .send(devis)
      .set('Authorization', token);

    expect(resPutDevis.status).toEqual(200);
  });

  it('test devis.e2e - get /devis', async () => {
    // get devis after update
    const resGetAfterUpdateDevis = await request(app.getHttpServer())
      .get('/devis/' + idDevis)
      .set('Authorization', token);

    expect(resGetAfterUpdateDevis.status).toEqual(200);
    expect(resGetAfterUpdateDevis.body.titre).toEqual(devis.titre);
  });

  // Disconnect Mongoose
  //  afterAll(async () => {
  //   await removeAllDataBaseTest();
  //   await mongoose.connection.close()
  // })
});
