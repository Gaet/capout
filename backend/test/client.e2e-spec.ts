import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ClientModule } from '../src/controllers/client/client.module';
import { IClientStructure, initClient } from '../../common/dto/client.dto';
import { AppModule } from '../src/app.module';
import { TENANT_TEST, getToken, initTenant } from './ressources/testHelper';
import { environment } from '../src/environment/environment';
jest.setTimeout(30000);
describe('Clients', () => {
  let app: INestApplication;
  let token = '';
  environment.databaseName = 'gecTest';
  environment.databasePort = '27018';

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ClientModule, AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
    token = await initTenant(app,TENANT_TEST);

  });

  // create client
  let idClient = '';
  let client: IClientStructure = initClient;
  client.nom = 'test_client';
  it('test client.e2e - post /client', async () => {
    const resPostClient = await request(app.getHttpServer())
      .post('/client')
      .set('Authorization', token)
      .send(client);
    expect(resPostClient.status).toEqual(200);

    idClient = await resPostClient.body.data._id;

    expect(idClient).not.toEqual('');
  });

  it('test client.e2e - get /client', async () => {
    // get client
    const resGetClient = await request(app.getHttpServer())
      .get('/client/' + idClient)
      .set('Authorization', token);

    expect(resGetClient.status).toEqual(200);
    expect(resGetClient.body.nom).toEqual(initClient.nom);
  });

  it('test client.e2e - put /client', async () => {
    // update client
    client.nom = 'test_client_update';
    client._id = idClient;

    const resPutClient = await request(app.getHttpServer())
      .put('/client/' + idClient)
      .send(client)
      .set('Authorization', token);

    expect(resPutClient.status).toEqual(200);
  });

  it('test client.e2e - get /client', async () => {
    // get client after update
    const resGetAfterUpdateClient = await request(app.getHttpServer())
      .get('/client/' + idClient)
      .set('Authorization', token);

    expect(resGetAfterUpdateClient.status).toEqual(200);
    expect(resGetAfterUpdateClient.body.nom).toEqual(client.nom);
  });

  it('test client.e2e - delete /client', async () => {
    // delete client
    const resDelClient = await request(app.getHttpServer())
      .delete('/client/' + idClient)
      .set('Authorization', token);

    expect(resDelClient.status).toEqual(200);
  });

  // afterAll(() => setTimeout(() => process.exit(), 1000))
});
