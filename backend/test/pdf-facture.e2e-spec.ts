import { environment } from '../src/environment/dev.env';
import {
  IFactureStructure,
  FACTURE_ETAT_ENCOURS,
  TYPE_FACTURE_FS,
  initFactureStructure,
} from '../../common/dto/facture.dto';
import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { FactureModule } from '../src/controllers/facture/facture.module';
import { AppModule } from '../src/app.module';
import {
  TENANT_TEST,
  getToken,
  initTenant,
} from './ressources/testHelper';
import { IPdfStructure } from '../../common/dto/pdf.dto';
import { PdfModule } from '../src/controllers/pdf/pdf.module';
import { FactureDbModule } from '../src/shared/database/facture/facture.db.module';

jest.setTimeout(30000);
describe('Pdf', () => {
  let app: INestApplication;
  let token = '';

  environment.databaseName = 'gecTest';
  environment.databasePort = '27018';

  const blocksCatalogue = [
    {
      titre: '',
      block: [
        {
          catalogueId: '5f7984598a274353d9d8ce7c',
          intitule: 'netoyage',
          quantite: 9,
          nbJoursLocation: 0,
          unite: 'u',
          hourPerPu: 5,
          coutHTAchatMatosPU: 0,
          coutHTReventeMatosPU: 0,
          coutPerPU: 150,
          hours: 0,
          tva: 20,
          totalLigne: 1350,
        },
      ],
    },
  ];
  let fs = require('fs');

  const dir = 'docker/test/pdf';
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        PdfModule,
        FactureModule,
        FactureModule,
        FactureDbModule,
        FactureDbModule,
        AppModule,
      ],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();

    token = await initTenant(app,TENANT_TEST);

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    } 
  });

  // create facture
  let idFacture = '';
  let client = '';

  let facture: IFactureStructure = initFactureStructure;
  facture.idClient = 'kjsdhskjdhskjdhskjdhsd123';
  facture.adresse = 'test_adresse_facture';
  facture.etat = FACTURE_ETAT_ENCOURS;
  facture.client = 'client_test_geco';

  //@ts-ignore
  facture.blocksCatalogue = blocksCatalogue;
  let resPostFacture;
  let resPostFactureForPdf;

  it('test pdf.e2e - post /facture', async () => {
    resPostFacture = await request(app.getHttpServer())
      .post('/facture')
      .set('Authorization', token)
      .send(facture);
    expect(resPostFacture.status).toEqual(200);

    idFacture = await resPostFacture.body.data._id;
    client = await resPostFacture.body.data.client;

    expect(idFacture).not.toEqual('');
  });

  it('déclenche la methode generate ', async () => {
    const data: IPdfStructure = {
      _id: idFacture,
      idContrat: '1',
      type: TYPE_FACTURE_FS,
      nameTenant: TENANT_TEST.name,
      nameClient: client,
      docName: 'docName',
      sendToMail: false,
      dateCreation: new Date(),
    };
    console.log(data);
    resPostFactureForPdf = await request(app.getHttpServer())
      .post('/pdf')
      .set('Authorization', token)
      .send(data);

    const pdf = fs.readdirSync(dir).filter((fn) => fn.endsWith('.pdf'));
    console.log('PDF   = ', pdf);

    //const rimraf = require("rimraf");
    // suppression du repertoire contenant les pdfs
    //rimraf.sync(dir);
    
    // vérifie qu'un fichier a bien été créé
    //expect(fs.existsSync(pdf)).toEqual(true);
  });
});
