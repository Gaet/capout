import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { TENANT_TEST, initTenant, getToken } from './ressources/testHelper';
import { environment } from '../src/environment/environment';
jest.setTimeout(30000);
export const authTest = describe('auth (e2e)', () => {
  let app: INestApplication;
  environment.databaseName = 'gecTest';
  environment.databasePort = '27018';
  let token = '';

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    token = await initTenant(app,TENANT_TEST);
  });

  it('test client.e2e - post /user', async () => {
    expect(token).toBeTruthy();
    expect(token).toContain('Bearer ');
  });
});
