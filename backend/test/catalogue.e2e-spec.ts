import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { CatalogueModule } from '../src/controllers/catalogue/catalogue.module';
import {
  ICatalogueStructure,
  initCatalogue,
} from '../../common/dto/catalogue.dto';
import { AppModule } from '../src/app.module';
import { TENANT_TEST, getToken, initTenant } from './ressources/testHelper';
import { environment } from '../src/environment/environment';
jest.setTimeout(30000);
describe('Catalogues', () => {
  let app: INestApplication;
  let token = '';
  environment.databaseName = 'gecTest';
  environment.databasePort = '27018';

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [CatalogueModule, AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
    token = await initTenant(app,TENANT_TEST);
  });

  // create catalogue
  let idCatalogue = '';
  let catalogue: ICatalogueStructure = initCatalogue;
  catalogue.intitule = 'test_catalogue';
  it('test catalogue.e2e - post /catalogue', async () => {
    const resPostCatalogue = await request(app.getHttpServer())
      .post('/catalogue')
      .set('Authorization', token)
      .send(catalogue);
    expect(resPostCatalogue.status).toEqual(200);

    idCatalogue = await resPostCatalogue.body.data._id;

    expect(idCatalogue).not.toEqual('');
  });

  it('test catalogue.e2e - get /catalogue', async () => {
    // get catalogue
    const resGetCatalogue = await request(app.getHttpServer())
      .get('/catalogue/' + idCatalogue)
      .set('Authorization', token);

    expect(resGetCatalogue.status).toEqual(200);
    expect(resGetCatalogue.body.intitule).toEqual(initCatalogue.intitule);
  });

  it('test catalogue.e2e - put /catalogue', async () => {
    // update catalogue
    catalogue.intitule = 'test_catalogue_update';
    catalogue._id = idCatalogue;

    const resPutCatalogue = await request(app.getHttpServer())
      .put('/catalogue/' + idCatalogue)
      .send(catalogue)
      .set('Authorization', token);

    expect(resPutCatalogue.status).toEqual(200);
  });

  it('test catalogue.e2e - get /catalogue', async () => {
    // get catalogue after update
    const resGetAfterUpdateCatalogue = await request(app.getHttpServer())
      .get('/catalogue/' + idCatalogue)
      .set('Authorization', token);

    expect(resGetAfterUpdateCatalogue.status).toEqual(200);
    expect(resGetAfterUpdateCatalogue.body.intitule).toEqual(
      catalogue.intitule,
    );
  });

  it('test catalogue.e2e - delete /catalogue', async () => {
    // delete catalogue
    const resDelCatalogue = await request(app.getHttpServer())
      .delete('/catalogue/' + idCatalogue)
      .set('Authorization', token);

    expect(resDelCatalogue.status).toEqual(200);
  });
});
