import { environment } from '../src/environment/dev.env';
import {
  IDevisStructure,
  initDevisStructure,
  DEVIS_ETAT_SIGNE,
  TYPE_CONTRAT_DEVIS,
} from '../../common/dto/devis.dto';
import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { DevisModule } from '../src/controllers/devis/devis.module';
import { AppModule } from '../src/app.module';
import {
  TENANT_TEST,
  getToken,
  initTenant,
} from './ressources/testHelper';
import { IPdfStructure } from '../../common/dto/pdf.dto';
import { PdfModule } from '../src/controllers/pdf/pdf.module';
import { FactureModule } from '../src/controllers/facture/facture.module';
import { DevisDbModule } from '../src/shared/database/devis/devis.db.module';
import { FactureDbModule } from '../src/shared/database/facture/facture.db.module';

jest.setTimeout(30000);
describe('Pdf', () => {
  let app: INestApplication;
  let token = '';

  environment.databaseName = 'gecTest';
  environment.databasePort = '27018';

  const blocksCatalogue = [
    {
      titre: '',
      block: [
        {
          catalogueId: '5f7984598a274353d9d8ce7c',
          intitule: 'netoyage',
          quantite: 9,
          nbJoursLocation: 0,
          unite: 'u',
          hourPerPu: 5,
          coutHTAchatMatosPU: 0,
          coutHTReventeMatosPU: 0,
          coutPerPU: 150,
          hours: 0,
          tva: 20,
          totalLigne: 1350,
        },
      ],
    },
  ];
  let fs = require('fs');

  const dir = 'docker/test/pdf';
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        PdfModule,
        DevisModule,
        FactureModule,
        DevisDbModule,
        FactureDbModule,
        AppModule,
      ],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();

    token = await initTenant(app,TENANT_TEST);

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    } 
  });

  // create devis
  let idDevis = '';
  let client = '';

  let devis: IDevisStructure = initDevisStructure;
  devis.idClient = 'kjsdhskjdhskjdhskjdhsd123';
  devis.titre = 'test_devis';
  devis.etat = DEVIS_ETAT_SIGNE;
  devis.client = 'client_test_geco';

  //@ts-ignore
  devis.blocksCatalogue = blocksCatalogue;
  let resPostDevis;
  let resPostDevisForPdf;

  it('test pdf.e2e - post /devis', async () => {
    resPostDevis = await request(app.getHttpServer())
      .post('/devis')
      .set('Authorization', token)
      .send(devis);
    expect(resPostDevis.status).toEqual(200);

    idDevis = await resPostDevis.body.data._id;
    client = await resPostDevis.body.data.client;

    expect(idDevis).not.toEqual('');
  });

  it('déclenche la methode generate ', async () => {
    const data: IPdfStructure = {
      _id: idDevis,
      idContrat: '1',
      type: TYPE_CONTRAT_DEVIS,
      nameTenant: TENANT_TEST.name,
      nameClient: client,
      docName: 'docName',
      sendToMail: false,
      dateCreation: new Date(),
    };
    console.log(data);
    resPostDevisForPdf = await request(app.getHttpServer())
      .post('/pdf')
      .set('Authorization', token)
      .send(data);

    const pdf = fs.readdirSync(dir).filter((fn) => fn.endsWith('.pdf'));
    console.log('PDF   = ', pdf);

    // const rimraf = require("rimraf");
    // suppression du repertoire contenant les pdfs
    // rimraf.sync(dir);
    
    // vérifie qu'un fichier a bien été créé
    //expect(fs.existsSync(pdf)).toEqual(true);
  });
});
