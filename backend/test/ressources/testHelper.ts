import { stringHelper } from '../../../common/stringHelper';
import * as request from 'supertest';
import { ITenantStructure } from '../../../common/dto/tenant.dto';
import { IUserLogin } from '../../../common/dto/user.dto';

const random = stringHelper.generateRandomString(8);
//const random = '';
// utile pour les tests jest dans le docker
export const TENANT_TEST:IUserLogin = {
  tenant: 'tenant_test_geco',
  name:'name_test_geco',
  email: 'email_test_geco' + random + '@hotmail.fr',
  tel: '0652324463',
  microEntreprise: false,
  exonereTva: false,
  associationExonereTva: false,
  password: 'SDkjhf1456/',
  forgetCode:14598,
};

// utile pour les tests pupetter connecté sur le vrai gec
export const TENANT_TEST_GEC:IUserLogin = {
  tenant: 'tenant_test_geco',
  name: 'name_test_geco',
  email: 'email_test_geco@hotmail.fr',
  tel: '0652324463',
  microEntreprise: false,
  exonereTva: false,
  associationExonereTva: false,
  password: 'SDkjhf1456/',
  forgetCode: 14598,
};

export const connexionTest = async (app) => {
  // connexion et recup token
  return await request(app.getHttpServer()).post('/auth/login').send({
    email: TENANT_TEST.email,
    password: TENANT_TEST.password,
  });
};

export const initTenant = async (app,tenant) => {
  // en admettant que le tenant n'existe pas on le crée

  let resPostUser = await request(app.getHttpServer())
    .post('/user')
    .send(tenant);
  if (resPostUser.status == 200 || resPostUser.status == 201 ) {
    console.log('OK');

    // on se connecte avec le nouveau tenant
    let resAuth = await request(app.getHttpServer()).post('/auth/login').send({
      email: tenant.email,
      password: tenant.password,
    });
    // on renvoie le token
    console.log('le tenant test a été crée, on renvoie le token');

    return 'Bearer ' + resAuth.body.token;
  } else {
    return getToken(app,tenant);
  }
};

export const getToken = async (app,tenant) => {
  // on se connecte avec le nouveau tenant
  let resAuth = await request(app.getHttpServer()).post('/auth/login').send({
    email: tenant.email,
    password: tenant.password,
  });
  // on renvoie le token
  console.log('le tenant test existe deja, on renvoie le token');
  return 'Bearer ' + resAuth.body.token;
};
