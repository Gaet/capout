import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from '../src/app.module';
import { environment } from '../src/environment/environment';
import { TENANT_TEST_GEC, initTenant } from './ressources/testHelper';
jest.setTimeout(30000);

describe('puppeteer test', async() => {
  let app: INestApplication;
  environment.databaseName = 'gec';
  environment.databasePort = '27017';

  const puppeteer = require('puppeteer');
  let browser;
  let page;
  let token = "";

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();

    token = await initTenant(app,TENANT_TEST_GEC);

    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--no-zygote'],
      headless: false,
    });
    page = await browser.newPage();
  });

  let login = {
    email: TENANT_TEST_GEC.email,
    password: TENANT_TEST_GEC.password,
    clientNomTest: 'client_puppeteer_test',
  };

  it('test pupetter.e2e - connection', async () => {
    await page.goto('http://localhost:4200/login');
    await page.waitForNavigation();

    await page.type('input[name="login_geco_email"]', login.email);
    await page.type('input[name="login_geco_password"]', login.password);
    await page.click('button[name="login_geco_submit"]');
  }, 15000);

  it('test pupetter.e2e - connection + creation client', async () => {
    await page.goto('http://localhost:4200/login');
    await page.waitForNavigation();

    await page.type('input[name="login_geco_email"]', login.email);
    await page.type('input[name="login_geco_password"]', login.password);
    await page.click('button[name="login_geco_submit"]');
    await page.waitForNavigation();

    await page.goto('http://localhost:4200/client/main');
    await page.waitForNavigation();

    if(document.getElementById("dialog_ok_first_connection")) {
      await page.click('button[name="dialog_ok_first_connection"]');
      await page.waitForNavigation();
    }

    await page.click('button[name="client_geco_nouveau_click"]');
    await page.type('input[name="client_geco_nom_click"]', login.clientNomTest);
    await page.click('button[name="client_geco_submit_click"]');
  }, 15000);

  afterAll(async () => {
    await page.close();
    await browser.close();
    // process.exit();
  }, 2000);
});
