import { environment } from '../src/environment/environment';
environment.databaseName = 'gecTest';

export const removeAllDataBaseTest = async () => {

  let url = `mongodb://${environment.databaseUser}:${encodeURIComponent(
    environment.databasePassword,
  )}@${environment.databaseHost}/?authSource=admin`;

  const mongoose = require('mongoose'),
    Admin = mongoose.mongo.Admin;

  /// create a connection to the DB
  const connection = mongoose.createConnection(url);
  const regExp = /tenant_test_bd_/;

  await connection.on('open', function () {
    // connection established
    new Admin(connection.db).listDatabases(async (err, result) => {
      // database list stored in result.databases
      var allDatabases = result.databases;
      for await(let base of allDatabases) {
        if (base.name.match(regExp)) {
          // console.log('collection test match :', base.name);
          const connection2 = mongoose.createConnection(
            `mongodb://${environment.databaseUser}:${encodeURIComponent(
              environment.databasePassword,
            )}@${environment.databaseHost}/${base.name}?authSource=admin`,
          );
           connection2.on('open', function () {
            connection2.db.dropDatabase();
          });
        }
      };
  
    });
  });
};
