import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { FactureModule } from '../src/controllers/facture/facture.module';
import {
  IFactureStructure,
  initFactureStructure,
  TYPE_FACTURE_ACC,
  TYPE_FACTURE_FS,
  initAccompte,
  FACTURE_ORIGINE_PAYE,
  PAYMENT_TYPE_HT,
} from '../../common/dto/facture.dto';
import { AppModule } from '../src/app.module';
import { TENANT_TEST, getToken, initTenant } from './ressources/testHelper';
import { environment } from '../src/environment/environment';
jest.setTimeout(30000);
describe('Factures', () => {
  let app: INestApplication;
  let token = '';

  environment.databaseName = 'gecTest';
  environment.databasePort = '27018';

  // put facture avec catalogue1
  const blocksCatalogue1 = [
    {
      titre: '',
      block: [
        {
          catalogueId: '5f7984598a274353d9d8ce7c',
          intitule: 'netoyage',
          quantite: 9,
          nbJoursLocation: 0,
          unite: 'u',
          hourPerPu: 5,
          coutHTAchatMatosPU: 0,
          coutHTReventeMatosPU: 0,
          coutPerPU: 150,
          hours: 0,
          tva: 20,
          totalLigne: 1350,
        },
      ],
    },
  ];
  // put facture avec catalogue2
  const blocksCatalogue2 = [
    {
      titre: 'block1',
      block: [
        {
          catalogueId: '5f7984598a274353d9d8ce7c',
          intitule: 'netoyage',
          quantite: 9,
          nbJoursLocation: 0,
          unite: 'u',
          hourPerPu: 5,
          coutHTAchatMatosPU: 0,
          coutHTReventeMatosPU: 0,
          coutPerPU: 150,
          hours: 0,
          tva: 20,
          totalLigne: 1350,
        },
        {
          catalogueId: '5f7984598a274353d9d8ce7c',
          intitule: 'netoyage',
          quantite: 9,
          nbJoursLocation: 0,
          unite: 'u',
          hourPerPu: 5,
          coutHTAchatMatosPU: 0,
          coutHTReventeMatosPU: 0,
          coutPerPU: 150,
          hours: 0,
          tva: 20,
          totalLigne: 1350,
        },
      ],
    },
    {
      titre: 'block2',
      block: [
        {
          catalogueId: '5f7984598a274353d9d8ce7c',
          intitule: 'netoyage',
          quantite: 9,
          nbJoursLocation: 0,
          unite: 'u',
          hourPerPu: 5,
          coutHTAchatMatosPU: 0,
          coutHTReventeMatosPU: 0,
          coutPerPU: 150,
          hours: 0,
          tva: 20,
          totalLigne: 1350,
        },
        {
          catalogueId: '5f7984598a274353d9d8ce7c',
          intitule: 'netoyage2',
          quantite: 9,
          nbJoursLocation: 0,
          unite: 'u',
          hourPerPu: 5,
          coutHTAchatMatosPU: 5,
          coutHTReventeMatosPU: 15,
          coutPerPU: 150,
          hours: 0,
          tva: 10,
          totalLigne: 1485,
        },
      ],
    },
  ];

  // create facture
  let idFacture = '';
  let facture: IFactureStructure = initFactureStructure;
  facture['paymentType'] = PAYMENT_TYPE_HT;
  facture.client = 'test_facture_client';
  facture.typeFacture = TYPE_FACTURE_FS;

  // create accompte
  let idAccompte = ''; // _id mongo
  let idAccompteContrat = 0 // id incremental des factures (number)
  let accompte: IFactureStructure = initAccompte;
  accompte.client = 'test_accompte_client';
  accompte.typeFacture = TYPE_FACTURE_ACC;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [FactureModule, AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
    token = await initTenant(app,TENANT_TEST);
  });

  it('test facture.e2e - post /facture', async () => {
    const resPostFacture = await request(app.getHttpServer())
      .post('/facture')
      .set('Authorization', token)
      .send(facture);
    expect(resPostFacture.status).toEqual(200);

    idFacture = await resPostFacture.body.data._id;

    expect(idFacture).not.toEqual('');
  });

  it('test facture.e2e - get /facture', async () => {
    // get facture
    const resGetFacture = await request(app.getHttpServer())
      .get('/facture/' + idFacture)
      .set('Authorization', token);

    expect(resGetFacture.status).toEqual(200);
    expect(resGetFacture.body.client).toEqual(initFactureStructure.client);
  });

  it('test facture.e2e - put /facture', async () => {
    // update facture
    facture.client = 'test_facture_client_update';
    facture._id = idFacture;


    const resPutFacture = await request(app.getHttpServer())
      .put('/facture/' + idFacture)
      .send(facture)
      .set('Authorization', token);

    expect(resPutFacture.status).toEqual(200);
  });

  it('test facture.e2e - get /facture', async () => {
    // get facture after update
    const resGetAfterUpdateFacture = await request(app.getHttpServer())
      .get('/facture/' + idFacture)
      .set('Authorization', token);

    expect(resGetAfterUpdateFacture.status).toEqual(200);
    expect(resGetAfterUpdateFacture.body.client).toEqual(facture.client);
  });

  it('test facture.e2e - update catalogue1 /facture', async () => {
    //@ts-ignore
    facture['blocksCatalogue'] = blocksCatalogue1;

    const resUpdate: any = await request(app.getHttpServer())
      .put('/facture/' + idFacture)
      .send(facture)
      .set('Authorization', token);
    expect(resUpdate.status).toEqual(200);
  });

  it('test facture.e2e - get after update catalogue1 /facture', async () => {
    // get facture after update
    const resGetAfterUpdateFacture = await request(app.getHttpServer())
      .get('/facture/' + idFacture)
      .set('Authorization', token);

    if (resGetAfterUpdateFacture.status == 200) {
      const factureUpdated = resGetAfterUpdateFacture.body;

      const totalHT =
        blocksCatalogue1[0].block[0].coutPerPU *
        blocksCatalogue1[0].block[0].quantite;

      const totalTVA = (blocksCatalogue1[0].block[0].tva / 100) * totalHT;
      const totalTTC = totalHT + totalTVA;

      expect(factureUpdated.totalTVA).toEqual([totalTVA]);
      expect(factureUpdated.tauxTVA).toEqual([20]);
      expect(factureUpdated.totalHT).toEqual(totalHT);
      expect(factureUpdated.totalTTC).toEqual(totalTTC);
    }
  });

  it('test facture.e2e - update catalogue2 /facture', async () => {
    //@ts-ignore
    facture['blocksCatalogue'] = blocksCatalogue2;

    const resUpdate: any = await request(app.getHttpServer())
      .put('/facture/' + idFacture)
      .send(facture)
      .set('Authorization', token);
    expect(resUpdate.status).toEqual(200);
  });

  it('test facture.e2e - get after update catalogue2 /facture', async () => {
    // get facture after update catalogue2
    const resGetAfterUpdateFacture = await request(app.getHttpServer())
      .get('/facture/' + idFacture)
      .set('Authorization', token);

    if (resGetAfterUpdateFacture.status == 200) {
      const factureUpdated = resGetAfterUpdateFacture.body;

      const coutPerPu00 = blocksCatalogue2[0].block[0].coutPerPU;
      const quantite00 = blocksCatalogue2[0].block[0].quantite;
      const tva00 = blocksCatalogue2[0].block[0].tva / 100;

      const coutPerPu01 = blocksCatalogue2[0].block[1].coutPerPU;
      const quantite01 = blocksCatalogue2[0].block[1].quantite;
      const tva01 = blocksCatalogue2[0].block[1].tva / 100;

      const coutPerPu10 = blocksCatalogue2[1].block[0].coutPerPU;
      const quantite10 = blocksCatalogue2[1].block[0].quantite;
      const tva10 = blocksCatalogue2[1].block[0].tva / 100;

      const coutPerPu11 = blocksCatalogue2[1].block[1].coutPerPU;
      const quantite11 = blocksCatalogue2[1].block[1].quantite;
      const tva11 = blocksCatalogue2[1].block[1].tva / 100;

      const coutHTReventeMatosPU11 =
        blocksCatalogue2[1].block[1].coutHTReventeMatosPU;

      const totalHT00 = coutPerPu00 * quantite00;
      const totalHT01 = coutPerPu01 * quantite01;
      const totalHT10 = coutPerPu10 * quantite10;
      const totalHT11 =
        coutPerPu11 * quantite11 + coutHTReventeMatosPU11 * quantite11;

      const totalHT = totalHT00 + totalHT01 + totalHT10 + totalHT11;

      const totalTVA = [
        totalHT00 * tva00 +
          totalHT01 * tva01 +
          totalHT10 * tva10,
        totalHT11 * tva11,
      ];

      const totalTTC =
        totalHT00 +
        totalHT00 * tva00 +
        totalHT01 +
        totalHT01 * tva01 +
        totalHT10 +
        totalHT10 * tva10 +
        totalHT11 +
        totalHT11 * tva11;

      expect(factureUpdated.totalTVA).toEqual(totalTVA);
      expect(factureUpdated.tauxTVA).toEqual([20, 10]);
      expect(factureUpdated.totalHT).toEqual(totalHT);
      expect(factureUpdated.totalTTC).toEqual(totalTTC);
    }
  });

  it('test facture.e2e - post accompte: creation facture accompte lié /facture', async () => {
    // creation accompte d'origine la facture ci dessus
    accompte.factureOrigine = idFacture;
    const resPostFacture: any = await request(app.getHttpServer())
      .post('/facture/')
      .send(accompte)
      .set('Authorization', token);
      idAccompte = await resPostFacture.body.data._id;
      idAccompteContrat = await resPostFacture.body.data.idContrat;

      expect(idAccompte).not.toEqual('');
  });

  it('test facture.e2e - update accompte: creation versement sur laccompte lié /facture', async () => {
    // update accompte d'origine la facture ci dessus avec un versement
    
    accompte._id = idAccompte;
    accompte.idContrat = idAccompteContrat;
    accompte.factureOrigine = idFacture;
    //@ts-ignore
    accompte['blocksCatalogue'] = blocksCatalogue1;
    accompte.totalHT = blocksCatalogue1[0].block[0].quantite *  blocksCatalogue1[0].block[0].coutPerPU;
    
    accompte.versements = [
      //@ts-ignore
      {
        date: '12/04/20',
        montant: 100,
      },
    ];

    const resPut: any = await request(app.getHttpServer())
      .put('/facture/' + idAccompte)
      .send(accompte)
      .set('Authorization', token);
    expect(resPut.status).toEqual(200);
  });

  it('test facture.e2e - update interne sur la facture origine lié a laccompte /facture', async () => {
    // la facture d'origine doit contenir l'accompte puisque les contrats sont liés
    // une mise à jour interne a du etre realisée

    const resGetAfterPostVersementAccompte: any = await request(
      app.getHttpServer(),
    )
      .get('/facture/' + idFacture)
      .set('Authorization', token);
    expect(resGetAfterPostVersementAccompte.status).toEqual(200);

    facture = resGetAfterPostVersementAccompte.body;

    expect(facture.versements[0].idAccompte).toEqual(idAccompteContrat.toString());
    expect(facture.versements[0].date).toEqual('12/04/20');
    expect(facture.versements[0].montant).toEqual(100);
    expect(facture.versements[0].dateCreation).not.toEqual(null);
    expect(facture.versements[0].id).not.toEqual(null);
  });

  it('test facture.e2e -  creation d\'1 versement supplementaire sur la facture d\'origine ', async () => {
    // update facture d'origine avec un versement supplementaire
    
    facture.versements.push
      //@ts-ignore
      ({
        date: '13/05/20',
        montant: 100,
      });

    const resPut: any = await request(app.getHttpServer())
      .put('/facture/' + idFacture)
      .send(facture)
      .set('Authorization', token);
    expect(resPut.status).toEqual(200);
  });

  it('test facture.e2e - get /facture', async () => {
    // la facture d'origine doit contenir 2 versements dont 1 d'orgine l'accompte lié
    // une mise à jour interne a du etre realisée

    const resGetAfterPostVersementAccompte: any = await request(
      app.getHttpServer(),
    )
      .get('/facture/' + idFacture)
      .set('Authorization', token);
    expect(resGetAfterPostVersementAccompte.status).toEqual(200);

    facture = resGetAfterPostVersementAccompte.body;

    expect(facture.versements.length).toEqual(2);


    expect(facture.versements[0].idAccompte).toEqual(idAccompteContrat.toString());
    expect(facture.versements[0].date).toEqual('12/04/20');
    expect(facture.versements[0].montant).toEqual(100);
    expect(facture.versements[0].dateCreation).not.toEqual(null);
    expect(facture.versements[0].id).not.toEqual(null);

    expect(facture.versements[1].date).toEqual('13/05/20');
    expect(facture.versements[1].montant).toEqual(100);
    expect(facture.versements[1].dateCreation).not.toEqual(null);
    expect(facture.versements[1].id).not.toEqual(null);
  });

  it('test facture.e2e - get accompte /facture', async () => {
    // on vérifie que l'accompte n'a pas bougé

    const resGetAfterPostVersementAccompte: any = await request(
      app.getHttpServer(),
    )
      .get('/facture/' + idAccompte)
      .set('Authorization', token);
    expect(resGetAfterPostVersementAccompte.status).toEqual(200);

    accompte = resGetAfterPostVersementAccompte.body;

    expect(accompte.versements.length).toEqual(1);

    expect(accompte.versements[0].date).toEqual('12/04/20');
    expect(accompte.versements[0].montant).toEqual(100);
    expect(accompte.versements[0].dateCreation).not.toEqual(null);
    expect(accompte.versements[0].id).not.toEqual(null);

    expect(accompte.versements[1]).toEqual(undefined);

  });

  it('test facture.e2e -  creation d\'1 versement supplementaire sur la facture d\'origine pour regler totalement ', async () => {
    // update facture d'origine avec un versement supplementaire pour régler totalement
    const reste = facture.totalHT - 200;
    facture.versements.push
      //@ts-ignore
      ({
        date: '14/05/20',
        montant: reste,
      });

    const resPut: any = await request(app.getHttpServer())
      .put('/facture/' + idFacture)
      .send(facture)
      .set('Authorization', token);
    expect(resPut.status).toEqual(200);
  });


  it('test facture.e2e - get /facture', async () => {
    // la facture d'origine doit contenir 3 versements dont 1 d'origine l'accompte lié
    // et elle doit etre entierement réglée

    const resGetAfterPostVersementAccompte: any = await request(
      app.getHttpServer(),
    )
      .get('/facture/' + idFacture)
      .set('Authorization', token);
    expect(resGetAfterPostVersementAccompte.status).toEqual(200);

    facture = resGetAfterPostVersementAccompte.body;

    expect(facture.versements.length).toEqual(3);


    expect(facture.versements[0].idAccompte).toEqual(idAccompteContrat.toString());
    expect(facture.versements[0].date).toEqual('12/04/20');
    expect(facture.versements[0].montant).toEqual(100);
    expect(facture.versements[0].dateCreation).not.toEqual(null);
    expect(facture.versements[0].id).not.toEqual(null);

    expect(facture.versements[1].date).toEqual('13/05/20');
    expect(facture.versements[1].montant).toEqual(100);
    expect(facture.versements[1].dateCreation).not.toEqual(null);
    expect(facture.versements[1].id).not.toEqual(null);
    
    const reste = facture.totalHT - 200;
    expect(facture.versements[2].date).toEqual('14/05/20');
    expect(facture.versements[2].montant).toEqual(reste);
    expect(facture.versements[2].dateCreation).not.toEqual(null);
    expect(facture.versements[2].id).not.toEqual(null);
    expect(facture.regleeTotalement).toEqual(true);

  });
  it('test facture.e2e - get /facture', async () => {
    // l'accompte lié doit contenir 2 versements
    // et doit aussi etre reglee totalement

    const resGetAfterPostVersementAccompte: any = await request(
      app.getHttpServer(),
    )
      .get('/facture/' + idAccompte)
      .set('Authorization', token);
    expect(resGetAfterPostVersementAccompte.status).toEqual(200);

    accompte = resGetAfterPostVersementAccompte.body;
    expect(accompte.versements.length).toEqual(2);

    expect(accompte.versements[0].date).toEqual('12/04/20');
    expect(accompte.versements[0].montant).toEqual(100);
    expect(accompte.versements[0].dateCreation).not.toEqual(null);
    expect(accompte.versements[0].id).not.toEqual(null);

    expect(accompte.versements[1].id).toEqual(FACTURE_ORIGINE_PAYE);
    expect(accompte.versements[1].montant).toEqual(1250);
    expect(accompte.regleeTotalement).toEqual(true);

  });

  it('test facture.e2e -  suppression d\'1 versement sur la facture d\'origine ', async () => {
    // update facture d'origine avec une suppression d'1 versement supplementaire 
    // la facture n'est donc plus réglée totalement: il faut le vérifier
    facture.versements.pop();
    facture.regleeTotalement = false;

    const resPut: any = await request(app.getHttpServer())
      .put('/facture/' + idFacture)
      .send(facture)
      .set('Authorization', token);
    expect(resPut.status).toEqual(200);
  });

  it('test facture.e2e - get /facture', async () => {
    // la facture d'origine doit contenir 2 versements dont 1 d'origine l'accompte lié
    // et elle ne doit plus etre entierement réglée

    const resGetAfterPostVersementAccompte: any = await request(
      app.getHttpServer(),
    )
      .get('/facture/' + idFacture)
      .set('Authorization', token);
    expect(resGetAfterPostVersementAccompte.status).toEqual(200);

    facture = resGetAfterPostVersementAccompte.body;

    expect(facture.versements.length).toEqual(2);


    expect(facture.versements[0].idAccompte).toEqual(idAccompteContrat.toString());
    expect(facture.versements[0].date).toEqual('12/04/20');
    expect(facture.versements[0].montant).toEqual(100);
    expect(facture.versements[0].dateCreation).not.toEqual(null);
    expect(facture.versements[0].id).not.toEqual(null);

    expect(facture.versements[1].date).toEqual('13/05/20');
    expect(facture.versements[1].montant).toEqual(100);
    expect(facture.versements[1].dateCreation).not.toEqual(null);
    expect(facture.versements[1].id).not.toEqual(null);

    expect(facture.regleeTotalement).toEqual(false);

  });

  it('test facture.e2e - get /facture', async () => {
    // l'accompte lié ne doit contenir plus qu'1 versement
    // et doit ne doit plus etre reglee totalement

    const resGetAfterPostVersementAccompte: any = await request(
      app.getHttpServer(),
    )
      .get('/facture/' + idAccompte)
      .set('Authorization', token);
    expect(resGetAfterPostVersementAccompte.status).toEqual(200);

    accompte = resGetAfterPostVersementAccompte.body;

    expect(accompte.versements.length).toEqual(1);

    expect(accompte.versements[0].date).toEqual('12/04/20');
    expect(accompte.versements[0].montant).toEqual(100);
    expect(accompte.versements[0].dateCreation).not.toEqual(null);
    expect(accompte.versements[0].id).not.toEqual(null);

    expect(accompte.versements[1]).toEqual(undefined);

    expect(accompte.regleeTotalement).toEqual(false);
  });

  // todo tests payeur
  

});
