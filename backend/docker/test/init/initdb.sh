#!/bin/bash
# redirect stdout/stderr to a file
exec &> loginit.txt
# Le principe pour le multitenant est le suivant :
# Un seul utilisateur "admin" est créé sur la table système admin
# avec le droit readWriteAnyDatabase.
# Il faut ensuite se connecter avec l'authenticationDatabase (la db de référence pour l'authentification)
# en la spécifiant dans l'url de connection `/ps?authSource=admin`
# cf https://github.com/Automattic/mongoose/issues/3905
# on peut donc se connecter en mongo de la manière suivante avec la console pour contrôler le fonctionnement attendu :
# `mongo --username=admin --password=admin* --authenticationDatabase=admin`
# @see https://docs.mongodb.com/manual/tutorial/enable-authentication/

# pour vérifer facilement avec admin mongo il faut créer un accé avec :
# mongodb://admin:admin*@127.0.0.1:27018/gecTest?authSource=admin

# si la variable n'a pas été initialisée (cas si pas utilisée par docker)
if [ -z ${MONGO_WORKING_DIRECTORY} ]; then
    MONGO_WORKING_DIRECTORY="./"
fi
if [ -z ${MONGO_INITDB_ROOT_USERNAME} ]; then
    MONGO_INITDB_ROOT_USERNAME="admin"
fi
if [ -z ${MONGO_INITDB_ROOT_PASSWORD} ]; then
    MONGO_INITDB_ROOT_PASSWORD="admin*"
fi

cd ${MONGO_WORKING_DIRECTORY}

echo ${MONGO_WORKING_DIRECTORY}
echo ${MONGO_INITDB_ROOT_USERNAME}
echo ${MONGO_INITDB_ROOT_PASSWORD}

# création de la bd gecTest et de l'utilisateur
mongo --username=${MONGO_INITDB_ROOT_USERNAME} --password=${MONGO_INITDB_ROOT_PASSWORD} gecuser.js

echo "import gecuser ok"
# répertoire courant des scripts

#import des communes
mongoimport --db gecTest --collection commune --drop --type json --jsonArray --file jsons/communes.json &&
echo "import communes ok"

#import des codes
mongoimport --db gecTest --collection code --drop --type json --jsonArray --file jsons/code.json
echo "import code ok"
