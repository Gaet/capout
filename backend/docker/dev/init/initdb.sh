# Le principe pour le multitenant est le suivant :
# Un seul utilisateur "admin" est créé sur la table système admin
# avec le droit readWriteAnyDatabase.
# Il faut ensuite se connecter avec l'authenticationDatabase (la db de référence pour l'authentification)
# en la spécifiant dans l'url de connection `/ps?authSource=admin`
# cf https://github.com/Automattic/mongoose/issues/3905
# on peut donc se connecter en mongo de la manière suivante avec la console pour contrôler le fonctionnement attendu :
# `mongo --username=admin --password=admin* --authenticationDatabase=admin`
# @see https://docs.mongodb.com/manual/tutorial/enable-authentication/

# si la variable n'a pas été initialisée (cas si pas utilisée par docker)
if [ -z ${MONGO_WORKING_DIRECTORY} ]; then
    MONGO_WORKING_DIRECTORY="./"
fi
if [ -z ${MONGO_INITDB_ROOT_USERNAME} ]; then
    MONGO_INITDB_ROOT_USERNAME="root"
fi
if [ -z ${MONGO_INITDB_ROOT_PASSWORD} ]; then
    MONGO_INITDB_ROOT_PASSWORD="root"
fi

cd ${MONGO_WORKING_DIRECTORY}

# création de la bd gec et de l'utilisateur
mongo --username=${MONGO_INITDB_ROOT_USERNAME} --password=${MONGO_INITDB_ROOT_PASSWORD} jsons/gecuser.js

# répertoire courant des scripts

#import des communes
mongoimport --db gec --collection commune --drop --type json --jsonArray --file jsons/communes.json &&

#import des codes
mongoimport --db gec --collection code --drop --type json --jsonArray --file jsons/code.json

#import des versions
mongoimport --db gec --collection version --drop --type json --jsonArray --file jsons/version.json

