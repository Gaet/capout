Améliorer l'arborescence car on a toute une série de modules dans la racine.

En effet, on mélange le module au sens REST (contrôleur) avec la notion de bd.

On a également l'environement ou le répertoire database dans la racine.

On pourrait faire ainsi : 

* src/controllers pour stocker tous les contrôleurs REST
* dans src/shared (comme c'est bien transverse), mettre : 
    * database qui contiendra pour chaque entité (client, code, etc...) le service de bd, schéma, modèle, etc...
    * services qui contiendra le service de log par exemple