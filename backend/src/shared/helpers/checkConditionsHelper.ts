import {
  ITenantStructure,
  TYPE_COMPTE_GRATUIT,
  TYPE_COMPTE_BUSINESS,
  TYPE_COMPTE_BASE,
} from '../../../../common/dto/tenant.dto';
import { isAfterDate, addDaysToDate } from './dateHelper';
import { IFactureStructure } from '../../../../common/dto/facture.dto';
import { isSameYear } from 'date-fns';
import { serverConfig } from './serverConfig';
import * as jsonGecoContratConf from '../../templates/geco-contrat-conf.json';

const isAbonnementResilie = (tenant: ITenantStructure) => {
  return tenant.resiliation ? true : false;
};

const conditionsAbonnementResilie = (
  tenant: ITenantStructure,
  checkConditionsToUseGeco,
) => {
  if (isAfterDate(new Date(), addDaysToDate(tenant.dateResiliation, 32))) {
    checkConditionsToUseGeco = {
      conditions: false,
      message:
        "Accès refusé car l'abonnement a été résilié depuis plus d'un mois",
      type: '',
    };
  }
  return checkConditionsToUseGeco;
};

export const checkConditionsToUseGecoHelper = async (
  factures,
  tenant: ITenantStructure,
  user: any,
) => {
  let checkConditionsToUseGeco = {
    conditions: true,
    isLastContratSigned: true,
    message: 'conditions ok',
    type: '',
  };

  if (isAbonnementResilie(tenant)) {
    // si le tenant a resilié, on laisse quand meme l'accé 1 mois gratuit
    return conditionsAbonnementResilie(tenant, checkConditionsToUseGeco);
  }

  // si compte gratuit :
  if (tenant.typeCompte == TYPE_COMPTE_GRATUIT) {
    // est-ce que aujourd'hui c'est après dateCreation + 15 jours ?
    if (isAfterDate(new Date(), addDaysToDate(tenant.dateCreation, 15))) {
      // si ca > caGratuit
      let ca = 0;
      let nombre = 0;
      factures.forEach((facture: IFactureStructure) => {
        const condition =
          isSameYear(new Date(), facture.dateValideDefinitivement) &&
          facture.valideDefinitivement;
        if (condition) {
          ca = ca + facture.totalHT;
          nombre = nombre + 1;
        }
      });

      if (nombre > serverConfig.nbFacturesGratuite) {
        checkConditionsToUseGeco.conditions = false;
        checkConditionsToUseGeco.type = 'gratuitInsuffisant';
        checkConditionsToUseGeco.message =
          " Le nombre de facture gratuite à été dépassé, veuillez s'il vous plait vous abonner pour continuer à utiliser GECOF";
      }

      if (ca > serverConfig.caGratuit) {
        checkConditionsToUseGeco.conditions = false;
        checkConditionsToUseGeco.type = 'gratuitInsuffisant';
        checkConditionsToUseGeco.message =
          "Votre chiffre d'affaire annuel de " +
          ca +
          "€ a dépassé la limite autorisé pour un compte gratuit, veuillez s'il vous plait vous abonner pour continuer à utiliser GECOF";
      }
    }
  }

  // si compte different de grandCompte :
  if (tenant.typeCompte != TYPE_COMPTE_BUSINESS) {
    // si ca > caBusiness
    let ca = 0;
    factures.forEach((facture: IFactureStructure) => {
      const condition =
        isSameYear(new Date(), facture.dateValideDefinitivement) &&
        facture.valideDefinitivement;
      if (condition) {
        ca = ca + facture.totalHT;
      }
    });

    if (ca > serverConfig.caBusiness) {
      checkConditionsToUseGeco.conditions = false;
      checkConditionsToUseGeco.type = 'baseInsuffisant';
      checkConditionsToUseGeco.message =
        "Votre chiffre d'affaire annuel de " +
        ca +
        "€ a dépassé la limite autorisé pour un compte de type 'BASE', veuillez s'il vous plait souscrire à l'abonnement de type 'BUSINESS' pour continuer à utiliser GECOF";
    }
  }

  // si nouvelle version de contrat non signé

  if (
    !tenant.dateContratSigned ||
    isAfterDate(
      new Date(jsonGecoContratConf.dateCreation),
      new Date(tenant.dateContratSigned),
    )
  ) {
    checkConditionsToUseGeco.conditions = false;
    checkConditionsToUseGeco.isLastContratSigned = false;
    checkConditionsToUseGeco.message =
      "Le contrat de location a été modifié, veuillez s'il vous plait accepter les nouveaux termes du contrat";
  }

  // si il y a eu un resetPwd et que le mdp n'a pas été changé après
  if (user.resetPwdFlag) {
    checkConditionsToUseGeco.conditions = false;
    checkConditionsToUseGeco.isLastContratSigned = false;
    checkConditionsToUseGeco.message =
      "Avant de continuer, veuillez s'il vous plait modifier votre mot de passe (profil)";
  }

  return checkConditionsToUseGeco;
};

export const apiCheckConditionsToChangeAbonnementHelper = async (
  factures,
  tenant,
  typeCompteSelected,
) => {
  let checkConditionsToChangeAbonnement = {
    conditions: true,
    message: 'conditions ok',
    type: '',
  };

  if (typeCompteSelected == TYPE_COMPTE_GRATUIT) {
    // est-ce que aujourd'hui c'est après dateCreation + 15 jours ?
    if (isAfterDate(new Date(), addDaysToDate(tenant.dateCreation, 15))) {
      // si ca > caGratuit
      let ca = 0;
      factures.forEach((facture: IFactureStructure) => {
        const condition =
          isSameYear(new Date(), facture.dateValideDefinitivement) &&
          facture.valideDefinitivement;
        if (condition) {
          ca = ca + facture.totalHT;
        }
      });

      if (ca > serverConfig.caGratuit) {
        checkConditionsToChangeAbonnement.conditions = false;
        checkConditionsToChangeAbonnement.type = 'gratuitInsuffisant';
        checkConditionsToChangeAbonnement.message =
          "Votre chiffre d'affaire annuel de " +
          ca +
          "€ a dépassé la limite autorisé pour un compte gratuit, veuillez s'il vous plait vous abonner pour continuer à utiliser GECOF";
      }
    }
  }

  // si demande de compte base:
  if (typeCompteSelected == TYPE_COMPTE_BASE) {
    // si ca > caBusiness
    let ca = 0;
    factures.forEach((facture: IFactureStructure) => {
      const condition =
        isSameYear(new Date(), facture.dateValideDefinitivement) &&
        facture.valideDefinitivement;
      if (condition) {
        ca = ca + facture.totalHT;
      }
    });

    if (ca > serverConfig.caBusiness) {
      checkConditionsToChangeAbonnement.conditions = false;
      checkConditionsToChangeAbonnement.type = 'baseInsuffisant';
      checkConditionsToChangeAbonnement.message =
        "Votre chiffre d'affaire annuel de " +
        ca +
        "€ a dépassé la limite autorisé pour un compte de type 'BASE', veuillez s'il vous plait souscrire à l'abonnement de type 'BUSINESS' pour continuer à utiliser GECOF";
    }
  }
  return checkConditionsToChangeAbonnement;
};
