import { serverConfig } from './serverConfig';
import { Request, Response } from 'express';
import { RequestContext } from '../services/contextTenant';
import { ERROR, UNKNOWN, businessErrorHttpCode, BUSINESS_ERROR } from './const';
import { IAppLogStructure } from '../../../../common/dto/app-log.dto';
import { AppLogDbService } from '../../shared/database/app-log/app-log.db.interface';
import { GecoError } from '../services/error.service';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { Injectable } from '@nestjs/common';

const log4js = require('log4js');
log4js.configure({
  appenders: { gec: { type: 'file', filename: 'gec.log' } },
  categories: {
    default: { appenders: ['gec'], level: 'trace' },
    gecError: { appenders: ['gec'], level: 'error' },
  },
});

export const RequestExpress = {} as Request;
const jwt = require('jwt-simple');
// import * as jwt from "jwt-simple";
@Injectable()
export class ExpressHelper {
  static buildRequestContext(req: any) {
    throw new Error('Method not implemented.');
  }

  constructor(private readonly appLogger: AppLoggerService) {}

  buildRequestContext(req: any) {
    const res = {} as RequestContext;
    res.ipAdress = this.getIp(req);

    // recuperation du token
    const tokenContent = this.getTokenContent(req.headers);
    if (!tokenContent) {
      return res;
    }

    const { userId, expireTime } = tokenContent;
    if (!userId || !expireTime) {
      return res;
    }

    // si le token a expiré, on ne fait rien
    if (Date.now() > expireTime) {
      res.hasExpired = true;
      return res;
    }

    // le token n'a pas expiré et userId est spécifié -> on va chercher le user en BD
    const user = null;
    // todo const user = await userService.getUser(userId);
    if (!user) {
      return res;
    }

    return res;
  }

  justLog(log: IAppLogStructure) {
    // on logge toutes les actions de post/put/delete
    if (log) {
      // logs bd
      this.appLogger.log(log);
      // logs file
      const logger = log4js.getLogger('gec');
      logger.trace(log.userId + ' ' + log.type);
    }
  }

  buildSuccess(res: Response, data: any, log: IAppLogStructure) {
    // on logge toutes les actions de post/put/delete
    if (log) {
      // logs bd
      this.appLogger.log(log);
      // logs file
      const logger = log4js.getLogger('gec');
      logger.trace(log.userId + ' ' + log.type);
    } else {
      log = {
        type: UNKNOWN,
        userId: 'non renseigné',
        timestamp: new Date(),
        idTarget: '',
        message: 'pas de log',
        private: false,
      };
    }

    return res.json({
      success: true,
      data: data,
      message: log.message,
    });
  }

  buildPublicError(res: any, message: any, typeError: string, option: any) {
    // logs file
    const logger = log4js.getLogger('gec');
    logger.error(message);
    // logs bd
    let log: IAppLogStructure = {
      type: typeError,
      userId: option.userId,
      timestamp: new Date(),
      idTarget: '',
      message: message,
      private: true,
    };
    this.appLogger.log(log);

    res.status(businessErrorHttpCode).json({
      success: false,
      message: message,
      statusCode: businessErrorHttpCode,
    });
  }

  buildUnauthorizedExceptionError(res: any, err: any) {
    console.error('UnauthorizedExceptionError: ', err.response);
    res.status(err.status).send(err.message);
  }

  buildPrivateError(res: any, err: any) {
 
    console.log('  -- ERROR PRIVATE -- ');
    console.error(err);
    console.log('  -- ERROR PRIVATE -- ');

    let log: IAppLogStructure = {
      type: ERROR,
      userId: err.option.userId,
      timestamp: new Date(),
      idTarget: '',
      message: JSON.stringify(err),
      private: true,
    };

    // logs file
    const logger = log4js.getLogger('gec');
    logger.error(err.stack);
    // logs bd
    this.appLogger.log(log);

    res.status(500).send('erreur serveur inconnue');
  }

  buildError(res: any, err: any) {
    let that = this;

    if (err instanceof GecoError) {
      that.buildPublicError(res, err.message, BUSINESS_ERROR, err.option);
    }
    // cas d'une validation mongoose -> on remonte l'erreur
    // TODO faire marcher le instanceof !
    else if (err.name === 'ValidationError') {
      that.buildPublicError(res, err.message, ERROR, err.option);
    } else if (err.status === 401) {
      that.buildUnauthorizedExceptionError(res, err);
    } else {
      that.buildPrivateError(res, err);
    }
  }

  buildToken(userId: String) {
    const expireTime = Date.now() + serverConfig.tokenExpirationTime;
    const tokenContent = {
      userId,
      expireTime,
    };
    const token = 'JWT ' + jwt.encode(tokenContent, serverConfig.secret);
    return token;
  }

  getToken(headers: any) {
    if (headers && headers.authorization) {
      var parted = headers.authorization.split(' ');
      if (parted.length === 2) {
        return parted[1];
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  getTokenContent(headers: any): { userId?: string; expireTime?: Number } {
    let token = this.getToken(headers);
    if (token) {
      const tokenContent = jwt.decode(token, serverConfig.secret);
      return tokenContent;
    }
    return null;
  }
  // TODO voir comment l'implemter proprement, peut etre avec canActivate
  getIp(req: Request): string {
    let forwardedForHeaders = req.headers['x-forwarded-for'];
    let ip: string;
    if (forwardedForHeaders) {
      if (typeof forwardedForHeaders === 'string') {
        ip = forwardedForHeaders;
      } else if (
        Array.isArray(forwardedForHeaders) &&
        forwardedForHeaders.length
      ) {
        ip = forwardedForHeaders[0];
      }
    }
    if (!ip) {
      ip = req.connection.remoteAddress || req.socket.remoteAddress;
    }

    return ip;
  }
}
