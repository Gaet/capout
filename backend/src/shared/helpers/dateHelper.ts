
import {format , getMonth, getYear, getDate, addDays, isAfter} from 'date-fns';


export const gecFormat = (date) => `${format(date, "yyyy-MM-dd")}`;
export const gecGetDay = (date) => `${getDate(date)}`;

export const gecGetMonth = (date) => {
  let month = getMonth(date);
  month = month +1;
  if (month < 9) {
    return "0" + month;
  } else {
    return month;
  }
};
export const gecGetYear = (date) => `${getYear(date)}`;
export const gecGetDate = (date) => `${getDate(date)}`;

/**
 * Exemple de retour : "Dimanche 12 juin" ou "Dimanche 12 juin 2017"
 * @param {*} date
 */
export function formatDateFull(date) {
  const days = [
    "Dimanche",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
  ];
  const months = [
    "janvier",
    "février",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "août",
    "septembre",
    "octobre",
    "novembre",
    "décembre",
  ];
  const dayStr = gecGetDate(date);
  const yearStr = gecGetYear(date);
  const output = `${days[date.getDay()]} ${dayStr} ${
    months[date.getMonth()]
  } ${yearStr}`;

  return output;
}

export function formatDateAnneMois(date) {
  // date = gecFormat(date);
  const year = gecGetYear(date);
  const month = gecGetMonth(date);
  const output = year + month;
  return output;
}

export function formatDateAnneMoisJour(date) {
  // date = gecFormat(date);
  const year = gecGetYear(date);
  const month = gecGetMonth(date);
  const day = gecGetDay(date);

  const output = year + month + day;
  return output;
}
export function formatDateJourMoisAnneeTiret(date) {
  // date = gecFormat(date);
  const year = gecGetYear(date);
  const month = gecGetMonth(date);
  const day = gecGetDay(date);

  const output = day +'-'+ month +'-'+ year;
  return output;
}

export function addDaysToDate(date, nbDaysToAdd) {
  const result = addDays(date, nbDaysToAdd);
  return result;
}

export function isAfterDate(date1, date2) {
  const result = isAfter(date1, date2);
  // console.log("est-ce-que "+date1+" est après "+date2)
  // console.log("resultat ", result)
  return result;
}
export function isSameYear(date1:Date, date2:Date) {
  const year1 = date1.getFullYear();
  const year2 = date2.getFullYear();
  return year1 == year2 ? true : false;
}

export function isSameDay(date1:Date, date2:Date) {
  const day1 = date1.getDay();
  const day2 = date2.getDay();
  return day1 == day2 ? true : false;
}
