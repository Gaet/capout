import { environment } from '../../environment/environment';
import { Connection, Model } from 'mongoose';

// to generate the service token and related implementation
// by default we use mongo db
export function getDbServiceProvider(serviceToken, serviceImpl) {
    return {
        provide: serviceToken, useClass: environment.databaseEngine === 'mongo' ? serviceImpl : null,
    }
}

// maps the model token to a given model (ie a collection) using schema class
export function getDbModelProvider(modelToken: string, modelName: string, schemaClass) {
    if (environment.databaseEngine === 'mongo') {
        return {
            provide: modelToken,
            useFactory: (connection: Connection) => connection.model(modelName, schemaClass,modelName),
            inject: ['DbConnectionToken'],
        }
    }
    else {
        return null;
    }
}


  /**
   * @return le model client (permettant de manipuler la table client.
   * @see https://github.com/szokodiakos/typegoose/issues/175
   */
export function getModelTenant(database, tenant: string, modelName: string, schemaClass) {
    // on se positionne sur la base de tenant
    const db = database.connection.useDb(tenant);
    // on crée le modèle à la volée
    // TODO optimiser ?
    return db.model(modelName, schemaClass, modelName);
  }

