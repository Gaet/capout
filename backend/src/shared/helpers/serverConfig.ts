class ServerConfig {
    secret: string = process.env.GECOF_SECRET || "gecofSecret_*Pjdkd!G";
    localUrl: string = process.env.GECOF_LOCAL_URL || "mongodb://localhost/gec";
    mailNoReply: string = process.env.GECOF_MAIL_FROM || '"Gecof" <noreply@gecof.fr>';
    mailGeco: string = process.env.GECOF_MAIL_GECOF || 'noreply@gecof.fr';
    mailSupportGeco: string = process.env.GECOF_MAIL_GECOF || 'contact@gecof.fr';
    apiPublicKeyMailJet : string = '32a3077ac3f4ddf9ea3114a381e26ad2';
    apiPrivateKeyMailJet : string = '322de8f3b021e4fbebbe855de7c105ec';
    apiPrivateKeyStripe : string = 'sk_live_LilSbXgUUjJCGveIBO4dmVXU00NHsyHh5i';
    mailPass: string = process.env.GECOF_MAIL_PASS || "mdpMail/";
    mailSmtp: string = process.env.GECOF_MAIL_SMTP || "pro1.mail.ovh.net";
    mailSmtpPort: number = parseInt(process.env.GECOF_MAIL_SMTP_PORT) || 587;
    mailprivateKey: string = process.env.GECOF_MAIL_PRIVATE_KEY || "MIIEpAIBAAKCAQEAxLnCh3yYC/DWoyM2DwjtS7ZmKjNV71GO7ce1hfg6G4m7YgkK418IE6+RbeSsj3a4oEnPD5drZQbv7a4Sr3rmCrXoUVpQbSCFlbptnrAKoeoNGq65tA6IdOer0RBPQ/U6nKE41T2FMxQDz+Gg0V8ilg02xDPzQXVy+HaW6m5YrA7EWWdnRpVB/JLVXgIJsqsbp5OFdOE9hpJSrLpJbweh6ExC1HRXCrds2wkIpTrBZsoXTCRIcl5MiQdZBu/umq9DK2aQh9F283gj4WBgvsVs+kxPI1eJ0FIK+jmvGKg/z1kHO0lbLJWj3c9xhAz17t/igs/Sz13seTWBNzHTwXXesQIDAQABAoIBAD7S18Wuy1uc2N6WQxBAGI/COxQk+r+Hey6CRXz4DY53yKiBHMIJuimXOskuatER59oAWCixgXFs6rgri/oQNGCdQQih3+dJqH9tXs57h5m3heHMhK90qK00wtDr9XRNXx9f+SdYEy6BqMn9Uv8p84CZzbCQqpFv8XxZWs6Y2KZxLTIQduCSlptbrjeJRxp1Ojriw8fAMbbDgHmMPJxHjd6g/r2vswnEiBTgObhbvz6RogBG05VdwhdQ0k6LeLGq1SY9G9KtO+Ed0CiHsy80P3FvN/xv0C9/bdfvO5kvZyxh1J0vEnHCNGDNtVnSeSpRp3ZssCLThkoNDOG44F+DSEECgYEA7xWcSy60dGvkgLYmTyuM+b9i4mNHhcsr1XEqHHq6ahqt/tRAJMI91Jdv5iUcFXegIbWPk7YEYe2q8NU+9eqtPWQQljw3PEwjdMdSC/9qZhfHkNFibR3EopsxI/LVY4O0hg5f4r+41IlS15NwiMYsrK5OgLkfKmmnQnjyLN60RVkCgYEA0qTuNz9bVtsbsD64PHtlQ619T2ixAF17USxKBixPJhbaEyN2fw59N52I+0DsqRezokQdsSZDMkKSOC3//5WncrbnxKYtsHRBIliM1g0lDVjdmkCwRqYa5r79vH+B5+f3IXLuS1c9ry9GxGPci7qXX5BrwuUQqsbL6zb60nzLwRkCgYEA1pwDa8lE2qxutMrMoIxrQ1P0o1qSvRqfAj1Om7ne05eXUAOegGCLt+Un4OK2zt5Os3OSqfjc/jqlEwGYPo6la1IDQZhTzKBbw1uX3oUrMPFvhiMtwtzRwVSlS6uSuH1k8mC82YFXnBCYeEI05dFeELN2bD3AKAYmyZhfar+N+iECgYAS4TmAtisHo+fdaiG1OhIfeMNMhMOolrhg1ClmWD5X2aB+KqWKSdArVfFbI7ySg09Ucep/YECRlqnoYycYz18MxxwK4iIiOKlF3M3yuYbipV5nXvjtvCGZIPE5HeyUzpO16ck184HR29jp7VFtLLI4nIcsFr/hrqPEV08v4oOYsQKBgQC7a+NIXpvgs+pR29eAByeoTLZg41qgsKlepVhmSjM/43cJBtEpBFRmxtWOZkwqFzwk01BKHOlgVRS+Af1Ym/N/ah81T6S7rH/Az4+8RTxrygPpWqQquQOYxzTq59bPuV6W3FW5uM/MfjwAuDW5EUFVos0IouFbk/p7/j1hPJaZHw==";
    port = process.env.PORT || 7500;
    // durée de validité d'un token en millisecondes (60000 * 60 = 1h)
    tokenExpirationTime = parseInt(process.env.GECOF_TOKEN_EXPIRATION_TIME) || ( 60000 * 60 );
    restApiUrl: string =
      process.env.GECOF_REST_API_URL || "http://localhost:" + this.port + "/";
    portFront = process.env.PORT || 4200;
    confirmUrl: string =  process.env.GECOF_CONFIRM_URL || "http://localhost:" + this.portFront + "/signupConfirm/";
    newLocation: string =  process.env.GECOF_NEW_LOCATION || "http://localhost:" + this.portFront + "/newLocation/";
    
    nbJoursGratuit : number = 15;
    nbFacturesGratuite: number = 10;
    caGratuit : number = 1500;
    caBusiness : number = 36500;

  }
  
  export let serverConfig = new ServerConfig();
  