import {
  TYPE_FACTURE_AV,
  PAYMENT_TYPE_TTC,
  FACTURE_ORIGINE_PAYE,
  FACTURE_ETAT_ENCOURS,
  FACTURE_ETAT_PARTIELLEMENT_PAYE,
  IFactureStructure,
  PAYMENT_TYPE_HT,
} from '../../../../common/dto/facture.dto';
import { ICatalogueStructure } from '../../../../common/dto/catalogue.dto';
import { stringHelper } from '../../../../common/stringHelper';
import {
  formatDateJourMoisAnneeTiret,
} from './dateHelper';

// quand le front envoie un contrat il est dépourvu de controle métier
// c'est ici que l'on fait les transformations nécessaire et les controles associé
// cette fonction doit être appliqué à tous les cruds contrats c'est à dire /devis et /facture
// peut importe ce qui est envoyé par le front : les tauxTVA, totalTVA et totalTTC sont recalculé ici
export const transformContrat = (contrat) => {
  console.log('transformContrat() - applique les transformations nécessaires');
  console.log(' --- __ ---');

  // un avoir n'utilise pas le catalogue
  if (contrat.typeFacture && contrat.typeFacture == TYPE_FACTURE_AV) {
    if (contrat.exonereTva || contrat.associationExonereTva) {
      contrat['tauxTVA'] = [0];
      contrat['totalTVA'] = [0];
      contrat['totalTTC'] = parseFloat(contrat['totalHT']);
    } else {
      contrat['tauxTVA'] = [contrat['tauxTVAAvoir']];
      contrat['totalTVA'] = [
        (parseFloat(contrat['totalHT']) * parseFloat(contrat['tauxTVAAvoir'])) / 100,
      ];
      contrat['totalTTC'] =
        parseFloat(contrat['totalHT']) + parseFloat(contrat['totalTVA']);
    }
  }
  if (
    contrat.blocksCatalogue &&
    contrat.blocksCatalogue[0] &&
    contrat.blocksCatalogue[0].block &&
    contrat.blocksCatalogue[0].block[0]
  ) {
    let tauxTVA = [];
    let totalTVA = [];
    let totalHT = [];
    let totalTTC = 0;
    // il faut dédire le tauxTVA, le totalTVA et le totalTTC du contrat
    // cas simple : tous les articles ont la meme tva
    let simpleTva = true;
    let i = 0;

    // tous les autres types de contrat oui:
    contrat.blocksCatalogue.forEach((blockCatalogue) => {
      blockCatalogue.block.forEach((block: ICatalogueStructure) => {
        // utile que pour un cas pas simple c'est à dire qu'il y a differents taux de tva
        // ** //

        // REGLE METIER - 3 cas possibles:
        // 1  la tva concerne concerne à la fois une prestation et de la vente de matériel
        // 2  la tva concerne concerne une prestation
        // 3  la tva concerne concerne de la vente de matériel

        //  dans tous les cas une seule tva n'est possible par article du catalogue
        let tvaArticle = 0;

        let montantHT =
          block.quantite * block.coutHTReventeMatosPU +
          block.quantite * block.coutPerPU;

        let montantTva = montantHT * (block.tva / 100);
        // il faut absolument tvaArticle en number
        tvaArticle = block.tva *1;

        //arrondis
        tvaArticle = Math.round(tvaArticle  * 100) / 100;
        montantTva = Math.round(montantTva  * 100) / 100;
        montantHT = Math.round(montantHT  * 100) / 100;

        if (i > 0) {
          let index = tauxTVA.indexOf(tvaArticle);
          // il a deja ce taux de tvaArticle
          if (index != -1) {
            console.log(
              i,
              'deja ce taux ' + tvaArticle + " à l'index ",
              index,
              ' on ajoute ',
              montantTva,
              ' a ' + totalTVA[index],
            );
            totalTVA[index] = totalTVA[index] + montantTva;
            totalHT[index] = totalHT[index] + montantHT;
          } else {
            // c'est un nouveau taux   
            console.log(
              i,
              'nouveau taux ' + tvaArticle + ' on ajoute ',
              montantTva,
            );
            tauxTVA.push(tvaArticle);
            totalTVA.push(montantTva);
            totalHT.push(montantHT);
            simpleTva = false;
          }
        } else {
          // i == 0 , c'est forcement un nouveau taux
          console.log(
            i,
            '1er taux ' + tvaArticle + ' on ajoute ',
            montantTva,
          );
          tauxTVA.push(tvaArticle);
          totalTVA.push(montantTva);
          totalHT.push(montantHT);
        }

        // ** //

        i++;
      });
    });
    if (contrat.exonereTva || contrat.associationExonereTva) {
      contrat['tauxTVA'] = [0];
      contrat['totalTVA'] = [0];
      contrat['totalTTC'] = parseFloat(contrat['totalHT']);
    } else {
      const reducer = (accumulator, currentValue) => accumulator + currentValue;

      contrat['totalHT'] = totalHT.reduce(reducer);

      if (simpleTva) {
        contrat['tauxTVA'] = [Math.round(tauxTVA[0]  * 100) / 100];
        contrat['totalTVA'] = [
          (parseFloat(contrat['totalHT']) * parseFloat(tauxTVA[0])) / 100,
        ];
        contrat['totalTVA'] = [Math.round(contrat['totalTVA'][0] * 100) / 100];

        contrat['totalTTC'] =
          parseFloat(contrat['totalHT']) + parseFloat(contrat['totalTVA'][0]);
          
      } else {
        contrat['tauxTVA'] = tauxTVA;
        contrat['totalTVA'] = totalTVA;
        totalTVA.forEach((totaltva, t) => {
          totalTTC += totalHT[t] + totaltva;
        });
        contrat['totalTTC'] = totalTTC;
      }
    
    }
    // arrondi

    contrat['totalTTC'] = Math.round(contrat['totalTTC']   * 100) / 100;
    contrat['totalHT'] = Math.round(contrat['totalHT']   * 100) / 100;

    console.log(simpleTva, contrat['totalTTC']);
    console.log(' --- __ ---');
  } else {
    console.log("Attention il n'y a pas de blockcatalogue");
    return contrat;
  }
  return contrat;
};

export const creationVersement = (facture) => {
  // s'il y a des versements et que les ids ne sont pas renseigné, ce sont des nouveaux il faut affectuer un id
  for (let i = 0; i < facture.versements.length; i++) {
    if (!facture.versements[i].id || facture.versements[i].id == '') {
      facture.versements[i].id = stringHelper.generateRandomString(8);
    }
  }
  return facture;
};

export const checkIfVersementExist = (facture, factureOrigine) => {
  // on vérifie que ces versements ne sont pas deja là
  for (let i = 0; i < facture.versements.length; i++) {
    let addVersement = true;
    for (let j = 0; j < factureOrigine.versements.length; j++) {
      if (facture.versements[i].id == factureOrigine.versements[j].id) {
        addVersement = false;
        // ce versement est deja présent dans la facture d'origine,
        // cependant le montant et/ou la date peuvent avoir été modifiée par l'utilisateur
        factureOrigine.versements[j].date = facture.versements[i].date;
        factureOrigine.versements[j].montant = facture.versements[i].montant;
      }
    }

    // ce versement est nouveau, on l'affecte donc aussi à la facture d'origine
    if (addVersement) {
      //idAccompte indique sur la facture d'origine que ce versement vient d'un versement sur un accompte lié
      facture.versements[i].idAccompte = facture.idContrat.toString();
      factureOrigine.versements.push(facture.versements[i]);
    }
  }
  return [facture, factureOrigine];
};

export const isFactureIsPaid = (facture:IFactureStructure) => {
  let versements = 0;
  facture.versements.forEach(versement => {
    versements += versement.montant
  });
  if(facture.paymentType == PAYMENT_TYPE_HT &&  versements == facture.totalHT) {
    return true;
  }
  else if(facture.paymentType == PAYMENT_TYPE_TTC &&  versements == facture.totalTTC) {
    return true;
  } 
  else if(facture.regleeTotalement) {
    return true;
  }
  return false;
}
export const isFactureIsPartialyPaid = (facture:IFactureStructure) => {
  let versements = 0;
  facture.versements.forEach(versement => {
    versements += versement.montant
  });
  if(versements > 0 && facture.paymentType == PAYMENT_TYPE_HT &&  versements < facture.totalHT) {
    return true;
  }
  else if(versements > 0 && facture.paymentType == PAYMENT_TYPE_TTC &&  versements < facture.totalTTC) {
    return true;
  } 
  else if(facture.regleeTotalement) {
    return false;
  }
  return false;
}

export const checkIfVersementWasDeleted = (facture, factureOrigine) => {
  // un ou plusieurs versements de l'accompte on pu etre supprimé par l'utilisateur
  // si la facture d'origine n'est pas figée, il faut enlever ces versements
  // il faut donc vérifier si la facture d'origine a des versements issu de l'accompte et que ne sont plus présent
  for (let i = 0; i < factureOrigine.versements.length; i++) {
    if (
      factureOrigine.versements[i].idAccompte == facture.idContrat.toString()
    ) {
      // vérifier qu'il est toujours là :

      let isVersementExist = false;
      for (let j = 0; j < facture.versements.length; j++) {
        if (factureOrigine.versements[i].id == facture.versements[j].id) {
          isVersementExist = true;
        }
      }

      if (!isVersementExist) {
        // il faut supprimer ce versement sur la facture d'origine
        console.log("suppression d'un versement en cascade");
        factureOrigine.versements.splice(i, 1);
      }
    }
  }
  return factureOrigine;
};

export const checkIfVersementsIsLikeTotalHT = (facture) => {
  // si tous les versements valent le totalHT alors la facture est considérée totalement réglée
  let sommeVersementsfacture = 0;
  for (let i = 0; i < facture.versements.length; i++) {
    sommeVersementsfacture =
      sommeVersementsfacture + facture.versements[i].montant;
  }

  if (sommeVersementsfacture == facture.totalHT) {
    console.log(
      'la somme des versements == totalHT, donc la facture est entierement reglée',
    );
    return true;
  }
};

export const checkIfVersementsIsLikeTotalTTC = (facture) => {
  // si tous les versements valent le totalHT alors la facture est considérée totalement réglée
  let sommeVersementsfacture = 0;
  for (let i = 0; i < facture.versements.length; i++) {
    sommeVersementsfacture =
      sommeVersementsfacture + facture.versements[i].montant;
  }

  if (sommeVersementsfacture == facture.totalTTC) {
    console.log(
      'la somme des versements == totalTTC, donc la facture est entierement reglée',
    );
    return true;
  }
};

export const updateAccompteLieeBecauseOfOriginIsALlPaid = (accLiee, index) => {
  const acc = accLiee[index];
  if (!acc.regleeTotalement) {
    let sommeVersements = 0;
    for (let v = 0; v < acc.versements.length; v++) {
      sommeVersements = sommeVersements + acc.versements[v].montant;
    }
    if (acc.paymentType == PAYMENT_TYPE_TTC) {
      if (sommeVersements < acc.totalTTC) {
        const reste = acc.totalTTC - sommeVersements;
        acc.versements.push({
          montant: reste,
          date: formatDateJourMoisAnneeTiret(new Date()),
          dateCreation: new Date(),
          id: FACTURE_ORIGINE_PAYE,
          idAccompte: acc.idContrat.toString(),
        });
        acc.regleeTotalement = true;
      }
    } else {
      if (sommeVersements < acc.totalHT) {
        const reste = acc.totalHT - sommeVersements;
        acc.versements.push({
          montant: reste,
          date: formatDateJourMoisAnneeTiret(new Date()),
          dateCreation: new Date(),
          id: FACTURE_ORIGINE_PAYE,
          idAccompte: acc.idContrat.toString(),
        });
        acc.regleeTotalement = true;
      }
    }
    return acc;
  }
};

export const updateAccompteLieeBecauseOfOriginIsNotLongerPaid = (
  accLiee,
  index,
) => {
  const acc = accLiee[index];

  for (let v = 0; v < acc.versements.length; v++) {
    // il faut chercher si un versement est d'origine "facture entierement payé"
    if (acc.regleeTotalement && acc.versements[v].id == FACTURE_ORIGINE_PAYE) {
      acc.regleeTotalement = false;
      acc.versements.splice(v, 1);
    }
  }
  return acc;
};
