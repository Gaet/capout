import { GecoError } from '../../shared/services/error.service';

export const businessCheckContrat = (contrat: any) => {
  console.log('businessCheckContrat() - vérifie la cohérence du contrat');
  console.log(' --- __ ---');

  if (
    contrat.blocksCatalogue &&
    contrat.blocksCatalogue[0] &&
    contrat.blocksCatalogue[0].block &&
    contrat.blocksCatalogue[0].block[0]
  ) {
    // ** controles métiers ** //
    // verif cohérence tva, ht, et ttc
    let tva = 0;
    let htCatalogue = 0;
    let ttcCatalogue = 0;
    let compteurTva = 0;
    let totalTVACatalogue = 0;
    contrat.blocksCatalogue.forEach((catalogue) => {
      catalogue.block.forEach((block) => {

        tva = tva + block.tva;
        compteurTva++;

        htCatalogue =
          htCatalogue +
          (block.coutHTReventeMatosPU * block.quantite +
            block.coutPerPU * block.quantite);
   
            if (!contrat.exonereTva && !contrat.associationExonereTva) {
              ttcCatalogue +=
              block.coutHTReventeMatosPU * block.quantite +
              block.coutPerPU * block.quantite +
              ((block.coutHTReventeMatosPU * block.quantite +
                block.coutPerPU * block.quantite) *
                block.tva) /
                100;

              totalTVACatalogue +=
                  ((block.coutHTReventeMatosPU * block.quantite +
                    block.coutPerPU * block.quantite) *
                    block.tva) /
                  100;
              } else {
                ttcCatalogue = htCatalogue;
              }
      });
    });

    ttcCatalogue = Math.round(ttcCatalogue * 100) / 100;
    htCatalogue = Math.round(htCatalogue * 100) / 100;
    totalTVACatalogue = Math.round(totalTVACatalogue * 100) / 100;

    const reducer = (accumulator, currentValue) => accumulator + currentValue;

    let totalTVAPrevue = contrat['totalTVA'].reduce(reducer);
    totalTVAPrevue = Math.round(totalTVAPrevue * 100) / 100;


    console.log('totalTVACatalogue = ', totalTVACatalogue);
    console.log('totalTVAPrevue = ', totalTVAPrevue);

    if (totalTVACatalogue != totalTVAPrevue) {
      throw new GecoError(
        "règle métier 1 facturation - Impossible d'enregistrer le contrat pour cause d'incohérence de la TVA",
      );
    }

    // HT
    let htPrevue = contrat.totalHT;
   
    console.log('htCatalogue = ', htCatalogue);
    console.log('htPrevue = ', htPrevue);

    if (htCatalogue != htPrevue) {
      throw new GecoError(
        "règle métier 2 facturation - Impossible d'enregistrer le contrat pour cause d'incohérence du total HT",
      );
    }

    // TTC
    let ttcPrevue = contrat.totalTTC;

    console.log('ttcCatalogue = ', ttcCatalogue);
    console.log('ttcPrevue = ', ttcPrevue);

    if (ttcCatalogue != ttcPrevue) {
      throw new GecoError(
        "règle métier 3 facturation - Impossible d'enregistrer le contrat pour cause d'incohérence du total TTC",
      );
    }
  } else {
    console.log("Attention il n'y a pas de blockcatalogue");
  }
  // Dates : si pas dans le futur et si pas entidaté
  const now = new Date();

  if (contrat.typeFacture) {
    if (
      contrat.dateValideDefinitivement &&
      (now < new Date(contrat.dateValideDefinitivement) ||
        new Date(contrat.dateValideDefinitivement) <
          new Date(contrat.dateCreation))
    ) {
      throw new GecoError(
        "règle métier 4 facturation - Impossible d'enregistrer le contrat pour cause d'incohérence de la date de facturation",
      );
    }
  } else {
    // todo voir si pertinent de rajouter le chps dateDevisSigne et d'en faire un controle
    // if (
    //   contrat.etat == DEVIS_ETAT_SIGNE &&
    //   (now < new Date(contrat.dateDevisSigne) ||
    //     new Date(contrat.dateDevisSigne) <
    //       new Date(contrat.dateCreation))
    // ) {
    //   throw new GecoError(
    //     "Impossible d'enregistrer le contrat pour cause d'incohérence de la date de signature du devis",
    //   );
    // }
  }

  console.log(' --- __ ---');

  return contrat;
};
