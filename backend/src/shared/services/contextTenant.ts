import { JwtService } from "@nestjs/jwt";
import { IUserStructure } from "../../../../common/dto/user.dto";

class TenantModel{
 //todo
}

export type RequestContext = {
  ipAdress?: string,
  hasExpired?: boolean,
  userId?: string,
  user:IUserStructure,
  tenant?: TenantModel,
}

  // @return le tenant à partir du token JWT
  export function getTenantFromToken(jwtService:JwtService,request: Request) {
    // TODO voir comment récupérer la fonction déjà existante de JwtStrategy qui le fait déjà !
    // Le pb c'est que l'autorization header est attendu en minuscule (express) au lieu de majuscules !
    // const token = ExtractJwt.fromAuthHeaderAsBearerToken()(request);
    
    const token = request.headers['authorization']
        ? request.headers['authorization'].replace('Bearer ', '')
        : null;

    if(!token) {
      return null
    }
  
    // const decoded = this.authService.decode(token);
    const decoded = jwtService.verify(token);
    return decoded.idTenant;
  }