import { parse } from 'json2csv';
const fs = require('fs');
import { Response } from 'express';
import { join } from 'path';

// liste des collections autorisées à l'export
const collections = ['client', 'catalogue', 'devis', 'facture'];

// export d'une collection
export async function exportCollection(
  tenant: string,
  collection: string,
  res: Response,
  datas: any,
): Promise<any> {
  // on vérifie qu'il y a bien des données en bd
  if (datas.length > 0) {
    // on vérifie que la collection demandé est autorisée à l'export
    if (
      collections.filter((collectionAllow) => collectionAllow == collection)
    ) {
      const goodDataToSend = [];
      datas.forEach((data: any) => {
        delete data._doc._id;
        goodDataToSend.push(data._doc);
      });

      const csvData = parse(goodDataToSend);

      const nameFile = `${tenant}-${collection}.csv`;

      const dir = '../exports/';

      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }

      const path = dir + nameFile;

      fs.writeFile(path, csvData, function (error) {
        if (error) throw error;
        console.log('make csv ' + collection + ' ok');
        res.sendFile(join(process.cwd(), dir, nameFile));
        console.log('send csv ' + collection + ' ok');
      });
    }
  } else {
    res.send('rien a exporter encore');
  }
}
