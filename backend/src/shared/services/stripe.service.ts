import { serverConfig } from '../../shared/helpers/serverConfig';
import { IpaymentStripe } from '../../../../common/dto/tenant.dto';
import { GecoError } from './error.service';

const stripe = require('stripe')(serverConfig.apiPrivateKeyStripe);

export async function createCostumerStripe(tenant: any): Promise<any> {
  // Create a new customer object
  const customer = await stripe.customers.create({
    email: tenant.email,
  });

  return customer;
}

export async function createSubscriptionStripe(
  body: IpaymentStripe,
  res: any,
): Promise<any> {
  // Attach the payment method to the customer
  try {
    await stripe.paymentMethods.attach(body.paymentMethodId, {
      customer: body.customerId,
    });
  } catch (error) {
    return res.status('402').send({ error: { message: error.message } });
  }

  // Change the default invoice settings on the customer to the new payment method
  await stripe.customers.update(body.customerId, {
    invoice_settings: {
      default_payment_method: body.paymentMethodId,
    },
  });

  // Create the subscription

  const subscription = await stripe.subscriptions.create({
    customer: body.customerId,
    items: [{ price: body.priceId }],
    expand: ['latest_invoice.payment_intent'],
  });
  // console.log("subscription = ", subscription)

  return subscription;
}

export async function retryInvoice(
  body: IpaymentStripe,
  res: any,
): Promise<any> {
  try {
    await stripe.paymentMethods.attach(body.paymentMethodId, {
      customer: body.customerId,
    });
    await stripe.customers.update(body.customerId, {
      invoice_settings: {
        default_payment_method: body.paymentMethodId,
      },
    });
  } catch (error) {
    // in case card_decline error
    return res
      .status('402')
      .send({ result: { error: { message: error.message } } });
  }

  const invoice = await stripe.invoices.retrieve(body.invoiceId, {
    expand: ['payment_intent'],
  });
  res.send(invoice);
}

export async function deletedSubscription(
  subscriptionId: string,
): Promise<any> {
  let deletedSubscription = null;

  // Delete the subscription stripe
  try {
    deletedSubscription = await stripe.subscriptions.del(subscriptionId);
  } catch {
    throw new GecoError(
      "suite à une erreur la désinscription est impossible, veuillez s'il vous plait nous contacter",
    );
  }


  return deletedSubscription;
}
export async function updateSubscription(
  body: IpaymentStripe,
  res: any,
): Promise<any> {
  const subscription = await stripe.subscriptions.retrieve(body.subscriptionId);
  const updatedSubscription = await stripe.subscriptions.update(
    body.subscriptionId,
    {
      cancel_at_period_end: false,
      items: [
        {
          id: subscription.items.data[0].id,
          price: body.priceId,
        },
      ],
    },
  );

  return updatedSubscription;
}
export async function webHook(req: any, res: any): Promise<any> {
  let event;
  try {
    event = stripe.webhooks.constructEvent(
      req.body,
      req.headers['stripe-signature'],
      process.env.STRIPE_WEBHOOK_SECRET,
    );
  } catch (err) {
    console.log(err);
    console.log(`⚠️  Webhook signature verification failed.`);
    console.log(`⚠️  Check the env file and enter the correct webhook secret.`);
    return res.sendStatus(400);
  }
  // Extract the object from the event.
  const dataObject = event.data.object;

  // Handle the event
  // Review important events for Billing webhooks
  // https://stripe.com/docs/billing/webhooks
  // Remove comment to see the various objects sent for this sample
  switch (event.type) {
    case 'invoice.paid':
      // Used to provision services after the trial has ended.
      // The status of the invoice will show up as paid. Store the status in your
      // database to reference when a user accesses your service to avoid hitting rate limits.
      break;
    case 'invoice.payment_failed':
      // If the payment fails or the customer does not have a valid payment method,
      //  an invoice.payment_failed event is sent, the subscription becomes past_due.
      // Use this webhook to notify your user that their payment has
      // failed and to retrieve new card details.
      break;
    case 'invoice.finalized':
      // If you want to manually send out invoices to your customers
      // or store them locally to reference to avoid hitting Stripe rate limits.
      break;
    case 'customer.subscription.deleted':
      if (event.request != null) {
        // handle a subscription cancelled by your request
        // from above.
      } else {
        // handle subscription cancelled automatically based
        // upon your subscription settings.
      }
      break;
    case 'customer.subscription.trial_will_end':
      if (event.request != null) {
        // handle a subscription cancelled by your request
        // from above.
      } else {
        // handle subscription cancelled automatically based
        // upon your subscription settings.
      }
      break;
    default:
    // Unexpected event type
  }
  res.sendStatus(200);
}
