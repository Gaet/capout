import { ExpressHelper } from '../../shared/helpers/expressHelper';

import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { AppLoggerService } from './app-logger.service';
import { JwtService } from '@nestjs/jwt';
import { getTenantFromToken } from '../../shared/services/contextTenant';

@Injectable()
@Catch()
export class GecoExceptionFilter implements ExceptionFilter {
  constructor(
    private readonly jwtService: JwtService,
    private readonly appLogger: AppLoggerService,
  ) {}
  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    // const status =
    //   exception instanceof HttpException
    //     ? exception.getStatus()
    //     : HttpStatus.INTERNAL_SERVER_ERROR;
        
    exception.option = {};

    const tenant = getTenantFromToken(this.jwtService, request);

    if (tenant) {
      exception.option.userId = tenant;
    } else {
      exception.option.userId = 'tenant inconnu';
    }

    //Handler pour les erreurs
    this.expressHelper.buildError(response, exception);
  }
}

export class GecoError extends HttpException {
  message: string;
  option: any;

  constructor(message: string, option?: any) {
    super(message, 422);
    let that = this;
    that.message = message;
    that.option = option;
    console.warn('Erreur métier 422 : ', message);
  }
}
