import { Module } from '@nestjs/common';
import { AppLoggerService } from './app-logger.service';
import { AppLogDbModule } from '../database/app-log/app-log.db.module';
import { MailService } from './mail.service';
import { PdfService } from '../../controllers/pdf/pdf.service';
import { CodeTenantDbService } from '../../shared/database/codeTenant/codeTenant.db.interface';
import { PreferenceDbService } from '../../shared/database/preference/preference.db.interface';
import { DevisDbService } from '../../shared/database/devis/devis.db.interface';
import { FactureDbService } from '../../shared/database/facture/facture.db.interface';
import { ClientDbService } from '../../shared/database/client/client.db.interface';
import { JwtService, JwtModule } from '@nestjs/jwt';
import { TenantDbService } from '../../shared/database/tenant/tenant.db.interface';
import { CodeTenantDbModule } from '../../shared/database/codeTenant/codeTenant.db.module';
import { PreferenceDbModule } from '../../shared/database/preference/preference.db.module';
import { DevisDbModule } from '../../shared/database/devis/devis.db.module';
import { FactureDbModule } from '../../shared/database/facture/facture.db.module';
import { ClientDbModule } from '../../shared/database/client/client.db.module';
import { TenantDbModule } from '../../shared/database/tenant/tenant.db.module';
import { environment } from '../../environment/dev.env';

@Module({
  imports: [
    AppLogDbModule,
    CodeTenantDbModule,
     PreferenceDbModule,
    DevisDbModule,
    FactureDbModule,
    ClientDbModule,
    JwtModule.register({
        secretOrPrivateKey: environment.jwtSecretKey,
        signOptions: {
          expiresIn: environment.jwtExpiration,
        },
      }),
    TenantDbModule,
  ],
  exports: [
    AppLogDbModule,
    AppLoggerService,
    MailService,
    PdfService,
  ],
  providers: [
    AppLogDbModule,
    AppLoggerService,
    MailService,
    PdfService,
    CodeTenantDbModule,
    
    
    PreferenceDbModule,
    DevisDbModule,
    FactureDbModule,
    ClientDbModule,
    TenantDbModule,
  ],
})
export class ServicesModule {}
