import { serverConfig } from '../../shared/helpers/serverConfig';
import * as mailJet from 'node-mailjet';
import { IUserStructure } from '../../../../common/dto/user.dto';

const fs = require('fs');
const path = require('path');
import * as handlebars from 'handlebars';
import { Injectable } from '@nestjs/common';
import { PdfService } from '../../controllers/pdf/pdf.service';

@Injectable()
export class MailService {
  /**
   * Generates a pdf from htmlFile with data injected
   * @param htmlFile 
   * @param data 
   */
  constructor(

    private readonly pdfService: PdfService,

  ) {}
  

 mailjet = mailJet.connect(
  serverConfig.apiPublicKeyMailJet,
  serverConfig.apiPrivateKeyMailJet,
);

 async sendMailSuccessStripe(body: any): Promise<any> {
  let templateHtml = fs.readFileSync(
    path.join(process.cwd(), 'src/templates/mailConfirmAbonnement.html'),
    'utf8',
  );

  let template = handlebars.compile(templateHtml);
  const annee = new Date().getFullYear();
  const data = { typeAbonnement: body.typeAbonnement, annee: annee };

  let html = template(data, {});

  const mailBody = {
    From: {
      Email: serverConfig.mailSupportGeco, // todo qd ok : serverConfig.mailGeco
      Name: 'admin GECOF',
    },
    To: [
      {
        Email: body.email,
        Name: body.name,
      },
    ],
    Bcc: [
      {
        Email: "gaetanbosc31@gmail.com"
      }
    ],
    Subject: 'Succès abonnement GECOF ' + body.typeAbonnement,
    TextPart: '',
    HTMLPart: html,
  };
  try {
    const response = await this.mailjet.post('send', { version: 'v3.1' }).request({
      Messages: [mailBody],
    });
    return response.body;
  } catch (err) {
    return err;
  }
}

 async sendPdfMail(body: any): Promise<any> {

  // d'abord on génère le pdf

  let templatePdf = 'template-contrat.html';
  // pour obtenir le pdf en base64 sendToMail = true
  body.sendToMail = true;

  const contratOut = await this.pdfService.traitementContrat(body.tenant,body);
  if(!contratOut.exonereTva){
    templatePdf='template-contrat-microEntreprise.html'
  }

  const pdfBase64 = await this.pdfService.generate(
    'src/templates/' + templatePdf,
    contratOut,body.docName,'devisFacture',
    '../pdf'
  );

 // ensuite concernant le mail :
  let templateHtml = fs.readFileSync(
    path.join(process.cwd(), 'src/templates/mailPdf.html'),
    'utf8',
  );

  let template = handlebars.compile(templateHtml);
  const annee = new Date().getFullYear();
  const data = { message: body.message, annee: annee,nameClient:body.client,nameTenant:body.nameTenant,nameContrat:body.docName };

  let html = template(data, {});
  const mailBody = {
    From: {
      Email: serverConfig.mailSupportGeco, // todo qd ok : serverConfig.mailGeco
      Name: 'admin GECOF',
    },
    To: [
      {
        Email: body.emailClient,
        Name: body.nameClient,
      },
    ],
    Subject:body.subject,
    HTMLPart: html,
    Attachments: [
      {
          ContentType: "application/pdf",
          Filename: body.docName+".pdf",
          Base64Content: pdfBase64
      }
  ]
  };

  try {
    const response = await this.mailjet.post('send', { version: 'v3.1' }).request({
      Messages: [mailBody],
    });
    return response.body;
  } catch (err) {
    return err;
  }
}
async sendMailSupport(body: any): Promise<any> {

  const mailBody = {
    From: {
      Email: serverConfig.mailSupportGeco, // todo qd ok : serverConfig.mailGeco
      Name: 'admin GECOF',
    },
    To: [
      {
        Email: serverConfig.mailSupportGeco, // todo qd ok : serverConfig.mailGeco
        Name: 'admin GECOF',
      },
    ],
    Bcc: [
      {
        Email: "gaetanbosc31@gmail.com"
      }
    ],
    Subject:
      'demande de support : ' +
      body.idTenant +
      ' - ' +
      body.email +
      ' - ' +
      body.name +
      ' - ' +
      body.isAdmin,
    TextPart: body.message,
    HTMLPart: "<html>"+body.message+"</html>"
  };

  try {
    const response = await this.mailjet.post('send', { version: 'v3.1' }).request({
      Messages: [mailBody],
    });
    return response.body;
  } catch (err) {
    return err;
  }
}

 async sendMailSignIn(
  user: IUserStructure,
  confirmUrl: string,
): Promise<any> {
  let templateHtml = fs.readFileSync(
    path.join(process.cwd(), 'src/templates/bienvenue.html'),
    'utf8',
  );

  handlebars.registerHelper('isAdmin', function (type, options) {
    if (type === true) {
      return options.fn(this);
    }
    return options.inverse(this);
  });
  handlebars.registerHelper('isnotAdmin', function (type, options) {
    if (type === false) {
      return options.fn(this);
    }
    return options.inverse(this);
  });

  let template = handlebars.compile(templateHtml);
  const annee = new Date().getFullYear();
  const data = { ...user, confirmUrl: confirmUrl, annee: annee };

  let html = template(data, {});

  const mailBody = {
    From: {
      Email: serverConfig.mailSupportGeco, // todo qd ok : serverConfig.mailGeco
      Name: 'admin GECOF',
    },
    To: [
      {
        Email: user.email,
        Name: user.name,
      },
    ],
    Bcc: [
      {
        Email: "gaetanbosc31@gmail.com"
      }
    ],
    Subject: 'Inscription GECOF - logiciel devis-facture ',
    TextPart: '',
    HTMLPart: html,
  };
  try {
    const response = await this.mailjet.post('send', { version: 'v3.1' }).request({
      Messages: [mailBody],
    });
    return response.body;
  } catch (err) {
    return err;
  }
}



async sendMailNewLocation(mailOptions: any): Promise<any> {
  const mailBody = {
    From: {
      Email: serverConfig.mailNoReply,
      Name: 'noreply',
    },
    To: [
      {
        Email: mailOptions.to,
        Name: mailOptions.name,
      },
    ],
    Subject: mailOptions.subject,
    HTMLPart: mailOptions.html,
  };

  try {
    const response = await this.mailjet.post('send', { version: 'v3.1' }).request({
      Messages: [mailBody],
    });
    return response.body;
  } catch (err) {
    return err;
  }
}
 async sendMailResetPwd(mailOptions: any): Promise<any> {
  const mailBody = {
    From: {
      Email: serverConfig.mailNoReply,
      Name: 'noreply',
    },
    To: [
      {
        Email: mailOptions.to,
        Name: mailOptions.name,
      },
    ],
    Subject: mailOptions.subject,
    TextPart: mailOptions.text,
    HTMLPart: mailOptions.html,
  };

  try {
    const response = await this.mailjet.post('send', { version: 'v3.1' }).request({
      Messages: [mailBody],
    });
    return response.body;
  } catch (err) {
    return err;
  }
}

  }