import { Injectable, Logger } from '@nestjs/common';
import { AppLogDbService, IAppLogStructure } from '../database/app-log/app-log.db.interface';
import { environment } from '../../environment/environment';

/**
 * This class allows to store user actions into database to track his activity.
 */
@Injectable()
export class AppLoggerService {

    private readonly logger: Logger = new Logger('AppLogDbService');

    constructor(private readonly appLogger: AppLogDbService) {}

    /**
     * To log some infos about user actions.
    */
    log(logI:IAppLogStructure) {

        // Furthermore, we log it we the common internal logger (console appender by default).
        if (environment.isDebug) {
            this.logger.log(`user ${logI.userId} has performed ${logI.message}`);
        }

        this.appLogger.create(logI);
    }
}
