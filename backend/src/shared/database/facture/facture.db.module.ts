import { Module, forwardRef } from '@nestjs/common';
import { FactureSchema } from './mongo/facture.schema';
import { DatabaseModule } from '../database.module';
import { FactureDbService } from './facture.db.interface';
import { FactureServiceMongo } from './mongo/facture.db.service';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';
import { TenantDbModule } from '../tenant/tenant.db.module';

@Module({
    imports: [DatabaseModule,forwardRef(() => TenantDbModule)],
    exports: [FactureDbService],
    providers: [
        getDbServiceProvider(FactureDbService, FactureServiceMongo),
        getDbModelProvider('FactureModelToken', 'facture', FactureSchema),
    ]
})
export class FactureDbModule {}
