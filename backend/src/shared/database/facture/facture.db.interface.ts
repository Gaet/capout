import { IFactureStructure } from '../../../../../common/dto/facture.dto';

export { IFactureStructure } from '../../../../../common/dto/facture.dto';

export abstract class FactureDbService {

    abstract create(tenant: string,facture: IFactureStructure): Promise<IFactureStructure>;

    abstract findAll(tenant: string,query:any): Promise<IFactureStructure[]>;

    abstract export(tenant: string,res:any): Promise<any>;

    abstract findOne(tenant: string,id: string): Promise<IFactureStructure>;

    abstract udpate(tenant: string,id: string,facture: IFactureStructure): Promise<IFactureStructure>;

    abstract delete(tenant: string, _id: string): Promise<any>;


}
