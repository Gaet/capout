import { Injectable, Inject } from '@nestjs/common';
import { IFactureStructure, FactureDbService } from '../facture.db.interface';
import { getModelTenant } from '../../../../shared/helpers/providers.helpers';
import { FactureSchema } from './facture.schema';
// import { CounterFactureSchema } from './facture.schema';
import { Response } from 'express';
import { exportCollection } from '../../../../shared/services/export.service';
import {
  TYPE_FACTURE_ACC,
  TYPE_FACTURE_FS,
  TYPE_FACTURE_FD,
  PAYMENT_TYPE_TTC,
  PAYMENT_TYPE_HT,
  FACTURE_ETAT_PAYE,
  FACTURE_ETAT_PARTIELLEMENT_PAYE,
} from '../../../../../../common/dto/facture.dto';
import {
  transformContrat,
  creationVersement,
  checkIfVersementExist,
  checkIfVersementWasDeleted,
  checkIfVersementsIsLikeTotalHT,
  updateAccompteLieeBecauseOfOriginIsALlPaid,
  updateAccompteLieeBecauseOfOriginIsNotLongerPaid,
  checkIfVersementsIsLikeTotalTTC,
  isFactureIsPaid,
  isFactureIsPartialyPaid,
} from '../../../../shared/helpers/contratHelper';
import { businessCheckContrat } from '../../../../shared/helpers/businessControl';
import { ITenantStructure } from '../../../../../../common/dto/tenant.dto';
import { TenantDbService } from '../../../../shared/database/tenant/tenant.db.interface';

// @UseFilters(GecoExceptionFilter)
@Injectable()
export class FactureServiceMongo implements FactureDbService {
  // on injecte le modèle sans le typer (Model<ICatalogueStructureDocument>)
  constructor(
    @Inject('DbConnectionToken') private readonly mongoose,
    private readonly tenantService: TenantDbService,
  ) {}

  // permet de raccourcir l'appel
  getModel(tenant: string) {
    return getModelTenant(this.mongoose, tenant, 'facture', FactureSchema);
  }

  async create(
    tenant: string,
    newFacture: IFactureStructure,
  ): Promise<IFactureStructure> {
    // applique les transformation nécessaire
    const contrat = transformContrat(newFacture);
    // controle que tout est ok à présent
    newFacture = businessCheckContrat(contrat);

    const idFactureCount = (await this.getModel(tenant).countDocuments({})) + 1;
    newFacture.idContrat = idFactureCount;
    // il faut supprimer le _id par défault donné par le front
    delete newFacture._id;

    // il faut savoir si à ce moment là le tenant est exonéré de la tva

    let tenantStruture: ITenantStructure = await this.tenantService.findOne(
      tenant,
    );
    newFacture.exonereTva = tenantStruture.exonereTva;
    newFacture.associationExonereTva = tenantStruture.associationExonereTva;

    const createdFacture = this.getModel(tenant)(newFacture);
    return await createdFacture.save();
  }

  async udpate(
    tenant: string,
    id: string,
    newFacture: IFactureStructure,
  ): Promise<IFactureStructure> {
    const filter = { _id: id };

    // applique les transformation nécessaire
    const contrat = transformContrat(newFacture);
    // controle que tout est ok à présent
    newFacture = businessCheckContrat(contrat);

    // ** gestion des versements ** //

    newFacture = creationVersement(newFacture);

    // s'il s'agit d'un accompte lié à une facture, et qu'il y a des versements ou qu'il est déclaré payé
    // il faut affecter les nouveaux versements à la facture d'origine

    if (
      newFacture.typeFacture == TYPE_FACTURE_ACC &&
      newFacture.factureOrigine &&
      newFacture.factureOrigine != ''
    ) {
      // on récupère la facture d'origine :
      let factureOrigine: IFactureStructure = await this.getModel(tenant)
        .findOne({ _id: newFacture.factureOrigine })
        .exec();

      const array = checkIfVersementExist(newFacture, factureOrigine);
      
      newFacture = array[0];
      factureOrigine = array[1]

      factureOrigine = checkIfVersementWasDeleted(newFacture, factureOrigine);

      if(isFactureIsPaid(factureOrigine) && !factureOrigine.regleeTotalement) {
        factureOrigine.regleeTotalement = true;
        factureOrigine.dateValideDefinitivement = new Date();
        factureOrigine.etat = FACTURE_ETAT_PAYE;
      }

      if(!isFactureIsPaid(factureOrigine) && !factureOrigine.regleeTotalement && isFactureIsPartialyPaid) {
        factureOrigine.etat = FACTURE_ETAT_PARTIELLEMENT_PAYE;
      }

      // update de la facture d'origine
      await this.getModel(tenant)
        .findOneAndUpdate({ _id: factureOrigine._id }, factureOrigine)
        .exec();
    }

    // en fonction du type de paiement on peut déclarer la facture payé
    if (newFacture.paymentType == PAYMENT_TYPE_TTC) {
      if (checkIfVersementsIsLikeTotalTTC(newFacture)) {
        newFacture.regleeTotalement = true;
        newFacture.dateValideDefinitivement = new Date();
        newFacture.etat = FACTURE_ETAT_PAYE;
      }
    }

    if (newFacture.paymentType == PAYMENT_TYPE_HT) {
      if (checkIfVersementsIsLikeTotalHT(newFacture)) {
        newFacture.regleeTotalement = true;
        newFacture.dateValideDefinitivement = new Date();
        newFacture.etat = FACTURE_ETAT_PAYE;
      }
    }

    // attention : sinon on ne met pas newFacture.regleeTotalement = false;
    // car l'utilisateur ne veut pas forcement rentrer les versements pour dire que la facture est réglé

    // s'il s'agit d'une facture entierement payée et qu il y a des accomptes liés on peut les déclarer payés aussi en completant les versements restant
    if (
      (newFacture.typeFacture == TYPE_FACTURE_FS ||
        newFacture.typeFacture == TYPE_FACTURE_FD) &&
      newFacture.regleeTotalement
    ) {
      const accLiee: IFactureStructure[] = await this.findAll(tenant, {
        factureOrigine: newFacture._id,
        typeFacture: TYPE_FACTURE_ACC,
      });

      for (let index = 0; index < accLiee.length; index++) {
        const acc = updateAccompteLieeBecauseOfOriginIsALlPaid(accLiee, index);
        if (acc.regleeTotalement) {
          await this.getModel(tenant)
            .findOneAndUpdate({ _id: acc._id }, acc)
            .exec();
          console.log(
            "maj d'un accompte car la facture d'origine est entierement payé",
          );
        }
      }
    }

    // la facture était réglée et a un accompte lié, cependant l'utilisateur a enlevé un versement donc elle n'est plus réglé.
    // il faut aller l'indiquer aussi pour l'accompte lié
    if (
      (newFacture.typeFacture == TYPE_FACTURE_FS ||
        newFacture.typeFacture == TYPE_FACTURE_FD) &&
      !newFacture.regleeTotalement
    ) {
      const accLiee: IFactureStructure[] = await this.findAll(tenant, {
        factureOrigine: newFacture._id,
        typeFacture: TYPE_FACTURE_ACC,
      });

      for (let index = 0; index < accLiee.length; index++) {
        const acc = updateAccompteLieeBecauseOfOriginIsNotLongerPaid(
          accLiee,
          index,
        );
        await this.getModel(tenant)
          .findOneAndUpdate({ _id: acc._id }, acc)
          .exec();
        console.log(
          "maj d'un accompte car la facture d'origine n'est plus entierement payé",
        );
      }
    }

    // il faut savoir si à ce moment là le tenant est exonéré de la tva
    // on ne met a jour dans le contrat que si il n'est pas figé pour avoir la cohérence entre le moment où le devis a été validé et l'etat du tenant à ce moment là
    if (!newFacture.valideDefinitivement) {
      let tenantStruture: ITenantStructure = await this.tenantService.findOne(
        tenant,
      );
      newFacture.exonereTva = tenantStruture.exonereTva;
      newFacture.associationExonereTva = tenantStruture.associationExonereTva;
    }

    // update de la facture
    const updateFacture = await this.getModel(tenant)
      .findOneAndUpdate(filter, newFacture)
      .exec();

    // ** fin versements ** //

    return updateFacture;
  }

  // export de la collection catalogue
  async export(tenant: string, res: Response): Promise<any> {
    const clients = await this.getModel(tenant).find().exec();
    exportCollection(tenant, 'facture', res, clients);
  }

  async findAll(tenant: string, query: any): Promise<IFactureStructure[]> {
    let clause: any = {};

    let factureOrigine = query.factureOrigine;
    if (factureOrigine) {
      clause.factureOrigine = factureOrigine;
    }
    let typeFacture = query.typeFacture;
    if (typeFacture) {
      clause.typeFacture = typeFacture;
    }

    //_id
    let _id = query._id;
    if (_id) {
      clause._id = _id;
    }

    //userName
    let userName = query.userName;
    if (userName && userName.length > 0) {
      const userNameLowerCase = userName.map((keyword) =>
        keyword.toLowerCase(),
      );
      clause['userName'] = { $regex: new RegExp('^' + userNameLowerCase, 'i') };
    }

    // numero du devis incremental
    let idContrat = query.idContrat;
    if (idContrat) {
      clause.idContrat = idContrat;
    }

    // lien vers un client
    let idClient = query.idClient;
    if (idClient) {
      clause.idClient = idClient;
    }

    // titre

    let titre = query.titre;
    if (titre && titre.length > 0) {
      const titreLowerCase = titre.map((keyword) => keyword.toLowerCase());
      clause['titre'] = { $regex: new RegExp('^' + titreLowerCase, 'i') };
    }

    // client

    let client = query.client;
    if (client && client.length > 0) {
      const clientLowerCase = client.map((keyword) => keyword.toLowerCase());
      clause['client'] = { $regex: new RegExp('^' + clientLowerCase, 'i') };
    }

    // ville
    let ville = query.ville;
    if (ville) {
      clause.ville = ville;
    }

    // totalHT
    let totalHT = query.totalHT;
    if (totalHT) {
      clause.totalHT = totalHT;
    }

    // totalTTC
    let totalTTC = query.totalTTC;
    if (totalTTC) {
      clause.totalTTC = totalTTC;
    }

    // joursTravail
    let joursTravail = query.joursTravail;
    if (joursTravail) {
      clause.joursTravail = joursTravail;
    }

    // nbEmployes
    let nbEmployes = query.nbEmployes;
    if (nbEmployes) {
      clause.nbEmployes = nbEmployes;
    }
    // maindoeuvre
    let maindoeuvre = query.maindoeuvre;
    if (maindoeuvre) {
      clause.maindoeuvre = maindoeuvre;
    }
    // envoye
    let envoye = query.envoye;
    if (envoye) {
      clause.envoye = envoye;
    }

    // dateCreation
    let dateCreation = query.dateCreation;
    if (dateCreation) {
      clause.dateCreation = dateCreation;
    }

    // dernierAcces
    let dernierAcces = query.dernierAcces;
    if (dernierAcces) {
      clause.dernierAcces = dernierAcces;
    }

    // dernierMAJ
    let dernierMAJ = query.dernierMAJ;
    if (dernierMAJ) {
      clause.dernierMAJ = dernierMAJ;
    }

    // nbAcces
    let nbAcces = query.nbAcces;
    if (nbAcces) {
      clause.nbAcces = nbAcces;
    }

    // etat
    let etat = query.etat;
    if (etat) {
      clause.etat = etat;
    }

    // regleeTotalement
    let regleeTotalement = query.regleeTotalement;
    if (regleeTotalement) {
      clause.regleeTotalement = regleeTotalement;
    }

    // valideDefinitivement
    let valideDefinitivement = query.valideDefinitivement;
    if (valideDefinitivement) {
      clause.valideDefinitivement = valideDefinitivement;
    }

    // dateValideDefinitivement
    let dateValideDefinitivement = query.dateValideDefinitivement;
    if (dateValideDefinitivement) {
      clause.dateValideDefinitivement = dateValideDefinitivement;
    }

    // dateDebut et dateFin
    let dateDebut = query.dateDebut;
    let dateFin = query.dateFin;
    if (dateDebut || dateFin) {
      clause['dateCreation'] = {
        $elemMatch: {
          $and: [],
        },
      };
      if (dateDebut) {
        clause['dateCreation']['$elemMatch']['$and'].push({
          dateCreation: { $gte: dateDebut },
        });
      }
      if (dateFin) {
        clause['dateCreation']['$elemMatch']['$and'].push({
          dateCreation: { $lte: dateFin },
        });
      }
    }

    // tri
    const clauseQuery = {};
    const sortQuery: String = query.sort;
    if (sortQuery) {
      clause.clauseQuery = sortQuery;
    }

    // resultats de la recherche en bd
    return await this.getModel(tenant).find(clause).sort(clauseQuery).exec();
  }

  async findOne(tenant: string, id: string): Promise<IFactureStructure> {
    return await this.getModel(tenant).findOne({ _id: id }).exec();
  }

  async delete(tenant: string, id: string): Promise<any> {
    const filter = { _id: id };
    return await this.getModel(tenant).find(filter).remove().exec();
  }
}
