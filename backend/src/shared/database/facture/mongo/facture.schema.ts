import * as mongoose from 'mongoose';

import { Document } from 'mongoose';
import { IFactureStructure, IVersementStructure } from '../../../../../../common/dto/facture.dto';
import { ICatalogueStructure } from '../../../../../../common/dto/catalogue.dto';



// pour éviter de définir une nouvelle structure, on réutilise la structure facture
// export interface IFactureStructureDocument extends Document, IFactureStructure { }

// const FactureStructure = new IFactureStructure(),
export const FactureSchema = new mongoose.Schema({
  id: String,
  titre: String,
  userName:String,
  // le numero de la facture incremental
  idContrat: Number,
  // lien vers un client
  idClient: String,
  // lien vers un devis si facture issu d'un devis
  idDevis: String,
  // identifiant de type de facture (seule, liée, etc...)
  typeFacture: String,
  factureOrigine: String, // permet d'identifier la facture d'origine pour un avoir

  // informations clients récupérées du client lié
  client: String,
  ville: String,
  codePostal: String,
  adresse: String,
  numeroAdresse: String,
  payeur: String,
  payeurVille: String,
  payeurCodePostal: String,
  payeurAdresse: String,
  payeurNumeroAdresse: String,
  // champs calculés du montant de la facture
  totalHT: Number,
  totalTTC: Number,
  totalTVA: [Number],
  tauxTVA: [Number],
  tauxTVAAvoir: Number,

  // nombre de jours de travail
  joursTravail: Number,
  nbEmployes: Number,
  totalHTPerDayPerEmployes: Number,

  // matériel : utile si le calcul par le catalogue est faux
  // lors de l'édition d'un devis on pourra changer sa valeur si besoin
  coutHTAchatMatos: Number,
  coutHTReventeMatos: Number,
  maindoeuvre: Number,
  // commentaire saisis par l'utilisateur
  commentaire: String,

  paymentCondition: String,
  paymentType: String,
  regleeTotalement: Boolean,
  remise:Number,

  // on a pas le droit d'éditer une facture une fois qu'elle a été imprimé
  valideDefinitivement: Boolean,
  dateValideDefinitivement: Date,
  envoye: Boolean,

  dateCreation: Date,
  dernierAcces: Date,
  dernierMAJ: Date,
  nbAcces: Number,

  bonDeCommande: String,

  etat: String,
  versements: Array,
  blocksCatalogue: Array,
})

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
