import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { Model } from 'mongoose';
import { ITenantStructure, TenantDbService } from '../tenant.db.interface';
import { TenantSchema } from './tenant.schema';
import { GecoError } from '../../../../shared/services/error.service';
import { serverConfig } from '../../../../shared/helpers/serverConfig';
import { FactureDbService } from '../../../../shared/database/facture/facture.db.interface';
import { ClientDbService } from '../../../../shared/database/client/client.db.interface';
import { DevisDbService } from '../../../../shared/database/devis/devis.db.interface';
import { CatalogueDbService } from '../../../../shared/database/catalogue/catalogue.db.interface';
import { TYPE_COMPTE_GRATUIT } from '../../../../../../common/dto/tenant.dto';

@Injectable()
export class TenantServiceMongo implements TenantDbService {
  constructor(
    @Inject('TenantModelToken') private readonly tenantModel,
    private readonly clientService: ClientDbService,
    @Inject(forwardRef(() => DevisDbService))
    private devisService: DevisDbService,
    @Inject(forwardRef(() => FactureDbService))
    private factureService: FactureDbService,

    private readonly catalogueService: CatalogueDbService,
  ) {}

  async create(tenant: ITenantStructure): Promise<ITenantStructure> {
    // creation d'un tenant
    delete tenant._id;
    const createdTenant = new this.tenantModel(tenant);
    const res = await createdTenant.save();
    return res;
  }

  async udpate(
    id: string,
    newTenant: ITenantStructure,
  ): Promise<ITenantStructure> {
    const filter = { _id: id };
    return await this.tenantModel.findOneAndUpdate(filter, newTenant).exec();
  }

  async createIdStripeCostumer(
    id: string,
    idStripeCostumer: string,
  ): Promise<ITenantStructure> {
    const filter = { _id: id };
    return await this.tenantModel
      .findOneAndUpdate(filter, {
        $set: { idStripeCostumer: idStripeCostumer },
      })
      .exec();
  }

  async findAllSuper(query): Promise<ITenantStructure[]> {
    let clause: any = {};
    // il faut obligatoirement le parametre secret d'un superadmin en plus
    if (query && query.secret === serverConfig.secret) {
      delete query.secret;
      // filtre par id
      if (query.tenant && query.tenant != '') {
        clause._id = query.tenant;
      }
      //filtre par nom du tenant
      if (query.nom && query.nom != '') {
        clause.tenant = {
          $regex: new RegExp('^' + query.nom.toLowerCase(), 'i'),
        };
      }
      //filtre par nom du gerant
      if (query.gerant && query.gerant != '') {
        clause.nomGerant = { $regex: new RegExp('^' + query.gerant, 'i') };
      }
      const res = await this.tenantModel.find(clause).exec();
      return res;
    }
  }

  async findAllDetailSuper(query): Promise<any> {
    let clause: any = {};
    // il faut obligatoirement le parametre secret d'un superadmin en plus
    if (query && query.secret === serverConfig.secret) {
      delete query.secret;

      // renvoie un objet { clients: [], contrats:[], catalogues : []}
      let res = { clients: [], contrats: [], catalogues: [] };

      let factures = await this.factureService.findAll(query.tenant, query);
      factures.forEach((facture) => {
        res.contrats.push(facture);
      });

      (await this.devisService.findAll(query.tenant, query)).forEach(
        (devis) => {
          res.contrats.push(devis);
        },
      );

      (await this.clientService.findAll(query.tenant, query)).forEach(
        (devis) => {
          res.clients.push(devis);
        },
      );

      (await this.catalogueService.findAll(query.tenant)).forEach(
        (catalogue) => {
          res.catalogues.push(catalogue);
        },
      );

      return res;
    }
  }

  async findOne(id: string): Promise<ITenantStructure> {
    let res = null;
    try {
      res = await this.tenantModel.findOne({ _id: id }).exec();
    } catch (e) {
      new GecoError("Problème d'identification de l'entreprise parente");
    }
    return res;
  }

  // faux delete, on update le tenant avec la propriété deleted = true
  async delete(id: string): Promise<any> {
    let findTheTenant = await this.tenantModel.findOne({ _id: id });
    findTheTenant['deleted'] = true;
    const filter = { _id: id };
    this.tenantModel.findOneAndUpdate(filter, findTheTenant).exec();
  }

  async signNewVersionOfContrat(id: string): Promise<ITenantStructure> {
    const filter = { _id: id };
    let tenant = await this.tenantModel.findOne(filter);
    tenant['dateContratSigned'] = new Date();
    return await this.tenantModel.findOneAndUpdate(filter, tenant).exec();
  }

  async resiliation(id: string): Promise<ITenantStructure> {
    let res = null;

    try {
      res = await this.tenantModel
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              typeCompte: TYPE_COMPTE_GRATUIT,
              dateResiliation: new Date(),
              resiliation: true,
            },
          },
        )
        .exec();
    } catch (e) {
      new GecoError("Problème d'identification de l'entreprise parente");
    }
    return res;
  }
}
