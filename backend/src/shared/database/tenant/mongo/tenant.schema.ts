import * as mongoose from 'mongoose';

import { Document } from 'mongoose';
import { ITenantStructure } from '../../../../../../common/dto/tenant.dto';


// pour éviter de définir une nouvelle structure, on réutilise la structure tenant
// export interface ITenantStructureDocument extends Document, ITenantStructure { }

// const TenantStructure = new ITenantStructure();


export const TenantSchema = new mongoose.Schema({
    id: String,
    idStripeCostumer:String,
    paymentData: {},
    priceProduct:String,
    typeCompte: String,
    profil: String,
    tenant: String,
    nomGerant: String,
    tel: String,
    telFixe: String,
    email: String,
    ville: String,
    adresse: String,
    codePostal: String,
    complementAdresse: String,
    statutEntreprise: String,
    microEntreprise: Boolean,
    exonereTva:Boolean,
    associationExonereTva: Boolean,
    siret: String,
    dateCreation: Date,
    dateResiliation: Date,
    resiliation:Boolean,
    dateContratSigned: Date,
});

  
// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
