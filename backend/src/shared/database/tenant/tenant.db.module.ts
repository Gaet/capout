import { Module, forwardRef } from '@nestjs/common';
import { TenantSchema } from './mongo/tenant.schema';
import { DatabaseModule } from '../database.module';
import { TenantDbService } from './tenant.db.interface';
import { TenantServiceMongo } from './mongo/tenant.db.service';
import {
  getDbServiceProvider,
  getDbModelProvider,
} from '../../helpers/providers.helpers';
import { DevisDbModule } from '../devis/devis.db.module';
import { ClientDbModule } from '../client/client.db.module';
import { FactureDbModule } from '../facture/facture.db.module';
import { CatalogueDbModule } from '../catalogue/catalogue.db.module';

@Module({
  imports: [DatabaseModule, CatalogueDbModule, ClientDbModule,forwardRef(() => DevisDbModule),forwardRef(() => FactureDbModule)],
  exports: [TenantDbService],
  providers: [
    getDbServiceProvider(TenantDbService, TenantServiceMongo),
    getDbModelProvider('TenantModelToken', 'tenant', TenantSchema),
    CatalogueDbModule,
    ClientDbModule,
    DevisDbModule,
    FactureDbModule,
  ],
})
export class TenantDbModule {}
