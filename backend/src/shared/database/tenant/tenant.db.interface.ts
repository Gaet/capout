import { ITenantStructure } from '../../../../../common/dto/tenant.dto';

export { ITenantStructure } from '../../../../../common/dto/tenant.dto';

export abstract class TenantDbService {

    abstract create( tenant: ITenantStructure): Promise<ITenantStructure>;

    abstract findAllSuper(filters:any): Promise<ITenantStructure[]>;  

    abstract findAllDetailSuper(filters:any): Promise<any>;  

    abstract createIdStripeCostumer( _id: string,idStripeCostumer: string): Promise<ITenantStructure>;

    abstract resiliation( _id: string): Promise<ITenantStructure>;

    abstract signNewVersionOfContrat(_id: string): Promise<any>;

    abstract udpate( _id: string,tenant: ITenantStructure): Promise<ITenantStructure>;

    abstract findOne(_id: string): Promise<ITenantStructure>;

    abstract delete(_id: string): Promise<any>;
}
