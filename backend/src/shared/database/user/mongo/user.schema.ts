import * as mongoose from 'mongoose';

import { IUserStructure } from '../../../../../../common/dto/user.dto';

import * as bcrypt from "bcryptjs";
import * as crypto from "crypto";

// pour éviter de définir une nouvelle structure, on réutilise la structure user
//export interface IUserStructureDocument extends Document, IUserStructure { }

// const UserStructure = new IUserStructure();
export const UserSchema = new mongoose.Schema({
    id: String,
    idTenant: String,
    profil: String,
    email: String,
    name:String,
    password: String,
    forgetCode:Number,
    resetPwdFlag:Boolean,
    thumbnail:String,
    isConnected: Boolean,
    isDisconnected: Boolean,
    isAdmin: Boolean,
    isConnecting: Boolean,
    isDisconnecting: Boolean,
    connectionFailed: Boolean,
    disconnectionFailed: Boolean,
    confirmed:Boolean,
    confirmationToken:String,
    deleted:Boolean,
    firstConnection:Boolean,
    signatureGerant_b64:String,
    ips:[String],
    ipKey:String

});


UserSchema.pre('save', function (next) {
    var that = this;
    if (that.isModified('password') || that.isNew) {
      bcrypt.genSalt(10, function (err, salt) {
        if (err) {
          return next(err);
        }
        bcrypt.hash(that.password, salt, function (err, hash) {
          if (err) {
            return next(err);
          }
          that.password = hash;
          next();
        });
      });
    } else {
      return next();
    }
  });
  

  UserSchema.methods.comparePassword = async function (password: string): Promise<Boolean> {
    const isMatch = await bcrypt.compare(password, this.password);
    return isMatch;
  };

  export type UserDoc = mongoose.Document & IUserStructure;
  export const User = mongoose.model("User", UserSchema);
  
  
// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
