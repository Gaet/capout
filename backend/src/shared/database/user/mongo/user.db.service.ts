import { Injectable, Inject, Query } from '@nestjs/common';
import { Model } from 'mongoose';
import { IUserStructure, UserDbService } from '../user.db.interface';
import { UserSchema, User, UserDoc } from './user.schema';
import { getModelTenant } from '../../../helpers/providers.helpers';
import { GecoError } from '../../../../shared/services/error.service';
import { serverConfig } from '../../../../shared/helpers/serverConfig';

@Injectable()
export class UserServiceMongo implements UserDbService {
  constructor() {}

  // retourne les users qui ont cet idTenant
  // on ne renvoie que les chps _id,name,email et signatureGerant_b64
  async findAll(idTenant): Promise<IUserStructure[]> {
    const filter = { idTenant: idTenant, deleted: { $ne: true } };
    let users = [];
    (await User.find(filter).exec()).forEach((user) => {
      if (!user.toJSON().isAdmin) {
        users.push({
          _id: user.toJSON()._id,
          name: user.toJSON().name,
          email: user.toJSON().email,
          signatureGerant_b64: user.toJSON().signatureGerant_b64,
        });
      }
    });

    return users;
  }

  // retourne tous les users
  async findAllSuper(filters): Promise<IUserStructure[]> {
    let clause: any = {};
    // il faut obligatoirement le parametre secret d'un superadmin en plus
    if (filters && filters.secret === serverConfig.secret) {
      delete filters.secret;

      let privateFLag = filters.private;
      if (privateFLag) {
        clause.private = privateFLag;
      }

      let users = [];
      (await User.find(clause).exec()).forEach((user) => {
        users.push({
          _id: user.toJSON()._id,
          idTenant: user.toJSON().idTenant,
          profil: user.toJSON().profil,
          tenant: user.toJSON().tenant,
          isAdmin: user.toJSON().isAdmin,
          name: user.toJSON().name,
          email: user.toJSON().email,
          confirmed: user.toJSON().confirmed,
          confirmationToken: user.toJSON().confirmationToken,
          deleted: user.toJSON().deleted,
        });
      });

      return users;
    }
  }

  // update du name de la signature et de l'email uniquement
  async udpate(id: string, user: any): Promise<IUserStructure> {
    const filter = { _id: id };

    let userStructured = await (
      await User.findOne({ _id: id }).exec()
    ).toJSON();

    // update automatique suite à une 1er connection
    if (!user.firstConnection) {
      userStructured = {
        ...userStructured,
        firstConnection: false,
      };
    } else {
      userStructured = {
        ...userStructured,
        name: user.name,
        signatureGerant_b64: user.signatureGerant_b64,
      };
    }

    // maj
    await User.findOneAndUpdate(filter, userStructured).exec();

    // maj du password si necessaire
    const userDoc: UserDoc = (await User.findOne({
      _id: id,
    }).exec()) as UserDoc;

    if (user.password) {
      userDoc.password = user.password;
      // si ce changement intervient après un resetPwd c'est parfait l'utilisateur à bien fait son job 
      if (userDoc.resetPwdFlag) {
        userDoc.resetPwdFlag = false;
      }
    }

    await userDoc.save();

    return userStructured;
  }

  async findOne(id: string): Promise<IUserStructure> {
    return await (await User.findOne({ _id: id }).exec()).toJSON();
  }

  // faux delete, on update le user avec la propriété deleted = true
  async delete(id: string): Promise<any> {
    const findTheUser = await User.findOne({ _id: id });
    findTheUser['deleted'] = true;
    const filter = { _id: id };
    return await User.findOneAndUpdate(filter, findTheUser).exec();
  }
}
