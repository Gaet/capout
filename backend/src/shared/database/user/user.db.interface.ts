import { IUserStructure } from '../../../../../common/dto/user.dto';

export { IUserStructure } from '../../../../../common/dto/user.dto';

export abstract class UserDbService {


    abstract findAll(idTenant:string): Promise<IUserStructure[]>;
    
    abstract findAllSuper(filters:any): Promise<IUserStructure[]>;

    abstract udpate( _id: string,user: any): Promise<IUserStructure>;

    abstract findOne( _id: string): Promise<IUserStructure>;

    abstract delete( _id: string): Promise<any>;
}
