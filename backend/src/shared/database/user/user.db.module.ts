import { Module } from '@nestjs/common';
import { UserSchema } from './mongo/user.schema';
import { DatabaseModule } from '../database.module';
import { UserDbService } from './user.db.interface';
import { UserServiceMongo } from './mongo/user.db.service';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [UserDbService],
    providers: [
        getDbServiceProvider(UserDbService, UserServiceMongo),
        getDbModelProvider('UserModelToken', 'user', UserSchema),
    ]
})
export class UserDbModule {}
