import { Module } from '@nestjs/common';
import { VersionDbService } from './version.db.interface';
import { VersionServiceMongo } from './mongo/version.service';
import { VersionSchema } from './mongo/version.schema';
import { DatabaseModule } from '../database.module';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [VersionDbService],
    providers: [
        getDbServiceProvider(VersionDbService, VersionServiceMongo),
        getDbModelProvider('VersionModelToken', 'version', VersionSchema),
    ],
})
export class VersionDbModule { }
