import * as mongoose from 'mongoose';

import { Document } from 'mongoose';

import { IVersionStructure } from '../../../../../../common/dto/version.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure version
export interface IVersionStructureDocument extends Document, IVersionStructure { }

export const VersionSchema = new mongoose.Schema({
    timestamp: Date,
    type: String,
    version: String,
    message: String,
});
