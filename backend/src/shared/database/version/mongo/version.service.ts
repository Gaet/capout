import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { VersionDbService } from '../version.db.interface';
import { IVersionStructure } from '../../../../../../common/dto/version.dto';
import { serverConfig } from '../../../helpers/serverConfig';

@Injectable()
export class VersionServiceMongo implements VersionDbService {
  // on injecte le modèle sans le typer (Model<IClientStructureDocument>)
  constructor(@Inject('VersionModelToken') private readonly versionModel) {}

  async create(version: IVersionStructure): Promise<IVersionStructure> {
    console.log(version)
    const versionDb = new this.versionModel(version);
    return await versionDb.save();
  }

  async findAll(filters): Promise<IVersionStructure[]> {
      return await this.versionModel.find().exec();
    }

  async findOne(_id: string): Promise<IVersionStructure> {
    return await this.versionModel.findOne({ _id: _id }).exec();
  }

}
