import { IVersionStructure } from '../../../../../common/dto/version.dto';

export { IVersionStructure } from '../../../../../common/dto/version.dto';

export abstract class VersionDbService {

    abstract create(version: IVersionStructure): Promise<IVersionStructure>;

    abstract findAll(filters:any): Promise<IVersionStructure[]>;

    abstract findOne(_id:string): Promise<IVersionStructure>;

   
}
