import { IClientStructure } from '../../../../../common/dto/client.dto';

export { IClientStructure } from '../../../../../common/dto/client.dto';

export abstract class ClientDbService {

    abstract create(tenant: string, client: IClientStructure): Promise<IClientStructure>;

    abstract findAll(tenant: string,query:any): Promise<IClientStructure[]>;

    abstract findAllSuper(tenant: string,query:any): Promise<IClientStructure[]>;

    abstract export(tenant: string,res:any): Promise<any>;

    abstract udpate(tenant: string, _id: string,client: IClientStructure): Promise<IClientStructure>;

    abstract findOne(tenant: string, _id: string): Promise<IClientStructure>;

    abstract delete(tenant: string, _id: string): Promise<any>;
}
