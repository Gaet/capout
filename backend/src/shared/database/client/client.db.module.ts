import { Module } from '@nestjs/common';
import { ClientSchema } from './mongo/client.schema';
import { DatabaseModule } from '../database.module';
import { ClientDbService } from './client.db.interface';
import { ClientServiceMongo } from './mongo/client.db.service';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [ClientDbService],
    providers: [
        getDbServiceProvider(ClientDbService, ClientServiceMongo),
        getDbModelProvider('ClientModelToken', 'client', ClientSchema),
    ]
})
export class ClientDbModule {}
