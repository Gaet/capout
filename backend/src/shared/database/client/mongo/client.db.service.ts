import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { IClientStructure, ClientDbService } from '../client.db.interface';
import { ClientSchema } from './client.schema';
import { getModelTenant } from '../../../helpers/providers.helpers';
import { parse } from 'json2csv';
const fs = require('fs');
import { Response } from 'express';
import { join } from 'path';
import { exportCollection } from '../../../../shared/services/export.service';
import { serverConfig } from '../../../../shared/helpers/serverConfig';
@Injectable()
export class ClientServiceMongo implements ClientDbService {
  // dans le cas tenant, on injecte mongoose directement pour pouvoir
  // swapper facilement de base de données
  constructor(@Inject('DbConnectionToken') private readonly mongoose) {}

  // permet de raccourcir l'appel
  getModel(tenant: string) {
    return getModelTenant(this.mongoose, tenant, 'client', ClientSchema);
  }

  async create(
    tenant: string,
    client: IClientStructure,
  ): Promise<IClientStructure> {
    // il faut supprimer le _id par défault donné par le front
    delete client._id;
    const idClientCount = (await this.getModel(tenant).countDocuments({})) + 1;
    client = {
      idIncrement: idClientCount,
      ...client,
    };
    const createdClient = await this.getModel(tenant)(client);
    return await createdClient.save();
  }

  async udpate(
    tenant: string,
    id: string,
    newClient: IClientStructure,
  ): Promise<IClientStructure> {
    const filter = { _id: id };
    return await this.getModel(tenant)
      .findOneAndUpdate(filter, newClient)
      .exec();
  }


  async findAllSuper(tenant, query:any): Promise<IClientStructure[]> {
    let clause: any = {};
    // il faut obligatoirement le parametre secret d'un superadmin en plus
    if (query && query.secret === serverConfig.secret) {
      delete query.secret;

      return await this.getModel(tenant).find(clause).exec();
    }
  }

  async findAll(tenant: string, query: any): Promise<IClientStructure[]> {
    const clause: any = {};

    //deleted
    let deleted = query.deleted;
    if (deleted) {
      clause.deleted = true;
    } else {
      clause.deleted = false;
    }

    //nom
    let nom = query.nom;
    if (nom) {
      clause.nom = nom;
    }

    //ville
    let ville = query.ville;
    if (ville) {
      clause.ville = ville;
    }

    //contactPhone
    let contactPhone = query.contactPhone;
    if (contactPhone) {
      clause.contactPhone = contactPhone;
    }

    //gerant
    let gerant = query.gerant;
    if (gerant) {
      clause.gerant = gerant;
    }

    //siret
    let siret = query.siret;
    if (siret) {
      clause.siret = siret;
    }

    //ville
    let interlocuteur = query.interlocuteur;
    if (interlocuteur) {
      clause.interlocuteur = interlocuteur;
    }

    // resultats de la recherche en bd
    return await this.getModel(tenant).find(clause).exec();
  }

  // export de la collection client
  async export(tenant: string, res: Response): Promise<any> {
    const clients = await this.getModel(tenant).find().exec();
    exportCollection(tenant, 'client', res, clients)
  }

  async findOne(tenant: string, id: string): Promise<IClientStructure> {
    return await this.getModel(tenant).findOne({ _id: id }).exec();
  }

  // faux delete, on update le client avec la propriété deleted = true
  async delete(tenant: string, id: string): Promise<any> {
    const findTheClient = await this.getModel(tenant).findOne({ _id: id });
    findTheClient['deleted'] = true;
    const filter = { _id: id };
    return await this.getModel(tenant).findOneAndUpdate(filter, findTheClient).exec();
  }
}
