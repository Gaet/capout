import * as mongoose from 'mongoose';

import { Document } from 'mongoose';
import { IClientStructure } from '../../../../../../common/dto/client.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure client
// export interface IClientStructureDocument extends Document, IClientStructure { }

// const ClientStructure = new IClientStructure();
export const ClientSchema = new mongoose.Schema({
    id: String,
    userName:String,
    idIncrement: String,
    nom: {
        type: String,
        required: true,
    },
    siret: String,
    gerant: String,
    activity: String,
    origin: String,
    qualification: String,
    paymentCondition: String,
    paymentDelay: String,
    ville: String,
    idCommune: String,
    codePostal: String,
    numeroAdresse: String,
    adresse: String,
    payeur:String,
    payeurNom: String,
    payeurVille: String,
    payeurCodePostal: String,
    payeurNumeroAdresse: String,
    payeurAdresse: String,
    interlocuteur: String,
    contactPhone: String,
    contactEmail: String,
    etat: String,
    deleted:Boolean,
});

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
