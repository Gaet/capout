import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  IPreferenceStructure,
  PreferenceDbService,
} from '../preference.db.interface';

import { getModelTenant } from '../../../../shared/helpers/providers.helpers';
import { PreferenceSchema } from './preference.schema';
import { ITenantStructure } from '../../../../../../common/dto/tenant.dto';
import { TenantDbService } from '../../../../shared/database/tenant/tenant.db.interface';
import { initPreference } from '../../../../../../common/dto/preference.dto';

@Injectable()
export class PreferenceServiceMongo implements PreferenceDbService {
  // on injecte le modèle sans le typer (Model<IPreferenceStructureDocument>)
  // swapper facilement de base de données

  constructor(
    @Inject('DbConnectionToken') private readonly mongoose,
    private readonly tenantService: TenantDbService,
  ) {}

  // permet de raccourcir l'appel
  getModel(tenant: string) {
    return getModelTenant(
      this.mongoose,
      tenant,
      'preference',
      PreferenceSchema,
    );
  }
  async create(
    tenant: string,
    preference: IPreferenceStructure,
  ): Promise<IPreferenceStructure> {
    delete preference._id;
    const createdPreference = this.getModel(tenant)(preference);
    return await createdPreference.save();
  }

  // executer une seule fois à la creation d'un nouveau tenant
  async initPreference(data: any) {

    const preference: IPreferenceStructure = {
      ...initPreference,
      idTenant: data.idTenant,
      tenant: data.tenant,
      nomGerant: data.name,
      tel: data.tel,
      siret: data.siret,
      adresse: data.adresse,
      codePostal: data.codePostal,
      ville: data.ville,
      signature_b64: data.signatureGerant_b64,
      associationExonereTva: data.associationExonereTva,
      microEntreprise: data.microEntreprise,
      exonereTva : data.exonereTva,
      informationPourcentage: 78,
      docDevisPourcentage: 100,
      docFacturePourcentage: 33,
      docCommunPourcentage: 50,
    };
    delete preference._id;
    const createdPreference = this.getModel(preference.idTenant.toString())(
      preference,
    );
    return await createdPreference.save();
  }

  async findAll(tenant: string): Promise<IPreferenceStructure[]> {
    return await this.getModel(tenant).find().exec();
  }

  async findOne(tenant: string): Promise<IPreferenceStructure> {
    return (await this.getModel(tenant).find().exec())[0];
  }
  async udpate(
    tenant: string,
    _id: string,
    preference: IPreferenceStructure,
  ): Promise<IPreferenceStructure> {
    // attention : il n'y a pas vraiment de notion d'id pour les preferences car il n'y a qu'un seul element preference
    // quand on met à jour les préférences il faut aussi mettre le tenant à jour
    let tenantToUpdate: ITenantStructure = await this.tenantService.findOne(
      tenant,
    );
    tenantToUpdate.nomGerant = preference.nomGerant;
    tenantToUpdate.tel = preference.tel;
    tenantToUpdate.telFixe = preference.telFixe;
    tenantToUpdate.email = preference.email;
    tenantToUpdate.ville = preference.ville;
    tenantToUpdate.adresse = preference.adresse;
    tenantToUpdate.codePostal = preference.codePostal;
    tenantToUpdate.complementAdresse = preference.complementAdresse;
    tenantToUpdate.statutEntreprise = preference.statutEntreprise;
    tenantToUpdate.microEntreprise = preference.microEntreprise;
    tenantToUpdate.exonereTva = preference.exonereTva;
    tenantToUpdate.associationExonereTva = preference.associationExonereTva;
    tenantToUpdate.siret = preference.siret;

    await this.tenantService.udpate(tenant, tenantToUpdate);

    delete preference._id;
    // update
    await this.getModel(tenant.toString())
      .findOneAndUpdate({}, preference)
      .exec();
    // get
    return await this.getModel(tenant.toString()).findOne({});
  }
}
