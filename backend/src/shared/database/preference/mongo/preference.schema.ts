import * as mongoose from 'mongoose';

import { Document } from 'mongoose';

import { IPreferenceStructure } from '../../../../../../common/dto/preference.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure preference
// export interface IPreferenceStructureDocument extends Document, IPreferenceStructure { }


// const PreferenceStructure = new IPreferenceStructure(),
export const PreferenceSchema = new mongoose.Schema({
    id: String,
    idTenant: String,
    tenant: String,
    intituleEntreprise: String,
    nomGerant: String,
    email:String,
    tel: String,
    telFixe: String,
    ville: String,
    adresse: String,
    codePostal: String,
    complementAdresse: String,
    siret: String,
    tva: Number,
    exonereTva:Boolean,
    statutEntreprise: String,
    microEntreprise: Boolean,
    associationExonereTva: Boolean,
    profil: String,
    typeCompte: String,
    dateCreation: Date,
    dateEnd: Date,
    encryptedPassword: String,
    logo_entreprise_b64: String,
    signature_b64: String,
    codeApe:String,
    codeTva:String,
    ribBooleanFacture:Boolean,
    titulaireRib: String,
    ribCodeBanque: String,
    ribCodeGuichet: String,
    ribNumeroCompte: String,
    ribCle: String,
    iban:String,
    bic:String,
    optionsDevis:Array,
    optionsFacture:Array,
    optionsCommun: Array,
    textFooter: String,

    informationsComplete:Boolean,
    informationPourcentage:Number,
  
    docDevisComplete:Boolean,
    docDevisPourcentage:Number,
  
    docFactureComplete:Boolean,
    docFacturePourcentage:Number,
  
  
    docCommunComplete:Boolean,
    docCommunPourcentage:Number,
});

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
