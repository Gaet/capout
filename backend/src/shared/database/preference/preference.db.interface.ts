import { IPreferenceStructure } from '../../../../../common/dto/preference.dto';

export { IPreferenceStructure } from '../../../../../common/dto/preference.dto';

export abstract class PreferenceDbService {
    abstract initPreference(preferenceInit: any);
    
    abstract create(tenant:string,preference: IPreferenceStructure);

    abstract findAll(tenant:string): Promise<IPreferenceStructure[]>;

    abstract findOne(tenant:string): Promise<IPreferenceStructure>;

    abstract udpate(tenant:string, id: string,preference: IPreferenceStructure): Promise<IPreferenceStructure>;

}
