import { Module } from '@nestjs/common';
import { Connection } from 'mongoose';
import { PreferenceSchema } from './mongo/preference.schema';
import { DatabaseModule } from '../database.module';
import { PreferenceDbService } from './preference.db.interface';
import { PreferenceServiceMongo } from './mongo/preference.service';
import { environment } from '../../../environment/environment';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';
import { TenantDbModule } from '../tenant/tenant.db.module';

@Module({
    imports: [DatabaseModule,TenantDbModule],
    exports: [PreferenceDbService],
    providers: [
        getDbServiceProvider(PreferenceDbService, PreferenceServiceMongo),
        getDbModelProvider('PreferenceModelToken', 'preference', PreferenceSchema),
  ],
})
export class PreferenceDbModule {}
