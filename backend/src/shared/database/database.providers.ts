import * as mongoose from 'mongoose';
import { Inject } from '@nestjs/common';
import { environment } from '../../environment/environment';

class DatabaseProvider {

    constructor() {}
}

export const databaseProviders = [
    {
        provide: 'DbConnectionToken',
        useFactory: async (): Promise<typeof mongoose> => {
            // let url = `mongodb://${environment.databaseUser}:${encodeURIComponent(environment.databasePassword)}@${environment.databaseHost}/${environment.databaseName}?authSource=admin`;
            let url = `mongodb://${environment.databaseUser}:${encodeURIComponent(environment.databasePassword)}@${environment.databaseHost}:${environment.databasePort}/${environment.databaseName}?authSource=admin`;
         
            return await mongoose.connect(url)
        }
    },
];
