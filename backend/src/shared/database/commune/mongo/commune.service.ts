import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { ICommuneStructure, CommuneDbService } from '../commune.db.interface';
import { Commune, CommuneDoc } from './commune.schema';

@Injectable()
export class CommuneServiceMongo implements CommuneDbService {

    // on injecte le modèle sans le typer (Model<ICommuneStructureDocument>)
    constructor(@Inject('CommuneModelToken') private readonly communeModel) {}


    async findAll(communeQuery:any): Promise<ICommuneStructure[]> {
           console.log(communeQuery)

            const clause: any = {};
          
             //codeCom
             let codeCom = communeQuery.codeCom;
             if (codeCom) {
               clause.codeCom = codeCom;
             }
             //nomCom
             let nomCom = communeQuery.nomCom;
             if (nomCom) {
               clause.nomCom = nomCom;
             }
            // on escape les caractères spéciaux (une parenthese faisait planter la requete...)
            if (!!nomCom && nomCom.length > 0) {
              const escapedFilter = nomCom.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
              clause.nomCom = { $regex: new RegExp('^' + escapedFilter.toLowerCase(), "i") };
            }
          
            //codeReg
            let codeReg = communeQuery.codeReg;
            if (codeReg && codeReg.length > 0) {
              clause.codeReg = codeReg;
            }
          
            //codeDep
            let codeDep = communeQuery.codeDep;
            if (codeDep && codeDep.length > 0) {
              clause.codeDep = codeDep;
            }
            
        
            //limitation dans communeQuery
            let limit = 50;
            if(communeQuery.limit){
              limit = parseInt(communeQuery.limit);
            }
           
          
            return await this.communeModel.find(clause).sort({ 'population': -1 }).limit(limit).exec();
         
    }

    async findOne(type: string): Promise<ICommuneStructure> {
        return await this.communeModel.findOne( {type: type}).exec();
    }


}
