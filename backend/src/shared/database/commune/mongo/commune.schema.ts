  
  import * as mongoose from "mongoose";
  
  export class CommuneModel{
      _id: string;
      codeReg: string;
      nomReg: string;
      codeDep: string;
      nomDep: string;
      nomCom: string;
      codeCom: string;
      population: number;
      loc: number[];
  }
  
  export type CommuneDoc = mongoose.Document & CommuneModel;
  
  export let CommuneSchema = {
      codeReg: String,
      nomReg: String,
      codeDep: String,
      nomDep: String,
      codeCom: String,
      nomCom: String,
      population: Number,
      loc: {
          type: [Number],  // [<longitude>, <latitude>]
          index: '2d'      // create the geospatial index
      },
  };
  
  
  export let CommuneInit:CommuneModel = {
      codeCom: "31555",
      codeDep: "00",
      codeReg: "76",
      _id: "5a78d33dd4eb3986d014d659",
      nomCom: "NON DEFINIE",
      nomDep: "HAUTE-GARONNE",
      nomReg: "OCCITANIE",
      population: 453317,
      loc:[1.43229743595, 43.5959710776],
  };
  
  const communeSchema = new mongoose.Schema(CommuneSchema, { timestamps: true });
  
  export const Commune = mongoose.model("Commune", communeSchema);