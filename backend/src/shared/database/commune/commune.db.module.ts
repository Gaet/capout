import { Module } from '@nestjs/common';
import { Connection } from 'mongoose';
import { CommuneSchema } from './mongo/commune.schema';
import { DatabaseModule } from '../database.module';
import { CommuneDbService } from './commune.db.interface';
import { CommuneServiceMongo } from './mongo/commune.service';
import { environment } from '../../../environment/environment';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [CommuneDbService],
    providers: [
        getDbServiceProvider(CommuneDbService, CommuneServiceMongo),
        getDbModelProvider('CommuneModelToken', 'commune', CommuneSchema),
  ],
})
export class CommuneDbModule {}
