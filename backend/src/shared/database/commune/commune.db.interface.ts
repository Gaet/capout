import { ICommuneStructure } from '../../../../../common/dto/commune.dto';

export { ICommuneStructure } from '../../../../../common/dto/commune.dto';

export abstract class CommuneDbService {

    abstract findAll(communeQuery:any): Promise<ICommuneStructure[]>;

    abstract findOne( _id: string): Promise<ICommuneStructure>;

   
}
