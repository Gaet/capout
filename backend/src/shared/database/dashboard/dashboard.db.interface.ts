import { IDashboardStructure } from '../../../../../common/dto/dashboard.dto';

export { IDashboardStructure } from '../../../../../common/dto/dashboard.dto';

export abstract class DashboardDbService {
  abstract find(tenant: string): Promise<IDashboardStructure>;

  abstract findNote(tenant: string): Promise<any>;

  abstract updateNote(tenant: string, note:string): Promise<IDashboardStructure>;
}
