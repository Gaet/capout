import * as mongoose from 'mongoose';

import { Document } from 'mongoose';

import { IDashboardStructure } from '../../../../../../common/dto/dashboard.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure dashboard
// export interface IDashboardStructureDocument extends Document, IDashboardStructure { }


// const DashboardStructure = new IDashboardStructure(),
export const DashboardSchema = new mongoose.Schema({
    id: String,
    text: String,
});

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
