import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  IDashboardStructure,
  DashboardDbService,
} from '../dashboard.db.interface';
import { getModelTenant } from '../../../../shared/helpers/providers.helpers';
import { IDevisStructure } from '../../../../../../common/dto/devis.dto';
import { DashboardSchema } from './dashboard.schema';

@Injectable()
export class DashboardServiceMongo implements DashboardDbService {
  // on injecte le modèle sans le typer (Model<ICatalogueStructureDocument>)
  constructor(@Inject('DbConnectionToken') private readonly mongoose) {}

  // permet de raccourcir l'appel
  getModel(tenant: string) {
    return getModelTenant(this.mongoose, tenant, 'dashboard', DashboardSchema);
  }

  async updateNote(tenant: string, note: string): Promise<IDashboardStructure> {
    const noteExist = await this.find(tenant);
    const dashboard:any = { "text" : note  }

    if (noteExist.length == 0 ) {
      const create = this.getModel(tenant)(dashboard);
      return await create.save();
    } else {
      return await this.getModel(tenant).findOneAndUpdate({_id:noteExist[0]._id}, dashboard).exec();
    }
  }

  async find(tenant: string): Promise<any> {
    return await this.getModel(tenant).find().exec();
  }

  async findNote(tenant: string): Promise<any> {
    return await (await this.getModel(tenant).find().exec())[0].text;
  }
}
