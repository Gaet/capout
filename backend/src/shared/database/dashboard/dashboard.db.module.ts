import { Module } from '@nestjs/common';
import { Connection } from 'mongoose';
import { DashboardSchema } from './mongo/dashboard.schema';
import { DatabaseModule } from '../database.module';
import { DashboardDbService } from './dashboard.db.interface';
import { DashboardServiceMongo } from './mongo/dashboard.service';
import { environment } from '../../../environment/environment';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [DashboardDbService],
    providers: [
        getDbServiceProvider(DashboardDbService, DashboardServiceMongo),
        getDbModelProvider('DashboardModelToken', 'dashboard', DashboardSchema),
  ],
})
export class DashboardDbModule {}
