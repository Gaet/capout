import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { IPlanningStructure, PlanningDbService } from '../planning.db.interface';
import { getModelTenant } from '../../../../shared/helpers/providers.helpers';
import { PlanningSchema } from './planning.schema';

@Injectable()
export class PlanningServiceMongo implements PlanningDbService {

    // on injecte le modèle sans le typer (Model<IPlanningStructureDocument>)
     constructor(@Inject('DbConnectionToken') private readonly mongoose) {}

  // permet de raccourcir l'appel
  getModel(tenant: string) {
      return getModelTenant(this.mongoose, tenant, 'planning', PlanningSchema);
  }

    async create(tenant:string,planning: IPlanningStructure): Promise<IPlanningStructure> {
        
        if(planning._id){
            delete planning._id; 
        }
         
        if(!planning.deleted){
            planning["deleted"] = false;
        }
        console.log(planning)

        const createdPlanning = this.getModel(tenant)(planning);
        return await createdPlanning.save();
    }

    async findAll(tenant:string): Promise<IPlanningStructure[]> {
        const filter = { deleted: false };
        return await this.getModel(tenant).find(filter).exec();
    }

    async findOne(tenant:string,_id: string): Promise<IPlanningStructure> {
        return await this.getModel(tenant).findOne( {_id: _id}).exec();
    }
    
    async udpate(tenant:string,_id:string, newPlanning:IPlanningStructure): Promise<IPlanningStructure> {
        const filter = { _id: _id };
        console.log(_id,newPlanning)
        return  await this.getModel(tenant).findOneAndUpdate(filter, newPlanning).exec();
    }

     // faux delete, on update l'event _id du planning avec la propriété deleted = true
    async delete(tenant:string,_id: string): Promise<any> {
        const findTheEvent = await this.getModel(tenant).findOne({ _id: _id });
        findTheEvent["deleted"] = true;
        const filter = { _id: _id };
        return await this.getModel(tenant).findOneAndUpdate(filter, findTheEvent).exec();
    }
}
