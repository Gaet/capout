import * as mongoose from 'mongoose';

import { Document } from 'mongoose';

import { IPlanningStructure } from '../../../../../../common/dto/planning.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure planning
// export interface IPlanningStructureDocument extends Document, IPlanningStructure { }


// const PlanningStructure = new IPlanningStructure(),
export const PlanningSchema = new mongoose.Schema({
    id: String,
    userName:String,
    intitule: String,
    client: String,
    idDevis: String,
    dateBegin: Date,
    dateEnd: Date,
    commentaire: String,
    lastAccess: Date,
    lastUpdate: Date,
    nbOfViews: Number,
    deleted:Boolean,
});

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
