import { Module } from '@nestjs/common';
import { Connection } from 'mongoose';
import { PlanningSchema } from './mongo/planning.schema';
import { DatabaseModule } from '../database.module';
import { PlanningDbService } from './planning.db.interface';
import { PlanningServiceMongo } from './mongo/planning.db.service';
import { environment } from '../../../environment/environment';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [PlanningDbService],
    providers: [
        getDbServiceProvider(PlanningDbService, PlanningServiceMongo),
        getDbModelProvider('PlanningModelToken', 'planning', PlanningSchema),
  ],
})
export class PlanningDbModule {}
