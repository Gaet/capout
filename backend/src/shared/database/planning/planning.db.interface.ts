import { IPlanningStructure } from '../../../../../common/dto/planning.dto';

export { IPlanningStructure } from '../../../../../common/dto/planning.dto';

export abstract class PlanningDbService {
    abstract create(tenant:string,planning: IPlanningStructure);

    abstract findAll(tenant:string): Promise<IPlanningStructure[]>;

    abstract findOne(tenant:string,type: string): Promise<IPlanningStructure>;

    abstract udpate(tenant:string,_id: string,planning: IPlanningStructure): Promise<IPlanningStructure>;
  
    abstract delete(tenant:string,_id: string): Promise<any>;

}
