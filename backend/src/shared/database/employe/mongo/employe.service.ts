import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { IEmployeStructure, EmployeDbService } from '../employe.db.interface';

@Injectable()
export class EmployeServiceMongo implements EmployeDbService {

    // on injecte le modèle sans le typer (Model<IEmployeStructureDocument>)
    constructor(@Inject('EmployeModelToken') private readonly employeModel) {}

    async create(employe: IEmployeStructure): Promise<IEmployeStructure> {
        const createdEmploye = new this.employeModel(employe);
        return await createdEmploye.save();
    }

    async findAll(): Promise<IEmployeStructure> {
        return await this.employeModel.find().exec();
    }

    async findOne(arg: string): Promise<IEmployeStructure> {
        // es6 [arg] donne la valeur de arg
        return await this.employeModel.findOne( {[arg]: arg}).exec();
    }
}
