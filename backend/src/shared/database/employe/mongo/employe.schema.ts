import * as mongoose from 'mongoose';

import { Document } from 'mongoose';

import { IEmployeStructure } from '../../../../../../common/dto/employe.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure employe
// export interface IEmployeStructureDocument extends Document, IEmployeStructure { }


// const EmployeStructure = new IEmployeStructure(),
export const EmployeSchema = new mongoose.Schema({
    id: String,
    name:String,
    tjm:String,
});

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
