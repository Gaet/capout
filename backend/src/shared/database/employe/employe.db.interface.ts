import { IEmployeStructure } from '../../../../../common/dto/employe.dto';

export { IEmployeStructure } from '../../../../../common/dto/employe.dto';

export abstract class EmployeDbService {
    abstract create(employe: IEmployeStructure);

    abstract findAll(): Promise<IEmployeStructure>;

    abstract findOne(type: string): Promise<IEmployeStructure>;
}
