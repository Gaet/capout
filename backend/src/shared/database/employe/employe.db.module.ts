import { Module } from '@nestjs/common';
import { Connection } from 'mongoose';
import { EmployeSchema } from './mongo/employe.schema';
import { DatabaseModule } from '../database.module';
import { EmployeDbService } from './employe.db.interface';
import { EmployeServiceMongo } from './mongo/employe.service';
import { environment } from '../../../environment/environment';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [EmployeDbService],
    providers: [
        getDbServiceProvider(EmployeDbService, EmployeServiceMongo),
        getDbModelProvider('EmployeModelToken', 'employe', EmployeSchema),
  ],
})
export class EmployeDbModule {}
