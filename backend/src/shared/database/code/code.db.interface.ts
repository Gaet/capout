import { ICodeStructure, ICodeElementWithType } from '../../../../../common/dto/code.dto';

export { ICodeStructure } from '../../../../../common/dto/code.dto';

export abstract class CodeDbService {
    abstract create(code: ICodeStructure);

    abstract findAll(): Promise<ICodeStructure[]>;

    abstract findOne(type: string): Promise<ICodeStructure>;

    abstract udpate(id: string,code: ICodeElementWithType): Promise<ICodeStructure>;

}
