import * as mongoose from 'mongoose';

import { Document } from 'mongoose';

import { ICodeStructure } from '../../../../../../common/dto/code.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure code
//export interface ICodeStructureDocument extends Document, ICodeStructure { }


// const CodeStructure = new ICodeStructure();
export const CodeSchema = new mongoose.Schema({
    type: String,
    name: String,
    list: {
        id: String,
        value: String,
    },
});

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
