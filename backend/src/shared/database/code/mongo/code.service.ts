import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { ICodeStructure, CodeDbService } from '../code.db.interface';
import { ICodeElementWithType } from '../../../../../../common/dto/code.dto';

@Injectable()
export class CodeServiceMongo implements CodeDbService {

    // on injecte le modèle sans le typer (Model<ICodeStructureDocument>)
    constructor(@Inject('CodeModelToken') private readonly codeModel) {}

    async create(code: ICodeStructure): Promise<ICodeStructure> {
        const createdCode = new this.codeModel(code);
        return await createdCode.save();
    }

    async findAll(): Promise<ICodeStructure[]> {
        return await this.codeModel.find().exec();
    }

    async findOne(type: string): Promise<ICodeStructure> {
        return await this.codeModel.findOne( {type: type}).exec();
    }

       
        // la seule valeur que le client peut modifier est elm.value
        
        // exemple :
        // le client envoie le type dans la requete, et dans le body : 
        // elem: {id: "1", value: "gros oeuvre22"}
        // name: "categorie"
        // type: "categorie"
    async udpate(type:string, newCode:ICodeElementWithType): Promise<ICodeStructure> {

        const conditions = {"type": type, "list.id": newCode.elem.id};
        
        const update = {
            $addToSet: {"list.$.value": "newCode.elem.value"}
        }

        return await this.codeModel.findOneAndUpdate(conditions, update).exec();

    }

}
