import { Module } from '@nestjs/common';
import { Connection } from 'mongoose';
import { CodeSchema } from './mongo/code.schema';
import { DatabaseModule } from '../../database/database.module';
import { CodeDbService } from './code.db.interface';
import { CodeServiceMongo } from './mongo/code.service';
import { environment } from '../../../environment/environment';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [CodeDbService],
    providers: [
        getDbServiceProvider(CodeDbService, CodeServiceMongo),
        getDbModelProvider('CodeModelToken', 'code', CodeSchema),
  ],
})
export class CodeDbModule {}
