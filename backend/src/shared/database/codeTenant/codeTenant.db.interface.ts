import { ICodeStructure, ICodeElementWithType, ICodeElement } from '../../../../../common/dto/code.dto';

export { ICodeStructure } from '../../../../../common/dto/code.dto';

export abstract class CodeTenantDbService {
    
    abstract initCodeTenant(idTenant: string);

    abstract create(tenant: string,  codeTenant: ICodeStructure);

    abstract findAll(tenant: string): Promise<ICodeStructure[]>;

    abstract findOne(tenant: string, type: string): Promise<ICodeStructure>;

    abstract udpate(tenant: string, type: string,codeTenant: ICodeElementWithType): Promise<ICodeStructure>;
   
    abstract findByNameChild(tenant: string,type: string, nameChild: string): Promise<ICodeElement>;

}