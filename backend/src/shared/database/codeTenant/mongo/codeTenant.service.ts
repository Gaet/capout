import { Injectable, Inject } from '@nestjs/common';
import {
  ICodeStructure,
  CodeTenantDbService,
} from '../codeTenant.db.interface';
import { ICodeElementWithType, ICodeElement, CODE_CATEGORIE } from '../../../../../../common/dto/code.dto';
import { CodeTenantSchema } from './codeTenant.schema';
import { getModelTenant } from '../../../helpers/providers.helpers';
import * as codetenantInit from '../../../../../mongodb/jsons/codetenant.json';

@Injectable()
export class CodeTenantServiceMongo implements CodeTenantDbService {
  // dans le cas tenant, on injecte mongoose directement pour pouvoir
  // swapper facilement de base de données
  constructor(@Inject('DbConnectionToken') private readonly mongoose) {}

  // permet de raccourcir l'appel
  getModel(tenant: string) {
      return getModelTenant(this.mongoose, tenant, 'codetenant', CodeTenantSchema);
  }

  // executer une seule fois à la creation d'un nouveau tenant
  // le datebasename = idTenant
  async initCodeTenant(idTenant:string){
    for await(let codeTenant of codetenantInit) {
      const createdCodeTenant = this.getModel(`${idTenant}`)(codeTenant);
       createdCodeTenant.save();
    }
    return 'ok';
  }

  async create(tenant: string, codeTenant: ICodeStructure): Promise<ICodeStructure> {
    const createdCodeTenant = new (this.getModel(tenant)(codeTenant));
    return await createdCodeTenant.save();
  }

  async findAll(tenant: string): Promise<ICodeStructure[]> {
    return await this.getModel(tenant).find().exec();
  }

  async findOne(tenant: string, type: string): Promise<ICodeStructure> {
    return await this.getModel(tenant).findOne({ type: type }).exec();
  }
  
  async findByNameChild(tenant: string,type: string, name: string): Promise<ICodeElement> {
    console.log('findByNameChild = ', tenant,type,name)
    const code = await this.getModel(tenant).findOne({ type: type }).exec();
    let returnValue = null;
    code.list.forEach((codeName: string, i:number) => {
      if (codeName == name) {
        const codeElement: ICodeElement = {
          id: code.list.id[i],
          value: code.list.value[i],
          name: code.list.name[i],
          label: '',
          parentCode: '',
          idParent: '',
        };
        if (code.list.label && code.list.label[i]) {
          codeElement['label'] = code.list.label[i];
        }
        if (code.list.parentCode && code.list.parentCode[i]) {
          codeElement['parentCode'] = code.list.parentCode[i];
        }
        if (code.list.idParent && code.list.idParent[i]) {
          codeElement['idParent'] = code.list.idParent[i];
        }
        returnValue =  codeElement;
      }
    });
    return returnValue;
  }
  async udpate(
    tenant: string,
    type: string,
    newCode: ICodeElementWithType,
  ): Promise<ICodeStructure> {

    const codes = await this.getModel(tenant).find().exec();
    let nextId = "0";

    codes.forEach(code => {
        if(code.type == newCode.type){
            nextId = (parseInt(code.list.id[code.list.id.length-1]) + 1).toString();
        }

    });

    let conditions = {};
    let codeElem = {};
    let update = {};

    // post : on créé un nouvel element dans la liste
    if(newCode.elem.id == '0'){
       conditions = { type: newCode.type,'list.value': { $ne: newCode.elem.value } };
       codeElem = {  id:nextId , value: newCode.elem.value};
       update = {
        $push: { list: codeElem },
      };
    } 
    // delete :
    else if(newCode.elem.id == 'delete'){
      conditions =  {type: newCode.type}
      update = {
        $pull:  { 'list': {value: newCode.elem.value} },
     };
     // update :
   } else {
    const conditions = {"type": newCode.type, "list.id": newCode.elem.id};
        
    const update = {
        $set: {"list.$.value": newCode.elem.value}
    }
    
    return await this.getModel(tenant).findOneAndUpdate(conditions, update).exec();

  }
    console.log(conditions,update)

    return await this.getModel(tenant).updateOne(conditions, update).exec();
  }
}
