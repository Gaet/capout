import * as mongoose from 'mongoose';

import { Document, Schema } from 'mongoose';

import { ICodeStructure } from '../../../../../../common/dto/code.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure codeTenant
//export interface ICodeTenantStructureDocument extends Document, ICodeStructure { }

// const CodeTenantStructure = new ICodeStructure();
export const CodeTenantSchema = new mongoose.Schema({
    id: String,
    type: String,
    name:String,
    codeTenant: String,
    parentName: String,
    list:
        [{
            id: String,
            value: {},
            name: String,
            label: String,
            parentCode: String,
            idParent: String,
        }],
});

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
