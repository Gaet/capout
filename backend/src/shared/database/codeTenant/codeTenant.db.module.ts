import { Module } from '@nestjs/common';
import { Connection } from 'mongoose';
import { CodeTenantSchema } from './mongo/codeTenant.schema';
import { DatabaseModule } from '../database.module';
import { CodeTenantDbService } from './codeTenant.db.interface';
import { CodeTenantServiceMongo } from './mongo/codeTenant.service';
import { environment } from '../../../environment/environment';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [CodeTenantDbService],
    providers: [
        getDbServiceProvider(CodeTenantDbService, CodeTenantServiceMongo),
        getDbModelProvider('CodeTenantModelToken', 'codetenant', CodeTenantSchema),
  ],
})
export class CodeTenantDbModule {}
