import { Module } from '@nestjs/common';
import { Connection } from 'mongoose';
import { CatalogueSchema } from './mongo/catalogue.schema';
import { DatabaseModule } from '../database.module';
import { CatalogueDbService } from './catalogue.db.interface';
import { CatalogueServiceMongo } from './mongo/catalogue.service';
import { environment } from '../../../environment/environment';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [CatalogueDbService],
    providers: [
        getDbServiceProvider(CatalogueDbService, CatalogueServiceMongo),
        getDbModelProvider('CatalogueModelToken', 'catalogue', CatalogueSchema),
  ],
})
export class CatalogueDbModule {}
