import { ICatalogueStructure } from '../../../../../common/dto/catalogue.dto';

export { ICatalogueStructure } from '../../../../../common/dto/catalogue.dto';

export abstract class CatalogueDbService {
    abstract create(tenant: string, catalogue: ICatalogueStructure);

    abstract findAll(tenant: string): Promise<ICatalogueStructure[]>;

    abstract export(tenant: string,res:any): Promise<any>;

    abstract findOne(tenant: string, _id: string): Promise<ICatalogueStructure>;

    abstract udpate(tenant: string, id: string,catalogue: ICatalogueStructure): Promise<ICatalogueStructure>;

    abstract delete(tenant: string, _id: string): Promise<any>;

}
