import * as mongoose from 'mongoose';

import { Document } from 'mongoose';

import { ICatalogueStructure } from '../../../../../../common/dto/catalogue.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure catalogue
//export interface ICatalogueStructureDocument extends Document, ICatalogueStructure { }


// const CatalogueStructure = new ICatalogueStructure(),
export const CatalogueSchema = new mongoose.Schema({
    id: String,
    userName:String,
    titre: String,
    intitule: String,
    blocId: String,
    categorie: String,
    sousCategorie: String,
    quantite:Number,
    nbJoursLocation:Number,
    unite: String,
    coutHTAchatMatosPU:Number,
    coutHTReventeMatosPU:Number,
    hourPerPu:Number,
    coutPerPU:Number,
    hours:Number,
    tva:Number,
    commentaire: String,
    lastAccess: Date,
    lastUpdate: Date,
    nbOfViews:Number,
});

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
