import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import {
  ICatalogueStructure,
  CatalogueDbService,
} from '../catalogue.db.interface';
import { getModelTenant } from '../../../../shared/helpers/providers.helpers';
import { ClientSchema } from '../../../../shared/database/client/mongo/client.schema';
import { CatalogueSchema } from './catalogue.schema';
import { exportCollection } from '../../../../shared/services/export.service';
import { Response } from 'express';

@Injectable()
export class CatalogueServiceMongo implements CatalogueDbService {
  // on injecte le modèle sans le typer (Model<ICatalogueStructureDocument>)
  constructor(@Inject('DbConnectionToken') private readonly mongoose) {}

  // permet de raccourcir l'appel
  getModel(tenant: string) {
    return getModelTenant(this.mongoose, tenant, 'catalogue', CatalogueSchema);
  }
  async create(
    tenant: string,
    catalogue: ICatalogueStructure,
  ): Promise<ICatalogueStructure> {
    // il faut supprimer le _id par défault donné par le front
    delete catalogue._id;
    const createdCatalogue = this.getModel(tenant)(catalogue);
    return await createdCatalogue.save();
  }

  async findAll(tenant: string): Promise<ICatalogueStructure[]> {
    // let catalogueList =  await this.getModel(tenant).find().exec();
    // catalogueList.
    return await this.getModel(tenant).find().exec();
  }

  // export de la collection catalogue
  async export(tenant: string, res: Response): Promise<any> {
    const clients = await this.getModel(tenant).find().exec();
    exportCollection(tenant, 'catalogue', res, clients);
  }

  async findOne(tenant: string, id: string): Promise<ICatalogueStructure> {
    return await this.getModel(tenant).findOne({ _id: id }).exec();
  }

  async udpate(
    tenant: string,
    id: string,
    newCatalogue: ICatalogueStructure,
  ): Promise<ICatalogueStructure> {
    const filter = { _id: id };
    return await this.getModel(tenant)
      .findOneAndUpdate(filter, newCatalogue)
      .exec();
  }

  async delete(tenant: string, id: string): Promise<any> {
    const filter = { _id: id };
    return await this.getModel(tenant).find(filter).remove().exec();
  }
}
