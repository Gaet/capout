import { IAppLogStructure } from '../../../../../common/dto/app-log.dto';

export { IAppLogStructure } from '../../../../../common/dto/app-log.dto';

export abstract class AppLogDbService {

    abstract create(log: IAppLogStructure): Promise<IAppLogStructure>;

    abstract findAll(filters:any): Promise<IAppLogStructure[]>;

    abstract findOne(_id:string): Promise<IAppLogStructure>;

    abstract findAllPrivate(tenant:string): Promise<IAppLogStructure[]>;
   
}
