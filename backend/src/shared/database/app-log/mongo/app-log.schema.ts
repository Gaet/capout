import * as mongoose from 'mongoose';

import { Document } from 'mongoose';

import { IAppLogStructure } from '../../../../../../common/dto/app-log.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure client
export interface IAppLogStructureDocument extends Document, IAppLogStructure { }

export const AppLogSchema = new mongoose.Schema({
    userId: String,
    timestamp: Date,
    type: String,
    idTarget: String,
    message: String,
    private:Boolean,
});
