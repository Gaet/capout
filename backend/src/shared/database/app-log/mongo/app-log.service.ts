import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { AppLogDbService } from '../app-log.db.interface';
import { IAppLogStructure } from '../../../../../../common/dto/app-log.dto';
import { serverConfig } from '../../../../shared/helpers/serverConfig';

@Injectable()
export class AppLogServiceMongo implements AppLogDbService {
  // on injecte le modèle sans le typer (Model<IClientStructureDocument>)
  constructor(@Inject('AppLogModelToken') private readonly appLogModel) {}

  async create(appLog: IAppLogStructure): Promise<IAppLogStructure> {
    const log = new this.appLogModel(appLog);
    return await log.save();
  }

  async findAll(filters): Promise<IAppLogStructure[]> {
    let clause: any = {};

    // il faut obligatoirement le parametre secret d'un superadmin en plus
    if (filters && filters.secret === serverConfig.secret) {
      delete filters.secret;

      let privateFLag = filters.private;
      if (privateFLag) {
        clause.private = privateFLag;
      }
      return await this.appLogModel.find().exec();
    }
  }

  async findOne(_id: string): Promise<IAppLogStructure> {
    return await this.appLogModel.findOne({ _id: _id }).exec();
  }

  // à destination du tableau de bord du tenant
  async findAllPrivate(tenant: string): Promise<IAppLogStructure[]> {
    const filter = { userId: tenant, private: false };
    return await this.appLogModel.find(filter).exec();
  }
}
