import { Module } from '@nestjs/common';
import { AppLogDbService } from './app-log.db.interface';
import { AppLogServiceMongo } from './mongo/app-log.service';
import { AppLogSchema } from './mongo/app-log.schema';
import { DatabaseModule } from '../database.module';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [AppLogDbService],
    providers: [
        getDbServiceProvider(AppLogDbService, AppLogServiceMongo),
        getDbModelProvider('AppLogModelToken', 'applog', AppLogSchema),
    ],
})
export class AppLogDbModule { }
