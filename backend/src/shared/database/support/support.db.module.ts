import { Module } from '@nestjs/common';
import { SupportSchema } from './mongo/support.schema';
import { DatabaseModule } from '../database.module';
import { SupportDbService } from './support.db.interface';
import { SupportServiceMongo } from './mongo/support.db.service';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';

@Module({
    imports: [DatabaseModule],
    exports: [SupportDbService],
    providers: [
        getDbServiceProvider(SupportDbService, SupportServiceMongo),
        getDbModelProvider('SupportModelToken', 'support', SupportSchema),
    ]
})
export class SupportDbModule {}
