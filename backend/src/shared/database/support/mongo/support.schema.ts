import * as mongoose from 'mongoose';

import { ISupportStructure } from '../../../../../../common/dto/support.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure support
//export interface ISupportStructureDocument extends Document, ISupportStructure { }

// const SupportStructure = new ISupportStructure();
export const SupportSchema = new mongoose.Schema({
  id: String,
  idTenant: String,
  tenant: String,
  user: String,
  type: String,
  date: Date,
  text: String,
  status: String,
});

export type SupportDoc = mongoose.Document & ISupportStructure;
export const Support = mongoose.model('Support', SupportSchema);