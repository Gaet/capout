import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { ISupportStructure, SupportDbService } from '../support.db.interface';
import { getModelTenant } from '../../../helpers/providers.helpers';
import { Support } from './support.schema';

@Injectable()
export class SupportServiceMongo implements SupportDbService {

  constructor() {}

  // retourne les supports qui ont cet idTenant
  // on ne renvoie que les chps _id,name,email
  async findAll(idTenant): Promise<ISupportStructure[]> {
    const filter = { idTenant: idTenant };
    let supports = [];
     (await Support.find(filter).exec()).forEach((support) =>{
       
        supports.push({
          _id:support.toJSON()._id,
          idTenant:support.toJSON().idTenant,
          tenant:support.toJSON().tenant,
          user:support.toJSON().user,
          date:support.toJSON().date,
          type:support.toJSON().type,
          text:support.toJSON().text,
          status:support.toJSON().status,

        })
       
    })

    return supports;
  }


  async findOne( id: string): Promise<ISupportStructure> {
    return await (await Support.findOne({ _id: id }).exec()).toJSON();
  }


}