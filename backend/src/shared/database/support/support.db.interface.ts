import { ISupportStructure } from '../../../../../common/dto/support.dto';

export { ISupportStructure } from '../../../../../common/dto/support.dto';

export abstract class SupportDbService {


    abstract findAll(idTenant:string): Promise<ISupportStructure[]>;

    abstract findOne( _id: string): Promise<ISupportStructure>;

}
