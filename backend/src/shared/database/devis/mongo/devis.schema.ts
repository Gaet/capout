import * as mongoose from 'mongoose';

import { Document } from 'mongoose';
import { IDevisStructure } from '../../../../../../common/dto/devis.dto';

// pour éviter de définir une nouvelle structure, on réutilise la structure devis
// export interface IDevisStructureDocument extends Document, IDevisStructure { }

//todo si besoin le countDocuments s'avère foireux
// export const CounterDevisSchema = new mongoose.Schema({
//   _id: {type: String, required: true},
//   seq: { type: Number, default: 0 }
// });

// const DevisStructure = new IDevisStructure(),
export const DevisSchema = new mongoose.Schema({
  id: String,
  userName:String,
  // lien vers un client
  idClient: {
    type: String,
    required: true,
},
  // le numero du devis incremental
  idContrat: Number,
  //titre du devis (optionnel)
  titre:String,
  
  // informations clients récupérées du client lié
  client: String,
  ville: String,
  codePostal: String,
  adresse: String,
  numeroAdresse: String,

  // champs calculés du montant du devis
  totalHT: Number,
  totalTTC: Number,
  totalTVA: [Number],
  tauxTVA: [Number],

  // commentaire saisis par l'utilisateur
  commentaire: String,

  // nombre de jours de travail
  joursTravail: Number,
  nbEmployes:Number,
  totalHTPerDayPerEmployes:Number,

  // matériel : utile si le calcul par le catalogue est faux
  // lors de l'édition d'un devis on pourra changer sa valeur si besoin
  coutHTAchatMatos: Number,
  coutHTReventeMatos: Number,
  maindoeuvre:Number,
  // si le devis a été validé (ie signé)
  valide: Boolean,

  dateCreation: Date,
  dernierAcces: Date,
  dernierMAJ: Date,
  nbAcces: Number,
  envoye:Boolean,

  etat:String,

  blocksCatalogue: Array,
});

// en dev, c'est pratique d'auto indexer, mais en prod à éviter
// à ajouter en paramètre de la définition du schéma { autoIndex: false }
// cf https://mongoosejs.com/docs/guide.html#indexes
