import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { IDevisStructure, DevisDbService } from '../devis.db.interface';
import * as mongoose from 'mongoose';
import { getModelTenant } from '../../../../shared/helpers/providers.helpers';
import { DevisSchema } from './devis.schema';
import { exportCollection } from '../../../../shared/services/export.service';
// import { CounterDevisSchema } from './devis.schema';
import { Response } from 'express';
import { transformContrat } from '../../../../shared/helpers/contratHelper';
import { businessCheckContrat } from '../../../../shared/helpers/businessControl';
import { ITenantStructure } from '../../../../../../common/dto/tenant.dto';
import { TenantDbService } from '../../../../shared/database/tenant/tenant.db.interface';
import { DEVIS_ETAT_SIGNE } from '../../../../../../common/dto/devis.dto';

@Injectable()
export class DevisServiceMongo implements DevisDbService {
  // on injecte le modèle sans le typer (Model<ICatalogueStructureDocument>)
  constructor(@Inject('DbConnectionToken') private readonly mongoose,private readonly tenantService: TenantDbService,) {}

  // permet de raccourcir l'appel
  getModel(tenant: string) {
    return getModelTenant(this.mongoose, tenant, 'devis', DevisSchema);
  }

  async create(
    tenant: string,
    newDevis: IDevisStructure,
  ): Promise<IDevisStructure> {
    // applique les transformation nécessaire
    const contrat = transformContrat(newDevis);
    // controle que tout est ok à présent
    newDevis = businessCheckContrat(contrat);

    const idDevisCount = (await this.getModel(tenant).countDocuments({})) + 1;
    newDevis.idContrat = idDevisCount;
    // il faut supprimer le _id par défault donné par le front
    delete newDevis._id;

    // il faut savoir si à ce moment là le tenant est exonéré de la tva
   
    let tenantStruture: ITenantStructure = await this.tenantService.findOne(
      tenant,
    );
    newDevis.exonereTva = tenantStruture.exonereTva;
    newDevis.associationExonereTva = tenantStruture.associationExonereTva;
    
    const createdDevis = this.getModel(tenant)(newDevis);
    return await createdDevis.save();
  }

  // export de la collection catalogue
  async export(tenant: string, res: Response): Promise<any> {
    const clients = await this.getModel(tenant).find().exec();
    exportCollection(tenant, 'devis', res, clients);
  }

  async udpate(
    tenant: string,
    id: string,
    newDevis: IDevisStructure,
  ): Promise<IDevisStructure> {
    // applique les transformation nécessaire
    const contrat = transformContrat(newDevis);
    // controle que tout est ok à présent
    newDevis = businessCheckContrat(contrat);
    
    // il faut savoir si à ce moment là le tenant est exonéré de la tva
    // on ne met a jour dans le contrat que si il n'est pas figé pour avoir la cohérence entre le moment où le devis a été validé et l'etat du tenant à ce moment là
    if(newDevis.etat == DEVIS_ETAT_SIGNE) {
      let tenantStruture: ITenantStructure = await this.tenantService.findOne(
        tenant,
      );
      newDevis.exonereTva = tenantStruture.exonereTva;
      newDevis.associationExonereTva = tenantStruture.associationExonereTva;   
    }
  
    const filter = { _id: id };
    return await this.getModel(tenant)
      .findOneAndUpdate(filter, newDevis)
      .exec();
  }

  async findAll(tenant: string, query: any): Promise<IDevisStructure[]> {
    const clause: any = {};
    //_id
    let _id = query._id;
    if (_id) {
      clause._id = _id;
    }

    //userName
    let userName = query.userName;
    if (userName && userName.length > 0) {
      const userNameLowerCase = userName.map((keyword) =>
        keyword.toLowerCase(),
      );
      clause['userName'] = { $regex: new RegExp('^' + userNameLowerCase, 'i') };
    }

    // numero du devis incremental
    let idContrat = query.idContrat;
    if (idContrat) {
      clause.idContrat = idContrat;
    }

    // lien vers un client
    let idClient = query.idClient;
    if (idClient) {
      clause.idClient = idClient;
    }

    // titre

    let titre = query.titre;
    if (titre && titre.length > 0) {
      const titreLowerCase = titre.map((keyword) => keyword.toLowerCase());
      clause['titre'] = { $regex: new RegExp('^' + titreLowerCase, 'i') };
    }

    // client

    let client = query.client;
    if (client && client.length > 0) {
      const clientLowerCase = client.map((keyword) => keyword.toLowerCase());
      clause['client'] = { $regex: new RegExp('^' + clientLowerCase, 'i') };
    }

    // ville
    let ville = query.ville;
    if (ville) {
      clause.ville = ville;
    }

    // totalHT
    let totalHT = query.totalHT;
    if (totalHT) {
      clause.totalHT = totalHT;
    }

    // totalTTC
    let totalTTC = query.totalTTC;
    if (totalTTC) {
      clause.totalTTC = totalTTC;
    }

    // joursTravail
    let joursTravail = query.joursTravail;
    if (joursTravail) {
      clause.joursTravail = joursTravail;
    }

    // nbEmployes
    let nbEmployes = query.nbEmployes;
    if (nbEmployes) {
      clause.nbEmployes = nbEmployes;
    }
    // maindoeuvre
    let maindoeuvre = query.maindoeuvre;
    if (maindoeuvre) {
      clause.maindoeuvre = maindoeuvre;
    }
    // envoye
    let envoye = query.envoye;
    if (envoye) {
      clause.envoye = envoye;
    }

    // dateCreation
    let dateCreation = query.dateCreation;
    if (dateCreation) {
      clause.dateCreation = dateCreation;
    }

    // dernierAcces
    let dernierAcces = query.dernierAcces;
    if (dernierAcces) {
      clause.dernierAcces = dernierAcces;
    }

    // dernierMAJ
    let dernierMAJ = query.dernierMAJ;
    if (dernierMAJ) {
      clause.dernierMAJ = dernierMAJ;
    }

    // nbAcces
    let nbAcces = query.nbAcces;
    if (nbAcces) {
      clause.nbAcces = nbAcces;
    }

    // etat
    let etat = query.etat;
    if (etat) {
      clause.etat = etat;
    }

    // dateDebut et dateFin
    let dateDebut = query.dateDebut;
    let dateFin = query.dateFin;
    if (dateDebut || dateFin) {
      clause['dateCreation'] = {
        $elemMatch: {
          $and: [],
        },
      };
      if (dateDebut) {
        clause['dateCreation']['$elemMatch']['$and'].push({
          dateCreation: { $gte: dateDebut },
        });
      }
      if (dateFin) {
        clause['dateCreation']['$elemMatch']['$and'].push({
          dateCreation: { $lte: dateFin },
        });
      }
    }

    // tri
    const clauseQuery = {};
    const sortQuery: String = query.sort;
    if (sortQuery) {
      clause.clauseQuery = sortQuery;
    }

    // resultats de la recherche en bd
    return await this.getModel(tenant).find(clause).sort(clauseQuery).exec();
  }

  async findOne(tenant: string, id: string): Promise<IDevisStructure> {
    return await this.getModel(tenant).findOne({ _id: id }).exec();
  }

  async delete(tenant: string, id: string): Promise<any> {
    const filter = { _id: id };
    return await this.getModel(tenant).find(filter).remove().exec();
  }
}
