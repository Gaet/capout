import { Module, forwardRef } from '@nestjs/common';
import { DevisSchema } from './mongo/devis.schema';
import { DatabaseModule } from '../database.module';
import { DevisDbService } from './devis.db.interface';
import { DevisServiceMongo } from './mongo/devis.db.service';
import { getDbServiceProvider, getDbModelProvider } from '../../helpers/providers.helpers';
import { UserDbModule } from '../user/user.db.module';
import { TenantDbModule } from '../tenant/tenant.db.module';

@Module({
    imports: [DatabaseModule ,UserDbModule,forwardRef(() => TenantDbModule)],
    exports: [DevisDbService],
    providers: [
        getDbServiceProvider(DevisDbService, DevisServiceMongo),
        getDbModelProvider('DevisModelToken', 'devis', DevisSchema),
    ]
})
export class DevisDbModule {}
