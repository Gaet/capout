import { IDevisStructure } from '../../../../../common/dto/devis.dto';

export { IDevisStructure } from '../../../../../common/dto/devis.dto';

export abstract class DevisDbService {

    abstract create(tenant: string,devis: IDevisStructure): Promise<IDevisStructure>;

    abstract findAll(tenant: string,query:any): Promise<IDevisStructure[]>;

    abstract export(tenant: string,res:any): Promise<any>;

    abstract findOne(tenant: string,id: string): Promise<IDevisStructure>;

    abstract udpate(tenant: string,id: string,devis: IDevisStructure): Promise<IDevisStructure>;

    abstract delete(tenant: string, _id: string): Promise<any>;


}
