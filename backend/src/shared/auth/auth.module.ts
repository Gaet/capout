import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { environment } from '../../environment/environment';
import { JwtStrategy } from '../../shared/auth/jwt.strategy';

import {
  getDbServiceProvider,
  getDbModelProvider,
} from '../../shared/helpers/providers.helpers';
import { UserDbService } from '../../shared/database/user/user.db.interface';
import { UserServiceMongo } from '../../shared/database/user/mongo/user.db.service';
import { UserSchema } from '../../shared/database/user/mongo/user.schema';
import { DatabaseModule } from '../../shared/database/database.module';
import { AuthService } from './auth.service';
import { ServicesModule } from '../../shared/services/services.module';
import { TenantDbModule } from '../../shared/database/tenant/tenant.db.module';
import { CodeTenantDbModule } from '../../shared/database/codeTenant/codeTenant.db.module';
import { PreferenceDbModule } from '../../shared/database/preference/preference.db.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';
import { MailService } from '../../shared/services/mail.service';
import { PdfService } from '../../controllers/pdf/pdf.service';
import { DevisDbModule } from '../../shared/database/devis/devis.db.module';
import { FactureDbModule } from '../../shared/database/facture/facture.db.module';
import { ClientDbModule } from '../../shared/database/client/client.db.module';

@Module({

  imports: [
    ServicesModule,
    TenantDbModule,
    CodeTenantDbModule,
    UserDbModule,
    PreferenceDbModule,
    DatabaseModule,
  
    ClientDbModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: environment.jwtSecretKey,
      signOptions: {
        expiresIn: environment.jwtExpiration,
      },
    }),
  ],
  exports: [AuthService, JwtStrategy],
  providers: [
    AuthService,
    JwtStrategy,
    MailService,
    PdfService,
  ],
})
export class AuthenticationModule {}


