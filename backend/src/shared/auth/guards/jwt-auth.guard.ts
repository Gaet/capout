import {
    ExecutionContext,
    Injectable,
    UnauthorizedException,
    Logger,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { environment } from '../../../environment/environment';
import { AuthService } from '../auth.service';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt/dist/jwt.service';
import { UserDbService, IUserStructure } from '../../../shared/database/user/user.db.interface';
import { GecoError } from '../../../shared/services/error.service';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {

    private readonly logger: Logger = new Logger('JwtAuthGuard');

    constructor(strategy: string, private authService: AuthService, private readonly userService: UserDbService) {
        super(strategy);
        // console.log('my auth service at injection' + this.authService)
    }

     async canActivate(context: ExecutionContext) : Promise<any>{

        // on envoie quand même un token de test
        if (environment.fakeAuth) {
            this.logger.log('Le token utilisateur est simulé, on l\'ajoute dans les headers');
            // FIXME le service est mal injecté mais il fonctionne pourtant bien sur la
            // branche avi_multi_db avec les mêmes dépendances.
            if (typeof(this.authService) === 'undefined') {
                console.log("Le service AuthService n'est pas injecté correctement dans JwtAuthGuard");
                // on le crée donc à la main...
                let jwtService = new JwtService({ secretOrPrivateKey: environment.jwtSecretKey});
                // todo voir si la valeur null pour le logger et le tenantService ne pose pas de pb, faut-il les injecter ici ?
                this.authService = new AuthService(jwtService,null,null,null,null,null);
            }
            console.log('my auth service at activation ' + this.authService);
            const token = this.authService.createToken(environment.fakeTokenUsername, environment.fakeTokenTenant);
            const ctx = GqlExecutionContext.create(context);
            ctx.getArgs().req.headers['Authorization'] = 'Bearer ' + token;
            return true;
        } else {
          
            console.log(" ----------- canActivate ----------- ")
            const ctx = GqlExecutionContext.create(context);

            let auth = ctx.getArgs().req.headers['authorization'];
                  
            // est-ce qu'il y Authorization  dans les headers : oui 
            if(auth) {
                auth = auth.split('Bearer ')[1];
                if(auth){
                   // const request = context.switchToHttp().getRequest();
                   // console.log(request.user)
                    // const user = request.user;
                    // const token = (await this.userService.findOne(user.tenant, user._id)).confirmationToken;
                    // const token = (await this.authService.findByToken(auth)).confirmationToken;
    
                    // if(!auth){
                    //     // non -> log(erreur) et la remonter vers le front
                    //     throw new GecoError("Problème grave d'authentification");
    
                    // } else {
                    //     return true;
                    // }
                    return true;
                }
             
            }
                      
        }

        // default activation (ie jwt token check)
        return super.canActivate(context);
    }

    handleRequest(err, user, info) {
        this.logger.log('On vérifie l\'utilisateur au niveau de la requête');
        if (err || !user) {
            throw err || new UnauthorizedException();
        }
        return user;
    }
}