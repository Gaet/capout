export interface JwtPayload {
    email: string;
    idTenant: string;
    secret:string;
}
