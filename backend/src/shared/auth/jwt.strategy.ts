import { Injectable, UnauthorizedException, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthService } from './auth.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { environment } from '../../environment/environment';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    private readonly logger: Logger = new Logger('JwtStrategy');

    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: environment.jwtSecretKey,
        });
    }

    async validate(payload: JwtPayload) {
        this.logger.log('Validation jwt strategy');
        const user = await this.authService.validateUser(payload);
        if (!user) {
            this.logger.log('L\'utilisateur n\'est pas valide');
            throw new UnauthorizedException();
        }
        return user;
    }
}
