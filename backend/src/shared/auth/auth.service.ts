import { Injectable, UseFilters } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import {
  GecoError,
  GecoExceptionFilter,
} from '../../shared/services/error.service';
import { IUserStructure, IUserLogin } from '../../../../common/dto/user.dto';
import { UserDoc, User } from './../../shared/database/user/mongo/user.schema';
import { stringHelper } from '../../../../common/stringHelper';
import { serverConfig } from '../../shared/helpers/serverConfig';
import {
  ExpressHelper,
  RequestExpress,
} from '../../shared/helpers/expressHelper';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { Response } from 'express';
import {
  TenantDbService,
  ITenantStructure,
} from '../../shared/database/tenant/tenant.db.interface';
import { PreferenceDbService } from '../../shared/database/preference/preference.db.interface';
import { CodeTenantDbService } from '../../shared/database/codeTenant/codeTenant.db.interface';
import { createCostumerStripe } from '../../shared/services/stripe.service';
import { MailService } from '../../shared/services/mail.service';
import { TYPE_COMPTE_GRATUIT, COMPTE_ADMIN } from '../../../../common/dto/tenant.dto';

// TODO write http tests to test token creation, expiration, etc...
@Injectable()
@UseFilters(GecoExceptionFilter)
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly logger: AppLoggerService,
    private readonly tenantService: TenantDbService,
    private readonly codeTenantService: CodeTenantDbService,
    private readonly preferenceService: PreferenceDbService,
    private readonly mailService: MailService,
  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.logger);

  // creates the jwt token with embeded email
  createToken(email: string, idTenant: string) {
    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'client',
      idTarget: '',
      message: 'AuthService createToken',
      private: true,
    };

    this.logger.log(log);
    let user: JwtPayload = {
      email: email,
      idTenant: idTenant,
      secret: serverConfig.secret,
    };

    // we generate the token from user email
    return this.jwtService.sign(user);
  }

  // vérifie que l'utilisateur existe bien dans le système avec le mot de passe donné
  // @return l'utilisateur si trouvé et correspondant dans la bd
  async checkUserInDb(
    request: any,
    email: string,
    password: string,
  ): Promise<UserDoc> {
    //Les 2 champs doivent être renseignés
    if (!email || !password) {
      throw new GecoError("Veuillez saisir l'email et le mot de passe.");
    }

    //Recherche de l'utilisateur par mail pas sensible à la case
    const user: UserDoc = (await User.findOne({
      email: email,
    }).exec()) as UserDoc;

    //Mail pas trouvé
    if (!user) {
      console.log('mail ' + email + ' pas trouvé');
      throw new GecoError('email incorrect');
    }

    //Verif du password
    const passwordOk = await user.comparePassword(password);

    if (!passwordOk) {
      console.log('password pas bon');

      throw new GecoError('Utilisateur ou mot de passe incorrect');
    }
    if (!user.confirmed) {
      throw new GecoError(
        "Vous devez d'abord valider votre inscription en cliquant sur le lien que vous avez reçu par mail, ATTENTION : vérifiez vos couriers indésirables",
      );
    }

    // vérif de l'ip
    if (!user.ips.includes(this.expressHelper.getIp(request))) {
      const ipKey = stringHelper.generateRandomString(8);

      // modif du user

      user.ipKey = ipKey;
      await user.save();

      // envoi email
      const mailOptions = {
        to: user.email,
        name: user.name,
        subject: 'GECOF - nouveau lieu de connexion',
        html:
          "Nous avons détecté une tentative de connexion au logiciel GECOF. <p> Comme ce lieu de connexion est nouveau, vous devez confirmer qu'il s'agit bien de vous en cliquant: <a rel='notrack' href='" +
          serverConfig.newLocation +
          ipKey +
          "'>Je confirme qu'il s'agit bien de moi</a></p>",
      };
      await this.mailService.sendMailNewLocation(mailOptions);

      // todo envoie mail
      //x1i24.mjt.lu/lnk/CAAAAWOtlZIAAAAAAAAAAHBouxcAAYAyWC8AAAAAAA5JHABgBrqK15teKazKT8iWmJpUG1yrtgAOJUU/1/B2d2fsDiFcNKuZuFDWn4Eg/aHR0cHM6Ly9nZWNvZi1sb2dpY2llbC5mci9uZXdMb2NhdGlvbi9nTUJVZlhvcQ
      http: throw new GecoError(
        "Votre adresse de connexion étant nouvelle vous devez d'abord la valider en cliquant sur le lien que vous allez recevoir par mail, ATTENTION : si besoin vérifiez vos couriers indésirables",
      );
    }

    const userDoc: UserDoc = (await user.save()) as UserDoc;
    return userDoc;
  }

  validateUser(payload: JwtPayload): string {
    let result = null;
    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'validateUser',
      idTarget: '',
      message: `On vérifie l\'utilisateur ${payload.email}`,
      private: true,
    };
    this.logger.log(log);
    // at this state we have email and exp
    //
    if (!payload || !payload.email) return result;

    // expiration date
    const currentDate = new Date().getTime();

    // expiration date (in number of seconds) is set dynamically to the payload (so it cannot be typed)
    if (payload['exp'] > currentDate) {
      const log = {
        userId: 'superadmin',
        timestamp: new Date(),
        type: 'validateUser',
        idTarget: '',
        message: `Token expiré pour l\'utilisateur ${payload.email}`,
        private: true,
      };
      this.logger.log(log);
      return result;
    }

    // we get the user

    // TODO if no user in database, we return
    const logOk = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'validateUser',
      idTarget: '',
      message: `L\'utilisateur ${payload.email} est ok`,
      private: true,
    };
    this.logger.log(logOk);

    return payload.email;
  }

  /**
   *
   * @param {any} user
   * @returns void
   */
  checkUserFields(user: IUserStructure): void {
    //Les 3 champs doivent être renseignés si 1er compte
    if (!user.idTenant && (!user.tenant || !user.email || !user.password)) {
      throw new GecoError(
        "Veuillez saisir le nom d'utilisateur, l'email et le mot de passe.",
      );
    }
    //Les 2 champs doivent être renseignés si sous compte
    if (user.idTenant && (!user.tenant || !user.email)) {
      throw new GecoError("Veuillez saisir le nom d'utilisateur et l'email");
    }
    //nom entre 3 et 64 caractères
    if (user.tenant.length < 3 || user.tenant.length > 64) {
      throw new GecoError(
        "Le nom d'utilisateur doit avoir un nombre de caractères compris entre 3 et 64.",
      );
    }
    //pattern email
    if (!stringHelper.isEmail(user.email)) {
      throw new GecoError("L'email saisi n'est pas valide");
    }

    //mail < 64 caractères
    if (user.email.length > 64) {
      throw new GecoError(
        "L'adresse email ne doit pas dépasser 64 caractères.",
      );
    }

    //Mot de passe entre 8 et 16 caractères
    if (
      !user.idTenant &&
      (user.password.length < 8 || user.password.length > 20)
    ) {
      throw new GecoError(
        'Le mot de passe doit avoir un nombre de caractères compris entre 8 et 20.',
      );
    }

    // forgetCode >= 4 caractères
    
    if (!user.forgetCode || user.forgetCode.toString().length < 4) {
      throw new GecoError('Le code secret doit être composé d\'au moins 4 chiffres');
    }

    // forgetCode pas trop pourri
    if (user.forgetCode.toString() == '0000' || user.forgetCode.toString() == '1234' || user.forgetCode.toString() == '4321') {
      throw new GecoError('Veuillez saisir un code secret de récupération différent');
    }
  }

  /**
   *
   *
   * @param {any} user
   * @returns Promise
   */
  async checkEmailExists(user: any): Promise<void> {
    const existingUser = await User.findOne({
      email: user.email,
    }).exec();
    if (existingUser) {
      throw new GecoError('Adresse email déjà utilisée');
    }
  }

  async findByToken(token: string): Promise<any> {
    return await User.findOne({
      confirmationToken: token,
    }).exec();
  }

  toSignupUserEntity(signupUserDto: any): IUserStructure {
    //Creation de l'utilisateur
    let newUser = new IUserStructure();
    if (signupUserDto.idTenant) {
      newUser.idTenant = signupUserDto.idTenant;
    }
    newUser.tenant = signupUserDto.tenant;
    newUser.name = signupUserDto.name;
    newUser.email = signupUserDto.email;
    newUser.password = signupUserDto.password;
    newUser.forgetCode = signupUserDto.forgetCode;
    newUser.signatureGerant_b64 = signupUserDto.signatureGerant_b64;    
    newUser.ips = [];
    newUser.confirmed = false;
    return newUser;
  }

  /**
   * @param {any} user
   * @returns Promise<any>
   */
  async signupUser(
    request: any,
    res: Response,
    user: IUserLogin,
  ): Promise<any> {
    // verif user
    let newUser = this.toSignupUserEntity(user);
    newUser.confirmed = false;
    newUser.confirmationToken = this.createToken(user.email, user.tenant);

    // creation du tenant si 1er cas
    if (!newUser.idTenant || newUser.idTenant == '') {
      newUser.isAdmin = true;
      newUser.profil = COMPTE_ADMIN;

      // creation de l'id stripe dès la creation du tenant (voir doc c'est conseillé par stripe de le faire à ce moment là)
      let customerStripe = { id: '' };
      // pas utile en test de polluer stripe
      if (user.tenant != 'tenant_test_geco') {
        customerStripe = await createCostumerStripe(newUser);
      }
      // si çà se passe mal il faut pas quitter pour autant, car du fait d'une erreur stripe le user ne pourrait pas s'inscrire
      if (!customerStripe) {
        customerStripe.id = 'Impossible de créer un client stripe';
      }

      const newTenantInit: ITenantStructure = {
        _id: '',
        idStripeCostumer: customerStripe.id,
        priceProduct: '',
        paymentData: {},
        typeCompte: TYPE_COMPTE_GRATUIT,
        profil: COMPTE_ADMIN,
        tenant: newUser.tenant,
        nomGerant: newUser.name,
        tel: user.tel,
        telFixe: '',
        email: newUser.email,
        ville: user.ville,
        adresse: user.adresse,
        codePostal: user.codePostal,
        complementAdresse: '',
        microEntreprise: user.microEntreprise,
        associationExonereTva: user.associationExonereTva,
        exonereTva: user.exonereTva,
        statutEntreprise: '',
        siret: user.siret,
        dateCreation: new Date(),
        dateResiliation: null,
        dateContratSigned: new Date(),
        resiliation: false,
      };

      if (user.microEntreprise) {
        newTenantInit.statutEntreprise = 'micro';
      }

      if (user.associationExonereTva) {
        newTenantInit.statutEntreprise =
          'association exonérée de la TVA due à son caractère non lucratif';
      }

      const newTenant = await this.tenantService.create(newTenantInit);
      if (!newTenant) {
        throw new GecoError("Impossible de créer l'entreprise parente");
      }

      // on affecte le nouveau tenant au user
      newUser.idTenant = newTenant._id;

      const log = {
        userId: 'superadmin',
        timestamp: new Date(),
        type: 'user',
        idTarget: newUser.idTenant,
        message: 'nouveau tenant créé : ' + newTenant,
        private: true,
      };

      // il faut creer les collections preferences et codeTenant

      await this.codeTenantService.initCodeTenant(newUser.idTenant);
      await this.preferenceService.initPreference({
        ...newUser,
        ...user,
      });

      this.expressHelper.justLog(log);
    } else {
      // verifie l'existence du tenant
      const verifTenant = await this.tenantService.findOne(newUser.idTenant);
      if (!verifTenant || !verifTenant._id) {
        throw new GecoError(
          "Problème d'identification de l'entreprise parente",
        );
      }
      newUser.isAdmin = false;
      newUser.profil = 'normal';
      newUser.password = stringHelper.generateRandomString(8);
      newUser.firstConnection = false; // car pas admin donc inutile de mettre à true
    }
    if (newUser['_id']) {
      delete newUser['_id'];
    }

    newUser.firstConnection = true;
    newUser.ips.push(this.expressHelper.getIp(request));
    newUser.ipKey = '';

    // le user doit confirmer par email
    // ne pas oublier d'indiquer le password pour un sous compte

    var confirmUrl = serverConfig.confirmUrl + newUser.confirmationToken;

    // à activer en prod ou pour faire des tests complet avec envoie de mail
    // mais normalement inutile donc en test on confirme le token : newUser.confirmed = true;
    if (user.tenant != 'tenant_test_geco') {
      await this.mailService.sendMailSignIn(newUser, confirmUrl);
    } else {
      newUser.confirmed = true;
    }
    // dans tous les cas sans erreur : creation de l'utilisateur
    await User.create(newUser);

    return newUser;
  }

  async signupConfirm(confirmationToken: any): Promise<void> {
    //Si pas de token dans la requête
    if (!confirmationToken) {
      throw new GecoError('Token de confirmation inconnu');
    }
    const user: UserDoc = (await User.findOne({
      confirmationToken: confirmationToken,
    }).exec()) as UserDoc;

    //Si pas d'utilisateur correspondant
    if (!user) {
      throw new GecoError('Token de confirmation inconnu');
    }

    //Tout est OK, on passe le flag confirmed à true si ce n'est déjà fait
    if (!user.confirmed) {
      user.confirmed = true;
      await user.save();
    }
  }

  async addNewLocation(request: any, ipKey: string): Promise<void> {
    //Si pas de token dans la requête
    if (!ipKey) {
      throw new GecoError('Confirmation de nouveau lieu de connexion inconnu');
    }
    const user: UserDoc = (await User.findOne({
      ipKey: ipKey,
    }).exec()) as UserDoc;

    //Si pas d'utilisateur correspondant
    if (!user) {
      throw new GecoError('Confirmation de nouveau lieu de connexion inconnu');
    }

    //Tout est OK, on ajoute l'ip de la machine de connexion

    user.ips.push(this.expressHelper.getIp(request));
    await user.save();
  }

  async resetPwd(email: string, forgetCode:string): Promise<void> {
    //pas d'email dans la requete
    if (!email || email == '') {
      throw new GecoError('Veuillez saisir une adresse email');
    }

     //pas de forgetCode dans la requete
     if (!forgetCode || forgetCode == '') {
      throw new GecoError('Veuillez saisir le code secret de récupération');
    }


    //Recherche de l'utilisateur par mail
    const user: UserDoc = (await User.findOne({
      email: email,
      forgetCode:forgetCode,
    }).exec()) as UserDoc;

    // user pas trouvé
    if (!user) {
      throw new GecoError('utilisateur inconnu');
    }

    //User pas confirmé
    if (!user.confirmed) {
      throw new GecoError(
        "Vous devez d'abord valider votre inscription en cliquant sur le lien que vous avez reçu par mail, ATTENTION : vérifiez vos couriers indésirables",
      );
    }

    //Tout est OK, on crée le nouveau mot de passe, on l'assigne à l'utilisateur et on l'envoie par mail
    const newPwd = stringHelper.generateRandomString(8);
    user.password = newPwd;
    user.resetPwdFlag = true;
    await user.save();

    // envoi email
    const mailOptions = {
      to: user.email,
      name: user.name,
      subject: 'Réinitialisation de votre mot de passe',
      text: 'Votre mot de passe temporaire: ' + newPwd,
      html:
        'Votre mot de passe temporaire: <b>' +
        newPwd +
        "</b> <p> Une fois connecté, veuillez s'il vous plait modifier votre mot de passe dans votre profil puis enregistrer.</p>",
    };

    await this.mailService.sendMailResetPwd(mailOptions);
  }
}
