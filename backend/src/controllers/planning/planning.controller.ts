import {
    Put,
    Delete,
    Body,
    Get,
    Post,
    Param,
    Controller,
    Request,
    Req,
    Res,
    UseGuards,
  } from '@nestjs/common';
  import {  Response } from 'express';
  
  import { ApiResponse } from '@nestjs/swagger';
  import {
    PlanningDbService,
    IPlanningStructure,
  } from '../../shared/database/planning/planning.db.interface';
  import { ApiImplicitBody } from '@nestjs/swagger';
  import { AppLoggerService } from '../../shared/services/app-logger.service';
  import { GecoError } from '../../shared/services/error.service';
  import { ExpressHelper } from '../../shared/helpers/expressHelper';
  import { RequestContext, getTenantFromToken } from '../../shared/services/contextTenant';
  // import { AuthGuard } from '@nestjs/passport';
  import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
  import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { JwtService } from '@nestjs/jwt';
  
  @Controller('planning')
   @UseGuards(JwtAuthGuard)
  export class PlanningController {
    // TODO See if we use a dedicated service
    constructor(
      private readonly planningService: PlanningDbService,
      private readonly appLogger: AppLoggerService,
      private readonly jwtService: JwtService,
    ) {}
  
    expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);
  
    @Get()
    async findAll(@Req() request: Request): Promise<IPlanningStructure[]> {
      return this.planningService.findAll(getTenantFromToken(this.jwtService,request));
    }
  
    @Get(':id')
    async findOne(@Req() request: Request,@Param('id') id: string): Promise<IPlanningStructure> {
      return this.planningService.findOne(getTenantFromToken(this.jwtService,request),id);
    }
  
    /**
     * TODO We can have a dedicated type for createBody
     * @See @ApiModelProperty to document this DTO on swagger side
     * This DTO could be shared with frontend part.
     */
    @Post()
    @ApiResponse({
      status: 201,
      description: 'The record has been successfully created.',
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async create(
      @Req() request: Request,
      @Res() res: Response,
      @Body() newPlanning: IPlanningStructure,
    ) {
     
  
        const planning = await this.planningService.create(getTenantFromToken(this.jwtService,request),newPlanning);
        if (!planning) {
          throw new GecoError("Impossible de créer l'événement");
        }
  
          const log = {
           userId: getTenantFromToken(this.jwtService,request),
            timestamp: new Date(),
            type: 'planning',
            idTarget:planning._id,
            message: 'création événement : ' + newPlanning.intitule,
            private:false,
          };
  
          const response = this.expressHelper.buildSuccess(res, planning, log);
  
          return response;
        
    }
  
    @Put(':_id')
    async update(@Param('_id') _id: string, @Body() newPlanning,@Req() request: Request,
    @Res() res: Response,) {
      
      const log = {
       userId: getTenantFromToken(this.jwtService,request),
        timestamp: new Date(),
        type: 'planning',
        idTarget:_id,
        message: 'planning mis à jour : ' + newPlanning.intitule,
        private:false,
      };
  
      const planning = await this.planningService.udpate(getTenantFromToken(this.jwtService,request),_id, newPlanning);
      if (!planning) {
        throw new GecoError("Impossible de mettre à jour le planning "+newPlanning.intitule);
      }
    
      const response = this.expressHelper.buildSuccess(res, planning, log);
  
      return response;
  
    }
  
    @Delete(':_id')
    async remove(@Param('_id') _id: string,@Req() request: Request,
    @Res() res: Response,) {
      const planningNom = (await this.planningService.findOne(getTenantFromToken(this.jwtService,request),_id)).intitule;
      const log = {
       userId: getTenantFromToken(this.jwtService,request),
        timestamp: new Date(),
        type: 'planning',
        idTarget:_id,
        message: 'suppression événement : ' + planningNom,
        private:false,
      };
      
    
      const planning = await this.planningService.delete(getTenantFromToken(this.jwtService,request),_id);
      if (!planning) {
        throw new GecoError("Impossible de supprimer l'événement "+planningNom);
      }
    
      const response = this.expressHelper.buildSuccess(res, null, log);
  
      return response;
  
    }
  }
  