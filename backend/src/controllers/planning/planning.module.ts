import { Module } from '@nestjs/common';
import { PlanningDbModule } from '../../shared/database/planning/planning.db.module';
import { PlanningController } from './planning.controller';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';

@Module({
  controllers: [PlanningController],
  imports: [PlanningDbModule , UserDbModule, ServicesModule, AuthenticationModule],
})
export class PlanningModule {}
