import { Module, Request } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserDbModule } from '../../shared/database/user/user.db.module';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
@Module({
    controllers: [UserController],
    imports: [UserDbModule, ServicesModule, AuthenticationModule],
})
export class UserModule {}
