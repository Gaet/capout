import {
  Put,
  Delete,
  Body,
  Get,
  Req,
  Request,
  Post,
  Param,
  Controller,
  Res,
  UseGuards,
  Query,
} from '@nestjs/common';
import { Response } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import {
  UserDbService,
  IUserStructure,
} from '../../shared/database/user/user.db.interface';
import { ApiImplicitBody } from '@nestjs/swagger';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import {
  ExpressHelper,
  RequestExpress,
} from '../../shared/helpers/expressHelper';
import {
  RequestContext,
  getTenantFromToken,
} from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { AuthService } from '../../shared/auth/auth.service';
import { JwtService } from '@nestjs/jwt/dist/jwt.service';
import { request } from 'http';
import { User } from '../../shared/database/user/mongo/user.schema';

@Controller('user')
export class UserController {
  // on n'injecte pas getTenantFromToken car user fait partie de la bd gec, mais on vérifie qd meme qu'il existe
  constructor(
    private readonly userService: UserDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,
    private readonly authService: AuthService,
  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);
  @UseGuards(JwtAuthGuard)
  @Get('list/:idTenant')
  async findAll(
    @Req() request: Request,
    @Param('idTenant') idTenant: string,
  ): Promise<IUserStructure[]> {
    if (getTenantFromToken(this.jwtService, request)) {
      return this.userService.findAll(idTenant);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('all')
  async findAllSuper(
    @Req() request: Request,
    @Query() filters: any,
  ): Promise<IUserStructure[]> {
    if (getTenantFromToken(this.jwtService, request)) {
      return this.userService.findAllSuper(filters);
    }
  }
  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(
    @Req() request: Request,
    @Param('id') id: string,
  ): Promise<IUserStructure> {
    if (getTenantFromToken(this.jwtService, request)) {
      return this.userService.findOne(id);
    }
  }

  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newUser: any,
  ) {
    // sortie en erreur si pas bon
    this.authService.checkUserFields(newUser);
    // sortie en erreur si pas bon
    await this.authService.checkEmailExists(newUser);

    // ecriture en minuscule
    newUser.email = newUser.email.toString().toLowerCase();
    newUser.tenant = newUser.tenant.toString().toLowerCase();

    const user = await this.authService.signupUser(request, res, newUser);

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'user',
      idTarget: user._id,
      message:
        "Merci ! Un email de confirmation a été envoyé à l'adresse " +
        newUser.email,
      private: true,
    };

    const response = this.expressHelper.buildSuccess(res, user, log);

    return response;
  }

  @UseGuards(JwtAuthGuard)
  @Put(':_id')
  async update(
    @Param('_id') _id: string,
    @Body() newUser,
    @Req() request: Request,
    @Res() res: Response,
  ) {
    let log = {
      userId: _id,
      timestamp: new Date(),
      type: 'user',
      idTarget: _id,
      message: 'profil de ' + newUser.name + ' mis à jour',
      private: false,
    };
    let userUpdated = null;
    if (getTenantFromToken(this.jwtService, request)) {
      userUpdated = await this.userService.udpate(_id, newUser);
      if (!userUpdated) {
        throw new GecoError('Impossible de mettre à jour le profil ');
      }
    }

    const response = this.expressHelper.buildSuccess(
      res,
      userUpdated,
      log,
    );

    return response;
  }
  @UseGuards(JwtAuthGuard)
  @Delete(':_id')
  async remove(
    @Param('_id') _id: string,
    @Req() request: Request,
    @Res() res: Response,
  ) {
    let log = {
      userId: _id,
      timestamp: new Date(),
      type: 'user',
      idTarget: _id,
      message: 'suppression user : ',
      private: false,
    };
    if (getTenantFromToken(this.jwtService, request)) {
      const user = await this.userService.findOne(_id);
      log = {
        userId: _id,
        timestamp: new Date(),
        type: 'user',
        idTarget: _id,
        message: 'suppression user : ' + user.name,
        private: false,
      };
      if (user.isAdmin) {
        throw new GecoError('Impossible de supprimer un user admin');
      }
    }

    if (getTenantFromToken(this.jwtService, request)) {
      const user = await this.userService.delete(_id);
      if (!user) {
        throw new GecoError('Impossible de supprimer le user ' + _id);
      }
    }

    const response = this.expressHelper.buildSuccess(res, null, log);

    return response;
  }
}
