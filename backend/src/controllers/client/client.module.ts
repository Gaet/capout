import { Module, Request } from '@nestjs/common';
import { ClientController } from './client.controller';
import { ClientDbModule } from '../../shared/database/client/client.db.module';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';
@Module({
    controllers: [ClientController],
    imports: [ClientDbModule, UserDbModule, ServicesModule, AuthenticationModule],
})
export class ClientModule {}
