import {
  Put,
  Delete,
  Body,
  Get,
  Req,
  Request,
  Post,
  Param,
  Controller,
  Res,
  UseGuards,
  Query,
} from '@nestjs/common';
import { Response, NextFunction } from 'express';


import { ApiResponse } from '@nestjs/swagger';
import {
  ClientDbService,
  IClientStructure,
} from '../../shared/database/client/client.db.interface';
import { ApiImplicitBody } from '@nestjs/swagger';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper,RequestExpress } from '../../shared/helpers/expressHelper';
import { RequestContext, getTenantFromToken } from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { AuthService } from '../../shared/auth/auth.service';
import {JwtService} from '@nestjs/jwt/dist/jwt.service';
import { request } from 'http';

@Controller('client')
@UseGuards(JwtAuthGuard)
export class ClientController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly clientService: ClientDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,
  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);
  
  @Get()
  async findAll(@Req() request: Request,@Query() query: any): Promise<IClientStructure[]> {
    return this.clientService.findAll(getTenantFromToken(this.jwtService,request), query);
  }

  @Get('all')
  async findAllSuper(@Req() request: Request,@Query() query: any): Promise<IClientStructure[]> {
    return this.clientService.findAll(query.tenant, query);
  }

  @Post('export')
  async export(@Req() request: Request , @Res() res: Response): Promise<any> {
    return this.clientService.export(getTenantFromToken(this.jwtService,request), res);
  }

  @Get(':id')
  async findOne(@Req() request: Request, @Param('id') id: string): Promise<IClientStructure> {
    return this.clientService.findOne(getTenantFromToken(this.jwtService,request), id);
  }

  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newClient: IClientStructure,
    @Query() query: string
  ) {


    const isExist = (await this.clientService.findAll(getTenantFromToken(this.jwtService,request),query)).filter(
      (clientBd: IClientStructure) => clientBd.nom == newClient.nom,
    );

    if (isExist.length != 0) {
      const message = {
        text:
          'impossible de rajouter ce client, car un client porte déjà le meme nom',
      };
      const log = {
       userId: getTenantFromToken(this.jwtService,request),
        timestamp: new Date(),
        type: 'client',
        idTarget:"creation impossible",
        message: message.text,
        private:false,
      };
      this.appLogger.log(log);
      return message;
    } else {
      const client = await this.clientService.create(getTenantFromToken(this.jwtService,request),newClient);
      if (!client) {
        throw new GecoError("Impossible de créer le client");
      }

        const log = {
         userId: getTenantFromToken(this.jwtService,request),
          timestamp: new Date(),
          type: 'client',
          idTarget:client._id,
          message: 'création client ' + newClient.nom,
          private:false,
        };

        const response = this.expressHelper.buildSuccess(res, client, log);
        return response;
      
    }
  }

  @Put(':_id')
  async update(@Param('_id') _id: string, @Body() newClient,@Req() request: Request,
  @Res() res: Response,) {

    const log = {
     userId: getTenantFromToken(this.jwtService,request),
      timestamp: new Date(),
      type: 'client',
      idTarget:_id,
      message: 'client mis à jour : ' + newClient.nom,
      private:false,
    };

    const client = await this.clientService.udpate(getTenantFromToken(this.jwtService,request),_id, newClient);
    if (!client) {
      throw new GecoError("Impossible de mettre à jour le client "+_id);
    }
  
    const response = this.expressHelper.buildSuccess(res, client, log);

    return response;

  }

  @Delete(':_id')
  async remove(@Param('_id') _id: string,@Req() request: Request,
  @Res() res: Response,) {
    const clientNom = (await this.clientService.findOne(getTenantFromToken(this.jwtService,request),_id)).nom;
    const log = {
     userId: getTenantFromToken(this.jwtService,request),
      timestamp: new Date(),
      type: 'client',
      idTarget:_id,
      message: 'suppression client : ' + clientNom,
      private:false,
    };

    const client = await this.clientService.delete(getTenantFromToken(this.jwtService,request),_id);
    if (!client) {
      throw new GecoError("Impossible de supprimer le client "+_id);
    }
  
    const response = this.expressHelper.buildSuccess(res, null, log);

    return response;

  }
}
