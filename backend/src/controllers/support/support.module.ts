import { Module, Request } from '@nestjs/common';
import { SupportController } from './support.controller';
import { SupportDbModule } from '../../shared/database/support/support.db.module';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
@Module({
    controllers: [SupportController],
    imports: [SupportDbModule, ServicesModule, AuthenticationModule],
})
export class SupportModule {}
