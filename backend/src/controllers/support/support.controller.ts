import {
  Put,
  Delete,
  Body,
  Get,
  Req,
  Request,
  Post,
  Param,
  Controller,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';


import { ApiResponse } from '@nestjs/swagger';
import {
  SupportDbService,
  ISupportStructure,
} from '../../shared/database/support/support.db.interface';
import { ApiImplicitBody } from '@nestjs/swagger';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper,RequestExpress } from '../../shared/helpers/expressHelper';
import { RequestContext, getTenantFromToken } from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { AuthService } from '../../shared/auth/auth.service';
import {JwtService} from '@nestjs/jwt/dist/jwt.service';
import { request } from 'http';
import { Support } from '../../shared/database/support/mongo/support.schema';

@Controller('support')

export class SupportController {
  // on n'injecte pas getTenantFromToken car support fait partie de la bd gec, mais on vérifie qd meme qu'il existe
  constructor(
    private readonly supportService: SupportDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,
    private readonly authService: AuthService,

  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  @UseGuards(JwtAuthGuard)
  @Get()
  async find(@Req() request: Request, @Param('id') id: string): Promise<any> {
    const idTenant = getTenantFromToken(this.jwtService,request);
    if(idTenant){
      return this.supportService.findAll(idTenant);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@Req() request: Request, @Param('id') id: string): Promise<ISupportStructure> {
    if(getTenantFromToken(this.jwtService,request)){
      return this.supportService.findOne(id);
    }
  }

  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request ,
    @Res() res: Response,
    @Body() support: ISupportStructure,
  ) {
 
    
  }


}
