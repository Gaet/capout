import { Module } from '@nestjs/common';
import { FactureController } from './facture.controller';
import { FactureDbModule } from '../../shared/database/facture/facture.db.module';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';

@Module({
    controllers: [FactureController],
    imports: [ UserDbModule, ServicesModule, AuthenticationModule],
})
export class FactureModule {}
