import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Controller,
  Req,
  Request,
  Res,
  UseGuards,
  UseInterceptors,
  UseFilters,
  Query,
} from '@nestjs/common';
import { Response } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import {
  FactureDbService,
  IFactureStructure,
} from '../../shared/database/facture/facture.db.interface';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import {
  GecoError,
  GecoExceptionFilter,
} from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import { getTenantFromToken } from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { JwtService } from '@nestjs/jwt';
import {
  TYPE_FACTURE_FS,
  TYPE_FACTURE_ACC,
  TYPE_FACTURE_AV,
  TYPE_FACTURE_FD,
} from '../../../../common/dto/facture.dto';

@UseGuards(JwtAuthGuard)
@Controller('facture')
export class FactureController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly factureService: FactureDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,
  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  convertType(type: string): string {
    if (type == TYPE_FACTURE_FS) {
      return 'facture seule';
    }
    if (type == TYPE_FACTURE_FD) {
      return "facture issue d'un devis";
    }
    if (type == TYPE_FACTURE_AV) {
      return 'AVOIR';
    }
    if (type == TYPE_FACTURE_ACC) {
      return 'accompte';
    }
  }

  @Get()
  async findAll(
    @Req() request: Request,
    @Query() query: string,
  ): Promise<IFactureStructure[]> {
    return this.factureService.findAll(
      getTenantFromToken(this.jwtService, request),
      query,
    );
  }

  @Post('export')
  async export(@Req() request: Request, @Res() res: Response): Promise<any> {
    return this.factureService.export(
      getTenantFromToken(this.jwtService, request),
      res,
    );
  }

  @Get(':id')
  async findOne(
    @Req() request: Request,
    @Param('id') id: string,
  ): Promise<IFactureStructure> {
    return this.factureService.findOne(
      getTenantFromToken(this.jwtService, request),
      id,
    );
  }

  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newFacture: IFactureStructure,
  ) {
    const facture = await this.factureService.create(
      getTenantFromToken(this.jwtService, request),
      newFacture,
    );
    if (!facture) {
      throw new GecoError('Impossible de créer le facture');
    }

    const typeFacture = this.convertType(newFacture.typeFacture);

    const log = {
      userId: getTenantFromToken(this.jwtService, request),
      timestamp: new Date(),
      type: typeFacture,
      idTarget: facture._id,
      message: 'création ' + typeFacture,
      private: false,
    };

    const response = this.expressHelper.buildSuccess(res, facture, log);

    return response;
  }

  @Put(':_id')
  async update(
    @Param('_id') _id: string,
    @Body() newFacture: IFactureStructure,
    @Req() request: Request,
    @Res() res: Response,
  ) {
    const typeFacture = this.convertType(newFacture.typeFacture);

    const log = {
      userId: getTenantFromToken(this.jwtService, request),
      timestamp: new Date(),
      type: typeFacture,
      idTarget: _id,
      message: 'contrat mis à jour',
      private: false,
    };

    const facture = await this.factureService.udpate(
      getTenantFromToken(this.jwtService, request),
      _id,
      newFacture,
    );
    if (!facture) {
      throw new GecoError(
        'Impossible de mettre à jour ' +
          typeFacture +
          ' ' +
          newFacture.idContrat,
      );
    }

    const response = this.expressHelper.buildSuccess(res, facture, log);

    return response;
  }

  @Delete(':_id')
  async remove(
    @Param('_id') _id: string,
    @Req() request: Request,
    @Res() res: Response,
  ) {
    const idFacture = (
      await this.factureService.findOne(
        getTenantFromToken(this.jwtService, request),
        _id,
      )
    ).idContrat;

    const facture = await this.factureService.delete(
      getTenantFromToken(this.jwtService, request),
      _id,
    );

    if (!facture) {
      throw new GecoError('Impossible de supprimer le contrat ' + idFacture);
    }
    const typeFacture = this.convertType(facture.typeFacture);
    const log = {
      userId: getTenantFromToken(this.jwtService, request),
      timestamp: new Date(),
      type: typeFacture,
      idTarget: _id,
      message: 'suppression : ' + idFacture,
      private: false,
    };

    const response = this.expressHelper.buildSuccess(res, null, log);

    return response;
  }
}
