import { Module, Request } from '@nestjs/common';
import { TenantController } from './tenant.controller';
import { TenantDbModule } from '../../shared/database/tenant/tenant.db.module';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';
@Module({
    controllers: [TenantController],
    imports: [TenantDbModule,UserDbModule, ServicesModule, AuthenticationModule],
})
export class TenantModule {}
