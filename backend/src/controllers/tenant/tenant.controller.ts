import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Request,
  Controller,
  Req,
  Res,
  UseGuards,
  Query,
} from '@nestjs/common';
import { Response, NextFunction } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import {
  TenantDbService,
  ITenantStructure,
} from '../../shared/database/tenant/tenant.db.interface';
import { ApiImplicitBody } from '@nestjs/swagger';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import {
  RequestContext,
  getTenantFromToken,
} from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { IUserStructure } from '../../../../common/dto/user.dto';
import { JwtService } from '@nestjs/jwt';

@Controller('tenant')
@UseGuards(JwtAuthGuard)
export class TenantController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly tenantService: TenantDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,
  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  @Get('all')
  async findAllSuper(
    @Req() request: Request,
    @Query() query: any,
  ): Promise<ITenantStructure[]> {
    if (getTenantFromToken(this.jwtService, request)) {
      return this.tenantService.findAllSuper(query);
    }
  }

  @Get('detail')
  async findAllDetailSuper(
    @Req() request: Request,
    @Query() query: any,
  ): Promise<ITenantStructure[]> {
    if (getTenantFromToken(this.jwtService, request)) {
      return this.tenantService.findAllDetailSuper(query);
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<ITenantStructure> {
    return this.tenantService.findOne(id);
  }

 
  @Post('signNewVersionOfContrat')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async signNewVersionOfContrat(
    @Req() request: Request,
    @Res() res: Response,
    @Body() body: any,
  ) {
    const tenant = getTenantFromToken(this.jwtService, request);
    const sign = this.tenantService.signNewVersionOfContrat(tenant);
    if (!sign) {
      throw new GecoError('Erreur : Impossible de signer le nouveau contrat');
    }

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'tenant',
      idTarget: tenant,
      message: 'nouvelle version du contrat de location signé par : ' + tenant,
      private: true,
    };

    const response = this.expressHelper.buildSuccess(res, tenant, log);

    return response;
  }

  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newTenant: ITenantStructure,
  ) {
    const tenant = await this.tenantService.create(newTenant);
    if (!tenant) {
      throw new GecoError('Impossible de créer le tenant');
    }

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'tenant',
      idTarget: tenant._id,
      message: 'nouveau tenant créée : ' + newTenant._id,
      private: true,
    };

    const response = this.expressHelper.buildSuccess(res, tenant, log);

    return response;
  }

  @Put(':_id')
  async update(
    @Param('_id') _id: string,
    @Body() newTenant: ITenantStructure,
    @Req() request: Request,
    @Res() res: Response,
  ) {
    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'tenant',
      idTarget: _id,
      message: 'tenant pour le client ' + newTenant + ' mis à jour',
      private: true,
    };

    const tenant = await this.tenantService.udpate(_id, newTenant);
    if (!tenant) {
      throw new GecoError('Impossible de mettre à jour le tenant ' + _id);
    }

    const response = this.expressHelper.buildSuccess(res, tenant, log);

    return response;
  }
}
