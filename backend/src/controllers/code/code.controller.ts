import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Controller,
  Req,
  Request,
  Res,
} from '@nestjs/common';
import { Response, NextFunction } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import {
  CodeDbService,
  ICodeStructure,
} from '../../shared/database/code/code.db.interface';
import { ApiImplicitBody } from '@nestjs/swagger';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import { RequestContext, getTenantFromToken } from '../../shared/services/contextTenant';
import { ICodeElementWithType } from '../../../../common/dto/code.dto';
import { JwtService } from '@nestjs/jwt';

@Controller('code')
export class CodeController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly codeService: CodeDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,
  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  @Get()
  async findAll(): Promise<ICodeStructure[]> {
    return this.codeService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<ICodeStructure> {
    return this.codeService.findOne(id);
  }

  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newCode: ICodeStructure,
  ) {


    const code = await this.codeService.create(newCode);
    if (!code) {
      throw new GecoError('Impossible de créer le code');
    }

    const log = {
     userId: getTenantFromToken(this.jwtService,request),
      timestamp: new Date(),
      type: 'code',
      idTarget: code._id,
      message: 'nouveau code créé : ' + newCode.name,
      private: false,
    };

    const response = this.expressHelper.buildSuccess(
      res,
      code,
      log,
    );

    return response;
  }

  @Put(':type')
  async update(
    @Param('type') type: string,
    @Body() newCode:ICodeElementWithType,
    @Req() request: Request,
    @Res() res: Response,
  ) {


    const log = {
     userId: getTenantFromToken(this.jwtService,request),
      timestamp: new Date(),
      type: 'code',
      idTarget: type,
      message: 'code mis à jour : ' + newCode.name,
      private: false,
    };

    const code = await this.codeService.udpate(type, newCode);
    if (!code) {
      throw new GecoError('Impossible de mettre à jour le code ' + newCode.name);
    }

    const response = this.expressHelper.buildSuccess(
      res,
      code,
      log,
    );

    return response;
  }

}
