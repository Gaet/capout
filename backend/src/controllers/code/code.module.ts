import { Module } from '@nestjs/common';
import { CodeDbModule } from '../../shared/database/code/code.db.module';
import { CodeController } from './code.controller';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';

@Module({
  controllers: [CodeController],
  imports: [CodeDbModule, ServicesModule, AuthenticationModule],
})
export class CodeModule {}
