import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Controller,
  Req,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import {
  CatalogueDbService,
  ICatalogueStructure,
} from '../../shared/database/catalogue/catalogue.db.interface';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import { getTenantFromToken } from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { JwtService } from '@nestjs/jwt';

@UseGuards(JwtAuthGuard)
@Controller('catalogue')
export class CatalogueController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly catalogueService: CatalogueDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,
  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  @Get()
  async findAll(@Req() request: Request): Promise<ICatalogueStructure[]> {
    return this.catalogueService.findAll(getTenantFromToken(this.jwtService,request));
  }

  @Get(':id')
  async findOne(@Req() request: Request,@Param('id') id: string): Promise<ICatalogueStructure> {
    return this.catalogueService.findOne(getTenantFromToken(this.jwtService,request),id);
  }

  @Post('export')
  async export(@Req() request: Request , @Res() res: Response): Promise<any> {
    return this.catalogueService.export(getTenantFromToken(this.jwtService,request), res);
  }
  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newCatalogue: ICatalogueStructure,
  ) {


    const isExist = (await this.catalogueService.findAll(getTenantFromToken(this.jwtService,request))).filter(
      (catalogueBd: ICatalogueStructure) =>
        catalogueBd.intitule == newCatalogue.intitule,
    );

    if (isExist.length != 0) {
      const message = {
        text:
          'impossible de rajouter cet article, car un article porte déjà le meme intitule',
      };
      const log = {
        userId: getTenantFromToken(this.jwtService,request),
        timestamp: new Date(),
        type: 'catalogue',
        idTarget:"creation impossible",
        message: message.text,
        private:false,
      };
      this.appLogger.log(log);
      return message;
    } else {
      const catalogue = await this.catalogueService.create(getTenantFromToken(this.jwtService,request),newCatalogue);
      if (!catalogue) {
        throw new GecoError("Impossible de créer l'article");
      }

        const log = {
         userId: getTenantFromToken(this.jwtService,request),
          timestamp: new Date(),
          type: 'catalogue',
          idTarget:catalogue._id,
          message: 'création nouvel article catalogue : ' + newCatalogue.intitule,
          private:false,
        };

        const response = this.expressHelper.buildSuccess(res, catalogue, log);

        return response;
      
    }
  }

  @Put(':_id')
  async update(@Param('_id') _id: string, @Body() newCatalogue,@Req() request: Request,
  @Res() res: Response,) {

    const log = {
     userId: getTenantFromToken(this.jwtService,request),
      timestamp: new Date(),
      type: 'catalogue',
      idTarget:_id,
      message: 'article catalogue mis à jour : ' + newCatalogue.intitule,
      private:false,
    };

    const catalogue = await this.catalogueService.udpate(getTenantFromToken(this.jwtService,request),_id, newCatalogue);
    if (!catalogue) {
      throw new GecoError("Impossible de mettre à jour l'article "+_id);
    }
  
    const response = this.expressHelper.buildSuccess(res, catalogue, log);

    return response;

  }

  @Delete(':_id')
  async remove(@Param('_id') _id: string,@Req() request: Request,
  @Res() res: Response,) {
    const catalogueIntitule = (await this.catalogueService.findOne(getTenantFromToken(this.jwtService,request),_id)).intitule;
    const log = {
     userId: getTenantFromToken(this.jwtService,request),
      timestamp: new Date(),
      type: 'catalogue',
      idTarget:_id,
      message: 'suppression article : ' + catalogueIntitule,
      private:false,
    };

    const catalogue = await this.catalogueService.delete(getTenantFromToken(this.jwtService,request),_id);
    if (!catalogue) {
      throw new GecoError("Impossible de supprimer l'article "+_id);
    }
  
    const response = this.expressHelper.buildSuccess(res, null, log);

    return response;

  }
}
