import { Module } from '@nestjs/common';
import { CatalogueController } from './catalogue.controller';
import { CatalogueDbModule } from '../../shared/database/catalogue/catalogue.db.module';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';

@Module({
    controllers: [CatalogueController],
    imports: [CatalogueDbModule, UserDbModule, ServicesModule, AuthenticationModule],
})
export class CatalogueModule {}
