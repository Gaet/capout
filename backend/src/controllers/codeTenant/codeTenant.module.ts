import { Module } from '@nestjs/common';
import { CodeTenantDbModule } from '../../shared/database/codeTenant/codeTenant.db.module';
import { CodeTenantController } from './codeTenant.controller';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';

@Module({
  controllers: [CodeTenantController],
  imports: [CodeTenantDbModule, UserDbModule, ServicesModule, AuthenticationModule],
})
export class CodeTenantModule {}
