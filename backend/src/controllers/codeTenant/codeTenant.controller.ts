import { Request, Req, Put, Delete, Body, Get, Post, Param, Controller, UseGuards, Res } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { CodeTenantDbService, ICodeStructure } from '../../shared/database/codeTenant/codeTenant.db.interface';
import { ApiImplicitBody } from '@nestjs/swagger';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { ExtractJwt } from 'passport-jwt';
import {JwtService} from '@nestjs/jwt/dist/jwt.service';
import { ExpressHelper , RequestExpress} from '../../shared/helpers/expressHelper';
import { Response, NextFunction } from 'express';
import { ICodeElement } from '../../../../common/dto/code.dto';
import { GecoError } from '../../shared/services/error.service';
import { getTenantFromToken } from '../../shared/services/contextTenant';
@Controller('codetenant')
@UseGuards(JwtAuthGuard)
export class CodeTenantController {

    // TODO See if we use a dedicated service
    constructor(
        private readonly codeTenantService: CodeTenantDbService,
        private readonly appLogger: AppLoggerService,
        private readonly jwtService: JwtService,
    ) {}
    
    expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);



    @Get()
    async findAll(@Req() request: Request): Promise<ICodeStructure[]> {
        return this.codeTenantService.findAll(getTenantFromToken(this.jwtService,request));
    }

    @Get(':id')
    async findOne(@Req() request: Request, @Param('id') id: string): Promise<ICodeStructure> {
        return this.codeTenantService.findOne(getTenantFromToken(this.jwtService,request), id);
    }

    @Get(':type/:nameChild')
    async findByNameChild( @Req() request: Request,@Param('type') type: string,@Param('nameChild') nameChild: string): Promise<ICodeElement> {    
      return await this.codeTenantService.findByNameChild(getTenantFromToken(this.jwtService,request),type,nameChild);
    }
  
    /**
     * TODO We can have a dedicated type for createBody
     * @See @ApiModelProperty to document this DTO on swagger side
     * This DTO could be shared with frontend part.
     */
    
    @Post()
    @ApiResponse({
      status: 201,
      description: 'The record has been successfully created.',
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async create(
      @Req() request: Request,
      @Res() res: Response,
      @Body() newCodeTenant: ICodeStructure,
    ) {
   
  
      const codeTenant = await this.codeTenantService.create(getTenantFromToken(this.jwtService,request),newCodeTenant);
      if (!codeTenant) {
        throw new GecoError('Impossible de créer le codeTenant');
      }
  
      const log = {
       userId: getTenantFromToken(this.jwtService,request),
        timestamp: new Date(),
        type: 'codeTenant',
        idTarget: codeTenant._id,
        message: 'nouveau codeTenant créé : ' + newCodeTenant.name,
        private: false,
      };
  
      const response = this.expressHelper.buildSuccess(
        res,
        codeTenant,
        log,
      );
  
      return response;
    }
  
    @Put(':type')
    async update(
      @Param('type') type: string,
      @Body() newCodeTenant,
      @Req() request: Request,
      @Res() res: Response,
    ) {
    
      let ms = '';
      if(newCodeTenant.elem.  id == '0') {
         ms = 'nouvel element de liste : ' + newCodeTenant.name;
      } else if(newCodeTenant.elem.id == 'delete') {
        ms = 'suppression dans la liste : ' + newCodeTenant.name;
      } else {
        ms = 'mise à jour dans la liste : ' + newCodeTenant.name;
      }
      const log = {
       userId: getTenantFromToken(this.jwtService,request),
        timestamp: new Date(),
        type: 'codeTenant',
        idTarget: type,
        message: ms,
        private: false,
      };
      
  
      const codeTenant = await this.codeTenantService.udpate(getTenantFromToken(this.jwtService,request),type, newCodeTenant);
      if (!codeTenant) {
        throw new GecoError('Impossible de mettre à jour le codeTenant ' + newCodeTenant);
      }
  
      const response = this.expressHelper.buildSuccess(
        res,
        codeTenant,
        log,
      );
  
      return response;
    }
  
  }
  