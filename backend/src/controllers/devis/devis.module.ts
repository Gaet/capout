import { Module } from '@nestjs/common';
import { DevisController } from './devis.controller';
import { DevisDbModule } from '../../shared/database/devis/devis.db.module';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';

@Module({
    controllers: [DevisController],
    imports: [ ServicesModule, AuthenticationModule],
})
export class DevisModule {}
