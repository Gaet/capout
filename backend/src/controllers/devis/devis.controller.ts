import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Controller,
  Req,
  Request,
  Res,
  UseGuards,
  Query,
} from '@nestjs/common';
import { Response } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import {
  DevisDbService,
  IDevisStructure,
} from '../../shared/database/devis/devis.db.interface';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import { getTenantFromToken } from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { JwtService } from '@nestjs/jwt';

@UseGuards(JwtAuthGuard)
@Controller('devis')
export class DevisController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly devisService: DevisDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,
  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  @Get()
  async findAll(
    @Req() request: Request,
    @Query() query: string,
  ): Promise<IDevisStructure[]> {
    return this.devisService.findAll(
      getTenantFromToken(this.jwtService, request),
      query,
    );
  }

  @Post('export')
  async export(@Req() request: Request, @Res() res: Response): Promise<any> {
    return this.devisService.export(
      getTenantFromToken(this.jwtService, request),
      res,
    );
  }

  @Get(':id')
  async findOne(
    @Req() request: Request,
    @Param('id') id: string,
  ): Promise<IDevisStructure> {
    return this.devisService.findOne(
      getTenantFromToken(this.jwtService, request),
      id,
    );
  }

  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newDevis: IDevisStructure,
  ) {
    const devis = await this.devisService.create(
      getTenantFromToken(this.jwtService, request),
      newDevis,
    );
    if (!devis) {
      throw new GecoError('Impossible de créer le devis');
    }

    const log = {
      userId: getTenantFromToken(this.jwtService, request),
      timestamp: new Date(),
      type: 'devis',
      idTarget: devis._id,
      message: 'création devis',
      private: false,
    };

    const response = this.expressHelper.buildSuccess(res, devis, log);

    return response;
  }

  @Put(':_id')
  async update(
    @Param('_id') _id: string,
    @Body() newDevis,
    @Req() request: Request,
    @Res() res: Response,
  ) {
    const log = {
      userId: getTenantFromToken(this.jwtService, request),
      timestamp: new Date(),
      type: 'devis',
      idTarget: _id,
      message: 'devis mis à jour',
      private: false,
    };

    const devis = await this.devisService.udpate(
      getTenantFromToken(this.jwtService, request),
      _id,
      newDevis,
    );
    if (!devis) {
      throw new GecoError(
        'Impossible de mettre à jour le devis ' + newDevis.idContrat,
      );
    }

    const response = this.expressHelper.buildSuccess(res, devis, log);

    return response;
  }

  @Delete(':_id')
  async remove(
    @Param('_id') _id: string,
    @Req() request: Request,
    @Res() res: Response,
  ) {
    const idDevis = (
      await this.devisService.findOne(
        getTenantFromToken(this.jwtService, request),
        _id,
      )
    ).idContrat;
    const log = {
      userId: getTenantFromToken(this.jwtService, request),
      timestamp: new Date(),
      type: 'devis',
      idTarget: _id,
      message: 'suppression devis : ' + idDevis,
      private: false,
    };

    const devis = await this.devisService.delete(
      getTenantFromToken(this.jwtService, request),
      _id,
    );
    if (!devis) {
      throw new GecoError('Impossible de supprimer le devis ' + idDevis);
    }

    const response = this.expressHelper.buildSuccess(res, null, log);

    return response;
  }
}
