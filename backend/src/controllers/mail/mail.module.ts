import { Module } from '@nestjs/common';
import { MailController } from './mail.controller';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { DevisDbModule } from '../../shared/database/devis/devis.db.module';
import { FactureDbModule } from '../../shared/database/facture/facture.db.module';

@Module({
    controllers: [MailController],
    imports: [ ServicesModule, AuthenticationModule],
})
export class MailModule {}
