import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Controller,
  Req,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import { getTenantFromToken } from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { JwtService } from '@nestjs/jwt';
import { resolve } from 'url';
import { TYPE_CONTRAT_DEVIS, IDevisStructure } from '../../../../common/dto/devis.dto';
import { TYPE_CONTRAT_FACTURE, TYPE_FACTURE_FD, TYPE_FACTURE_FS, TYPE_FACTURE_ACC, TYPE_FACTURE_AV, IFactureStructure } from '../../../../common/dto/facture.dto';
import { FactureDbService } from '../../shared/database/facture/facture.db.interface';
import { DevisDbService } from '../../shared/database/devis/devis.db.interface';
import { MailService } from '../../shared/services/mail.service';

@UseGuards(JwtAuthGuard)
@Controller('mail')
export class MailController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly appLogger: AppLoggerService,
    private readonly factureService: FactureDbService,
    private readonly devisService: DevisDbService,
    private readonly mailService: MailService,
    private readonly jwtService: JwtService,


  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async send(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newMail: any,
  ) {
    const mail = await this.mailService.sendMailSupport(newMail);
    if (!mail) {
      throw new GecoError("Impossible d'envoyer le mail");
    }

    const log = {
     userId: getTenantFromToken(this.jwtService,request),
      timestamp: new Date(),
      type: 'mail',
      idTarget:'',
      message: ' mail envoyé au support GECOF ',
      private: false,
    };

    const response = this.expressHelper.buildSuccess(
      res,
      newMail,
      log,
    );

    console.log(response)

    return response;
  }
  @Post('sendPdfMail')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async sendPdfMail(
    @Req() request: Request,
    @Res() res: Response,
    @Body() body: any,
  ) {

    // le body contient l'id du tenant, le nom du tenant , et le contrat complet
    // { tenant: .....,nameTenant:....,_id:.....,typeFacture:....}
    
    body.message = "Bonjour, veuillez trouver ci-joint ";
    body.subject = "";

    if(!body.typeFacture) {
      body.message += "le devis demandé ";
      body.subject = "Devis "+body.idContrat+" - "+body.nameTenant;
    } else if(body.typeFacture == TYPE_FACTURE_FD || body.typeFacture == TYPE_FACTURE_FS){
      body.message += "la facture demandée ";
      body.subject = "Facture "+body.idContrat+" - "+body.nameTenant;
    } else if(body.typeFacture == TYPE_FACTURE_ACC){
      body.message += "la facture d'accompte demandée ";
      body.subject = "Facture d'accompte"+body.idContrat+" - "+body.nameTenant;
    }else if(body.typeFacture == TYPE_FACTURE_AV){
      body.message += "l'avoir demandé ";
      body.subject = "Avoir "+body.idContrat+" - "+body.nameTenant;
    }

    if(body.contratTitle){
      body.message+='concernant '+ body.contratTitle;
    }
    const mail = await this.mailService.sendPdfMail(body);
    if (!mail) {
      throw new GecoError("Impossible d'envoyer le mail");
    }

    const log = {
     userId: getTenantFromToken(this.jwtService,request),
      timestamp: new Date(),
      type: 'mail',
      idTarget:'',
      message: ' mail avec pdf envoyé au client '+body.nameClient + '  '+ body.emailClient,
      private: false,
    };

    const response = this.expressHelper.buildSuccess(
      res,
      body,
      log,
    );

    console.log(response)

    return response;
  }
  
}
