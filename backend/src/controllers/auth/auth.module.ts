import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { getDbServiceProvider } from '../../shared/helpers/providers.helpers';
import { UserDbService } from '../../shared/database/user/user.db.interface';
import { UserServiceMongo } from '../../shared/database/user/mongo/user.db.service';
import { ServicesModule } from '../../shared/services/services.module';

@Module({
    imports: [AuthenticationModule, ServicesModule],
    controllers: [AuthController],
    providers: [
        getDbServiceProvider(UserDbService, UserServiceMongo),
      ],
})
export class AuthModule {}
