import {
  Controller,
  Get,
  Param,
  Req,
  Res,
  Body,
  Post,
  UseGuards,
  UseFilters,
} from '@nestjs/common';
import { AuthService } from '../../shared/auth/auth.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import { VALIDATION_INSCRIPTION, FORGET_PWD } from '../../shared/helpers/const';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { Request, Response } from 'express';
import { IAppLogStructure } from '../../../../common/dto/app-log.dto';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { IUserLogin } from '../../../../common/dto/user.dto';
import { GecoExceptionFilter } from '../../shared/services/error.service';
import { TenantDbService } from '../../shared/database/tenant/tenant.db.interface';
import {
  TYPE_COMPTE_GRATUIT,
  TYPE_COMPTE_BUSINESS,
} from '../../../../common/dto/tenant.dto';
import {
  isAfterDate,
  addDaysToDate,
  isSameYear,
} from '../../shared/helpers/dateHelper';
import { FactureServiceMongo } from '../../shared/database/facture/mongo/facture.db.service';
import { FactureDbService } from '../../shared/database/facture/facture.db.interface';
import { serverConfig } from '../../shared/helpers/serverConfig';
import {
  checkConditionsToUseGecoHelper,
  apiCheckConditionsToChangeAbonnementHelper,
} from '../../shared/helpers/checkConditionsHelper';
import { UserDoc, User } from './../../shared/database/user/mongo/user.schema';

// @UseFilters(GecoExceptionFilter)
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly appLogger: AppLoggerService,
    private readonly tenantService: TenantDbService,
    private readonly factureService: FactureDbService,
  ) {}
  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  async createToken(
    @Req() request: Request,
    email: string,
    password: string,
  ): Promise<any> {
    const user = await this.authService.checkUserInDb(request, email, password);

    const tenant = await this.tenantService.findOne(user['idTenant']);
    return {
      user: user,
      tenant: tenant,
      token: this.authService.createToken(email, user['idTenant']),
    };
  }

  @Get('signupConfirm/:confirmationToken')
  async signupConfirm(
    @Req() req: Request,
    @Res() res: Response,
    @Param('confirmationToken') confirmationToken: string,
  ) {
    const requestContext = await this.expressHelper.buildRequestContext(req);
    await this.authService.signupConfirm(confirmationToken);
    let userId = 'non connu';
    if (requestContext.user) {
      userId = requestContext.user._id;
    }
    let journal: IAppLogStructure = {
      type: VALIDATION_INSCRIPTION,
      userId: userId,
      timestamp: new Date(),
      idTarget: userId,
      private: true,
      message: VALIDATION_INSCRIPTION + ' ' + requestContext.user,
    };
    return this.expressHelper.buildSuccess(
      res,
      'Incription confirmée ! Vous pouvez maintenant vous connecter',
      journal,
    );
  }

  @Post('resetPwd')
  async resetPwd(
    @Req() request: Request,
    @Res() res: Response,
    @Body() form: any,
  ) {
    await this.authService.resetPwd(form.email,form.forgetCode);

    let journal: IAppLogStructure = {
      type: FORGET_PWD,
      userId: form.email,
      timestamp: new Date(),
      idTarget: form.email,
      private: true,
      message: FORGET_PWD,
    };
    return this.expressHelper.buildSuccess(
      res,
      'email envoyé ! vous y trouverez un mot de passe temporaire afin de vous connecter de nouveau à GECOF',
      journal,
    );
  }

  @Get('newLocation/:ipKey')
  async newLocationConfirm(
    @Req() request: Request,
    @Res() res: Response,
    @Param('ipKey') ipKey: string,
  ) {
    await this.authService.addNewLocation(request, ipKey);

    return this.expressHelper.buildSuccess(
      res,
      'Merci, vous pouvez désormais vous connecter à partir de cet appareil',
      null,
    );
  }

  // @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Req() request: Request, @Body() user: IUserLogin) {
    const email = user.email.toLowerCase();
    const password = user.password;
    const token = await this.createToken(request, email, password);

    return token;
  }

  // todo voir si utile
  @Get('logout')
  logout(@Req() req, @Res() res: Response) {
    req.logout();
    // res.redirect('/');
  }

  // doit recevoir le token dans les headers, et en retour donner le user correspondant
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Req() req) {
    //   console.log("profil = " , req.user);
    return req.user;
  }
  @UseGuards(JwtAuthGuard)
  @Get('apiCheckConditionsToChangeAbonnement/:tenant/:typeCompteSelected')
  async apiCheckConditionsToChangeAbonnement(
    @Req() req: Request,
    @Res() res: Response,
    @Param('tenant') tenant: string,
    @Param('typeCompteSelected') typeCompteSelected: string,
  ) {
    // 2 choses à vérifier si compte gratuit :  les 15 jours gratuit et le chiffre d'affaire pour l'année en cours
    let tenantToCheck = await this.tenantService.findOne(tenant);

    let factures = await this.factureService.findAll(tenant, {});

    const checkConditionsToChangeAbonnement = await apiCheckConditionsToChangeAbonnementHelper(
      factures,
      tenantToCheck,
      typeCompteSelected,
    );

    const response = this.expressHelper.buildSuccess(
      res,
      checkConditionsToChangeAbonnement,
      null,
    );

    return response;
  }
  @UseGuards(JwtAuthGuard)
  @Get('checkConditionsToUseGeco/:tenant/:user')
  async checkConditionsToUseGeco(
    @Req() req: Request,
    @Res() res: Response,
    @Param('tenant') idTenant: string,
    @Param('user') idUser: string,

  ) {
    // 3 choses à vérifier:
    // - si compte gratuit :  les 15 jours gratuit 
    // - si le chiffre d'affaire pour l'année en cours est ok en ft abonnement
    // - si !resetPwd
    let tenantToCheck = await this.tenantService.findOne(idTenant);
    let factures = await this.factureService.findAll(idUser, {});
    const user: UserDoc = (await User.findOne({
      _id: idUser
    }).exec()) as UserDoc;

    const checkConditionsToUseGeco = await checkConditionsToUseGecoHelper(
      factures,
      tenantToCheck,
      user
    );

    const response = this.expressHelper.buildSuccess(
      res,
      checkConditionsToUseGeco,
      null,
    );

    return response;
  }
}
