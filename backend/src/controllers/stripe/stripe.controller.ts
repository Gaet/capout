import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Controller,
  Req,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import { getTenantFromToken } from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { JwtService } from '@nestjs/jwt';
import {
  createCostumerStripe,
  createSubscriptionStripe,
  webHook,
  retryInvoice,
  deletedSubscription,
  updateSubscription,
} from '../../shared/services/stripe.service';
import { resolve } from 'url';
import { TenantDbService } from '../../shared/database/tenant/tenant.db.interface';
import { IpaymentStripe } from '../../../../common/dto/tenant.dto';
import { MailService } from '../../shared/services/mail.service';

@UseGuards(JwtAuthGuard)
@Controller('stripe')
export class StripeController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly appLogger: AppLoggerService,
    private readonly tenantService: TenantDbService,
    private readonly mailService: MailService,
    private readonly jwtService: JwtService,

  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  // utile dans le cas où la création n'a pas fonctionnée lors de l'inscription du tenant
  @Post('createCustomer')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async createCostumer(
    @Req() request: Request,
    @Res() res: Response,
    @Body() tenant: string,
  ) {
    const customer = await createCostumerStripe(tenant);
    if (!customer) {
      throw new GecoError('Impossible de créer un client stripe');
    }

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'stripe',
      idTarget: '',
      message: ' stripe inscription costumer ',
      private: true,
    };

    // maj du tenant, on rajoute l'id généré par stripe
    const updateTenant = await this.tenantService.createIdStripeCostumer(
      tenant,
      customer.id,
    );

    const response = this.expressHelper.buildSuccess(res, customer, log);

    console.log(response);

    return response;
  }

  @Post('stripeWebhook')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async stripeWebHook(
    @Req() request: Request,
    @Res() res: Response,
    @Body() body: any,
  ) {
    const webHookReq = await webHook(request, res);
    if (!webHookReq) {
      throw new GecoError('Impossible de créer un webHook stripe ');
    }

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'stripe',
      idTarget: '',
      message: ' stripe webHook',
      private: true,
    };

    return webHookReq;
  }

  @Post('retryInvoice')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async retryInvoice(
    @Req() request: Request,
    @Res() res: Response,
    @Body() body: IpaymentStripe,
  ) {
    const retryInvoiceReq = await retryInvoice(body, res);
    if (!retryInvoiceReq) {
      throw new GecoError('Impossible de créer un payment stripe ');
    }

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'stripe',
      idTarget: '',
      message: ' stripe subscription payment',
      private: true,
    };

    return retryInvoiceReq;
  }

  @Post('createSubscription')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async createSubscription(
    @Req() request: Request,
    @Res() res: Response,
    @Body() body: IpaymentStripe,
  ) {
    const subscription = await createSubscriptionStripe(body, res);
    if (!subscription) {
      throw new GecoError('Impossible de créer une subscription stripe ');
    }
    

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'stripe',
      idTarget: '',
      message: ' stripe subscription',
      private: true,
    };

    const response = this.expressHelper.buildSuccess(
      res,
      subscription,
      log,
    );

    return response;
  }

  // si la subscription à stripe s'est bien déroulé on notifie ici que le tenant a bien demandé un abonnement
  @Post('notifyPay')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async notifyPay(
    @Req() request: Request,
    @Res() res: Response,
    @Body() body: any,
  ) {
    // maj du tenant, on rajoute l'id généré par stripe
    let tenantToUpdate = await this.tenantService.findOne(body.tenant);
    tenantToUpdate.paymentData = body.data;
    tenantToUpdate.priceProduct = body.priceProduct;
    tenantToUpdate.typeCompte = body.typeCompte;
    tenantToUpdate.dateContratSigned = new Date();

  
    const updateTenant = await this.tenantService.udpate(
      body.tenant,
      tenantToUpdate,
    );

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'stripe',
      idTarget: '',
      message: ' stripe payment ok tenant ' + body.tenant,
      private: true,
    };

    // on envoie une notif mail aussi

    await this.mailService.sendMailSuccessStripe({
      email: tenantToUpdate.email,
      name: tenantToUpdate.tenant,
      typeAbonnement: tenantToUpdate.typeCompte,
    });

    const response = this.expressHelper.buildSuccess(
      res,
      updateTenant,
      log,
    );

    return response;
  }

  @Get('deleteSubscription')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async deleteSubscription(
    @Req() request: Request,
    @Res() res: Response,
  ) {
    

    const findTenant = await this.tenantService.findOne( getTenantFromToken(this.jwtService, request));
    const subscriptionId = findTenant.paymentData.id;
    console.log("subscriptionId = " , subscriptionId)
    const subscription = await deletedSubscription(subscriptionId);

    if (!subscription) {
      throw new GecoError('deleted subscription stripe ');
    }


    // maj du profil tenant
    const updateTenant = await this.tenantService.resiliation(getTenantFromToken(this.jwtService, request));

    if (!updateTenant) {
      throw new GecoError('error resiliation tenant');
    }

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'stripe',
      idTarget: '',
      message: ' GECOF désinscription Ok',
      private: true,
    };
    const response = this.expressHelper.buildSuccess(res, updateTenant, log);

    return response;
  }

  @Post('updateSubscription')
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async updateSubscription(
    @Req() request: Request,
    @Res() res: Response,
    @Body() body: IpaymentStripe,
  ) {
    const subscription = await updateSubscription(body, res);
    if (!subscription) {
      throw new GecoError('update subscription stripe ');
    }

    const log = {
      userId: 'superadmin',
      timestamp: new Date(),
      type: 'stripe',
      idTarget: '',
      message: ' stripe subscription update payment',
      private: true,
    };

    return subscription;
  }
}
