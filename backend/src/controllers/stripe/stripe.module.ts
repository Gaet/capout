import { Module } from '@nestjs/common';
import { StripeController } from './stripe.controller';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { TenantDbModule } from '../../shared/database/tenant/tenant.db.module';
import { MailModule } from '../../controllers/mail/mail.module';

@Module({
    controllers: [StripeController],
    imports: [ ServicesModule, TenantDbModule,AuthenticationModule,MailModule],

})
export class StripeModule {}
