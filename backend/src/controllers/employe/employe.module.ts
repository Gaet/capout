import { Module } from '@nestjs/common';
import { EmployeDbModule } from '../../shared/database/employe/employe.db.module';
import { EmployeController } from './employe.controller';

@Module({
  controllers: [EmployeController],
  imports: [EmployeDbModule],
})
export class EmployeModule {}
