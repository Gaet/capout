import { Put, Delete, Body, Get, Post, Param, Controller } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { EmployeDbService, IEmployeStructure } from '../../shared/database/employe/employe.db.interface';
import { ApiImplicitBody } from '@nestjs/swagger';
// todo

@Controller('employe')
export class EmployeController {

    // TODO See if we use a dedicated service
    constructor(private readonly employeService: EmployeDbService) {}

    @Get()
    async findAll(): Promise<IEmployeStructure> {
        return this.employeService.findAll();
    }

    @Get(':id')
    async findOne(@Param('id') id: string): Promise<IEmployeStructure> {
        return this.employeService.findOne(id);
    }

    /**
     * TODO We can have a dedicated type for createBody
     * @See @ApiModelProperty to document this DTO on swagger side
     * This DTO could be shared with frontend part.
     */
    @Post()
    @ApiResponse({ status: 201, description: 'The record has been successfully created.'})
    @ApiResponse({ status: 403, description: 'Forbidden.'})
    async create(@Body() newEmploye: IEmployeStructure) {
        return this.employeService.create(newEmploye);
    }

    @Put(':id')
    update(@Param('id') id: string, @Body() newEmploye) {
        return `On met à jour le employe ${id}`;
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return `On supprime le employe ${id}`;
    }

}
