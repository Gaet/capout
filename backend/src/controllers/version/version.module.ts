import { Module } from '@nestjs/common';
import { VersionController } from './version.controller';
import { VersionDbModule } from '../../shared/database/version/version.db.module';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';

@Module({
    controllers: [VersionController],
    imports: [VersionDbModule, UserDbModule, ServicesModule, AuthenticationModule],
})
export class VersionModule {}
