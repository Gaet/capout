import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Controller,
  Req,
  Res,
  UseGuards,
  Request,
  Query,
} from '@nestjs/common';
import { Response, NextFunction } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import {
IVersionStructure,VersionDbService
} from '../../shared/database/version/version.db.interface';
import { ApiImplicitBody } from '@nestjs/swagger';
import { AppLoggerService} from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { getTenantFromToken } from '../../shared/services/contextTenant';
import { JwtService } from '@nestjs/jwt';

@Controller('version')
export class VersionController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly versionService: VersionDbService,
    private readonly appLoggerService: AppLoggerService,
    private readonly jwtService: JwtService,

  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLoggerService);

  @Get()
  async findAll(@Query() filters: any): Promise<IVersionStructure[]> {
    return this.versionService.findAll(filters);
  }


  @Get(':id')
  async findOne(@Param('id') id: string): Promise<IVersionStructure> {
    return this.versionService.findOne(id);
  }

  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newversion: IVersionStructure,
  ) {

      const version = await this.versionService.create(newversion);
      if (!version) {
        throw new GecoError("Impossible de créer ligne version");
      }

        const versionLog = {
          userId: 'superadmin',
          timestamp: new Date(),
          type: 'version',
          idTarget:version.version,
          message: 'nouvelle ligne version créé : ' + newversion.version,
          private:true,
        };
        

        const response = this.expressHelper.buildSuccess(res, newversion, versionLog);

        return response;
      
  }



}
