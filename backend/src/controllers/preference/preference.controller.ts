import {
    Put,
    Body,
    Get,
    Post,
    Param,
    Controller,
    Req,
    Request,
    Res,
    UseGuards,
  } from '@nestjs/common';
  import {  Response } from 'express';
  
  import { ApiResponse } from '@nestjs/swagger';
  import {
    PreferenceDbService,
    IPreferenceStructure,
  } from '../../shared/database/preference/preference.db.interface';
  import { AppLoggerService } from '../../shared/services/app-logger.service';
  import { GecoError } from '../../shared/services/error.service';
  import { ExpressHelper } from '../../shared/helpers/expressHelper';
  import { getTenantFromToken } from '../../shared/services/contextTenant';
  // import { AuthGuard } from '@nestjs/passport';
  import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
  import { JwtService } from '@nestjs/jwt/dist/jwt.service';
  
  @Controller('preference')
   @UseGuards(JwtAuthGuard)
  export class PreferenceController {
    // TODO See if we use a dedicated service
    constructor(
      private readonly preferenceService: PreferenceDbService,
      private readonly appLogger: AppLoggerService,
      private readonly jwtService: JwtService,

    ) {}
  
    expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);
  
    @Get()
    async findAll(@Req() request: Request): Promise<IPreferenceStructure> {
      return (await this.preferenceService.findAll(getTenantFromToken(this.jwtService,request)))[0];
    }
  
  
    /**
     * TODO We can have a dedicated type for createBody
     * @See @ApiModelProperty to document this DTO on swagger side
     * This DTO could be shared with frontend part.
     */
    @Post()
    @ApiResponse({
      status: 201,
      description: 'The record has been successfully created.',
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async create(
      @Req() request: Request,
      @Res() res: Response,
      @Body() newPreference: IPreferenceStructure,
    ) {

        const preference = await this.preferenceService.create(getTenantFromToken(this.jwtService,request),newPreference);
        if (!preference) {
          throw new GecoError("Impossible de créer la préférence");
        }
  
          const log = {
           userId: getTenantFromToken(this.jwtService,request),
            timestamp: new Date(),
            type: 'preference',
            idTarget:preference._id,
            message: 'création préférence',
            private:false,
          };
  
          const response = this.expressHelper.buildSuccess(res, preference, log);
  
          return response;
        
    }
  
    @Put(':_id')
    async update(@Param('_id') _id: string, @Body() newPreference:IPreferenceStructure,@Req() request: Request,
    @Res() res: Response,) {
   
      const log = {
       userId: getTenantFromToken(this.jwtService,request),
        timestamp: new Date(),
        type: 'preference',
        idTarget:_id,
        message: 'préférence mis à jour',
        private:false,
      };
  
      const preference = await this.preferenceService.udpate(getTenantFromToken(this.jwtService,request),_id, newPreference);
      if (!preference) {
        throw new GecoError("Impossible de mettre à jour la préférence "+_id);
      }
    
      const response =  this.expressHelper.buildSuccess(res, preference, log);
      //console.log(response)
  
      return response;
  
    }
  
  }
  