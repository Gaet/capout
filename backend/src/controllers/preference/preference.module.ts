import { Module } from '@nestjs/common';
import { PreferenceDbModule } from '../../shared/database/preference/preference.db.module';
import { PreferenceController } from './preference.controller';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';

@Module({
  controllers: [PreferenceController],
  imports: [PreferenceDbModule, UserDbModule, ServicesModule, AuthenticationModule],
})
export class PreferenceModule {}
