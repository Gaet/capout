import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Controller,
  Req,
  Res,
  UseGuards,
  Request,
  Query,
} from '@nestjs/common';
import { Response, NextFunction } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import {
  AppLogDbService,
  IAppLogStructure,
} from '../../shared/database/app-log/app-log.db.interface';
import { ApiImplicitBody } from '@nestjs/swagger';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport/dist/auth.guard';
import { getTenantFromToken } from '../../shared/services/contextTenant';
import { JwtService } from '@nestjs/jwt';

 @UseGuards(JwtAuthGuard)
@Controller('applog')
export class AppLogController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly appLogService: AppLogDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,

  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  @Get('logs')
  async findAll(@Query() filters: any): Promise<IAppLogStructure[]> {
    return this.appLogService.findAll(filters);
  }

  @Get('journalUser')
  async findAllPrivate(@Req() request: Request): Promise<IAppLogStructure[]> {
    return this.appLogService.findAllPrivate(getTenantFromToken(this.jwtService,request));
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<IAppLogStructure> {
    return this.appLogService.findOne(id);
  }

  /**
   * TODO We can have a dedicated type for createBody
   * @See @ApiModelProperty to document this DTO on swagger side
   * This DTO could be shared with frontend part.
   */
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Req() request: Request,
    @Res() res: Response,
    @Body() newAppLog: IAppLogStructure,
  ) {
 

      const appLog = await this.appLogService.create(newAppLog);
      if (!appLog) {
        throw new GecoError("Impossible de créer ligne log");
      }

        const log = {
          userId: 'superadmin',
          timestamp: new Date(),
          type: 'appLog',
          idTarget:appLog.idTarget,
          message: 'nouvelle ligne log créé : ' + newAppLog.idTarget,
          private:true,
        };

        const response = this.expressHelper.buildSuccess(res, newAppLog, log);

        return response;
      
  }



}
