import { Module } from '@nestjs/common';
import { AppLogController } from './applog.controller';
import { AppLogDbModule } from '../../shared/database/app-log/app-log.db.module';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';

@Module({
    controllers: [AppLogController],
    imports: [AppLogDbModule, UserDbModule, ServicesModule, AuthenticationModule],
})
export class AppLogModule {}
