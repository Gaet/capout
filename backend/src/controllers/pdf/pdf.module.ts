import { Module } from '@nestjs/common';
import { PdfController } from './pdf.controller';
import { PdfService } from './pdf.service';
import { FactureDbModule } from '../../shared/database/facture/facture.db.module';
import { DevisDbModule } from '../../shared/database/devis/devis.db.module';
import { CodeTenantDbModule } from '../../shared/database/codeTenant/codeTenant.db.module';
import { PreferenceDbModule } from '../../shared/database/preference/preference.db.module';
import { ClientDbModule } from '../../shared/database/client/client.db.module';
import { JwtStrategy } from '../../shared/auth/jwt.strategy';
import { AuthenticationModule } from '../../shared/auth/auth.module';

@Module({
  imports: [AuthenticationModule,ClientDbModule,CodeTenantDbModule, PreferenceDbModule],
  controllers: [PdfController],
  providers: [JwtStrategy,PdfService,ClientDbModule,CodeTenantDbModule,PreferenceDbModule],
  exports: [JwtStrategy],
})
export class PdfModule {}
