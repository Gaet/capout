import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { PdfService } from './pdf.service';
import { CodeTenantDbService } from '../../shared/database/codeTenant/codeTenant.db.interface';
import { PreferenceDbService } from '../../shared/database/preference/preference.db.interface';
import { DevisDbService } from '../../shared/database/devis/devis.db.interface';
import { FactureDbService } from '../../shared/database/facture/facture.db.interface';
import { ClientDbService } from '../../shared/database/client/client.db.interface';
import { JwtService } from '@nestjs/jwt';
import { TenantDbService } from '../../shared/database/tenant/tenant.db.interface';

describe('PdfService', () => {
    codeTenantService: CodeTenantDbService;
    preferenceService: PreferenceDbService;
    devisService: DevisDbService;
    factureService: FactureDbService;
    clientService: ClientDbService;
    jwtService:JwtService;
    tenantService: TenantDbService;


  describe('generate pdf from html content', () => {

    it('should generate a simple pdf', () => {
      const pdfService = new PdfService(this.codeTenantService,
        this.preferenceService,
        this.devisService,
        this.factureService,
        this.clientService,
        this.jwtService,
        this.tenantService);
      pdfService.generate(
        './test/ressources/report-example.html', {},
        './test/ressources/report-example.pdf', 'test',
      );
    });

    it('should generate a pdf facture', () => {
      const pdfService = new PdfService(this.codeTenantService,
        this.preferenceService,
        this.devisService,
        this.factureService,
        this.clientService,
        this.jwtService,
        this.tenantService);
      pdfService.generate(
        './test/ressources/factures/facture_bootstrap/index.html',  {
          facture: {
            numero: 'FS_00001',
            totalHT: 500 * 2,
            totalTTC: 1200,
            totalTVA: 200,
          },
          payeur: {
            nom: 'M. le Payeur',
            rue: '3 rue des champs',
            codePostal: '31000',
            ville: 'Toulouse',
          },
          catalogue: [
            {
              intitule: 'Mon article 1',
              prixUnitaireHT: 500,
              quantite: 2,
              total: 500 * 2,
            },
          ],
        },
        './test/ressources/factures/facture_bootstrap/index.pdf','test'
      );
    });
  });

});