import { IPdfStructure } from '../../../../common/dto/pdf.dto';

export abstract class PdfDbService {
    abstract traitementContrat(tenant:string,contrat: IPdfStructure);

}
