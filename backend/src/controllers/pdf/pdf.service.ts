import { Injectable } from '@nestjs/common';
const fs = require('fs');
const $ = require('jsrender');
const path = require('path');
const puppeteer = require('puppeteer');
import * as handlebars from 'handlebars';
import { IPdfStructure } from '../../../../common/dto/pdf.dto';
import { TYPE_CONTRAT_DEVIS, DEVIS_ETAT_SIGNE, DEVIS_PROVISOIRE } from '../../../../common/dto/devis.dto';
import {
  TYPE_FACTURE_FD,
  TYPE_FACTURE_FS,
  TYPE_FACTURE_ACC,
  TYPE_FACTURE_AV,
  PAYMENT_TYPE_HT,
} from '../../../../common/dto/facture.dto';
import { getTenantFromToken } from '../../shared/services/contextTenant';
import {
  formatDateFull,
  formatDateAnneMoisJour,
  formatDateJourMoisAnneeTiret,
} from '../../shared/helpers/dateHelper';
import { gecoLogo } from '../../templates/geco-logo';
import { CodeTenantDbService } from '../../shared/database/codeTenant/codeTenant.db.interface';
import { PreferenceDbService } from '../../shared/database/preference/preference.db.interface';
import { DevisDbService } from '../../shared/database/devis/devis.db.interface';
import { FactureDbService } from '../../shared/database/facture/facture.db.interface';
import { ClientDbService } from '../../shared/database/client/client.db.interface';
import { JwtService } from '@nestjs/jwt';
import { TenantDbService } from '../../shared/database/tenant/tenant.db.interface';
import { PdfDbService } from './pdf.interface';
import { TYPE_COMPTE_BUSINESS } from '../../../../common/dto/tenant.dto';
import {
  isAfterDate,
  addDaysToDate,
  isSameYear,
} from '../../shared/helpers/dateHelper';
import {
  ICatalogueStructure,
  initCatalogue,
  SOUS_TOTAL,
} from '../../../../common/dto/catalogue.dto';

handlebars.registerHelper('isOdd', function (v1, options) {
  if (v1 % 2 === 0) {
    return options.fn(this);
  }
  return options.inverse(this);
});
// let index = 0;
// handlebars.registerHelper('isToBigg', function(indexLigne, options) {
//   index ++ ;
//   console.log(index)
//   if( index === 10 || index === 30 || index === 40 || index === 50) {
//     return options.fn(this);
//   }
//   return options.inverse(this);
// });

handlebars.registerHelper('isDevis', function (type, options) {
  if (type === 'devis') {
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isFacture', function (type, options) {
  if (type === 'facture') {
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isRib', function (rib, options) {
  if (rib) {
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isRemise', function (remise, options) {
  if (remise) {
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isNotMultipleTVA', function (multipleTVA, options) {
  if (multipleTVA.length == 0 || multipleTVA.length == 1) {
    console.log('isNotMultipleTVA');
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isMultipleTVA', function (multipleTVA, options) {
  if (multipleTVA.length > 1) {
    console.log('isMultipleTVA');
    return options.fn(this);
  }
  return options.inverse(this);
});

handlebars.registerHelper('isExonereTvaOrAsso', function (
  exonereTvaOrAsso,
  options,
) {
  if (exonereTvaOrAsso) {
    console.log('exonereTvaOrAsso');
    return options.fn(this);
  }
  return options.inverse(this);
});

handlebars.registerHelper('isNotExonereTvaOrAsso', function (
  exonereTvaOrAsso,
  options,
) {
  if (!exonereTvaOrAsso) {
    console.log('isNotExonereTvaOrAsso');
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('exonereTva', function (
  exonereTva,
  options,
) {
  if (exonereTva) {
    console.log('exonereTva');
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isAssociationExonereTva', function (
  associationExonereTva,
  options,
) {
  if (associationExonereTva) {
    console.log('associationExonereTva');
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isNotAssociationExonereTva', function (
  associationExonereTva,
  options,
) {
  if (!associationExonereTva) {
    console.log('associationExonereTva');
    return options.fn(this);
  }
  return options.inverse(this);
});

handlebars.registerHelper('isExonereTva', function (
  exonereTva,
  options,
) {
  if (exonereTva) {
    console.log('exonereTva');
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isNotExonereTva', function (
  exonereTva,
  options,
) {
  if (!exonereTva) {
    console.log('exonereTva');
    return options.fn(this);
  }
  return options.inverse(this);
});

handlebars.registerHelper('IsBusinessCompte', function (typeContrat, options) {
  if (typeContrat == TYPE_COMPTE_BUSINESS) {
    console.log('compte business');
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isSousTotal', function (intitule, options) {
  if (intitule == SOUS_TOTAL) {
    return options.fn(this);
  }
  return options.inverse(this);
});

handlebars.registerHelper('isNotPaymentTypeHT', function (paymentType, options) {
  if (paymentType != PAYMENT_TYPE_HT) {
    return options.fn(this);
  }
  return options.inverse(this);
});

handlebars.registerHelper('isDevisProvisoire', function (test, options) {
  if (test) {
    return options.fn(this);
  }
  return options.inverse(this);
});

handlebars.registerHelper('isNotDevisProvisoire', function (test, options) {
  if (!test) {
    return options.fn(this);
  }
  return options.inverse(this);
});


handlebars.registerHelper('isBonDeCommande', function (test, options) {
  if (test) {
    return options.fn(this);
  }
  return options.inverse(this);
});

handlebars.registerHelper('isNotSousTotal', function (intitule, options) {
  if (intitule != SOUS_TOTAL) {
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('math', function (lvalue, operator, rvalue, options) {
  lvalue = parseFloat(lvalue);
  rvalue = parseFloat(rvalue);

  return {
    '+': lvalue + rvalue,
    '-': lvalue - rvalue,
    '*': lvalue * rvalue,
    '/': lvalue / rvalue,
    '%': lvalue % rvalue,
  }[operator];
});

handlebars.registerHelper('isNotSignature', function (signature, options) {
  if (signature == "") {
    return options.fn(this);
  }
  return options.inverse(this);
});
handlebars.registerHelper('isSignature', function (signature, options) {
  if (signature != "") {
    return options.fn(this);
  }
  return options.inverse(this);
});
/**
 * This class allows to generate a pdf from an html file.
 */
@Injectable()
export class PdfService implements PdfDbService {
  /**
   * Generates a pdf from htmlFile with data injected
   * @param htmlFile
   * @param data
   */
  constructor(
    private readonly codeTenantService: CodeTenantDbService,
    private readonly preferenceService: PreferenceDbService,
    private readonly devisService: DevisDbService,
    private readonly factureService: FactureDbService,
    private readonly clientService: ClientDbService,
    private readonly jwtService: JwtService,
    private readonly tenantService: TenantDbService,
  ) {}

  async traitementContrat(tenant: string, contrat: any) {
    // les infos du tenant
    const tenantProperties = await this.tenantService.findOne(tenant);
    console.log(tenantProperties);

    // données liées au contrat
    let data: any;
    let preferences: any;
    let codesTenant: any;
    let contratType: string;
    //let client: any;

    if (contrat.type == TYPE_CONTRAT_DEVIS) {
      contratType = 'devis';
      data = await this.devisService.findOne(tenant, contrat._id);
      data._doc['devisProvisoire'] = true;

      if(data._doc['etat'] == DEVIS_ETAT_SIGNE) {
          data._doc['devisProvisoire'] = false;
      }


    } else if (
      contrat.type == TYPE_FACTURE_FD ||
      contrat.type == TYPE_FACTURE_FS
    ) {
      contratType = 'facture';
      data = await this.factureService.findOne(tenant, contrat._id);
    } else if (contrat.type == TYPE_FACTURE_ACC) {
      contratType = 'accompte';
      data = await this.factureService.findOne(tenant, contrat._id);
    } else if (contrat.type == TYPE_FACTURE_AV) {
      contratType = 'avoir';
      data = await this.factureService.findOne(tenant, contrat._id);
    }

    // données liées aux préférence du tenant
    preferences = await this.preferenceService.findOne(tenant);
    codesTenant = await this.codeTenantService.findAll(tenant);

    //client =  await this.clientService.findOne(data._doc.idClient);


    let periodeValidite = '';
    let prefixContrat = '';

    codesTenant.forEach((code) => {
      code.list.forEach((element, i) => {
        if (element.name) {
          if (
            (element.name == 'periodeValiditeFacture' &&
              contrat.type == TYPE_FACTURE_FD) ||
            contrat.type == TYPE_FACTURE_FS ||
            contrat.type == TYPE_FACTURE_ACC
          ) {
            periodeValidite = element.value;
          }
          if (
            element.name == 'periodeValiditeDevis' &&
            contrat.type == TYPE_CONTRAT_DEVIS
          ) {
            periodeValidite = element.value;
          }
        
          if (
            contrat.type == TYPE_CONTRAT_DEVIS &&
            element.name == 'devisPrefix'
          ) {
            prefixContrat = element.value;
          }
          if (
            (contrat.type == TYPE_FACTURE_FS ||
              contrat.type == TYPE_FACTURE_FD) &&
            element.name == 'facturePrefix'
          ) {
            prefixContrat = element.value;
          }
          if (
            contrat.type == TYPE_FACTURE_ACC &&
            element.name == 'factureAccomptePrefix'
          ) {
            prefixContrat = element.value;
          }
          if (
            contrat.type == TYPE_FACTURE_AV &&
            element.name == 'factureAvoirPrefix'
          ) {
            prefixContrat = element.value;
          }
         
        }
      });
    });

    // en commun devis et facture
    contrat['idContrat'] = data._doc.idContrat;


    // catalogue sous totaux
    let totalLigne = 0;
    data._doc['blocksCatalogue'].forEach((blockCatalogue, i: number) => {
      if (blockCatalogue.titre && blockCatalogue.titre != '') {
        // dès qu'il y a un sous titre il faut calculer le sous total du block
        data._doc['blocksCatalogue'][i].block.forEach((block) => {
          totalLigne += block.totalLigne;
        });
        const block: any = {
          intitule: SOUS_TOTAL,
          totalLigne: totalLigne,
        };
        data._doc['blocksCatalogue'][i].block.push(block);
        totalLigne = 0;
      }
    });

    // a qui s'adresse la facture ?
    // : le client si pas de payeur, le payeur si un payeur

    if (data._doc.payeur && data._doc.payeur != '') {
      contrat['nomClient'] = data._doc.payeur;
      contrat['villeClient'] = data._doc.payeurVille;
      contrat['codePostalClient'] = data._doc.payeurCodePostal;
      contrat['adresseClient'] = data._doc.payeurAdresse;
    } else {
      contrat['nomClient'] = data._doc.client;
      contrat['villeClient'] = data._doc.ville;
      contrat['codePostalClient'] = data._doc.codePostal;
      contrat['adresseClient'] = data._doc.adresse;
    }

    // pediodeValidité
    data._doc['periodeValidite'] = periodeValidite;

    // dates
    data._doc['anneMoisJourContrat'] = formatDateAnneMoisJour(
      data.dateCreation,
    );
    data._doc['dateContrat'] =  formatDateJourMoisAnneeTiret(
      data.dateCreation,
    );

    //  docName ou nomContrat = prefix_idContrat_anneMoisContrat
    contrat['docName'] =  prefixContrat +
    '_' +
    data._doc.idContrat +
    '_' +
    data._doc['anneMoisJourContrat'];
    contrat['nomContrat'] = contrat['docName'];
    contrat['typeContrat'] = contratType;

    //tel
    if (preferences.telFixe && preferences.telFixe != '') {
      data._doc['tel'] = preferences.tel + ' - ' + preferences.telFixe;
    } else {
      data._doc['tel'] = preferences.tel;
    }

    data._doc['multipleTVA'] = [];
    // TVA
    // si il y a plusieurs totalTVA
    if (data.totalTVA || data.totalTVA.length > 1) {
      data._doc['tauxTVA'].forEach((taux, t) => {
        data._doc['multipleTVA'].push({
          tauxTVA: taux,
          totalTVA: data._doc['totalTVA'][t],
        });
      });
    } else {
      data._doc['totalTVA'] = data._doc['totalTVA'][0];
      data._doc['tauxTVA'] = data._doc['tauxTVA'][0];
    }
    console.log('tva => ', data._doc['multipleTVA']);

    // spécificité facture

    if (
      contrat.type == TYPE_FACTURE_FD ||
      contrat.type == TYPE_FACTURE_FS ||
      contrat.type == TYPE_FACTURE_ACC
    ) {
      // reste à payer: total - versements
      let versements = 0;
      data.versements.forEach((versement) => {
        // attention on ne met dans le doc que les versements qui ont été effectués avant la cloture de la facture
        if (
          isAfterDate(
            new Date(data._doc['dateValideDefinitivement']),
            new Date(versement.dateCreation),
          )
        ) {
          versements = versements + versement.montant;
        } else {
          console.log('versement non pris en compte cause date');
        }
      });
      data._doc['dejaRegle'] = versements;
      // suivant si le client doit payer le ht ou ttc :
      if(data["paymentType"] == PAYMENT_TYPE_HT){
        data._doc['resteApayer'] = data.totalHT - versements;
      } else {
        data._doc['resteApayer'] = data.totalTTC - versements;
      }

      // remise en % : on en déduit le HT et le TTC
      let remise = data._doc['remise'];
      if (remise && remise > 0) {
        const resteApayer =
          data._doc['resteApayer'] - (remise / 100) * data._doc['resteApayer'];
        data._doc['resteApayer'] = Math.round(resteApayer * 100) / 100;
        const totalHTApresRemise =
          data._doc['totalHT'] - (remise / 100) * data._doc['totalHT'];
        data._doc['totalHTApresRemise'] =
          Math.round(totalHTApresRemise * 100) / 100;
        const ttc =
          data._doc['totalTTC'] - (remise / 100) * data._doc['totalTTC'];
        data._doc['totalTTC'] = Math.round(ttc * 100) / 100;
      }
    }

    data._doc['logoGeco'] = gecoLogo;

    // ce qui concerne la rcpro ou le rcs il vaut mieux le mettre sous le tenant en haut à gauche du document
    preferences._doc['optionsCommunHaut'] = [];

    preferences._doc['optionsDevisBas'] = [];
    preferences._doc['optionsFactureBas'] = [];
    
    const rcpro = preferences._doc['optionsCommun'].filter(option => option.typeOption == 'rcpro')[0];
    const rcs = preferences._doc['optionsCommun'].filter(option => option.typeOption == 'rcs')[0];

    preferences._doc['optionsDevisBas'] = preferences._doc['optionsDevis'].filter(option => option.typeOption != 'rcs' && option.typeOption != 'rcpro');
    preferences._doc['optionsFactureBas'] = preferences._doc['optionsFacture'].filter(option => option.typeOption != 'rcs' && option.typeOption != 'rcpro');

    if(rcpro) {
      preferences._doc['optionsCommunHaut'].push(rcpro);
    }

    if(rcs) {
      preferences._doc['optionsCommunHaut'].push(rcs);
    }

    return (contrat = { ...contrat,  ...data._doc, ...preferences._doc });
  }

  async generate(
    htmlFile: string,
    data: any,
    docName: string,
    typeDoc: string,
    pdfPath: string,
  ) {
    let templateHtml = fs.readFileSync(
      path.join(process.cwd(), htmlFile),
      'utf8',
    );
    let template = handlebars.compile(templateHtml);
    let templateFooterHtml = fs.readFileSync(
      path.join(process.cwd(), 'src/templates/template-footer.html'),
      'utf8',
    );
    let footer = handlebars.compile(templateFooterHtml);
    handlebars.registerPartial('footer', footer);

    let html = template(data, {
      allowedProtoMethods: {
        // lignesCatalogue: true
      },
    });

    let footerHtml = footer(data, {
      allowedProtoMethods: {
        // lignesCatalogue: true
      },
    });

    let milis: Date = new Date();
    let milisN: number = milis.getTime();

    pdfPath = path.join(pdfPath, `${docName}`);

    let options = {};
    if (typeDoc == 'contratGeco') {
      options = {
        format: 'A4',
        margin: {
          top: 50,
          bottom: 125,
        },
        printBackground: true,
        path: pdfPath,
      };
    } else if (typeDoc == 'devisFacture') {
      options = {
        format: 'A4',
        headerTemplate: '<p></p>',
        footerTemplate: footerHtml,
        displayHeaderFooter: true,
        margin: {
          top: 50,
          bottom: 125,
        },
        printBackground: true,
        path: pdfPath,
      };
    }
    const browser = await puppeteer.launch({
      args: ['--no-sandbox', '--no-zygote'],
      headless: true,
    });

    let page = await browser.newPage();

    await page.setContent(`${html}`, {
      waitUntil: 'networkidle0',
    });

    let pdf;
    // si c'est une demande d'envoie de mail en encode le pdf en base64
    if (data.sendToMail) {
      //pdf = await page.screenshot({ encoding: "base64" })
      pdf = await page.pdf(options);
      let buff = new Buffer(pdf);
      pdf = buff.toString('base64');
    }
    // sinon pdf classique
    else {
      pdf = await page.pdf(options);
    }
    await page.close();
    await browser.close();
    return pdf;
  }

  /**
   * Generates a pdf from htmlFile with data injected
   * @param htmlFile
   * @param data
   */

  async apercu(htmlFile: string, data: any) {
    let templateHtml = fs.readFileSync(
      path.join(process.cwd(), htmlFile),
      'utf8',
    );
    let template = handlebars.compile(templateHtml);
    let templateFooterHtml = fs.readFileSync(
      path.join(process.cwd(), 'src/templates/template-footer.html'),
      'utf8',
    );
    var footer = handlebars.compile(templateFooterHtml);
    handlebars.registerPartial('footer', footer);

    let html = template(data, {
      allowedProtoMethods: {
        // lignesCatalogue: true
      },
    });

    let options = {
      format: 'A4',
      headerTemplate: '<p></p>',
      footerTemplate: '<p></p>',
      displayHeaderFooter: false,
      margin: {
        top: '10px',
        bottom: '30px',
      },
      printBackground: true,
    };
    const browser = await puppeteer.launch({
      args: ['--no-sandbox', '--no-zygote'],
      headless: true,
    });

    let page = await browser.newPage();

    await page.setContent(`${html}`, {
      waitUntil: 'networkidle0',
    });

    const htmlToReturn = await page.content({ options });
    await page.close();
    await browser.close();

    return htmlToReturn;
  }
}
