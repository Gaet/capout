import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Request,
  Controller,
  UseGuards,
  Header,
  Res,
  Query,
  Req,
} from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { PdfService } from './pdf.service';
import { DevisDbService } from '../../shared/database/devis/devis.db.interface';
import { CodeTenantDbService } from '../../shared/database/codeTenant/codeTenant.db.interface';
import { FactureDbService } from '../../shared/database/facture/facture.db.interface';
import { PreferenceDbService } from '../../shared/database/preference/preference.db.interface';

import {
  formatDateFull,
  formatDateAnneMois,
  formatDateAnneMoisJour,
  gecGetMonth,
  gecGetYear,
} from '../../shared/helpers/dateHelper';
import { gecoLogo } from 'templates/geco-logo';
import { ClientDbService } from '../../shared/database/client/client.db.interface';
import {
  TYPE_CONTRAT_DEVIS,
  DEVIS_ETAT_SIGNE,
} from '../../../../common/dto/devis.dto';
import {
  TYPE_FACTURE_FD,
  TYPE_FACTURE_FS,
  TYPE_FACTURE_ACC,
  TYPE_FACTURE_AV,
} from '../../../../common/dto/facture.dto';
import { IPdfStructure } from '../../../../common/dto/pdf.dto';
import { getTenantFromToken } from '../../shared/services/contextTenant';
import { JwtService } from '@nestjs/jwt';
import { TenantDbService } from '../../shared/database/tenant/tenant.db.interface';
import {
  TYPE_COMPTE_BUSINESS,
  PRICE_BUSINESS,
  TYPE_COMPTE_BASE,
  PRICE_BASE,
  TYPE_COMPTE_GRATUIT,
  PRICE_GRATUIT,
} from '../../../../common/dto/tenant.dto';
import { GecoError } from '../../shared/services/error.service';
import { User } from './../../shared/database/user/mongo/user.schema';
import { apiCheckConditionsToChangeAbonnementHelper } from '../../shared/helpers/checkConditionsHelper';
import { serverConfig } from '../../shared/helpers/serverConfig';
import * as jsonGecoContratConf from '../../templates/geco-contrat-conf.json';
const path = require('path');

// test facture :local gaet {"id":"5ea01f948b722b2349623282","type":"facture","nameTenant":"moimeme","nameClient":"gaet"}
// test devis :local gaet {"id":"5eac03262405f51f31277bc0","type":"devis","nameTenant":"moimeme","nameClient":"gaet"}
@Controller('pdf')
export class PdfController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly pdfService: PdfService,
    private readonly codeTenantService: CodeTenantDbService,
    private readonly preferenceService: PreferenceDbService,
    private readonly devisService: DevisDbService,
    private readonly factureService: FactureDbService,
    private readonly clientService: ClientDbService,
    private readonly jwtService: JwtService,
    private readonly tenantService: TenantDbService,
  ) {}

  // genere un apercu d'un devis, facture...
  @Get()
  async get(@Query() query: any, @Req() request: Request) {
    let template = 'template-contrat.html';
    // données liées au contrat
    const contrat = {
      _id: query._id,
      idContrat: query.idContrat,
      type: query.type,
      nameTenant: query.nameTenant,
      nameClient: query.nameClient,
      docName: '',
      dateCreation: query.dateCreation,
      sendToMail: false,
    };

    let contratOut = null;

    // si parametre secret d'un superadmin en plus
    if (query && query.secret === serverConfig.secret) {
      contratOut = await this.pdfService.traitementContrat(
        query.tenant,
        contrat,
      );
    } else {
      contratOut = await this.pdfService.traitementContrat(
        getTenantFromToken(this.jwtService, request),
        contrat,
      );
    }
    if (contratOut.exonereTva || contratOut.associationExonereTva) {
      template = 'template-contrat-microEntreprise.html';
      contratOut['exonereTvaOrAsso'] = true;
    } else {
      contratOut['exonereTvaOrAsso'] = false;
    }
    return this.pdfService.apercu('src/templates/' + template, contratOut);
  }

  // genere un pdf d'un devis, facture...
  @Header('Content-Type', 'application/pdf')
  @Header('Content-Disposition', 'attachment; filename=monpdf.pdf')
  @Post()
  @ApiResponse({
    status: 201,
    description: 'The pdf has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(@Body() contrat: any, @Res() res, @Req() request: Request) {
    let template = 'template-contrat.html';
    // données liées au contrat
    let contratOut = null;
    let idTenant = '';

    // si parametre secret d'un superadmin en plus
    if (contrat.secret === serverConfig.secret) {
      idTenant = contrat.tenant;
      contratOut = await this.pdfService.traitementContrat(
        contrat.tenant,
        contrat,
      );
    } else {
      idTenant = getTenantFromToken(this.jwtService, request);

      contratOut = await this.pdfService.traitementContrat(
        getTenantFromToken(this.jwtService, request),
        contrat,
      );
    }

    const date =
      gecGetYear(new Date(contratOut.dateCreation)) +
      '-' +
      gecGetMonth(new Date(contratOut.dateCreation));

    // secure: obligatoire de déclarer ici à la racine
    let pdfPath = 'pdf/' + idTenant + '/' + date + '/';

    // si figé dans fige/
    if (
      contratOut.valideDefinitivement ||
      contratOut.etat == DEVIS_ETAT_SIGNE
    ) {
      pdfPath = pdfPath + 'fige';
    } else {
      pdfPath = pdfPath + 'encours';
    }

    // cas particulier pour les tests
    if (contrat.nameClient.includes('client_test_geco')) {
      pdfPath = 'docker/test/pdf';
    }

    let nomPdf = `${contratOut.docName}-${contrat.nameTenant}-${contrat.nameClient}-${contrat._id}.pdf`;

    nomPdf = nomPdf.replace(/\s/g, '');
    let pdfPathComplete = path.join(pdfPath, nomPdf);

    // si ce n'est pas un test

    if (!contrat.nameClient.includes('client_test_geco')) {
      // création du répertoire si nécessaire
      var fs = require('fs');
      let realDir = './../pdf';
      if (!fs.existsSync(realDir)) {
        fs.mkdirSync(realDir);
      }

      realDir = './../pdf/' + idTenant;
      if (!fs.existsSync(realDir)) {
        fs.mkdirSync(realDir);
      }
      realDir = './../pdf/' + idTenant + '/' + date;
      if (!fs.existsSync(realDir)) {
        fs.mkdirSync(realDir);
      }

      if (
        contratOut.valideDefinitivement ||
        contratOut.etat == DEVIS_ETAT_SIGNE
      ) {
        realDir = './../pdf/' + idTenant + '/' + date + '/fige';
        if (!fs.existsSync(realDir)) {
          fs.mkdirSync(realDir);
        }
      } else {
        realDir = './../pdf/' + idTenant + '/' + date + '/encours';
        if (!fs.existsSync(realDir)) {
          fs.mkdirSync(realDir);
        }
      }
    }
    // Si le contrat a deja été généré et qu'il est été validé on ne le regenere pas pour risque d'incohérence
    // on le télécharge directement
    if (
      !contrat.nameClient.includes('client_test_geco') &&
      fs.existsSync('./../' + pdfPathComplete) &&
      (contratOut.valideDefinitivement || contratOut.etat == DEVIS_ETAT_SIGNE)
    ) {
      res.header('Content-Disposition', 'attachment; filename=' + nomPdf);
      res.sendFile(pdfPathComplete, { root: './../' });
    } else {
      // sinon il faut générer
      if (contratOut.exonereTva || contratOut.associationExonereTva) {
        template = 'template-contrat-microEntreprise.html';
        contratOut['exonereTvaOrAsso'] = true;
      } else {
        contratOut['exonereTvaOrAsso'] = false;
      }
      let realPdfPath = './../' + pdfPath;
      let rootPath = './../';
      // spec tests
      if (contrat.nameClient.includes('client_test_geco')) {
        realPdfPath = pdfPath;
        rootPath = './';
      }

      const pdf = this.pdfService.generate(
        'src/templates/' + template,
        contratOut,
        nomPdf,
        'devisFacture',
        realPdfPath,
      );
      pdf.then(() => {
        res.header('Content-Disposition', 'attachment; filename=' + nomPdf);
        res.sendFile(pdfPathComplete, { root: rootPath });
      });
    }
  }

  // renvoie le contrat GECOF pour un tenant particulier au format html
  @Get('contratGecoHtml')
  async contratGecoHtml(@Query() query: any, @Req() request: Request) {
    let template = 'geco-contrat.html';
    query.sendToMail = false;
    query.date = formatDateFull(new Date());
    let tenant = await this.tenantService.findOne(query.tenant);
    let userStructured = await (
      await User.findOne({ idTenant: query.tenant }).exec()
    ).toJSON();
    let signature = userStructured.signatureGerant_b64;

    // if (!signature || signature == "") {
    //   throw new GecoError(
    //     "Veuillez d'abord insérer la signature du représentant légal de votre entreprise",
    //   );
    // }

    let factures = await this.factureService.findAll(query.tenant, {});

    const checkConditionsToChangeAbonnement = await apiCheckConditionsToChangeAbonnementHelper(
      factures,
      tenant,
      query.typeCompteSelected,
    );

    if (!checkConditionsToChangeAbonnement.conditions) {
      throw new GecoError(checkConditionsToChangeAbonnement.message);
    }
    if (query.typeContrat == TYPE_COMPTE_GRATUIT) {
      query.price = PRICE_GRATUIT;
    }
    if (query.typeContrat == TYPE_COMPTE_BASE) {
      query.price = PRICE_BASE;
    }
    if (query.typeContrat == TYPE_COMPTE_BUSINESS) {
      query.price = PRICE_BUSINESS;
    }

    return this.pdfService.apercu('src/templates/' + template, {
      price: query.price,
      tenant: tenant.tenant,
      siret: tenant.siret,
      adresse: tenant.adresse,
      codePostal: tenant.codePostal,
      ville: tenant.ville,
      nomGerant: tenant.nomGerant,
      sendToMail: query.sendToMail,
      date: query.date,
      dateCompteCreation: formatDateFull(tenant.dateCreation),
      typeContrat: query.typeContrat,
      signatureGerant_b64: signature,
      version: jsonGecoContratConf.version,
    });
    
  }

  @Header('Content-Type', 'application/pdf')
  @Header('Content-Disposition', 'attachment; filename=monpdf.pdf')
  @Get('contratGecoPdf')
  @ApiResponse({
    status: 201,
    description: 'The pdf has been successfully created.',
  })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async contratGecoPdf(@Query() query: any, @Res() res) {
    let template = 'geco-contrat.html';
    query.sendToMail = false;
    query.date = formatDateFull(new Date());
    let tenant = await this.tenantService.findOne(query.tenant);
    let userStructured = await (
      await User.findOne({ idTenant: query.tenant }).exec()
    ).toJSON();

    let signature = userStructured.signatureGerant_b64;

    // if (!signature || signature == "") {
    //   throw new GecoError(
    //     "Veuillez d'abord insérer la signature du représentant légal de votre entreprise",
    //   );
    // }

    if (query.typeContrat == TYPE_COMPTE_BUSINESS) {
      query.price = PRICE_BUSINESS;
    }
    if (query.typeContrat == TYPE_COMPTE_BASE) {
      query.price = PRICE_BASE;
    }
    if (query.typeContrat == TYPE_COMPTE_GRATUIT) {
      query.price = PRICE_GRATUIT;
    }

    // création du répertoire si nécessaire
    let fs = require('fs');

    // secure: obligatoire de déclarer ici à la racine
    let pdfPath = 'pdf/contratsGecof';

    let realDir = './../pdf';

    if (!fs.existsSync(realDir)) {
      fs.mkdirSync(realDir);
    }

    realDir = './../pdf/contratsGecof';
    if (!fs.existsSync(realDir)) {
      fs.mkdirSync(realDir);
    }

    let nomPdf = `contrat-location-gecof-${jsonGecoContratConf.version}-${tenant.tenant}-${tenant._id}.pdf`;
    nomPdf = nomPdf.replace(/\s/g, '');

    if (query.test && query.test.includes('client_test_geco')) {
      pdfPath = 'docker/test/pdf';
    }

    let pdfPathComplete = path.join(pdfPath, nomPdf);

    // Si le contrat a deja été généré et qu'il est été validé on ne le regenere pas pour risque d'incohérence
    // on le télécharge directement
    if (fs.existsSync('./../' + pdfPathComplete)) {
      res.header('Content-Disposition', 'attachment; filename=' + nomPdf);
      res.sendFile(pdfPathComplete, { root: './../' });
    } else {
      // sinon il faut générer
      console.log(tenant)
      const pdf = this.pdfService.generate(
        'src/templates/' + template,
        {
          price: query.price,
          tenant: tenant.tenant,
          siret: tenant.siret,
          adresse: tenant.adresse,
          codePostal: tenant.codePostal,
          ville: tenant.ville,
          nomGerant: tenant.nomGerant,
          sendToMail: query.sendToMail,
          date: query.date,
          dateCompteCreation: formatDateFull(tenant.dateCreation),
          typeContrat: query.typeContrat,
          signatureGerant_b64: signature,
          version: jsonGecoContratConf.version,
        },
        nomPdf,
        'contratGeco',
        './../' + pdfPath,
      );

      pdf.then(() => {
        //let pdfPath = path.join(dirSecure, nomPdf);
        res.header('Content-Disposition', 'attachment; filename=' + nomPdf);
        res.sendFile(pdfPathComplete, { root: './../' });
      });
    }
  }
}
