import {
  Put,
  Body,
  Get,
  Req,
  Request,
  Param,
  Controller,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import {
  DashboardDbService,
  IDashboardStructure,
} from '../../shared/database/dashboard/dashboard.db.interface';
import { getTenantFromToken } from '../../shared/services/contextTenant';
import { JwtService } from '@nestjs/jwt';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import { AppLoggerService } from '../../shared/services/app-logger.service';

@Controller('dashboard')
export class DashboardController {
  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  // TODO See if we use a dedicated service

  constructor(
    private readonly appLogger: AppLoggerService,
    private readonly dashboardService: DashboardDbService,
    private readonly jwtService: JwtService,
  ) {}

  @Get()
  async findAll(@Req() request: Request): Promise<IDashboardStructure> {
    return this.dashboardService.find(
      getTenantFromToken(this.jwtService, request),
    );
  }

  @Get('notes')
  async findNotes(@Req() request: Request): Promise<any> {
    const notes =  await this.dashboardService.findNote(
      getTenantFromToken(this.jwtService, request),
    );
    return notes;
  }

  @Put('notes')
  async updateNotes(
    @Body() data,
    @Req() request: Request,
    @Res() res: Response,
  ) {
    const log = {
      userId: getTenantFromToken(this.jwtService, request),
      timestamp: new Date(),
      type: 'notes tableau de bord',
      idTarget: 'none',
      message: 'notes tableau de bord mis à jour : ',
      private: false,
    };


    const notesToUpdate = await this.dashboardService.updateNote(
      getTenantFromToken(this.jwtService, request),
      data.note,
    );

    if (!data.note) {
      throw new GecoError('Impossible de mettre à jour les notes ');
    }

    const response = this.expressHelper.buildSuccess(
      res,
      notesToUpdate,
      log,
    );

    return response;
  }
}
