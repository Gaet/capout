import { Module } from '@nestjs/common';
import { DashboardDbModule } from '../../shared/database/dashboard/dashboard.db.module';
import { DashboardController } from './dashboard.controller';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';

@Module({
  controllers: [DashboardController],
  imports: [DashboardDbModule, ServicesModule, AuthenticationModule]
})
export class DashboardModule {}
