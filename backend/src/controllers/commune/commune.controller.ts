import {
  Put,
  Delete,
  Body,
  Get,
  Post,
  Param,
  Controller,
  Req,
  Request,
  Res,
  UseGuards,
  Query,
} from '@nestjs/common';
import { Response } from 'express';

import { ApiResponse } from '@nestjs/swagger';
import {
  CommuneDbService,
  ICommuneStructure,
} from '../../shared/database/commune/commune.db.interface';
import { AppLoggerService } from '../../shared/services/app-logger.service';
import { GecoError } from '../../shared/services/error.service';
import { ExpressHelper } from '../../shared/helpers/expressHelper';
import { getTenantFromToken } from '../../shared/services/contextTenant';
// import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../../shared/auth/guards/jwt-auth.guard';
import { JwtService } from '@nestjs/jwt';

@UseGuards(JwtAuthGuard)
@Controller('commune')
export class CommuneController {
  // TODO See if we use a dedicated service
  constructor(
    private readonly communeService: CommuneDbService,
    private readonly appLogger: AppLoggerService,
    private readonly jwtService: JwtService,
  ) {}

  expressHelper: ExpressHelper = new ExpressHelper(this.appLogger);

  @Get()
  async findAll(@Req() request: Request,@Query() query: string): Promise<ICommuneStructure[]> {
    if(getTenantFromToken(this.jwtService,request)){
      return this.communeService.findAll(query);
    }
  }

  @Get(':id')
  async findOne(@Req() request: Request,@Param('id') id: string): Promise<ICommuneStructure> {
    if(getTenantFromToken(this.jwtService,request)){
      return this.communeService.findOne(id);
    }
  }

}
