import { Module } from '@nestjs/common';
import { CommuneController } from './commune.controller';
import { ServicesModule } from '../../shared/services/services.module';
import { AuthenticationModule } from '../../shared/auth/auth.module';
import { UserDbModule } from '../../shared/database/user/user.db.module';
import { CommuneDbModule } from '../../shared/database/commune/commune.db.module';

@Module({
    controllers: [CommuneController],
    imports: [CommuneDbModule, UserDbModule, ServicesModule, AuthenticationModule],
})
export class CommuneModule {}
