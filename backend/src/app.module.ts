import { Module } from '@nestjs/common';
import { ClientModule } from './controllers/client/client.module';
import { DevisModule } from './controllers/devis/devis.module';
import { FactureModule } from './controllers/facture/facture.module';
import { CodeModule } from './controllers/code/code.module';
import { CodeTenantModule } from './controllers/codeTenant/codeTenant.module';
import { PreferenceModule } from './controllers/preference/preference.module';
import { CatalogueModule } from './controllers/catalogue/catalogue.module';
import { PlanningModule } from './controllers/planning/planning.module';
import { DashboardModule } from './controllers/dashboard/dashboard.module';
import { EmployeModule } from './controllers/employe/employe.module';
import { AppLogModule } from './controllers/applog/applog.module';
import { PdfModule } from './controllers/pdf/pdf.module';
import { AuthModule } from './controllers/auth/auth.module';
import { UserModule } from './controllers/user/user.module';
import { TenantModule } from './controllers/tenant/tenant.module';
import { APP_FILTER } from '@nestjs/core';
import { GecoExceptionFilter } from './shared/services/error.service';
import { CommuneModule } from './controllers/commune/commune.module';
import { MailModule } from './controllers/mail/mail.module';
import { StripeModule } from './controllers/stripe/stripe.module';
import { VersionModule  } from './controllers/version/version.module';



@Module({
  imports: [VersionModule,StripeModule,MailModule,CommuneModule, UserModule, TenantModule, CodeModule, CodeTenantModule, ClientModule, DevisModule,FactureModule,PreferenceModule,CatalogueModule,PlanningModule,DashboardModule,EmployeModule,AppLogModule, PdfModule, AuthModule],
  providers: [
    {
      provide: APP_FILTER,
      useClass: GecoExceptionFilter,
    },
  ],})
export class AppModule {}
