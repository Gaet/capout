import { Controller, Get, Request, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './shared/auth/guards/jwt-auth.guard';
import { LocalAuthGuard } from './shared/auth/guards/local-auth.guard';

@Controller()
export class AppController {
  root(): any {
    return "Hello World!"
  }
  constructor() {}

//   @UseGuards(LocalAuthGuard)
//   @Post('auth/login')
//   async login(@Request() req) {
//     return this.authService.login(req.user.email,req.user.password);
//   }

//   @UseGuards(JwtAuthGuard)
//   @Get('profile')
//   getProfile(@Request() req) {
//     return req.user;
//   }
}