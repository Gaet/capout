// en 1er on va chercher nos variables d'env
import * as dotenv from 'dotenv';
dotenv.config();

import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as cors from 'cors';
import { GecoExceptionFilter } from './shared/services/error.service';
import { Catch } from '@nestjs/common';
import { serverConfig } from './shared/helpers/serverConfig';
import * as express from 'express';

async function bootstrap() {

  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Geco API')
    .setDescription("Description de l'API Geco")
    .setVersion('1.0')
    .addTag('client')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  app.useGlobalFilters(new GecoExceptionFilter(app.get('JwtService'),app.get('AppLoggerService')));
  
  app.use(cors());
  app.use(express.json({limit: '5mb'}));
  app.use(express.urlencoded({limit: '5mb'}));

  await app.listen(7500);
}
bootstrap();
