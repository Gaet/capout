export const environment = {
    databaseEngine: 'mongo',
    databaseUser: 'admin',
    databasePassword: 'admin*',
    databaseHost: '127.0.0.1',
    databaseName: 'gec',
    databasePort: '27017',
    isDebug: true,
    fakeAuth: false,
    fakeTokenUsername: 'albo@tono.com',
    fakeTokenTenant: 'tenant1',
    jwtSecretKey: 'gecSeCrEtKey2019',
    jwtExpiration: 3600000,
};