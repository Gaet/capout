# Présentation rapide

Un test du backend en node/typescript utilisant nestJS.

Cf https://docs.nestjs.com/

L'intérêt est qu'on a une stack déjà prête pour plusieurs de nos besoins (comme Angular) :

* une intégration de passport pour l'authentification facilitée (cf https://docs.nestjs.com/techniques/authentication
* gestion des environnements
* ORMs
* une arborescence déjà pensée pour placer nos classes et fichiers
* etc...

A valider, ce n'est qu'un test.

# Lancement rapide

`yarn run start` permet d'avoir un serveur sur `http://localhost:7500`

# Documentation principes

## Utilisation contrôleurs

On fonctionne par annotation.

cf https://docs.nestjs.com/controllers

## Documentation api rest

On utile le module swagger (hérité de python) très pratique.

https://docs.nestjs.com/recipes/swagger

Accessible via http://localhost:3000/api

# Mongoose

Exemple mongoose pur sans nestjs.

~~~
    // full mongoose test
    var mongoose = require('mongoose');
    mongoose.connect('mongodb://root:root@172.17.0.2/gec');
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        // we're connected!
        var kittySchema = new mongoose.Schema({
            name: String,
        });

        var Kitten = mongoose.model('Kitten', kittySchema);
        Kitten.find(function (err, kittens) {
            if (err) return console.error(err);
            console.log(`kittens found ${kittens}`);
        })

        new Kitten({ name: 'superadmin'}).save(function (ko, ok) {

            console.log(`create ok: ${ok}`);
            console.log(`create ko: ${ko}`);
        });
    });
~~~

# Tests authentification

Si la variable fakeAuth est à false, l'authentification réelle via Jwt est activée.

On ne peut alors accéder à l'api qu'avec un token valide.

## Génération du token

On spécifie via le mail.

~~~
curl localhost:7500/auth/monemail@mail.com
~~~

## Envoi du token pour accès

~~~
curl localhost:7500/clients -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im15bWFpbCIsImlhdCI6MTU0OTMwODMxMywiZXhwIjoxNTQ5MzExOTEzfQ.ZjpLQYQPPCdCyhZz0VNMbAfmE6RwQyT9Vo-1LNxsI_E"
~~~

