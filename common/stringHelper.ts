class StringHelper {

    private static MULTI_STRING_SEPARATOR = ",";
    private static DATE_FORMAT = "YYYY-MM-DD'T'HH-mm-ss";
    private static EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    private static PHONE_PATTERN = /^(0)[1-9]([-. ]?[0-9]{2}){4}$/;
    private static WEBSITE_PATTERN = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;


    /**
     * Parse un booleen dans une string (retourne undefined si str n'est pas défini)
     * @param {string} [str] 
     * @returns 
     * @memberof StringHelper
     */
    parseBool(str?: string) {
        let res = undefined;
        if (str) {
            res = (str === "true");
        }
        return res;
    }

    /**
     * Parse un entier dans une string (retourne undefined si str n'est pas défini)
     * @param {(string | undefined)} str 
     * @returns 
     * @memberof StringHelper
     */
    parseInt(str?: string) {
        let res = undefined;
        if (str) {
            res = parseInt(str, 10);
            if (isNaN(res)) {
                res = undefined;
            }
        }
        return res;
    }

    /**
     * Parse un float dans une string (retourne undefined si str n'est pas défini)
     * @param {string} [str] 
     * @returns 
     * @memberof StringHelper
     */
    parseFloat(str?: string) {
        let res = undefined;
        if (str) {
            res = parseFloat(str);
            if (isNaN(res)) {
                res = undefined;
            }
        }
        return res;
    }

    /**
     * Transforme une string en string[] en prenant comme séparateur '+'.
     * Retourne undefined si str undefined
     * @param {string} [str] 
     * @returns {string[]} 
     * @memberof StringHelper
     */
    parseMultiString(str?: string): string[] {
        let res: string[] = [];
        if (str && str.length > 0) {
            let subStrs = str.split(StringHelper.MULTI_STRING_SEPARATOR);
            subStrs.forEach(subStr => {
                if (subStr && subStr.length > 0) {
                    res.push(subStr);
                }
            });
        }
        if (res.length === 0) {
            res = undefined;
        }
        return res;
    }


    /**
     * Teste si la chaine est bien une adresse email
     * @param str 
     */
    isEmail(str: string): boolean{
        let re = StringHelper.EMAIL_PATTERN;
        return re.test(str);
    }

    isPhone(str: string): boolean{
        let re = StringHelper.PHONE_PATTERN;
        return re.test(str);
    }

    isWebsite(str: string): boolean{
        let re = StringHelper.WEBSITE_PATTERN;
        return re.test(str);
    }

    removeAccents(str:any) {
      const accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
      const accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
      str = str.split('');
      const strLen = str.length;
      let i;
      let test;
      for (i = 0; i < strLen; i++) {
        test = accents.indexOf(str[i]);
        if (test !== -1) {
          str[i] = accentsOut[test];
        }
      }
      return str.join('');
    }

    generateRandomString(strLength: number) {
        var text = '';
        var possible =
          'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < strLength; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
      }
  
}

export let stringHelper = new StringHelper();