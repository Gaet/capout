import { ICatalogueStructure } from "./catalogue.dto";
import { blockCatalogue } from "./devis.dto";

export class IFacture {
  current: IFactureStructure;
  updateMode: boolean;
  createMode: boolean;
}

export class IVersementStructure {
  id: string;
  idAccompte: string;
  date: string;
  dateCreation: Date;
  montant: number;
}

export class IFactureStructure {
  _id: string;
  titre: string;
  // user qui crée
  userName: string;
  // numero de la facture incremental
  idContrat: number;
  // lien vers un client
  idClient: string;
  // lien vers un devis
  idDevis: string;
  // identifiant de type de facture (seule, liée, etc...)
  typeFacture: string;
  factureOrigine: string; // permet d'identifier la facture d'origine pour un avoir

  // informations clients récupérées du client lié
  client: string;
  ville: string;
  codePostal: string;
  adresse: string;
  numeroAdresse: string;
  payeur: string;
  payeurVille: string;
  payeurCodePostal: string;
  payeurAdresse: string;
  payeurNumeroAdresse: string;

  // au moment t il faut savoir si:
  exonereTva:boolean;
  associationExonereTva:boolean;

  // champs calculés du montant de la facture
  totalHT: number;
  totalTTC: number;
  totalTVA: number[];
  tauxTVA: number[];
  tauxTVAAvoir:number;

  // nombre de jours de travail
  joursTravail: number;
  nbEmployes: number;
  totalHTPerDayPerEmployes: number;

  // matériel : utile si le calcul par le catalogue est faux
  // lors de l'édition d'un devis on pourra changer sa valeur si besoin
  coutHTAchatMatos: number;
  coutHTReventeMatos: number;
  maindoeuvre: number;
  // commentaire saisis par l'utilisateur
  commentaire: string;
  remise: number; // remise en %
  paymentCondition: string;
  paymentType: string;
  regleeTotalement: boolean;

  // on a pas le droit d'éditer une facture une fois qu'elle a été imprimé
  valideDefinitivement: boolean;
  dateValideDefinitivement: Date;
  envoye: boolean;

  dateCreation: Date;
  dernierAcces: Date;
  dernierMAJ: Date;
  nbAcces: number;

  bonDeCommande: string;
  

  etat: string;
  commentaireAvoir: string;
  versements: IVersementStructure[];
  blocksCatalogue: blockCatalogue[];
}

// gestion semi automatique de l'etat d'une facture car renseigné automatiquement
// et modifiable par l'utilisateur si besoin notamment pour signalé que la facture est payé
export const FACTURE_ETAT_ENCOURS: string = "FACTURE_ETAT_ENCOURS";
export const FACTURE_ETAT_SUPPRIME: string = "FACTURE_ETAT_SUPPRIME";
export const FACTURE_ETAT_PARTIELLEMENT_PAYE: string =
  "FACTURE_ETAT_PARTIELLEMENT_PAYE";
export const FACTURE_ETAT_PAYE: string = "FACTURE_ETAT_PAYE";
export const FACTURE_ETAT_IMPAYE: string = "FACTURE_ETAT_IMPAYE";

// type générique facture
export const TYPE_CONTRAT_FACTURE: string = "FAC";

// types de facture
export const TYPE_FACTURE_FS: string = "FS";
export const TYPE_FACTURE_FD: string = "FD";
export const TYPE_FACTURE_AV: string = "AV";
export const TYPE_FACTURE_ACC: string = "AC";

// création nouveau contrat de type facture
export const NOUVEAU_CONTRAT_FACTURE:string = "nouvelle_facture"
//paymentType
export const PAYMENT_TYPE_TTC: string = "T.T.C.";
export const PAYMENT_TYPE_HT: string = "H.T.";
//
export const FACTURE_ORIGINE_PAYE: string = "factureOrigineregleeTotalement";
export const ID_PAYE: string = "regleeTotalement";

export const initFactureFromCopyStructure = {
  _id: "",
  idContrat: 0,
  idDevis: "",
  factureOrigine: "", 
  regleeTotalement: false,
  valideDefinitivement: false,
  dateValideDefinitivement: new Date(),
  envoye: false,
  dateCreation: new Date(),
  dernierAcces: new Date(),
  dernierMAJ: new Date(),
  nbAcces: 0,
  etat: FACTURE_ETAT_ENCOURS,
  commentaireAvoir: "",
  versements: [],
};

export const initFactureStructure: IFactureStructure = {
  _id: "",
  titre:"",
  userName: "",
  idContrat: 0,
  idClient: "",
  idDevis: "",
  typeFacture: "",
  factureOrigine: "",
  client: "",
  ville: "",
  codePostal: "",
  adresse: "",
  numeroAdresse: "",
  payeur: "",
  payeurVille: "",
  payeurCodePostal: "",
  payeurAdresse: "",
  payeurNumeroAdresse: "",
  exonereTva:false,
  associationExonereTva:false,
  totalHT: 0,
  totalTTC: 0,
  totalTVA: [0],
  tauxTVA: [0],
  tauxTVAAvoir:0,
  joursTravail: 0,
  nbEmployes: 0,
  totalHTPerDayPerEmployes: 0,
  coutHTAchatMatos: 0,
  coutHTReventeMatos: 0,
  maindoeuvre: 0,
  commentaire: "",
  remise: 0,
  paymentCondition: "",
  paymentType: "",
  regleeTotalement: false,
  valideDefinitivement: false,
  dateValideDefinitivement: null,
  envoye: false,

  dateCreation: new Date(),
  dernierAcces: new Date(),
  dernierMAJ: new Date(),
  nbAcces: 0,
  bonDeCommande:"",
  etat: FACTURE_ETAT_ENCOURS,
  commentaireAvoir: "",
  versements: [],
  blocksCatalogue: [],
};

export const initAccompte: IFactureStructure = {
  _id: "",
  titre:"",
  userName: "",
  idContrat: 0,
  idClient: "",
  idDevis: "",
  typeFacture: "",
  factureOrigine: "",
  client: "",
  ville: "",
  codePostal: "",
  adresse: "",
  numeroAdresse: "",
  payeur: "",
  payeurVille: "",
  payeurCodePostal: "",
  payeurAdresse: "",
  payeurNumeroAdresse: "",
  exonereTva:false,
  associationExonereTva:false,
  totalHT: 0,
  totalTTC: 0,
  totalTVA: [0],
  tauxTVA: [0],
  tauxTVAAvoir:0,
  joursTravail: 0,
  nbEmployes: 0,
  totalHTPerDayPerEmployes: 0,
  coutHTAchatMatos: 0,
  coutHTReventeMatos: 0,
  maindoeuvre: 0,
  commentaire: "",
  remise: 0,
  paymentCondition: "",
  paymentType: "",
  regleeTotalement: false,
  valideDefinitivement: false,
  dateValideDefinitivement: null,
  envoye: false,
  bonDeCommande:"",

  dateCreation: new Date(),
  dernierAcces: new Date(),
  dernierMAJ: new Date(),
  nbAcces: 0,
  etat: FACTURE_ETAT_ENCOURS,
  commentaireAvoir: "",
  versements: [],
  blocksCatalogue: [],
};