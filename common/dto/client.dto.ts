export const NOUVEAU_CLIENT: string = "nouveau_client";

export class IClient{
  currentClient: IClientStructure;
  updateMode: boolean;
  createMode: boolean;
}
export class IClientStructure {
  _id: string;
  // user qui crée le devis
  userName:string;
  idIncrement: string;
  nom: string;
  siret: string;
  gerant: string;
  activity: string;
  origin: string;
  qualification: string;
  paymentCondition: string;
  paymentDelay: string;
  ville: string;
  idCommune:string;
  codePostal: string;
  numeroAdresse: string;
  adresse: string;
  payeur: string;
  payeurVille: string;
  payeurCodePostal: string;
  payeurNumeroAdresse: string;
  payeurAdresse: string;
  interlocuteur: string;
  contactPhone: string;
  contactEmail: string;
  etat:string;
  commentaire:string;
  deleted:boolean;
}

export const initClient: IClientStructure = {
  _id: "",
  userName:"",
  idIncrement: "",
  nom: "",
  siret: "",
  gerant: "",
  activity: "",
  origin: "",
  qualification: "",
  paymentCondition: "",
  paymentDelay: "",
  ville: "",
  idCommune:"",
  codePostal: "",
  numeroAdresse: "",
  adresse: "",
  payeur: "",
  payeurVille: "",
  payeurCodePostal: "",
  payeurNumeroAdresse: "",
  payeurAdresse: "",
  interlocuteur: "",
  contactPhone: "",
  contactEmail: "",
  etat:"",
  commentaire:"",
  deleted:false
}


// a void client by default
export function clientInitialState(): IClient {
  return {
    currentClient: clientStructureInitialState(),
    createMode: false,
    updateMode: false
  };
}
export function clientStructureInitialState(): IClientStructure {
  return {
    _id: '',
    userName:'',
    idIncrement:'',
    nom: '',
    siret: '',
    gerant: '',
    activity: '',
    origin: '',
    qualification: '',
    paymentCondition: '',
    paymentDelay: '',
    ville: '',
    idCommune:'',
    codePostal: '',
    numeroAdresse: '',
    adresse: '',
    payeur: '',
    payeurVille: '',
    payeurCodePostal: '',
    payeurNumeroAdresse: '',
    payeurAdresse: '',
    interlocuteur: '',
    contactPhone: '',
    contactEmail: '',
    etat:'',
    commentaire:'',
    deleted:false,
  };

}
