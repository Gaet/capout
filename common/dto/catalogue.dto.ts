export const NOUVEAU_CATALOGUE: string = "nouveau_catalogue"

// To track current edited catalogue and associated mode in list/detail
export class ICatalogue {
    currentCatalogue: ICatalogueStructure;
    updateMode: boolean;
    createMode: boolean;
  }
  export class ICatalogueStructure {
    _id: string;
    // user qui crée
    userName:string;
    titre: string;
    intitule: string;
    categorie: string;
    sousCategorie: string;
    quantite: number;
    nbJoursLocation: number;
    unite: string;
    coutHTAchatMatosPU: number;
    coutHTReventeMatosPU: number;
    hourPerPu: number;
    coutPerPU: number;
    hours: number;
    tva: number;
    commentaire: string;
    lastAccess: Date;
    lastUpdate: Date;
    nbOfViews: number;
    totalLigne: number;
  }

  export const initCatalogue: ICatalogueStructure =  {
    _id: "",
    // user qui crée
    userName:"",
    titre: "",
    intitule: "",
    categorie: "",
    sousCategorie: "",
    quantite: 0,
    nbJoursLocation: 0,
    unite: "",
    coutHTAchatMatosPU: 0,
    coutHTReventeMatosPU: 0,
    hourPerPu: 0,
    coutPerPU: 0,
    hours: 0,
    tva: 0,
    commentaire: "",
    lastAccess: new Date(),
    lastUpdate: new Date(),
    nbOfViews: 0,
    totalLigne:0
  }

  export const SOUS_TOTAL:string = "Sous Total";