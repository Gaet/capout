import { ICatalogueStructure } from "./catalogue.dto";

export class IDevis {
  current: IDevisStructure;
  updateMode: boolean;
  createMode: boolean;
}
export class blockCatalogue {
  titre: string;
  block: Array<ICatalogueStructure>;
}

export class IDevisStructure {
  _id: string;
  // user qui crée le devis
  userName: string;
  // numero du devis incremental
  idContrat: number;
  // lien vers un client
  idClient: string;
  //titre du devis (optionnel)
  titre: string;

  // informations clients récupérées du client lié
  client: string;
  ville: string;
  codePostal: string;
  adresse: string;
  numeroAdresse: string;

  // au moment t il faut savoir si:
  exonereTva: boolean;
  associationExonereTva: boolean;
  // champs calculés du montant du devis
  totalHT: number;
  totalTTC: number;
  totalTVA: number[];
  tauxTVA: number[];

  // commentaire saisis par l'utilisateur
  commentaire: string;

  // nombre de jours de travail
  joursTravail: number;
  nbEmployes: number;
  totalHTPerDayPerEmployes: number;

  // matériel : utile si le calcul par le catalogue est faux
  // lors de l'édition d'un devis on pourra changer sa valeur si besoin
  coutHTAchatMatos: number;
  coutHTReventeMatos: number;
  maindoeuvre: number;
  // si le devis a été validé (ie signé,accepté par le client)

  envoye: boolean;

  dateCreation: Date;
  dernierAcces: Date;
  dernierMAJ: Date;
  nbAcces: number;

  etat: string;

  blocksCatalogue: blockCatalogue[];
}

export const DEVIS_ETAT_ENCOURS: string = "DEVIS_ETAT_ENCOURS";
export const DEVIS_ETAT_SIGNE: string = "DEVIS_ETAT_SIGNE";
export const DEVIS_ETAT_SUPPRIME: string = "DEVIS_ETAT_SUPPRIME";
export const DEVIS_ETAT_SANS_REPONSE: string = "DEVIS_ETAT_SANS_REPONSE";
export const TYPE_CONTRAT_DEVIS: string = "DE";
export const DEVIS_PROVISOIRE:string = "DEVIS_PROVISOIRE";

export const NOUVEAU_CONTRAT_DEVIS: string = "nouveau_devis";

export const initDevisFromCopyStructure = {
  _id: "",
  // numero du devis incremental
  idContrat: 0,
  envoye: false,
  dateCreation: new Date(),
  dernierAcces: new Date(),
  dernierMAJ: new Date(),
  nbAcces: 0,
  etat: DEVIS_ETAT_ENCOURS,
};
export const initDevisStructure: IDevisStructure = {
  _id: "",
  userName: "",
  // numero du devis incremental
  idContrat: 0,
  // lien vers un client
  idClient: "",

  titre: "",

  // informations clients récupérées du client lié
  client: "",
  ville: "",
  codePostal: "",
  adresse: "",
  numeroAdresse: "",

  exonereTva: false,
  associationExonereTva: false,

  // champs calculés du montant de la devis
  totalHT: 0,
  totalTTC: 0,
  totalTVA: [0],
  tauxTVA: [0],

  // commentaire saisis par l'utilisateur
  commentaire: "",

  // nombre de jours de travail
  joursTravail: 0,
  nbEmployes: 0,
  totalHTPerDayPerEmployes: 0,

  //materiel

  coutHTAchatMatos: 0,
  coutHTReventeMatos: 0,
  maindoeuvre: 0,

  envoye: false,

  dateCreation: new Date(),
  dernierAcces: new Date(),
  dernierMAJ: new Date(),
  nbAcces: 0,

  etat: DEVIS_ETAT_ENCOURS,

  blocksCatalogue: [],
};
