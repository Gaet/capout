export class IUserStructure {
    _id: string;
    idTenant: string;
    profil: string;
    tenant:string; // nom de l'entreprise
    email: string;
    name:string;
    password: string;
    thumbnail: string;
    isConnected: boolean;
    isDisconnected: boolean;
    isAdmin: boolean;
    isConnecting: boolean;
    isDisconnecting: boolean;
    connectionFailed: boolean;
    disconnectionFailed: boolean;
    confirmed: boolean;
    confirmationToken: string;
    comparePassword: (passw: string) => Promise<Boolean>;
    forgetCode:number;
    resetPwdFlag:Boolean;
    deleted:Boolean;
    firstConnection:Boolean;
    signatureGerant_b64:string;
    ips:string[];
    ipKey:string;
}

export class IUserLogin {
    tenant:string;
    name:string;
    email: string;
    tel:string;
    microEntreprise:boolean;
    associationExonereTva: boolean;
    exonereTva:boolean;
    password: string;
    forgetCode:number;
    siret:string;
    ville:string;
    adresse:string;
    codePostal:string;
    signatureGerant_b64:string;
}

export class IUsers {
    byId: {
      [_id: string]: IUserStructure;
    };
    allIds: string[];
  }
  
  export function userslistInitialState(): IUsers {
    return {
      byId: {},
      allIds: []
    };
  }