export class IVersionStructure  {
    type: string; 
    version:string;
    message: string;
    timestamp: Date; 
  }