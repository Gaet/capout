export class IEmploye {
    currentEmploye: IEmployeStructure;
    updateMode: boolean;
    createMode: boolean;
  }
  export class IEmployeStructure {
    id: string;
    name: string;
    tjm: Number;
  }
  export class IEmployeModel {
    _id: string;
    name: string;
    tjm: Number;
  }