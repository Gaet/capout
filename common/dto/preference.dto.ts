// To track current edited preference and associated mode in list/detail
export class IPreference {
  currentPreference: IPreferenceStructure;
  updateMode: boolean;
  createMode: boolean;
}

export class IDynamicDoc {
  id: string;
  nom: string;
  label: string;
  value: string;
}

export const NB_OF_INFORMATIONS_PERSONNELLES = 7
export const NB_OF_DOC_DEVIS = 2
export const NB_OF_DOC_FACTURE = 4
export const NB_OF_DOC_COMMUN = 2



export class IPreferenceStructure {
  
  _id: string;
  idTenant: string;
  tenant: string;
  intituleEntreprise: string;
  tva: number;

  // commun avec tenant
  nomGerant: string;
  tel: string;
  telFixe: string;
  email: string;
  ville: string;
  adresse: string;
  codePostal: string;
  complementAdresse: string;
  statutEntreprise: string;
  microEntreprise: boolean;
  exonereTva:boolean;
  associationExonereTva: boolean;
  siret: string;
//
  profil: string;
  typeCompte: string;
  dateCreation: Date;
  dateEnd: Date;
  encryptedPassword: string;
  logo_entreprise_b64: string;
  signature_b64: string;
  codeApe: string;
  codeTva: string;
  ribBooleanFacture: boolean;
  titulaireRib: string;
  ribCodeBanque: string;
  ribCodeGuichet: string;
  ribNumeroCompte: string;
  ribCle: string;
  iban: string;
  bic: string;
  optionsDevis: any[];
  optionsFacture: any[];
  optionsCommun: any[];
  textFooter: string;


  informationsComplete:boolean;
  informationPourcentage:number;

  docDevisComplete:boolean;
  docDevisPourcentage:number;

  docFactureComplete:boolean;
  docFacturePourcentage:number;


  docCommunComplete:boolean;
  docCommunPourcentage:number;
}

export const initPreference: IPreferenceStructure = {
  _id: "",
  idTenant: "",
  tenant: "",
  intituleEntreprise: "",
  nomGerant: "",
  tel: "",
  telFixe: "",
  ville: "",
  adresse: "",
  codePostal: "",
  complementAdresse: "",
  siret: "",
  tva: 20,
  email:"",
  exonereTva:false,
  statutEntreprise: "",
  microEntreprise: false,
  associationExonereTva: false,
  profil: "utilisateur",
  typeCompte: "gratuit",
  dateCreation: new Date(),
  dateEnd: new Date(),
  encryptedPassword: "",
  logo_entreprise_b64: "",
  signature_b64: "",
  codeApe: "",
  codeTva: "",
  titulaireRib: "",
  ribBooleanFacture: true,
  ribCodeBanque: "",
  ribCodeGuichet: "",
  ribNumeroCompte: "",
  ribCle: "",
  iban: "",
  bic: "",
  optionsDevis: [],
  optionsFacture: [],
  optionsCommun: [],
  textFooter: "Aucun escompte consenti pour règlement anticipé. Tout incident de paiement est passible d'intérêt de retard. Le montant des pénalités résulte de l'application aux sommes restant dues d'un taux d'intérêt légal en vigueur au moment de l'incident. Indemnité forfaitaire pour frais de recouvrement due au créancier en cas de retard de paiement: 40€",

  informationsComplete:false,
  informationPourcentage:0,

  docDevisComplete:false,
  docDevisPourcentage:0,

  docFactureComplete:false,
  docFacturePourcentage:0,

  docCommunComplete:false,
  docCommunPourcentage:0
};
