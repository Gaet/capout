export class ICode {
  currentCode: ICodeElement;
  currentCodeType: string;
  currentCodeName: string;
  updateMode: boolean;
  createMode: boolean;
}
export class ICodeElement {
  id: string;
  value: any;
  name?: string;
  label?: string;
  parentCode?: string;
  idParent?: string;
}
export class ICodeElementWithType {
  elem: ICodeElement;
  type: string;
  name: string;
}
export class ICodeStructure {
  _id: string;
  codeTenant?: boolean;
  type: string;
  name: string;
  list: ICodeElement[];
  parentName?: string;
}
export class ICodeInitStructure {
  codeTenant?: boolean;
  type: string;
  name: string;
  list: ICodeElement[];
  parentName?: string;
}
export const initCodeElement: ICodeElement = {
  id: "",
  value: "",
  name: "",
  label: "",
  parentCode: "",
  idParent: "",
};
export const initCode: ICodeStructure = {
  _id: "",
  codeTenant: false,
  name: "",
  type: "",
  list: [],
  parentName: "",
};

export const CODE_CATEGORIE = "categorie";
export const CODE_SOUSCATEGORIE = "sousCategorie";
export const CODE_UNITE = "unite";
