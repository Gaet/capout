export class ITenantStructure {
  _id: string;
  idStripeCostumer: string;
  priceProduct:string;
  paymentData:any;
  typeCompte: string;
  profil: string;
  tenant: string;
// commun avec preference
  nomGerant: string;
  tel: string;
  telFixe: string;
  email: string;
  ville: string;
  adresse: string;
  codePostal: string;
  complementAdresse: string;
  statutEntreprise: string;
  microEntreprise: boolean;
  exonereTva:boolean;
  associationExonereTva: boolean;
  siret: string;
//
  dateCreation: Date;
  dateResiliation: Date;
  resiliation:boolean;
  //
  dateContratSigned: Date;
}

export class IpaymentStripe {
  paymentMethodId: string;
  customerId: string;
  priceId: string;
  invoiceId: string;
  subscriptionId: string;
}






export const COMPTE_ADMIN: string = "admin";
export const TYPE_COMPTE_GRATUIT: string = "gratuit";
export const TYPE_COMPTE_BASE: string = "base";
export const TYPE_COMPTE_BUSINESS: string = "business";
export const KEY_STRIPE: string = "pk_live_xFnqS7Ieavu7b2nmvQ8nexOg00colqLewC";
export const KEY_STRIPE_PRODUCT_ID_BASE: string = "prod_JbpSKgbQLghK6s";
export const KEY_STRIPE_PRODUCT_ID_BUSINESS: string = "prod_JbpSivTJjWJAqO";
export const KEY_PRICE_ID_BASE: string = "price_1IybnqGlHgKbCNGZbzmTDOtx";
export const KEY_PRICE_ID_BUSINESS: string = "price_1IybneGlHgKbCNGZ5PoL4BFz";
export const PRICE_GRATUIT: number = 0;
export const PRICE_BASE: number = 5;
export const PRICE_BUSINESS: number = 25;