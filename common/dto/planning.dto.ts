export class IPlanning {
    currentPlanning: IPlanningStructure;
    updateMode: boolean;
    createMode: boolean;
  }
  export class IPlanningStructure {
    _id: string;
    // user qui crée
    userName:string;
    intitule: string;
    client: string;
    idDevis: string;
    dateBegin: Date;
    dateEnd: Date;
    commentaire: string;
    lastAccess: Date;
    lastUpdate: Date;
    nbOfViews: number;
    deleted:boolean;
  }

  export const initPlanning: IPlanningStructure = {
    _id: "",
    userName:"",
    intitule:"" ,
    client: "",
    idDevis: "",
    dateBegin: new Date(),
    dateEnd: new Date(),
    commentaire: "",
    lastAccess:new Date(),
    lastUpdate:new Date(),
    nbOfViews:0,
    deleted:false,
  };
