export class ISupportStructure  {
    _id: string;
    idTenant: string;
    tenant: string;
    user: string;
    type: string;
    date: Date; 
    text: string;
    status:string
  }