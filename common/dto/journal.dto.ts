export class IJournalStructure  {
  _id: string;
  type: string;
  date: Date; 
  text: string;
}