export class ICommuneStructure {
    id: string
    codePostal:string
    libelleCom:string
    codeReg: string
    nomReg: string
    codeDep: string
    nomDep: string
    nomCom: string
    codeCom: string
    population: number
    loc: number[]
  }
  