
/**
 * entete d'un contrat
 */
export interface IPdfStructure {
  _id: string; // le vrai _id du contrat en fait
  idContrat: string; // l'id incrementé
  dateCreation: Date;
  type: string; // facture,devis
  nameTenant: string;
  nameClient: string;
  docName: string;
  sendToMail: boolean;

}