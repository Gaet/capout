export class IDashboard {
    currentDashboard: IDashboardStructure;
    updateMode: boolean;
    createMode: boolean;
  }
  export class IDashboardStructure {
    id: string;
    text: string;
  }
  export class IDashboardModel {
    _id: string;
    text: string;
  }