export const ChoixUniteVitesse= [
        {id: 1, nom: 'km/h', coef:1/360000}, 
        {id: 2, nom: 'm/s' , coef:1/360000},
        {id: 3, nom: 'miles/h' ,coef:1/360000},     
    ];

export const ChoixUniteDistance=    [
        {id: 1, nom: 'km', coef:1},
        {id: 2, nom: 'm' , coef:0.001},
        {id: 3, nom: 'miles' ,coef: 1.60934},    
    ]; 

    export const ChoixBareme=    [
        {id: 1, nom: 'RM'},
        {id: 2, nom: 'RF' },
        {id: 3, nom: 'IA'}, 
        {id: 4, nom: 'IB'},    
        {id: 5, nom: 'N1'},
        {id: 6, nom: 'N2'},  
        {id: 7, nom: 'N3'},  
        {id: 8, nom: 'N4'},  
        {id: 9, nom: 'IR1'},
        {id: 10, nom: 'IR2'},
        {id: 11, nom: 'IR3'},
        {id: 12, nom: 'IR4'},
        {id: 13, nom: 'R1'},
        {id: 14, nom: 'R2'},
        {id: 15, nom: 'R3'},
        {id: 16, nom: 'R4'},
        {id: 17, nom: 'R5'},
        {id: 18, nom: 'R6'},
        {id: 19, nom: 'D1'},
        {id: 20, nom: 'D2'},
        {id: 21, nom: 'D3'},
        {id: 22, nom: 'D4'},
        {id: 23, nom: 'D5'},
        {id: 24, nom: 'D6'},
        {id: 25, nom: 'D7'}
       
         
                
    ]; 
    console.log(ChoixUniteVitesse)
    console.log(ChoixUniteDistance)
    console.log(ChoixBareme)