
     export default function tpspassageCtrl(shareVitesseService, calculvitesseService, timeToTextService, tpspassagesService, graphService, graphService2,  $timeout, $anchorScroll, $scope) {
         const ctl = this;
        
         //*** Select bareme ***//
         // ctl.ObjetChoixBareme = ChoixBareme;
         // ctl.setChoixBareme = ctl.ObjetChoixBareme[0];
         // il faut partager entre les services un etat, des que vitesse change il faut changer un etat ds un service partagé
         $scope.$watch(function() {
             return shareVitesseService.getStateVitesse();
         }, function(newValue, oldValue) {
             if (newValue !== oldValue) {
              
                 const ObjetVitesse = calculvitesseService.getObjetVitesse();
                 var distance = ObjetVitesse["distance"];
                 var setUniteDistance = ObjetVitesse["setUniteDistance"];
                 var vitesse = ObjetVitesse["vitessemoyenne"];  
                 var temps = ObjetVitesse["temps"];
           
                 ctl.tpspassages = tpspassagesService.basics(distance, setUniteDistance, vitesse, temps, timeToTextService);
              
             }
         });
     }