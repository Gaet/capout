     // inputs //
     import {
         ChoixUniteVitesse
     } from '../const/input.js';
     import {
         ChoixUniteDistance
     } from '../const/input.js';
    
     export default function vitesseCtrl(shareVitesseService, calculvitesseService, timeToTextService, tpspassagesService, graphService,graphService2,  $timeout, $anchorScroll, $scope) {
         const ctl = this;
        
             //*** Select Vitesse ***//
         ctl.Objetunitevitesse = ChoixUniteVitesse;
         ctl.setUniteVitesse = ctl.Objetunitevitesse[0];
         //*** Select distance ***//
         ctl.Objetunitedistance = ChoixUniteDistance;
         ctl.setUniteDistance = ctl.Objetunitedistance[0];

         //*** Initialisation des variables bindés ***//
         ctl.vitesse = 0;
         ctl.distance = "";
         ctl.heure = "";
         ctl.minute = "";
         ctl.seconde = "";
         ctl.centseconde = "";
         ctl.info = "";

         

         // ----------------------------------------------------------------- //   

         ctl.VitesseMoyenne = function() {
         
             if (calculvitesseService.isCalculVitessePossible(ctl.distance, ctl.heure, ctl.minute, ctl.seconde, ctl.centseconde)) {
                 /////////////////////////////////VITESSE MOYENNE///////////////////////////////////
                 console.log("calcul vitesse possible")
                 const ObjetVitesse = calculvitesseService.setObjetVitesse(ctl.distance, ctl.heure, ctl.minute, ctl.seconde, ctl.centseconde, ctl.setUniteDistance, ctl.setUniteVitesse);
                 var vitesse = ObjetVitesse["vitessemoyenne"];
                 ctl.vitesse = vitesse;
                 // var temps = ObjetVitesse["temps"];
                 
                 ctl.showallure = true;
                 ctl.nav_showallure  = true; 
                 ctl.nav_showgraph_perf = true;

             } else {
                 ctl.showallure = false;

                  ctl.nav_showallure  = false; 
                  ctl.nav_showgraph_perf = false;

                 // ctl.focusvitesse = {
                 //     'border-bottom-style': 'solid',
                 //     'border-width': '5px',
                 //     'border-color': '#626e84'
                 // };
             }
         }; // ----------------------- fin fonction vitesse moyenne----------------------- //   
         ctl.ffa_distance = {};
         $scope.$watch('vm.vitesse', function(newValue, oldValue) {
             if (newValue !== oldValue) {
                 shareVitesseService.setStateVitesse(newValue);
                 console.log("la vitesse a changée");
             }
         });
     } // fin controleur principal