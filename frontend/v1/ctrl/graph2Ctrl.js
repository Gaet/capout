     // inputs //
     import {
         ChoixUniteVitesse
     } from '../const/input.js';
     import {
         ChoixUniteDistance
     } from '../const/input.js';
     import {
         ChoixBareme
     } from '../const/input.js';
     import baremeffa from '../json/baremeH.json';
     export default function graph2Ctrl(shareVitesseService, calculvitesseService, timeToTextService, tpspassagesService, graphService, graphService2, $timeout, $anchorScroll, $scope) {
         const ctl = this;
         const jsonbaremeffa = baremeffa;
         //*** Select bareme ***//
         ctl.ObjetChoixBareme = ChoixBareme;
         ctl.setChoixBareme = ctl.ObjetChoixBareme[0];


         ctl.bindgraphService = function() {
                 const ObjetVitesse = calculvitesseService.getObjetVitesse();
                 var vitesse = ObjetVitesse["vitessemoyenne"];
                 ctl.vitesse = vitesse;
                 // ctl.setUniteVitesse=;
                 var temps = ObjetVitesse["temps"];

                 var distance = ObjetVitesse["distance"];
                 var setUniteDistance = ObjetVitesse["setUniteDistance"];

                 graphService2.normal(ctl.setChoixBareme.nom,distance,setUniteDistance, temps, vitesse, jsonbaremeffa);
                 /////////////////////////////////CHART 2 BINDING/////////////////////////////////// 
                 ctl.dataGraph2= graphService2.getValeurs();
                 ctl.labelsGraph2= graphService2.getDistance();
                 ctl.seriesGraph2= graphService2.getSeries();
                 ctl.colorsGraph2= graphService2.getColors();
                 ctl.datasetOverrideGraph2= graphService2.getDatasetOverride();
                 ctl.optionsGraph2= graphService2.getOptions();
                 ctl.ecartV=graphService2.getEcartV()[0];
             }




              //*** Action sur le graph : ***//
         ctl.actionGraph2= function(points, evt) {
             
             var id =  points[0]._index;
             
             ctl.info_distance = graphService2.getDistance()[id];
            
             ctl.info_performance = timeToTextService.transformationPerformanceTexte(graphService2.getTab_performance()[points[0]._index],1);
             
             ctl.info_points = graphService2.getTab_Points()[id];
              
               // on force le binding par un pseudo digest avec timeout, pas terrible
             // TODO WATCHERS
             $timeout(function() {});
         };

         // il faut partager entre les services un etat, des que vitesse change il faut changer un etat ds un service partagé
         $scope.$watch(function() {
             return shareVitesseService.getStateVitesse();
         }, function(newValue, oldValue) {
             if (newValue !== oldValue) {
                 $scope.firstName = newValue;
                 const ObjetVitesse = calculvitesseService.getObjetVitesse();
                 var distance = ObjetVitesse["distance"];
                 var setUniteDistance = ObjetVitesse["setUniteDistance"];
                 var vitesse = ObjetVitesse["vitessemoyenne"];
                 ctl.vitesse = vitesse;
                 // ctl.setUniteVitesse=;
                 var temps = ObjetVitesse["temps"];
               
                     ///////////////////////////////GRAPH PERFORMANCE//////////////////////////////
                     // si distance autorisé pour tracé un graph + sur une distance officielle :
                    if (graphService2.isDistanceOfficielle(distance, temps, setUniteDistance, jsonbaremeffa)) {
                     graphService2.normal("RF",distance,setUniteDistance, temps, vitesse, jsonbaremeffa);
                     } 

                    ////////////////////////////////CHART  BINDING/////////////////////////////////// 
                 ctl.dataGraph2= graphService2.getValeurs();
                 ctl.labelsGraph2= graphService2.getDistance();
                 ctl.seriesGraph2= graphService2.getSeries();
                 ctl.colorsGraph2= graphService2.getColors();
                 ctl.datasetOverrideGraph2= graphService2.getDatasetOverride();
                 ctl.optionsGraph2= graphService2.getOptions();
                 ctl.ecartV=graphService2.getEcartV()[0];
                 
                 }
             });
       
     }