     // inputs //
     import {
         ChoixUniteVitesse
     } from '../const/input.js';
     import {
         ChoixUniteDistance
     } from '../const/input.js';
     import {
         ChoixBareme
     } from '../const/input.js';
     import baremeffa from '../json/baremeH.json';
     export default function graph1Ctrl(shareVitesseService, postDataService,calculvitesseService, timeToTextService, tpspassagesService, graphService, graphService2,  $timeout, $anchorScroll, $scope) {
         const ctl = this;
         const jsonbaremeffa = baremeffa;
         //*** Select bareme ***//
         ctl.ObjetChoixBareme = ChoixBareme;
         ctl.setChoixBareme = ctl.ObjetChoixBareme[0];




             //*** Action sur le graph : ***//
         ctl.actionGraph1 = function(points, evt) {
             console.log("point: "+points[0]._index)
             //var id = graphService.getTab_id_depart_selection()[0] + points[0]._index;
             // console.log(points[0]._index)
             if (jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[1]].distance < 1) {
                 ctl.info_distance =jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[1]].distance * 1000 + ' m';
             } else {
                 ctl.info_distance =jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[1]].distance + ' km';
             }
             ctl.info_performance = timeToTextService.transformationPerformanceTexte(graphService.getTab_performance()[points[0]._index],1);
             ctl.info_points = graphService.getTab_Points()[points[0]._index];
             if (graphService.getTab_libelle()[points[0]._index] == "" || graphService.getTab_libelle()[points[0]._index] == "RM" || graphService.getTab_libelle()[points[0]._index] == "RF") {
                 ctl.info_libelle = "IA";
             } else {
                 ctl.info_libelle = graphService.getTab_libelle()[points[0]._index];
             }
             if (graphService.getcoefD() == 1) {
                 ctl.info_athlete_RF = jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[2]].athlete;
                 ctl.info_daterecord_RF = jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[2]].date;
                 ctl.info_athlete_RM = jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[1]].athlete;
                 ctl.info_daterecord_RM = jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[1]].date;
             } else {
                 ctl.info_athlete_RF = "inconnu";
                 ctl.info_daterecord_RF = "inconnu";
                 ctl.info_athlete_RM = "inconnu";
                 ctl.info_daterecord_RM = "inconnu";
             }
             // on force le binding par un pseudo digest avec timeout, pas terrible
             // TODO WATCHERS
             $timeout(function() {});
         };

         // il faut partager entre les services un etat, des que vitesse change il faut changer un etat ds un service partagé
         $scope.$watch(function() {
             return shareVitesseService.getStateVitesse();
         }, function(newValue, oldValue) {
             if (newValue !== oldValue) {
                 $scope.firstName = newValue;
                 const ObjetVitesse = calculvitesseService.getObjetVitesse();
                 var distance = ObjetVitesse["distance"];
                 var setUniteDistance = ObjetVitesse["setUniteDistance"];
                 var vitesse = ObjetVitesse["vitessemoyenne"];
                 var setUniteVitesse = ObjetVitesse["setUniteVitesse"];

                 ctl.vitesse = vitesse;
                 // ctl.setUniteVitesse=;
                 var temps = ObjetVitesse["temps"];
                 // vitesse en km/centieme de seconde utilisé pour le graph
                 //var vitesseCs = ObjetVitesse["vitessemoyennecs"];
                 
                 // si distance autorisé pour tracé un graph:
                 if (graphService.distancemax(distance, setUniteDistance, jsonbaremeffa)) {
                     ctl.showgraph_perf = true;
                     ctl.showallure = true;
                     ctl.nav_showvitesse = true;

                     ctl.nav_showgraph_perf = true;
                     ctl.nav_showallure = true;


                     // 1 ) sur une distance officielle :
                     if (graphService.isDistanceOfficielle(distance, temps, setUniteDistance, jsonbaremeffa)) {
                         graphService.normal(distance, temps, vitesse, setUniteDistance, jsonbaremeffa);
                         } else {
                         graphService.extrapolation(distance, temps, vitesse, setUniteDistance, jsonbaremeffa);
                         // 2 ) sur une distance non officielle :
                         // on tente d'interpoller entre deux distance officielle i-1 et i                               
                     }
                     var changeStylePerformance = angular.element( document.querySelector( '#graph1' ) );
                     changeStylePerformance.css('opacity','1');
                     var changeStyleAllure = angular.element( document.querySelector( '#allure' ) );
                     changeStyleAllure.css('opacity','1');
                     var changeStyleTpsPassage = angular.element( document.querySelector( '#tpspassage' ) );
                     changeStyleTpsPassage.css('opacity','1');
                     var changeStylePerformanceRelative = angular.element( document.querySelector( '#graph2' ) );
                     changeStylePerformanceRelative.css('opacity','1');
                     var changeStylePhoto = angular.element( document.querySelector( '.sous_header' ) );
                     changeStylePhoto.css('filter','sepia(20%) blur(0px)');

                     //mobile :
                     var changeStylenav_allure = angular.element( document.querySelector( '#nav_allure' ) );
                      changeStylenav_allure.css('opacity','1');
                      changeStylenav_allure.css('cursor','pointer');
                      var changeStylenav_tpspassage = angular.element( document.querySelector( '#nav_tpspassage' ) );
                      changeStylenav_tpspassage.css('opacity','1');
                      changeStylenav_tpspassage.css('cursor','pointer');
                      var changeStylenav_graph1 = angular.element( document.querySelector( '#nav_graph1' ) );
                      changeStylenav_graph1.css('opacity','1');
                      changeStylenav_graph1.css('cursor','pointer');

                     
                     /////////////////////////////////CHART 1 BINDING///////////////////////////////////                 
                     ctl.data = graphService.getValeurs();
                     ctl.labels = graphService.getTab_libelle();
                     ctl.series = graphService.getSeries();
                     ctl.colors = graphService.getColors();
                     ctl.datasetOverride = graphService.getDatasetOverride();
                     ctl.options = graphService.getOptions();
                 
                              ///////////////////////////////// - FIN CHAR BINDING - ///////////////////////////////////
                     const indicejson = graphService.getIndice_performance_personnelle();
                     const coefD = graphService.getcoefD();




                 //////////////////////////// on insère en base le résultat: /////////////////////////////////
                  //////////////////////////// on insère en base le résultat: /////////////////////////////////
                 postDataService.async(distance+" "+setUniteDistance.nom,vitesse+" "+setUniteVitesse.nom,timeToTextService.transformationPerformanceTexte(temps,1),jsonbaremeffa.baremeffa[indicejson].libelle);
                 // on fait rien en retour
                 //////////////////////////// on insère en base le résultat: /////////////////////////////////
                  //////////////////////////// on insère en base le résultat: /////////////////////////////////




                     // on affiche les infos de base sans avoir a cliquer sur le graph //   
                     ctl.info_performance = timeToTextService.transformationPerformanceTexte(temps,1);
                     ctl.info_points = jsonbaremeffa.baremeffa[indicejson].points;
                     ctl.info_distance = distance + " " + setUniteDistance.nom;
                     if (jsonbaremeffa.baremeffa[indicejson].libelle == "" || jsonbaremeffa.baremeffa[indicejson].libelle == "RM" || jsonbaremeffa.baremeffa[indicejson].libelle == "RF") {
                         ctl.info_libelle = "IA";
                     } else {
                         ctl.info_libelle = jsonbaremeffa.baremeffa[indicejson].libelle;
                     }
                     if (coefD == 1) {
                         ctl.showrecords = true;
                         ctl.typedistance = "La distance renseignée est officielle: records et barême FFA officiel";
                         ctl.info_athlete_RF = jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[2]].athlete;
                         ctl.info_daterecord_RF = jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[2]].date;
                         ctl.info_performance_RF = timeToTextService.transformationPerformanceTexte(jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[2]].performance,1);
                         ctl.info_athlete_RM = jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[1]].athlete;
                         ctl.info_daterecord_RM = jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[1]].date;
                         ctl.info_performance_RM = timeToTextService.transformationPerformanceTexte(jsonbaremeffa.baremeffa[graphService.getTab_id_depart_selection()[1]].performance,1);
                     } else {
                         ctl.showrecords = false;
                         ctl.typedistance = "La distance renseignée est non officielle : extrapolation des records et du bareme FFA ";
                     }
                 }
             }
         });
     }