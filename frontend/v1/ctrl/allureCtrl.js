
     export default function allureCtrl(shareVitesseService, calculvitesseService, timeToTextService, tpspassagesService, graphService, graphService2,  $timeout, $anchorScroll, $scope) {
         const ctl = this;
     

         // il faut partager entre les services un etat, des que vitesse change il faut changer un etat ds un service partagé
         $scope.$watch(function() {
             return shareVitesseService.getStateVitesse();
         }, function(newValue, oldValue) {
             if (newValue !== oldValue) {
                 const ObjetVitesse = calculvitesseService.getObjetVitesse();
                 // vitesse en km/centieme de seconde utilisé pour le graph
                 var vitesseCs = ObjetVitesse["vitessemoyennecs"];
                 // temps réalisé en version texte
                 const Timetotext = timeToTextService.transformationGroupePerformanceTexte(new Map([
                     ["100m", 0.1 / vitesseCs],
                     ["200m", 0.2 / vitesseCs],
                     ["400m", 0.4 / vitesseCs],
                     ["800m", 0.8 / vitesseCs],
                     ["1km", 1 / vitesseCs],
                     ["1.5km", 1.5 / vitesseCs],
                     ["3km", 3 / vitesseCs],
                     ["5km", 5 / vitesseCs],
                     ["10km", 10 / vitesseCs],
                     ["semi", 21.1 / vitesseCs],
                     ["marathon", 42.2 / vitesseCs],
                     ["100km", 100 / vitesseCs]
                 ]));
                 ///////////////////////////////// Equivalence sur quelques distances ///////////////////////////////////   
                 var tableauCleValeurAllure = [
                     ["min", Timetotext["1km"].replace("'", " min ").replace("00", "")],
                     ["100m", Timetotext["100m"]],
                     ["200m", Timetotext["200m"]],
                     ["400m", Timetotext["400m"]],
                     ["800m", Timetotext["800m"]],
                     ["1km", Timetotext["1km"]],
                     ["1.5km", Timetotext["1.5km"]],
                     ["3km", Timetotext["3km"]],
                     ["5km", Timetotext["5km"]],
                     ["10km", Timetotext["10km"]],
                     ["semi", Timetotext["semi"]],
                     ["marathon", Timetotext["marathon"]],
                     ["100km", Timetotext["100km"]]
                 ];
                 ctl.allure = new Map(tableauCleValeurAllure);             
             }
         });
     }