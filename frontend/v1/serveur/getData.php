<?php
header("Access-Control-Allow-Origin: *");
require('connexion.php');



try {
    $db = new PDO(DB_DRIVER . ":dbname=" . DB_DATABASE . ";host=" . DB_SERVER . ";charset=utf8", DB_USER, DB_PASSWORD);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec("set names utf8");
     

    setlocale(LC_TIME, 'french'); // sinon octobre = october ..etc

    $stmt = $db->prepare("SELECT * FROM capout order by id DESC limit 10 ");
    $stmt->execute();

   // set the resulting array to associative
   echo  json_encode($stmt->fetchAll(PDO::FETCH_CLASS));



}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$db = null;

?>