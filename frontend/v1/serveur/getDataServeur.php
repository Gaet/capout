<?php
header("Access-Control-Allow-Origin: *");
require('connexion.php');



try {
    $db = new PDO(DB_DRIVER . ":dbname=" . DB_DATABASE . ";host=" . DB_SERVER . ";charset=utf8", DB_USER, DB_PASSWORD);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec("set names utf8");
     

    setlocale(LC_TIME, 'french'); // sinon octobre = october ..etc

    $stmt = $db->prepare("SELECT * FROM capout");
    $stmt->execute();

   // set the resulting array to associative
  
   $resultats = $stmt->fetchAll(PDO::FETCH_CLASS);
   echo "<table class='table'>";
   echo "<tr><td style='display:none'>Date</td><td>Course à pied</td><td>Vitesse</td><td>Bareme FFA</td></tr>";

    foreach ($resultats as $ligne) {  
        $date=$ligne->date;
        $distance=$ligne->distance;
        $vitesse=$ligne->vitesse;
        $temps=$ligne->temps;
        $bareme=$ligne->bareme;

 echo "<tr>
    <td style='display:none'>$date</td>

   
<td><a href='http://www.capout.fr/course-a-pied-$distance' title='Course à pied de $distance parcourue en $temps' hreflang='fr'><span class='hidden-xs'>Course à pied de</span> <strong>$distance</strong> <span class='hidden-xs'>réalisée en $temps</span></a>
</td><td>$vitesse</td>
<td>$bareme</td>  

 </tr>";
  
    }

    echo '</table>';
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$db = null;

?>