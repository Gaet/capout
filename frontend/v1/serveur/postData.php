<?php
header("Access-Control-Allow-Origin: *");
require('connexion.php');

        $distance = "";
        $vitesse = "";
        $temps = "";
        $bareme = "";

if ($_GET['distance'] !='' && $_GET['vitesse'] != '' && $_GET['temps'] != '' && $_GET['bareme'] != '') {
        $distance = $_GET['distance'];
        $vitesse = $_GET['vitesse'];
        $temps = $_GET['temps'];
        $bareme = $_GET['bareme'];
try {
    $db = new PDO(DB_DRIVER . ":dbname=" . DB_DATABASE . ";host=" . DB_SERVER . ";charset=utf8", DB_USER, DB_PASSWORD);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec("set names utf8");
     

    setlocale(LC_TIME, 'french'); // sinon octobre = october ..etc

    $requete = $db->prepare("INSERT INTO capout (distance,vitesse,temps,bareme) VALUES (:distance,:vitesse,:temps,:bareme)");

        $requete->bindParam(':distance', $distance);
        $requete->bindParam(':vitesse', $vitesse);
        $requete->bindParam(':temps', $temps);
        $requete->bindParam(':bareme', $bareme);

    $requete->execute();

    // set the resulting array to associative
   echo  json_encode("OK");



}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$db = null;
}


?>