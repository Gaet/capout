<div id="presentation">
        <div class="container post">
        <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
        <div id="description" >
        
        
<h1 class="titre_index">Vitesse et Performance en course à pied</h1>





<h2 ><span align="center">Capout, l'application web qui calcule la vitesse moyenne pour la course à pied.</span></h2>   

<p class="p_index">En calculant la vitesse moyenne de votre sortie en course à pied en km/h, en m/s, ou en miles/h, on obtient:</p>

<h2 class="hr"><span class="hr">VITESSE</span></h2>


<blockquote>
  <p><strong>L'allure de course à pied</strong>, c'est à dire le temps nécessaire pour parcourir 1km. </p>
</blockquote>

<blockquote>
  <p><strong>Le Bareme FFA</strong> correspondant à la performance réalisée.</p>
</blockquote>

<blockquote>
  <p><strong>Les temps de passages</strong> à respecter pour être sûr de réaliser l'objectif de course.</p>
</blockquote>

<p class="p_index">En renseignant votre VMA et votre référence sur 10km, l'application vous 
donnera aussi une estimation de la performance que vous devriez être capable de réaliser sur la distance choisie.
Cette estimation comporte des erreurs d'incertitudes notables lorsque le parcours est non linéaire.
Pour une estimation de qualité, le parcours doit être du même type que votre référence sur 10 km, donc comporter les mêmes difficultés</p>

<h2 class="hr"><span class="hr">PERFORMANCE (en cours de developpement)</span></h2>

<blockquote>
  <p><strong>VMA</strong> Calcul de votre VMA (Vitesse Maximale Aérobie)  </p>
</blockquote>

<blockquote>
   <p><strong>Estimation du temps de parcours</strong> sur un semi-marathon par exemple</p>
</blockquote>

<blockquote>
   <p><strong>Estimation de votre performance</strong> sur un marathon par exemple</p>
</blockquote>

<p>


<p>*Bareme FFA: <a href="http://www.athle.fr/asp.net/main.html/html.aspx?htmlid=125" target="_blank" >Bareme officiel de la Fédération Française d'Athétisme.</a></p>
<p>*VMA: <a href="https://fr.wikipedia.org/wiki/Vitesse_maximale_a%C3%A9robie" target="_blank" >Vitesse Maximale Aérobie.</a></p>

<h3 style="margin-top:100px">Les dernières courses à pieds:</h3>

<?php echo require('serveur/getDataServeur.php');?>

</div>
</div>
</div>
</div>
</div>