     // module capout 
    var capout = angular.module('capout',['chart.js','ngRoute']);
    //  routes

   

       capout.config( function($routeProvider,$locationProvider) {
            

            $routeProvider         
            .when('/', {
                templateUrl : function(){
          var w = window,
          d = document,
          e = d.documentElement,
          g = d.getElementsByTagName('body')[0],
          x = w.innerWidth || e.clientWidth || g.clientWidth,
          y = w.innerHeight|| e.clientHeight|| g.clientHeight;


          return (x < 400) ? 'pages/accueil-mobile.html' : 'pages/accueil.html';
                },
                controller  : 'MonController',
                controllerAs : 'vm'
            });
              
              // $locationProvider.html5Mode(true);
              // $locationProvider.hashPrefix('!');

});
   



capout.run(function ($anchorScroll) {
      //this will make anchorScroll scroll to the div minus 50px
      $anchorScroll.yOffset = 50;
  });


function MonController(httpService,indexService,MenuService,$timeout,$anchorScroll ) {
var tab_distance = new Array();
       var tab_vitesse = new Array();
       var tab_performance = new Array();
       var tab_points = new Array();
       var tab_libelle = new Array();
       var tab_id_depart_selection = new Array();
       var temps=0;
       var tab_vitesse = new Array();     // km/h
       var tab_vitesse_perso = new Array();
       var indice_performance_personnelle=0;
       var flag_p1=true;

    const ctl = this;

   ctl.showgraph_perf = false;
   ctl.showallure = false;
   ctl.showvitesse = false;

   ctl.nav_showaccueil = true;
   ctl.nav_showvitesse = true;
   ctl.nav_showgraph_perf = false;
   ctl.nav_showallure = false;


ctl.scrollTo = function (id) {
if(id!='vitesse'){ 
  ctl.focusvitesse = {};
   if(ctl.distance!="" && ctl.temps!=""){
     ctl.showvitesse = true;
     ctl.showallure = true;
  ctl.showgraph_perf = true;
  }
}else{
  ctl.showvitesse = true;
  if(ctl.distance!="" && ctl.temps!=""){
     ctl.showvitesse = true;
     ctl.showallure = true;
  ctl.showgraph_perf = true;
  }
 
  indexService.cacherindex();


}
  $anchorScroll(id);  
}

ctl.chargerpage=function (page) {

  ctl.showvitesse = false;
  ctl.showallure = false;
  ctl.showgraph_perf = false;
  indexService.montrerindex();
  $anchorScroll(page);  
}
ctl.chargerpageMobile=function (page) {

     ctl.templates = [{
     name: 'pages/'+page+'.html',
     url: 'pages/'+page+'.html'
   }];


  indexService.cacherindex();
  ctl.template=  ctl.templates[0];

  if(page!='vitesse'){ 
  ctl.focusvitesse = {};
   if(ctl.distance!="" && ctl.temps!=""){
     ctl.showvitesse = true;
     ctl.showallure = true;
  ctl.showgraph_perf = true;
  }
}else{
  ctl.showvitesse = true;
  if(ctl.distance!="" && ctl.temps!=""){
     ctl.showvitesse = true;
     ctl.showallure = true;
  ctl.showgraph_perf = true;
  }
 
  indexService.cacherindex();


}
 
}


    //*** Début Select Vitesse ***//

ctl.Objetunitevitesse = [
        {id: 1, nom: 'km/h', coef:1/360000}, 
        {id: 2, nom: 'm/s' , coef:1/360000},
        {id: 3, nom: 'miles/h' ,coef:1/360000},     
    ];

 ctl.setUniteVitesse = ctl.Objetunitevitesse[0];

    //*** Fin Select Vitesse ***//


      //*** Début Select distance ***//

    ctl.Objetunitedistance = [
        {id: 1, nom: 'km', coef:1},
        {id: 2, nom: 'm' , coef:0.001},
        {id: 3, nom: 'miles' ,coef: 1.60934},
     
    ];
 
 ctl.setUniteDistance = ctl.Objetunitedistance[0];

        //*** Fin Select distance ***//


 
// init //
   ctl.vitesse=0;
   ctl.distance="";
   ctl.heure="";
   ctl.minute="";
   ctl.seconde="";
   ctl.centseconde="";
   ctl.info="";
      
       
       // var tab_libelle_perso ="moi";
    
      
   

  // chargement du json : 


    var json= "";

   var ChargerJson = function(){ 
     httpService.get()
      .then(function(resultat) {       
        json=  JSON.parse(JSON.stringify(resultat.data));        
      });
   };


   ChargerJson();

    ctl.onClick = function (points, evt) {
  
   //console.log(points[0]._index)
   var id=tab_id_depart_selection[0]+points[0]._index;
   //console.log(id)
   if(json.baremeffa[id].distance<1){ctl.info_distance=json.baremeffa[id].distance*1000+' m';}else{
          ctl.info_distance=json.baremeffa[id].distance+' km';
        }
   
   ctl.info_performance=TransformationPerformanceTexte(json.baremeffa[id].performance);
   ctl.info_points=json.baremeffa[id].points;

   if(json.baremeffa[id].libelle=="" || json.baremeffa[id].libelle=="RM" || json.baremeffa[id].libelle=="RF" ){
    ctl.info_libelle="IA";
}else{

   ctl.info_libelle=json.baremeffa[id].libelle;
}

ctl.info_athlete_RF=json.baremeffa[tab_id_depart_selection[2]].athlete;
ctl.info_daterecord_RF=json.baremeffa[tab_id_depart_selection[2]].date;

ctl.info_athlete_RM=json.baremeffa[tab_id_depart_selection[1]].athlete;
ctl.info_daterecord_RM=json.baremeffa[tab_id_depart_selection[1]].date;



 
    $timeout(function () {});
     
        

  };


 if(window.addEventListener) {
    window.addEventListener('resize', function() {
       
         ctl.VitesseMoyenne();
    }, true);
}






   ctl.tracerGraph =  function tracer(indicejson,coefD){



         tab_id_depart_selection.push(json.baremeffa[indicejson].id-1);



          if(screen.width>400){
        //console.log((json.baremeffa[indicejson].distance)/(json.baremeffa[indicejson].performance/360000));    
         tab_libelle.push(json.baremeffa[indicejson].libelle);   
         tab_vitesse.push((json.baremeffa[indicejson].distance)/(json.baremeffa[indicejson].performance/360000));
         tab_distance.push(coefD*json.baremeffa[indicejson].distance);  
         tab_performance.push(coefD*json.baremeffa[indicejson].performance);  
         tab_id.push(json.baremeffa[indicejson].id); 

       }else
       {
        // on n'affiche que le 1er resultat,le dernier resultat; le resultat avant la perf, la perf, le resultat apres la perf 
        // 1ER ET DERNIER:
        if(indicejson==0  || indicejson==json.baremeffa.length-1){
         tab_libelle.push(json.baremeffa[indicejson].libelle);   
         tab_vitesse.push((json.baremeffa[indicejson].distance)/(json.baremeffa[indicejson].performance/360000));
         tab_distance.push(coefD*json.baremeffa[indicejson].distance);  
         tab_performance.push(coefD*json.baremeffa[indicejson].performance);  
         tab_id.push(json.baremeffa[indicejson].id); 
         tab_vitesse_perso.push("NaN");
       }
        // apres perf:
        if(indicejson>indice_performance_personnelle && flag_p1){
         tab_libelle.push(json.baremeffa[indice_performance_personnelle+1].libelle);   
         tab_vitesse.push((json.baremeffa[indice_performance_personnelle+1].distance)/(json.baremeffa[indice_performance_personnelle+1].performance/360000));
         tab_distance.push(coefD*json.baremeffa[indice_performance_personnelle+1].distance);  
         tab_performance.push(coefD*json.baremeffa[indice_performance_personnelle+1].performance);  
         tab_id.push(json.baremeffa[indice_performance_personnelle+1].id); 
         flag_p1=false;
         tab_vitesse_perso.push("NaN");
        
       }


       
      }
        
         


        if(temps<=coefD*json.baremeffa[indicejson].performance && temps>coefD*json.baremeffa[indicejson-1].performance){
          indice_performance_personnelle=indicejson;

 
        if(screen.width<400){
           // on affiche le point avant -1 la performance perso
         tab_libelle.push(json.baremeffa[indicejson-2].libelle);   
         tab_vitesse.push((json.baremeffa[indicejson-2].distance)/(json.baremeffa[indicejson-2].performance/360000));
         tab_distance.push(coefD*json.baremeffa[indicejson-2].distance);  
         tab_performance.push(coefD*json.baremeffa[indicejson-2].performance);  
         tab_id.push(json.baremeffa[indicejson-2].id); 
         tab_vitesse_perso.push("NaN");

          // on affiche le point avant la performance perso
         tab_libelle.push(json.baremeffa[indicejson-1].libelle);   
         tab_vitesse.push((json.baremeffa[indicejson-1].distance)/(json.baremeffa[indicejson-1].performance/360000));
         tab_distance.push(coefD*json.baremeffa[indicejson-1].distance);  
         tab_performance.push(coefD*json.baremeffa[indicejson-1].performance);  
         tab_id.push(json.baremeffa[indicejson-1].id); 
         tab_vitesse_perso.push("NaN");


        // on affiche le point après la performance perso
         tab_vitesse_perso.push(ctl.vitesse); //[ctl.vitesse];

         tab_libelle.push(json.baremeffa[indicejson].libelle);   
         tab_vitesse.push((json.baremeffa[indicejson].distance)/(json.baremeffa[indicejson].performance/360000));
         tab_distance.push(coefD*json.baremeffa[indicejson].distance);  
         tab_performance.push(coefD*json.baremeffa[indicejson].performance);  
         tab_id.push(json.baremeffa[indicejson].id); 
         tab_vitesse_perso.push("NaN");

           // on affiche le point après +1 la performance perso
         tab_vitesse_perso.push(ctl.vitesse); //[ctl.vitesse];

         tab_libelle.push(json.baremeffa[indicejson+1].libelle);   
         tab_vitesse.push((json.baremeffa[indicejson+1].distance)/(json.baremeffa[indicejson].performance/360000));
         tab_distance.push(coefD*json.baremeffa[indicejson+1].distance);  
         tab_performance.push(coefD*json.baremeffa[indicejson+1].performance);  
         tab_id.push(json.baremeffa[indicejson+1].id); 
         tab_vitesse_perso.push("NaN");




 
       }else{
        // on affiche le point après la performance perso
         tab_vitesse_perso.push(ctl.vitesse); //[ctl.vitesse];

       }
   



        // on affiche les infos de base sans avoir a cliquer sur le graph //   

   
   ctl.info_performance=TransformationPerformanceTexte(temps);
   ctl.info_points=json.baremeffa[indicejson].points;

 if(coefD*json.baremeffa[indicejson].distance<1){ctl.info_distance=coefD*json.baremeffa[indicejson].distance*1000+' m';}else{
          ctl.info_distance=coefD*json.baremeffa[indicejson].distance+' km';
        }

   if(json.baremeffa[indicejson].libelle=="" || json.baremeffa[indicejson].libelle=="RM" || json.baremeffa[indicejson].libelle=="RF" ){
    ctl.info_libelle="IA";
}else{

   ctl.info_libelle=json.baremeffa[indicejson].libelle;
}
if(coefD==1){
  ctl.showrecords=true;
  ctl.typedistance="DISTANCE OFFICIELLE";
ctl.info_athlete_RF=json.baremeffa[tab_id_depart_selection[2]].athlete;
ctl.info_daterecord_RF=json.baremeffa[tab_id_depart_selection[2]].date;
ctl.info_performance_RF=TransformationPerformanceTexte(json.baremeffa[tab_id_depart_selection[2]].performance);

ctl.info_athlete_RM=json.baremeffa[tab_id_depart_selection[1]].athlete;
ctl.info_daterecord_RM=json.baremeffa[tab_id_depart_selection[1]].date;
ctl.info_performance_RM=TransformationPerformanceTexte(json.baremeffa[tab_id_depart_selection[1]].performance);

}else{
ctl.showrecords=false;
 ctl.typedistance="DISTANCE NON OFFICIELLE: ADAPTATION DU BAREME FFA";

}

 $timeout(function () {});

     // ----------------------------------------------------------------- //   

        }

       
if(screen.width>400){
 tab_vitesse_perso.push("NaN");
}

};

   ctl.VitesseMoyenne = function(){ 

       

        tab_distance = new Array();
        tab_vitesse = new Array();
        tab_performance = new Array();
        tab_points = new Array();
        tab_libelle = new Array();
        tab_id = new Array();
        tab_id_depart_selection = new Array();
        tab_vitesse_perso= new Array();



     
      

    // temps en centieme de seconde
    temps=ctl.heure*360000+ctl.minute*6000+ctl.seconde*100+ctl.centseconde*1;
   // vitesse en km/h
   var vitesse=0;



   // si km et km/h
   if(ctl.setUniteDistance.nom=="km" && ctl.setUniteVitesse.nom=="km/h")
   {
    vitesse=(ctl.distance*ctl.setUniteDistance.coef)/(temps*ctl.setUniteVitesse.coef);
   }
   // si m et km/h
   if(ctl.setUniteDistance.nom=="m" && ctl.setUniteVitesse.nom=="km/h")
   {
    vitesse=(ctl.distance*ctl.setUniteDistance.coef)/(temps*ctl.setUniteVitesse.coef);
   }
   // si miles et km/h

     if(ctl.setUniteDistance.nom=="miles" && ctl.setUniteVitesse.nom=="km/h")
   {
    vitesse=(ctl.distance*ctl.setUniteDistance.coef)/(temps*ctl.setUniteVitesse.coef);
   }



   // si km et m/s
   if(ctl.setUniteDistance.nom=="km" && ctl.setUniteVitesse.nom=="m/s")
   {
    vitesse=(ctl.distance*ctl.setUniteDistance.coef)/(temps*ctl.setUniteVitesse.coef*3.6);
   }
   // si m et m/s
   if(ctl.setUniteDistance.nom=="m" && ctl.setUniteVitesse.nom=="m/s")
   {
    vitesse=(ctl.distance*ctl.setUniteDistance.coef)/(temps*ctl.setUniteVitesse.coef*3.6);
   }
   // si miles et m/s

     if(ctl.setUniteDistance.nom=="miles" && ctl.setUniteVitesse.nom=="m/s")
   {
    vitesse=(ctl.distance*ctl.setUniteDistance.coef)/(temps*ctl.setUniteVitesse.coef*3.6);
   }



      // si km et m/s
   if(ctl.setUniteDistance.nom=="km" && ctl.setUniteVitesse.nom=="miles/h")
   {
    vitesse=(ctl.distance*ctl.setUniteDistance.coef)/(temps*ctl.setUniteVitesse.coef*1.60934);
   }
   // si m et m/s
   if(ctl.setUniteDistance.nom=="m" && ctl.setUniteVitesse.nom=="miles/h")
   {
    vitesse=(ctl.distance*ctl.setUniteDistance.coef)/(temps*ctl.setUniteVitesse.coef*1.60934);
   }
   // si miles et m/s

     if(ctl.setUniteDistance.nom=="miles" && ctl.setUniteVitesse.nom=="miles/h")
   {
    vitesse=(ctl.distance*ctl.setUniteDistance.coef)/(temps*ctl.setUniteVitesse.coef*1.60934);
   }




   // vitesse en km/centieme de seconde
   var vitesseCS=(ctl.distance*ctl.setUniteDistance.coef)/(temps);

   if(vitesse=="Infinity"){
   ctl.vitesse="";
   }else{
    ctl.vitesse=Math.round(vitesse*100)/100;
   }


   

   var tableauCleValeurAllure = 
   [["min",  TransformationPerformanceTexte(1/vitesseCS).replace("'"," min ").replace("00","")], 
    ["100m", TransformationPerformanceTexte(0.1/vitesseCS)],
    ["200m", TransformationPerformanceTexte(0.2/vitesseCS)], 
    ["400m", TransformationPerformanceTexte(0.4/vitesseCS)], 
    ["800m", TransformationPerformanceTexte(0.8/vitesseCS)], 
    ["1km", TransformationPerformanceTexte(1/vitesseCS)], 
    ["1.5km", TransformationPerformanceTexte(1.5/vitesseCS)], 
    ["3km", TransformationPerformanceTexte(3/vitesseCS)], 
    ["5km", TransformationPerformanceTexte(5/vitesseCS)], 
    ["10km", TransformationPerformanceTexte(10/vitesseCS)], 
    ["semi", TransformationPerformanceTexte(21.1/vitesseCS)], 
    ["marathon", TransformationPerformanceTexte(42.2/vitesseCS)], 
    ["100km", TransformationPerformanceTexte(100/vitesseCS)]]; 
    
   ctl.allure = new Map(tableauCleValeurAllure);
      
 var flag_interpol=true;
if(ctl.vitesse!=0 && ctl.vitesse !="Infinity")
         {
       for (var i = 0; i < json.baremeffa.length; i++) {
        // deux cas possible,  

        // 1 ) sur une distance officielle :
        // console.log(json.baremeffa[i].distance);
        if(ctl.distance*ctl.setUniteDistance.coef==parseFloat(json.baremeffa[i].distance))
        {

          ctl.showgraph_perf = true;
          ctl.showallure = true;

          ctl.nav_showvitesse = true;
          ctl.focusvitesse = {
          'border-bottom-style': 'solid',
          'border-width': '5px',
          'border-color': '#626e84'
           };
          ctl.nav_showgraph_perf = true;
          ctl.nav_showallure = true;

        

          ctl.tracerGraph(i,1);

      

        }
if(i>0 && flag_interpol){
         if(ctl.distance*ctl.setUniteDistance.coef>parseFloat(json.baremeffa[i-1].distance) && ctl.distance*ctl.setUniteDistance.coef<parseFloat(json.baremeffa[i].distance) )
        { 
// 2 ) sur une distance non officielle : on tente d'interpoller entre deux distance officielle i-1 et i
         var coefD= ctl.distance*ctl.setUniteDistance.coef/json.baremeffa[i-1].distance;
flag_interpol=false;
        
           

               ctl.showgraph_perf = true;
           ctl.showallure = true;
            
               for (var j = i-27; j < i; j++) {
              
             ctl.tracerGraph(j,coefD);
          }
         // var performanceinterpol=(ctl.distance*ctl.setUniteDistance.coef-json.baremeffa[i].distance+coefD*json.baremeffa[i].performance)/coefD;

    
           


        }

      }
 
               }

      

       
    
    ctl.data = new Array(tab_vitesse,tab_vitesse_perso);
    ctl.labels = tab_libelle;
    ctl.series = ['Bareme FFA','Performance personelle'];

    ctl.colors = ['#45b7cd', '#ff6384'];

  
  ctl.datasetOverride = [ {
        
        borderWidth: 1,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        type: 'line'
      },
      {
       
        borderWidth: 3,
        borderColor:'#ff6384',
        hoverBackgroundColor: '#ff6384',
        hoverBorderColor: '#ff6384',
        type: 'line',
        radius:8

      }];

  ctl.options = {
    // maintainAspectRatio: true,
    // responsive: false,

     elements:{
      point:{
        radius:4,
        borderWidth:2
      }
    },
    scales: {
      yAxes: [
        {
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left'
        }
      ], xAxes: [{
                display: true
            }],

            spanGaps: true,

           
       
      

    }, title: {
        display: true,
        text: 'Vitesse moyenne (km/h) et performance',

    },
      legend: {
            display: true,
            labels: {
                fontColor: '#FFF', fontStyle:"bold"
            }
        }
          


  };
    
  // ctl.options.elements.point.radius =  18;
  

}
  

   }; // fin fonction vitesse moyenne





    ctl.ffa_distance={};



}
function TransformationPerformanceTexte(t){
  // t en centieme de secondes


  var hours   = Math.floor(t / 360000);
  var minutes = Math.floor((t - (hours * 360000)) / 6000);
  // console.log("minutes = "+minutes)
  var reste = t - (hours * 360000) - (minutes * 6000);
 // console.log("reste = "+reste)
  var seconds = Math.floor(reste / 100);
 //  console.log("sec = "+seconds)

  var centseconds = reste- 100*seconds;
 // console.log("cent s="+centseconds)

//  var seconds = t - (hours * 3600) - (minutes * 6000);
var result="";

     

          if(hours!=0){
      result = hours+"h";
    
    }

      if(minutes!=0){
        if(hours!=0){
      result +=  (minutes < 10 ? "0" + minutes : minutes)+ "'" ;
    }else{
       result +=  minutes+ "'" ;
    }
       if(seconds==0){
      result +=  (seconds  < 10 ? "0" + seconds : seconds);    
    }
    
    }
     if(seconds!=0){
       if(minutes==0){
      result +=  seconds+"''";
    }else{
       result +=  (seconds  < 10 ? "0" + seconds : seconds)+"''";
         
    }

         if(centseconds!=0){
      result += Math.floor((centseconds  < 10 ? "0" + centseconds : centseconds)); 
    }
    }
    


  return result;

}

function httpService($http) {
  var test=document.getElementById('checkHomme');
  if(test==null){
 this.get = function() {
    var json="json/ffa_homme2";
   return  $http.get(json);        
  }

  }else{
    this.get = function() {
    var json="";
   
    if (document.getElementById('checkHomme').checked){

    json= "json/ffa_homme2";
}  
      if (document.getElementById('checkFemme').checked){

    json= "json/ffa_femme";
}

   return  $http.get(json);        
  }
  }
}
  


capout.controller('MonController', MonController)
.directive('directive', function() {

   return {
       restrict: 'E',
       link: function(scope, element, attrs) {      
        scope.chargerpage = function(pagecible){       
        scope.contentUrl= 'pages/'+pagecible+'.html'; 


      }

       },
        templateUrl: function(elem,attrs) {
           return 'pages/'+attrs.templateUrl+'.html' || 'pages/vitesse.html'

       }
   }
})
.directive('directive2',['indexService', function(indexService) {

   return {
       restrict: 'E',
   
      
    
       link: function(scope, element, attrs, MonController) {      
       scope.chargerpage = function(pagecible){       
       scope.contentUrl= 'pages/'+pagecible+'.html'; 

      }

       },
       //  templateUrl: function(elem,attrs) {
       //     return 'pages/'+attrs.templateUrl+'.html' || 'pages/vitesse.html'
       // }
       template: '<div  ng-include="contentUrl"></div>'
   }
}]);


capout.service('httpService', httpService);

capout.service('indexService', function() {
    this.cacherindex = function () {
     
    var element = document.getElementById('description');
var n = document.createTextNode(' ');
var disp = element.style.display;  // don't worry about previous display style

element.appendChild(n);
element.style.display = 'none';
    
    }

     this.montrerindex = function () {
     
    var element = document.getElementById('description');
var n = document.createTextNode(' ');
var disp = element.style.display;  // don't worry about previous display style

element.appendChild(n);
element.style.display = 'block';
    
    }
});
capout.service('MenuService', function() {
    this.show = function (bool) {  
    console.log('service menu ='+bool)   
    return bool;   
    }

    this.afficher=function(page){

    }
})