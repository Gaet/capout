export default function graphService() {
    console.log('run service graphService');
    var tab_point = new Array();
    var tab_libelle = new Array();
    var tab_vitesse = new Array();
    var tab_distance = new Array();
    var tab_performance = new Array();
    var tab_performance_test = new Array();
    var tab_id = new Array();
    var tab_id_depart_selection = new Array();
    var tab_vitesse_perso = new Array();
    var flag_vitesse_eleve = false;
    var indice_performance_personnelle = 0;
    var flag_p1 = true;
    var flag_indice_performance_personnelle = false;
    var coefD = 1;
    var indicespecial = 1;
    this.test=function(){
        console.log('hhhh')
    }
    this.Raz = function() {
        tab_point = new Array();
        tab_libelle = new Array();
        tab_vitesse = new Array();
        tab_distance = new Array();
        tab_performance = new Array();
        tab_performance_test = new Array();
        tab_id = new Array();
        tab_id_depart_selection = new Array();
        tab_vitesse_perso = new Array();
        flag_vitesse_eleve = false;
        indice_performance_personnelle = 0;
        flag_p1 = true;
        flag_indice_performance_personnelle = false;
        coefD = 1;
        indicespecial = 1;
    }
    this.isDistanceOfficielle = function(distance, temps, setUniteDistance, jsonbaremeffa) {
        var flag = false;
        distance = distance * setUniteDistance.coef;
        this.Raz();
        for (var i = 0; i < jsonbaremeffa.baremeffa.length; i++) {
            if (distance == parseFloat(jsonbaremeffa.baremeffa[i].distance)) {
                flag = true;
                break;
            }
        }
        return flag;
    }
    this.normal = function(distance, temps, vitesse, setUniteDistance, jsonbaremeffa) {
        this.Raz();
        distance = distance * setUniteDistance.coef;
        for (var i = 0; i < jsonbaremeffa.baremeffa.length; i++) {
            if (distance == parseFloat(jsonbaremeffa.baremeffa[i].distance)) {
                // pas d'interpolation donc : coefD =1 , b=0
                var performance = jsonbaremeffa.baremeffa[i].performance;
                AlimentationGraph(distance, jsonbaremeffa, i, vitesse, setUniteDistance, temps, performance);
            }
        }
    }
    this.distancemax = function(distance, setUniteDistance, jsonbaremeffa) {
        if (distance * setUniteDistance.coef > jsonbaremeffa.baremeffa[jsonbaremeffa.baremeffa.length - 1].distance) {
            return false
        } else {
            return true;
        }
    }
    this.vitessemax = function(vitesse, setUniteDistance, jsonbaremeffa) {
        // todo vitesse superieure à la vitesse 0
        if (vitesse > (jsonbaremeffa.baremeffa[0].distance) / (jsonbaremeffa.baremeffa[0].performance / 360000)) {
            flag_vitesse_eleve = true;
            console.log('todo vitesse eleve')
        }
    }
    this.extrapolation = function(distance, temps, vitesse, setUniteDistance, jsonbaremeffa) {
        this.Raz();
        var flag_interpol = true;
        distance = distance * setUniteDistance.coef;
        for (var i = 0; i < jsonbaremeffa.baremeffa.length; i++) {
            if (i > 0 && flag_interpol) {
                if (distance > parseFloat(jsonbaremeffa.baremeffa[i - 1].distance) && distance < parseFloat(jsonbaremeffa.baremeffa[i].distance)) {
                    // 2 ) sur une distance non officielle : performance = coefD x distance + b 
                    //on tente d'interpoller entre deux distance officielle i-1 et i
                    flag_interpol = false;
                    for (var j = i+1; j < i + 27; j++) {
                        coefD = (jsonbaremeffa.baremeffa[j].performance - jsonbaremeffa.baremeffa[j - 27].performance) / (jsonbaremeffa.baremeffa[j].distance - jsonbaremeffa.baremeffa[j - 27].distance);
                        var b = jsonbaremeffa.baremeffa[j].performance - coefD * jsonbaremeffa.baremeffa[j].distance;
                        var performanceinterpol = distance * coefD + b;
                        //  console.log("performance i-27    ="+jsonbaremeffa.baremeffa[j-27].performance)
                        //   console.log("performanceinterpol    ="+performanceinterpol)
                        //   console.log("performance i       ="+jsonbaremeffa.baremeffa[j].performance)
                        // console.log("performanceinterpol ="+performanceinterpol)
                        
                        AlimentationGraph(distance, jsonbaremeffa, j, vitesse, setUniteDistance, temps, performanceinterpol);
                    }
                    break;
                }
            }
        }
    }
    var AlimentationGraph = function(distance, jsonbaremeffa, indicejson, vitesse, setUniteDistance, temps, performance) {
        tab_id_depart_selection.push(jsonbaremeffa.baremeffa[indicejson].id - 1);
        tab_performance_test.push(performance);

        if (screen.width > 400) {
            //console.log((jsonbaremeffa.baremeffa[indicejson].distance)/(jsonbaremeffa.baremeffa[indicejson].performance/360000));    
            if (performance != jsonbaremeffa.baremeffa[indicejson].performance && (jsonbaremeffa.baremeffa[indicejson].libelle == 'RF' || jsonbaremeffa.baremeffa[indicejson].libelle == 'RM')) { // sur une distance non officielle, on considre qu'il n'y a pa de record : 
                tab_libelle.push("");
            } else {
                tab_libelle.push(jsonbaremeffa.baremeffa[indicejson].libelle);
            }
            tab_performance.push(performance);
            tab_point.push(jsonbaremeffa.baremeffa[indicejson].points);
            tab_distance.push(distance);
            tab_vitesse.push(Math.round(1000*distance / (performance / 360000))/1000);
            // les performances sont pondérés : on va obtenir une courbe intermédiaire entre j et j-27 :
            tab_id.push(jsonbaremeffa.baremeffa[indicejson].id);
        } else {
            // on n'affiche que le 1er resultat,le dernier resultat; le resultat avant la perf, la perf, le resultat apres la perf 
            // 1ER ET DERNIER:
            if (tab_id_depart_selection.length == 1 || tab_id_depart_selection.length == 27) {
                if (performance != jsonbaremeffa.baremeffa[indicejson].performance && (jsonbaremeffa.baremeffa[indicejson].libelle == 'RF' || jsonbaremeffa.baremeffa[indicejson].libelle == 'RM')) { // sur une distance non officielle, on considre qu'il n'y a pa de record : 
                    tab_libelle.push("");
                } else {
                    tab_libelle.push(jsonbaremeffa.baremeffa[indicejson].libelle);
                }
                tab_performance.push(performance);
                tab_point.push(jsonbaremeffa.baremeffa[indicejson].points);
                tab_distance.push(distance);
                tab_vitesse.push(Math.round(1000*distance / (performance / 360000))/1000);
                tab_id.push(jsonbaremeffa.baremeffa[indicejson].id);
                tab_vitesse_perso.push("NaN");
                // console.log(Math.round(1000*distance / (performance / 360000))/1000)
            }
            // apres perf:
            if (indicejson > indice_performance_personnelle && flag_p1 && flag_indice_performance_personnelle) {
                if (performance != jsonbaremeffa.baremeffa[indicejson].performance && (jsonbaremeffa.baremeffa[indicejson].libelle == 'RF' || jsonbaremeffa.baremeffa[indicejson].libelle == 'RM')) { // sur une distance non officielle, on considre qu'il n'y a pa de record : 
                    tab_libelle.push("");
                } else {
                    tab_libelle.push(jsonbaremeffa.baremeffa[indicejson].libelle);
                }
                tab_performance.push(performance);
                tab_point.push(jsonbaremeffa.baremeffa[indicejson].points);
                tab_distance.push(distance);
                tab_vitesse.push(Math.round(1000*distance / (performance / 360000))/1000);
                tab_id.push(jsonbaremeffa.baremeffa[indicejson].id);
                tab_vitesse_perso.push("NaN");
                flag_p1 = false;
                flag_indice_performance_personnelle = false;
            }
        }


        /////////



        if (temps== tab_performance_test[tab_performance_test.length-1] || (temps <= tab_performance_test[tab_performance_test.length - 1] && temps > tab_performance_test[tab_performance_test.length - 2])) 
        {
            indice_performance_personnelle = indicejson;
            flag_indice_performance_personnelle = true;
            if (screen.width < 400) {
                tab_performance.push(tab_performance_test[tab_performance_test.length - 2]);
                // on affiche le point avant -1 la performance perso
                if (performance != jsonbaremeffa.baremeffa[indicejson - 1].performance && (jsonbaremeffa.baremeffa[indicejson - 1].libelle == 'RF' || jsonbaremeffa.baremeffa[indicejson - 1].libelle == 'RM')) { // sur une distance non officielle, on considre qu'il n'y a pa de record : 
                    tab_libelle.push("");
                } else {
                    tab_libelle.push(jsonbaremeffa.baremeffa[indicejson - 1].libelle);
                }
                tab_point.push(jsonbaremeffa.baremeffa[indicejson - 1].points);
                tab_distance.push(distance);
                tab_vitesse.push(Math.round(1000*distance / (tab_performance_test[tab_performance_test.length - 2] / 360000))/10000);
                tab_id.push(jsonbaremeffa.baremeffa[indicejson].id);
                tab_vitesse_perso.push("NaN");
                tab_performance.push(performance);
                // performance
                if (performance != jsonbaremeffa.baremeffa[indicejson].performance && (jsonbaremeffa.baremeffa[indicejson].libelle == 'RF' || jsonbaremeffa.baremeffa[indicejson].libelle == 'RM')) { // sur une distance non officielle, on considre qu'il n'y a pa de record : 
                    tab_libelle.push("");
                } else {
                    tab_libelle.push(jsonbaremeffa.baremeffa[indicejson].libelle);
                }
                tab_point.push(jsonbaremeffa.baremeffa[indicejson].points);
                tab_distance.push(distance);
                tab_vitesse.push(Math.round(1000*distance / (performance / 360000))/1000);
                tab_id.push(jsonbaremeffa.baremeffa[indicejson].id);
                tab_vitesse_perso.push(vitesse);
            } else {
                // on affiche le point après la performance perso
                tab_vitesse_perso.push(vitesse); //[ctl.vitesse];
            }
        }

        if (screen.width > 400) {
            tab_vitesse_perso.push("NaN");
        }
        if (!flag_vitesse_eleve && vitesse > (jsonbaremeffa.baremeffa[tab_id_depart_selection[0]].distance) / (jsonbaremeffa.baremeffa[tab_id_depart_selection[0]].performance / 360000)) {
            tab_vitesse_perso.push(vitesse);
            flag_vitesse_eleve = true;
            console.log('todo vitesse eleve')
        }
    }
    this.getIndice_performance_personnelle = function() {
        return indice_performance_personnelle;
    };
    this.getcoefD = function() {
        return coefD;
    };
    this.getTab_vitesse = function() {
        return tab_vitesse;
    };
    this.getTab_vitesse_perso = function() {
        return tab_vitesse_perso;
    };
    this.getDistance = function() {
        return tab_distance;
    };
    this.getTab_libelle = function() {
        return tab_libelle;
    };
    this.getTab_Points = function() {
        return tab_point;
    };
    this.getTab_performance = function() {
        return tab_performance;
    };
    this.getTab_id = function() {
        return tab_id;
    };
    this.getTab_id_depart_selection = function() {
        return tab_id_depart_selection;
    };
    this.getValeurs = function() {
        return new Array(tab_vitesse, tab_vitesse_perso);
    };
    this.getSeries = function() {
        return ['Vitesse', 'Performance personelle'];
    };
    this.getColors = function() {
        return ['#45b7cd', '#ff6384'];
    };
    this.getDatasetOverride = function() {
        return [{
            borderWidth: 1,
            hoverBackgroundColor: "rgba(255,99,132,0.4)",
            hoverBorderColor: "rgba(255,99,132,1)",
            type: 'line'
        }, {
            borderWidth: 2,
            borderColor: '#ff6384',
            hoverBackgroundColor: '#ff6384',
            hoverBorderColor: '#ff6384',
            type: 'line',
            radius: 7
        }];
    }
    this.getOptions = function() {
        return {
            // maintainAspectRatio: true,
            // responsive: false,
            elements: {
                point: {
                    radius: 4,
                    borderWidth: 2
                }
            },
            scales: {
                yAxes: [{
                    id: 'y-axis-1',
                    type: 'linear',
                    display: true,
                    position: 'left'
                }],
                xAxes: [{
                    display: true
                }],
                spanGaps: true,
            },
            title: {
                display: true,
                text: 'Vitesse moyenne (km/h) - Bareme FFA',
            },
            legend: {
                display: true,
                labels: {
                    fontColor: '#FFF',
                    fontStyle: "bold"
                }
            }
        }; // fin options graph    
        // ctl.options.elements.point.radius =  18;
    };
}