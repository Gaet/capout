export default function timeToTextService() {
    console.log('run service timeToTextService');
    this.transformationGroupePerformanceTexte = function(map) {
        // t en centieme de secondes
        var tableau_timetotext = {};
        map.forEach(function(valeur, clef) {
            var t = valeur;
            var hours = Math.floor(t / 360000);
            var minutes = Math.floor((t - (hours * 360000)) / 6000);
            // console.log("minutes = "+minutes)
            var reste = t - (hours * 360000) - (minutes * 6000);
            // console.log("reste = "+reste)
            var seconds = Math.floor(reste / 100);
            //  console.log("sec = "+seconds)
            var centseconds = reste - 100 * seconds;
            // console.log("cent s="+centseconds)
            //  var seconds = t - (hours * 3600) - (minutes * 6000);
            var result = "";
            if (hours != 0) {
                result = hours + "h";
            }
            if (minutes != 0) {
                if (hours != 0) {
                    result += (minutes < 10 ? "0" + minutes : minutes) + "'";
                } else {
                    result += minutes + "'";
                }
                if (seconds == 0) {
                    result += (seconds < 10 ? "0" + seconds : seconds);
                }
            }
            if (seconds != 0) {
                if (minutes == 0) {
                    result += seconds + "''";
                } else {
                    result += (seconds < 10 ? "0" + seconds : seconds) + "''";
                }
                if (centseconds != 0) {
                    result += Math.floor((centseconds < 10 ? "0" + centseconds : centseconds));
                }
            }
            tableau_timetotext[clef] = result;
            // console.log('tableau_timetotext['+clef+']= '+result)
        }, map)
        return tableau_timetotext;
    }
    this.transformationPerformanceTexte = function(t, precision) {
        // t en centieme de secondes
        // precision : boolean si ==1 precision max, si ==0 presision à la seconde
        var hours = Math.floor(t / 360000);
        var minutes = Math.floor((t - (hours * 360000)) / 6000);
        // console.log("minutes = "+minutes)
        var reste = t - (hours * 360000) - (minutes * 6000);
        // console.log("reste = "+reste)
        var seconds = Math.floor(reste / 100);
        //  console.log("sec = "+seconds)
        var centseconds = reste - 100 * seconds;
        // console.log("cent s="+centseconds)
        //  var seconds = t - (hours * 3600) - (minutes * 6000);
        var result = "";
        if (hours != 0) {
            result = hours + "h";
        }
        if (minutes != 0) {
            if (hours != 0) {
                result += (minutes < 10 ? "0" + minutes : minutes) + "'";
            } else {
                result += minutes + "'";
            }
            if (precision) {
                if (seconds == 0) {
                    result += (seconds < 10 ? "0" + seconds : seconds);
                }
            }
        }
        if (precision) {
            if (seconds != 0) {
                if (minutes == 0) {
                    result += seconds + "''";
                } else {
                    result += (seconds < 10 ? "0" + seconds : seconds) + "''";
                }
                if (centseconds != 0) {
                    result += Math.floor((centseconds < 10 ? "0" + centseconds : centseconds));
                }
            }
        }
        return result;
    }
} //n fin service