export default function calculvitesseService() {
    console.log('run service calculvitesseService');

    var ObjetVitesse=[];

    this.isCalculVitessePossible = function(distance, heure, minute, seconde, centseconde) {

     
        if (distance != "" && distance != 0 && distance !='undefined' && heure * 360000 + minute * 6000 + seconde * 100 + centseconde * 1 != 0) {
            if (distance / (heure * 360000 + minute * 6000 + seconde * 100 + centseconde * 1) != "Infinity" && distance / (heure * 360000 + minute * 6000 + seconde * 100 + centseconde * 1) != 0 && distance / (heure * 360000 + minute * 6000 + seconde * 100 + centseconde * 1) != "undefined") {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    this.getObjetVitesse = function(){
    	return ObjetVitesse;
    }
    this.setObjetVitesse = function(distance, heure, minute, seconde, centseconde, setUniteDistance, setUniteVitesse) {
        //valeur de retour : un objet contenant : le temps en centieme de seconde, la vitesse moyenne en km/h, la vitesse moyenne en km/cs
        // temps en centieme de seconde   



        var temps = heure * 360000 + minute * 6000 + seconde * 100 + centseconde * 1;
        // vitesse en km/h
        var vitesse = 0;
        // si km et km/h
        if (setUniteDistance.nom == "km" && setUniteVitesse.nom == "km/h") {
            vitesse = (distance * setUniteDistance.coef) / (temps * setUniteVitesse.coef);
        }
        // si m et km/h
        if (setUniteDistance.nom == "m" && setUniteVitesse.nom == "km/h") {
            vitesse = (distance * setUniteDistance.coef) / (temps * setUniteVitesse.coef);
        }
        // si miles et km/h
        if (setUniteDistance.nom == "miles" && setUniteVitesse.nom == "km/h") {
            vitesse = (distance * setUniteDistance.coef) / (temps * setUniteVitesse.coef);
        }
        // si km et m/s
        if (setUniteDistance.nom == "km" && setUniteVitesse.nom == "m/s") {
            vitesse = (distance * setUniteDistance.coef) / (temps * setUniteVitesse.coef * 3.6);
        }
        // si m et m/s
        if (setUniteDistance.nom == "m" && setUniteVitesse.nom == "m/s") {
            vitesse = (distance * setUniteDistance.coef) / (temps * setUniteVitesse.coef * 3.6);
        }
        // si miles et m/s
        if (setUniteDistance.nom == "miles" && setUniteVitesse.nom == "m/s") {
            vitesse = (distance * setUniteDistance.coef) / (temps * setUniteVitesse.coef * 3.6);
        }
        // si km et m/s
        if (setUniteDistance.nom == "km" && setUniteVitesse.nom == "miles/h") {
            vitesse = (distance * setUniteDistance.coef) / (temps * setUniteVitesse.coef * 1.60934);
        }
        // si m et m/s
        if (setUniteDistance.nom == "m" && setUniteVitesse.nom == "miles/h") {
            vitesse = (distance * setUniteDistance.coef) / (temps * setUniteVitesse.coef * 1.60934);
        }
        // si miles et m/s
        if (setUniteDistance.nom == "miles" && setUniteVitesse.nom == "miles/h") {
            vitesse = (distance * setUniteDistance.coef) / (temps * setUniteVitesse.coef * 1.60934);
        }
        var vitessemoyenne = vitesse != "Infinity" ? Math.round(vitesse * 100) / 100 : "";
        var vitessemoyennecs = vitesse != "Infinity" ? (distance * setUniteDistance.coef) / (temps) : "";

        
         ObjetVitesse={
         	"distance":distance,
         	"setUniteDistance":setUniteDistance,
            "setUniteVitesse":setUniteVitesse,
            "vitessemoyenne": vitessemoyenne,
            "temps": temps,
            "vitessemoyennecs": vitessemoyennecs
        };

        return (ObjetVitesse);
    }
}