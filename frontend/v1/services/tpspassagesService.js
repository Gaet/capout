export default function tpspassagesService() {
    console.log('run service tpspassagesService');

    function decimalAdjust(type, value, exp) {
        // Si la valeur de exp n'est pas définie ou vaut zéro...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // Si la valeur n'est pas un nombre 
        // ou si exp n'est pas un entier...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Décalage
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Décalage inversé
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }
    // Arrondi décimal
    if (!Math.round10) {
        Math.round10 = function(value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }

    function isrestenull(a, b) {
        if (a % b == 0) {
            return true;
        } else {
            return false;
        }
    }
    this.basics = function(distance, setUniteDistance, vitesse, temps, timeToTextService) {
    
        var tmp = [];
        var tpspassages = [];
        var rested = distance * setUniteDistance.coef - Math.trunc(distance * setUniteDistance.coef);
        var rested_arrondi = Math.round10(rested, -2);
        //var restet = Math.round(360000 * rested / vitesse, 2);
        // var videliste=10-(distance/10-Math.trunc(distance/10))*10;
        if (distance * setUniteDistance.coef >= 2 && distance * setUniteDistance.coef <= 300) {
            tmp[1] = Math.round(temps / distance * setUniteDistance.coef, 2);
            tpspassages.push({
                nb: 1,
                distance_passage: 1 + ' km',
                temps_passage: timeToTextService.transformationPerformanceTexte(tmp[1],0)
            });
            for (var i = 2; i <= distance * setUniteDistance.coef; i++) {
                tmp[i] = Math.round(temps / distance * setUniteDistance.coef + tmp[i - 1], 2);
                tpspassages.push({
                    nb: i,
                    distance_passage: i + ' km',
                    temps_passage: timeToTextService.transformationPerformanceTexte(tmp[i],0)
                });
            }
            if (rested != 0) {
                // 12.6 km => 0.6 km
                //  console.log(restet)
                tpspassages.push({
                    nb: i + 1,
                    // distance_passage: distance + ' km ' + '(' + affichagerested * 1000 + 'm)',
                    distance_passage: distance + ' km ',
                    temps_passage: timeToTextService.transformationPerformanceTexte(temps,0)
                        // temps_passage: timeToTextService.transformationPerformanceTexte(restet)
                });
            }
        
        }
        if (distance * setUniteDistance.coef >= 0.4 && distance * setUniteDistance.coef <= 2) {
            tmp[1] = Math.round(36000 / vitesse, 2);
            // console.log(tmp[1])
            tpspassages.push({
                nb: 1,
                distance_passage: 100 + ' m',
                temps_passage: timeToTextService.transformationPerformanceTexte(tmp[1],1)
            });
            for (var i = 2; i <= distance * setUniteDistance.coef * 10; i++) {
                tmp[i] = Math.round(36000 / vitesse + tmp[i - 1], 2);
                tpspassages.push({
                    nb: i,
                    distance_passage: i * 100 + ' m',
                    temps_passage: timeToTextService.transformationPerformanceTexte(tmp[i],1)
                });
            }
            // console.log("rested_arrondi = "+rested_arrondi)
            // console.log("isresr = "+isrestenull(rested_arrondi*100,10))
            if (!isrestenull(rested_arrondi * 100, 10)) {
                // 12.6 km => 0.6 km
                tpspassages.push({
                    nb: i + 1,
                    // distance_passage: distance + ' km ' + '(' + affichagerested * 1000 + 'm)',
                    distance_passage: Math.round(distance * setUniteDistance.coef*1000,2) + ' m ',
                    temps_passage: timeToTextService.transformationPerformanceTexte(temps,1)
                        // temps_passage: timeToTextService.transformationPerformanceTexte(restet)
                });
            }
           
        }
        if (distance * setUniteDistance.coef < 0.4) {
            tmp[1] = Math.round(18000 / vitesse, 2);
            // console.log(tmp[1])
            tpspassages.push({
                nb: 1,
                distance_passage: 50 + ' m',
                temps_passage: timeToTextService.transformationPerformanceTexte(tmp[1],1)
            });
            for (var i = 2; i <= distance * setUniteDistance.coef * 20; i++) {
                tmp[i] = Math.round(18000 / vitesse + tmp[i - 1], 2);
                tpspassages.push({
                    nb: i,
                    distance_passage: i * 50 + ' m',
                    temps_passage: timeToTextService.transformationPerformanceTexte(tmp[i],1)
                });
            }
           // console.log("rested_arrondi = " + rested_arrondi)
           // console.log("isresr = " + isrestenull(rested_arrondi * 100, 10))
            if (!isrestenull(rested_arrondi * 100, 5)) {
                // 12.6 km => 0.6 km
                tpspassages.push({
                    nb: i + 1,
                    // distance_passage: distance + ' km ' + '(' + affichagerested * 1000 + 'm)',
                    distance_passage: Math.round(distance * setUniteDistance.coef*1000,2) + ' m ',
                    temps_passage: timeToTextService.transformationPerformanceTexte(temps,1)
                        // temps_passage: timeToTextService.transformationPerformanceTexte(restet)
                });
            }
          
        }
        return tpspassages;
    }
}