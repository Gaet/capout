
export default function() {

}
export const unitSpeed = [
    { id: 1, name: 'km/h', coef: 1 / 360000 },
    { id: 2, name: 'm/s', coef: 1/360000 },
    { id: 3, name: 'miles/h', coef: 1 / 360000 },
];

export const unitDistance = [
    { id: 1, name: 'km', coef: 1 },
    { id: 2, name: 'm', coef: 0.001 },
    { id: 3, name: 'miles', coef: 1.60934 },
];

export const bareme = [
    { id: 1, name: 'RM' },
    { id: 2, name: 'RF' },
    { id: 3, name: 'IA' },
    { id: 4, name: 'IB' },
    { id: 5, name: 'N1' },
    { id: 6, name: 'N2' },
    { id: 7, name: 'N3' },
    { id: 8, name: 'N4' },
    { id: 9, name: 'IR1' },
    { id: 10, name: 'IR2' },
    { id: 11, name: 'IR3' },
    { id: 12, name: 'IR4' },
    { id: 13, name: 'R1' },
    { id: 14, name: 'R2' },
    { id: 15, name: 'R3' },
    { id: 16, name: 'R4' },
    { id: 17, name: 'R5' },
    { id: 18, name: 'R6' },
    { id: 19, name: 'D1' },
    { id: 20, name: 'D2' },
    { id: 21, name: 'D3' },
    { id: 22, name: 'D4' },
    { id: 23, name: 'D5' },
    { id: 24, name: 'D6' },
    { id: 25, name: 'D7' }
];
