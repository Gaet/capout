import { unWrap, getErrorResponse } from '~/utils/fetchUtils'

export default function ({ $config }, inject) {
    const headers = {
        'X-Algolia-API-Key': $config.algolia.key,
        'X-Algolia-Application-Id': $config.algolia.appId,
    }
    inject('dataApi', {
        getHome,
        getReviewsByHomeId,
        getUserByHomeId,
        getHomesByLocation,
        getHomes,
        getUsers,
        getUserById,
        getTrainings,
        getBaremeffaF,
        getBaremeffaH,
        getCommunes,
        getColors,
        getLastSpeedCalc,
        postLastSpeedCalc,
    })

    async function getHome(homeId) {
        try {
            return unWrap(await fetch(`https://${$config.algolia.appId}-dsn.algolia.net/1/indexes/homes/${homeId}`, { headers }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
    async function getUsers(query) {
        // pour l'instant avec le json serve on ne fait pas de pagination
        delete query.perPage
        delete query.page
        console.log(query)
        try {
            return unWrap(await fetch(`http://localhost:3000/rest/users?` + new URLSearchParams(query).toString(), { headers }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
    async function getTrainings(query) {
        // pour l'instant avec le json serve on ne fait pas de pagination
        try {
            return unWrap(await fetch(`http://localhost:3000/rest/trainings?` + new URLSearchParams(query).toString(), { headers }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
    async function getColors(query) {
        // pour l'instant avec le json serve on ne fait pas de pagination
        try {
            return unWrap(await fetch(`http://localhost:3000/rest/colors?` + new URLSearchParams(query).toString(), { headers }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
    async function getBaremeffaF(query) {
        // pour l'instant avec le json serve on ne fait pas de pagination
        try {
            return unWrap(await fetch(`http://localhost:3000/rest/baremeffaF?` + new URLSearchParams(query).toString(), { headers }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
    async function getBaremeffaH(query) {
        // pour l'instant avec le json serve on ne fait pas de pagination
        try {
            return unWrap(await fetch(`http://localhost:3000/rest/baremeffaH?` + new URLSearchParams(query).toString(), { headers }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
    async function getCommunes(query) {
        // pour l'instant avec le json serve on ne fait pas de pagination
        try {
            return unWrap(await fetch(`http://localhost:3000/rest/communes?` + new URLSearchParams(query).toString(), { headers }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
    async function getUserById(id) {
        try {
            return unWrap(await fetch(`http://localhost:3000/rest/users/?` + id, { headers }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
    async function getLastSpeedCalc() {
        try {
            return unWrap(await fetch(`http://localhost:3000/lastSpeedCalc/`, { headers }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }

    async function postLastSpeedCalc(requestOptions) {
        try {
            return unWrap(await fetch(`http://localhost:3000/lastSpeedCalc/`, {
                headers: requestOptions.headers,
                method: requestOptions.method,
                body: requestOptions.body
            }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }

    async function getReviewsByHomeId(homeId) {
        try {
            return unWrap(await fetch(`https://${$config.algolia.appId}-dsn.algolia.net/1/indexes/reviews/query`, {
                headers,
                method: 'POST',
                body: JSON.stringify({
                    filters: `homeId:${homeId}`,
                    hitsPerPage: 6,
                    attributesToHighlight: [],
                })
            }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }

    async function getUserByHomeId(homeId) {
        try {
            return unWrap(await fetch(`https://${$config.algolia.appId}-dsn.algolia.net/1/indexes/users/query`, {
                headers,
                method: 'POST',
                body: JSON.stringify({
                    filters: `homeId:${homeId}`,
                    attributesToHighlight: [],
                })
            }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }

    async function getHomesByLocation(lat, lng, start, end, radiusInMeters = 1500 * 15) {
        try {
            const days = []

            for (var day = parseInt(start); day <= parseInt(end); day += 86400) {
                days.push(`availability:${day}`)
            }
            return unWrap(await fetch(`https://${$config.algolia.appId}-dsn.algolia.net/1/indexes/homes/query`, {
                headers,
                method: 'POST',
                body: JSON.stringify({
                    aroundLatLng: `${lat},${lng}`,
                    aroundRadius: radiusInMeters,
                    hitsPerPage: 10,
                    filters: days.join(' AND '),
                    attributesToHighlight: [],
                })
            }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
    async function getHomes() {
        try {
            return unWrap(await fetch(`https://${$config.algolia.appId}-dsn.algolia.net/1/indexes/homes/query`, {
                headers,
                method: 'POST',
                body: JSON.stringify({
                    hitsPerPage: 3,
                    attributesToHighlight: [],
                })
            }))
        } catch (error) {
            return getErrorResponse(error)
        }
    }
}