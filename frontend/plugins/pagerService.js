export default function (context, inject) {
    
    let perPage = 24;

    inject('pagerService', {
        getPerPage,
        setPerPage,
    })

    function getPerPage() {
        return perPage;
    }

    function setPerPage(perPageToSet) {
        perPage = perPageToSet;
    }
}