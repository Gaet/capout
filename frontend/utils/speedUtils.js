export default () => {
};
export function secondsToHms(d) {
    //
    d = Number(d);
    let h = Math.floor(d / 3600);
    let m = Math.floor((d % 3600) / 60);
    let s = Math.floor((d % 3600) % 60);

    let hDisplay = h > 0 ? h + (h == 1 ? " h " : " h ") : "";

    let mDisplay = m > 0 ? m + (m == 1 ? " min " : " min ") : "";

    if (m > 0 && m < 10) {
        mDisplay = "0" + mDisplay;
    }
    let sDisplay = s > 0 ? s + (s == 1 ? " sec " : " sec ") : "";
    return hDisplay + mDisplay + sDisplay;
};

export function secondsToTime(secs) {
    let ret = ""; // Return value
    let s = Math.floor(secs); // Whole seconds
    let m = Math.floor(s / 60); // Whole minutes

    m = m % 60;
    if (m < 10) ret += "0";
    ret += m;
    s = s % 60;
    ret += ":";
    if (s < 10) ret += "0";
    ret += s;
    return ret + " min/km";
};
export function convertIntoPace(v) {

    let paceRun = 3600 / v;
    return secondsToTime(paceRun);
};
export function distanceToTime(d, paceSelected) {
    const seconds =
        (convertPaceIntoSeconds(paceSelected) * d) / 100;
    return secondsToHms(seconds);
};
export function convertPaceIntoSeconds(pace) {
    // nb de sec pour parcourir 1 km
    const n = pace.replace(" ", "").split("min/km")[0];
    const min = n.split(":")[0];
    const sec = n.split(":")[1];
    return 60 * min + sec;
};