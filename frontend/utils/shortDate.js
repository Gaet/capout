
import * as dateFns from 'date-fns';

export default() => {
  const date = new Date(dateStr)
  return date.toLocaleDateString(undefined, {day:'numeric', month: 'long', year: 'numeric' })
}

export function formatDateFull(dateStr) {
    const date = new Date(dateStr)
    return date.toLocaleDateString(undefined, {day:'numeric', month: 'long', year: 'numeric' })
}

export const gecFormat = (date) => `${dateFns.format(date, "yyyy-MM-dd")}`;

export const gecGetDay = (date) => {
  let day = dateFns.getDate(date);
  day = day +1;
  if (day < 9) {
    return "0" + day;
  } else {
    return day;
  }
};
export const gecGetMonth = (date) => {
  let month = dateFns.getMonth(date);
  month = month +1;
  if (month < 9) {
    return "0" + month;
  } else {
    return month;
  }
};
export const gecGetYear = (date) => `${dateFns.getYear(date)}`;
export const gecGetDate = (date) => `${dateFns.getDate(date)}`;


/**
 * Exemple de retour : "Dimanche 12 juin" ou "Dimanche 12 juin 2017"
 * @param {*} date
 */
export function oldformatDateFull(date) {
  const days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
  const months = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'];
  const dayStr =  gecGetDate(date);
  const yearStr = gecGetYear(date);
  const output = `${days[date.getDay()]} ${dayStr} ${months[date.getMonth()]} ${yearStr}`;

  return output;
}
/**
 * Exemple de retour : "Dimanche"
 * @param {*} date
 */
 export function dayFR(j) {
  const days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
  const output = days[j];
  return output;
}
export function formatDateAnneMois(date) {
   // date = gecFormat(date);
    const year = gecGetYear(date);
    const month = gecGetMonth(date);
    const output = year +""+ month;
    return output;
  }
  export function formatDateAnneMoisJour(date) {
    // date = gecFormat(date);
    const year = gecGetYear(date);
    const month = gecGetMonth(date).toString();
    const day = gecGetDay(date).toString();
  
    const output = day +'-'+ month +'-'+ year;
    return output;
  }
